#version 330 core

layout (location = 0) in vec3 vertex_position;     
layout (location = 1) in vec3 vertex_normal;        
layout (location = 2) in vec2 vertex_texCoords;    

#if defined( NORMAL_MAP )
layout (location = 3) in vec3 vertex_tangent;     
layout (location = 4) in vec3 vertex_bitangent;    
#endif	

struct Vertex
{
    vec3    normal;            
    vec2    texCoords;       

    vec3    lightDirection;    
    vec3    viewDirection;     

    float   distance;          
};

struct Light
{
    vec3    position;   
    vec4    ambient;    
    vec4    diffuse;    
    vec4    specular;   

    //float   radius;     
};

struct Material
{
    vec4    		ambient;    		
    vec4    		diffuseColor;    		

#if !defined( EMISSION_MAP )
    vec4    		emission;   		
#endif

#if !defined( SPECULAR_MAP )
    float   		specular;   		
#endif

    float   		shininess;  		
	
#if defined( DIFFUSE_MAP )
	sampler2D		diffuseMap;			
#endif

#if defined( NORMAL_MAP )
	sampler2D		normalMap;			
#endif

#if defined( SPECULAR_MAP )
	sampler2D		specularMap;		
#endif

#if defined( EMISSION_MAP )	
	sampler2D		emissionMap;		
#endif		
};

out Vertex  vertex;     				

uniform Light   		light;                     	
uniform Material    	material;      				
uniform vec3    		viewPosition;       
uniform vec4    		textureRect;     
uniform vec2			spriteSize;    	
uniform mat4    		matrix_Projection;   
uniform mat4			matrix_Transformation;      	

void main()
{
	vec3 positionFrag = vertex_position * vec3( spriteSize, 1.f );
	positionFrag = vec3( matrix_Transformation * vec4( positionFrag, 1.f ) );    	
	vertex.normal = vec3( matrix_Transformation * vec4( vertex_normal, 0.f ) );
	vertex.texCoords = textureRect.xy + ( vertex_texCoords * textureRect.zw );

#if !defined( NORMAL_MAP )
		vertex.viewDirection = viewPosition - positionFrag;
		vertex.lightDirection = light.position - positionFrag;
#else	
		vec3 tangent = vec3( matrix_Transformation * vec4( vertex_tangent, 0.f ) );
		vec3 bitangent = vec3( matrix_Transformation * vec4( vertex_bitangent, 0.f ) );	
		mat3 matrix_TBN = transpose( mat3( tangent, bitangent, vertex.normal ) );
		
		vec3 tbn_lightPosition = matrix_TBN * light.position;
		vec3 tbn_viewPosition = matrix_TBN * viewPosition;
		vec3 tbn_positionFrag = matrix_TBN * positionFrag;
		
		vertex.viewDirection = tbn_viewPosition - tbn_positionFrag;
		vertex.lightDirection = tbn_lightPosition; // - tbn_positionFrag;		
#endif

    vertex.distance = length( vertex.lightDirection );	
    gl_Position = matrix_Projection * vec4( positionFrag, 1.f );
}
