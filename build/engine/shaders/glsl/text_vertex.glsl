#version 330 core

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec2 vertex_texCoords;

out vec2 			texCoords;

uniform mat4 		matrix_Projection;
uniform mat4		matrix_Transformation;

void main()
{
	texCoords = vertex_texCoords;
	gl_Position = matrix_Projection * matrix_Transformation * vec4( vertex_position, 1.f );	
}
