#version 330 core

in vec2 				texCoords;
out vec4				color;

uniform sampler2D		glyph;
uniform vec3			textColor;

void main()
{
	float				alphaGlyph = texture( glyph, texCoords ).r;
	
	if ( alphaGlyph < 0.001 ) 
		discard; 
	else
		color = vec4( textColor, alphaGlyph );
}
