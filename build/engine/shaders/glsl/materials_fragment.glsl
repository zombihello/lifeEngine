#version 330 core

struct Vertex
{
    vec3    normal;           
    vec2    texCoords;          

    vec3    lightDirection;     
    vec3    viewDirection;      

    float   distance;          
};

struct Light
{
    vec3    		position;   		
    vec4    		ambient;    		
    vec4    		diffuse;    		
    vec4    		specular;   		
		
   // float   		radius;   
};

struct Material
{
    vec4    		ambient;    		
    vec4    		diffuseColor;    		

#if !defined( EMISSION_MAP )
    vec4    		emission;   		
#endif

#if !defined( SPECULAR_MAP )
    float   		specular;   		
#endif

    float   		shininess;  		
	
#if defined( DIFFUSE_MAP )
	sampler2D		diffuseMap;			
#endif

#if defined( NORMAL_MAP )
	sampler2D		normalMap;			
#endif

#if defined( SPECULAR_MAP )
	sampler2D		specularMap;		
#endif

#if defined( EMISSION_MAP )	
	sampler2D		emissionMap;		
#endif
};

in Vertex       		vertex;           

out vec4        		color;              

uniform Light       	light;        
uniform Material    	material;      

void main()
{
	/*vec3    normal;
	
#if !defined( NORMAL_MAP )
		normal = normalize( vertex.normal );
#else
		normal = texture2D( material.normalMap, vertex.texCoords ).rgb;
		normal = normalize(normal * 2.0 - 1.0);  
#endif
	
    vec3    lightDirection = normalize( vertex.lightDirection );
    vec3    viewDirection = normalize( vertex.viewDirection );

    //float linearCoeff = ( 2.0f * vertex.distance ) / light.radius;
   // float quadCoeff = vertex.distance * vertex.distance / ( light.radius * light.radius );

   // float attenuation = 1.0f / ( 1.0f + linearCoeff + quadCoeff );

	
#if !defined( EMISSION_MAP )
		color = material.emission;
#else
		color = texture2D( material.emissionMap, vertex.texCoords );
#endif
	

    color += material.ambient * light.ambient;

    float NdotL = max( dot( normal, lightDirection ), 0.f );

#if !defined( DIFFUSE_MAP )
		color += material.diffuse * light.diffuse * NdotL;
#else
		vec4	diffuse = texture2D( material.diffuseMap, vertex.texCoords );
		color += diffuse * light.diffuse * NdotL;
#endif
	

    float RdotVpow  = max( pow( dot( reflect( -lightDirection, normal ), viewDirection ), material.shininess ), 0.f );

#if !defined(SPECULAR_MAP)
		color += material.specular * light.specular * RdotVpow;
#else	
		float	specular = texture2D( material.specularMap, vertex.texCoords ).r;
		color += specular * light.specular * RdotVpow;
#endif*/

#if !defined( DIFFUSE_MAP )
		color = material.diffuseColor;		
#else
		color = texture2D( material.diffuseMap, vertex.texCoords ) * material.diffuseColor;
		color.a = min( texture2D( material.diffuseMap, vertex.texCoords ).a, material.diffuseColor.a );				
#endif

	if ( color.a < 0.01f ) 
		discard;
}
