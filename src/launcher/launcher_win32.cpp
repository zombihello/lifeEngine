//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <Windows.h>
#include <iostream>
#include <fstream>
using namespace std;

#include "common/log.h"
#include "common/configurations.h"
#include "common/gamemanifest.h"
#include "engine/lifeEngine.h"
#include "engine/dirsinfo.h"
#include "engine/icore.h"

#define DEFAULT_GAME					"testBed"

//-------------------------------------------------------------------------//

/*
 * Точка входа в лаунчер
 * -------------------------
 */

int WINAPI WinMain( HINSTANCE hInst, HINSTANCE hPreInst, LPSTR lpCmdLine, int nCmdShow )
{	
	// Загружаем библиотеку движка

	HINSTANCE					coreDLL = nullptr;
	le::Func_LE_CreateCore		LE_CreateCore = nullptr;
	le::Func_LE_DeleteCore		LE_DeleteCore = nullptr;
	
	SetDllDirectoryA( LENGINE_ENGINE_DIR );

	try
	{
		coreDLL = LoadLibraryA( LENGINE_ENGINE_LIB );

		if ( !coreDLL )
			throw "Failed to load [" LENGINE_ENGINE_LIB "]";

		LE_CreateCore = ( le::Func_LE_CreateCore ) GetProcAddress( coreDLL, "LE_CreateCore" );
		if ( !LE_CreateCore )
			throw "Failed get adress on function [LE_CreateCore]";

		LE_DeleteCore = ( le::Func_LE_DeleteCore ) GetProcAddress( coreDLL, "LE_DeleteCore" );
		if ( !LE_DeleteCore )
			throw "Failed get adress on function [LE_DeleteCore]";
	}
	catch ( const char* Message )
	{
		MessageBoxA( nullptr, Message, "Error", MB_OK | MB_ICONERROR );
		return 1;
	}

	// Cчитываем аргументы запуска лаунчера и
	// инициализируем движок
	
	int				argc;
	LPWSTR*			argList = CommandLineToArgvW( GetCommandLineW(), &argc );
	char**			argv = new char* [ argc ];

	for ( int index = 0; index < argc; ++index )
	{
		int			size = wcslen( argList[ index ] ) + 1;
		argv[ index ] = new char[ size ];

		wcstombs( argv[ index ], argList[ index ], size );
	}

	le::ICore*			core = LE_CreateCore();
	string				gameDir = DEFAULT_GAME;

	{
		// Загружаем конфигурации движка, если нет - сохраняем
		
		if ( !core->LoadConfig( LENGINE_CONFIG_FILE ) )
			core->SaveConfig( LENGINE_CONFIG_FILE );

		le::Configurations			configurations = core->GetConfigurations();

		// Парсим аргументы запуска и меняем
		// конфигурации

		for ( int index = 0; index < argc; ++index )
		{
			if ( strstr( argv[ index ], "-game" ) && index + 1 < argc )
			{
				gameDir = argv[ index + 1 ];
				++index;
			}
			else if ( ( strstr( argv[ index ], "-width" ) || strstr( argv[ index ], "-w" ) ) && index + 1 < argc )
			{
				configurations.windowWidth = atoi( argv[ index + 1 ] );
				++index;
			}
			else if ( ( strstr( argv[ index ], "-height" ) || strstr( argv[ index ], "-h" ) ) && index + 1 < argc )
			{
				configurations.windowHeight = atoi( argv[ index + 1 ] );
				++index;
			}
		}

		// Инициализируем движок для запуска игры

		core->SetConfigurations( configurations );
		core->SetEngineDir( LENGINE_ENGINE_DIR );
		core->InitializeEngine();

		// Загружаем информацию об игре и ее логику

		try
		{			
			if ( !core->LoadGameManifest( gameDir ) )
				throw "Fail load game manifest in directory [" + gameDir + "]";

			if ( !core->LoadGame() )
				throw "Fail load game [" + core->GetGameManifest().game + "]";
		}
		catch ( const string& Message )
		{
			MessageBoxA( nullptr, Message.c_str(), "Error", MB_OK | MB_ICONERROR );
			return 1;
		}
	}

	// Удаляем выделенную память под аргументы
	// и запускаем игру

	LocalFree( argList );
	delete[] argv;

	core->RunSimulation();

	// После отработки движка
	// удаляем его и выгружаем библиотеку движка

	LE_DeleteCore( core );
	FreeLibrary( coreDLL );
	return 0;
}

//---------------------------------------------------------------------//
