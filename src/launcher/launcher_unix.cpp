//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <unistd.h>
#include <SDL2/SDL.h>
using namespace std;

#include "engine/lifeEngine.h"
#include "engine/dirsinfo.h"
#include "engine/iengine.h"

//-------------------------------------------------------------------------//


//-------------------------------------------------------------------------//

/*
 * Точка входа в лаунчер
 * -------------------------
 */

int main( int argc, char** argv )
{
	// Настраиваем вывод логов в файл
	
	ofstream            logFile( "./engine.log" );

    cout.rdbuf( logFile.rdbuf() );
    cerr.rdbuf( logFile.rdbuf() );

    cout.setf( ios::dec | ios::showbase );
	
	void* 									handle = nullptr;
	le::Func_LE_CreateCoreEngine_t			LE_CreateCoreEngine = nullptr;
	le::Func_LE_DeleteCoreEngine_t			LE_DeleteCoreEngine = nullptr;

	// Задаем рабочий каталог

    {
		string 			workDir = argv[0];
		if ( workDir.find_last_of( '/' ) != -1 )
			workDir.erase( workDir.find_last_of( '/' ), workDir.size() );
		
		chdir( workDir.c_str() );
    }

	try
	{
        handle = SDL_LoadObject( LENGINE_ENGINE_DIR "/" LENGINE_ENGINE_LIB );
		if ( !handle ) 				throw SDL_GetError();

		LE_CreateCoreEngine = ( le::Func_LE_CreateCoreEngine_t ) SDL_LoadFunction( handle, "LE_CreateCoreEngine" );
		if ( !LE_CreateCoreEngine ) throw SDL_GetError();

		LE_DeleteCoreEngine = ( le::Func_LE_DeleteCoreEngine_t ) SDL_LoadFunction( handle, "LE_DeleteCoreEngine" );
		if ( !LE_DeleteCoreEngine)	throw SDL_GetError();
	}
	catch( const char* Message )
	{
		SDL_ShowSimpleMessageBox( SDL_MESSAGEBOX_ERROR, "Error", Message, nullptr );
		return 1;
	}
	
	le::IEngine*		engine = LE_CreateCoreEngine();
    if ( !engine->Initialize( LENGINE_ENGINE_DIR, argc, argv ) )
		return 2;
	
	engine->Run();

	LE_DeleteCoreEngine( engine );
	SDL_UnloadObject( handle );
	return 0;
}

//---------------------------------------------------------------------//
