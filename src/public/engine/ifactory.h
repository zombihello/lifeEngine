//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (��������� �����) ***
//				Copyright (C) 2018-2019
//
// ����������� ������:  https://gitlab.com/zombihello/lifeEngine
// ������:				���� �������� (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef IFACTORY_H
#define IFACTORY_H

#include <string>
using namespace std;

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	typedef void*		( *Func_CreateObjectFactory_t )( );					///< ��� ��������� �� ������� �������� ������� �������

	//---------------------------------------------------------------------//

	//////////////////////////////////////////////////////////////////////
	/// \brief ��������� �������
	//////////////////////////////////////////////////////////////////////
	class IFactory
	{
	public:
		//////////////////////////////////////////////////////////////////////
		/// \brief ������� ������
		///
		/// \param[in] Name �������� ������, ��������, Material001
		/// \return ��������� �� ������
		//////////////////////////////////////////////////////////////////////
		virtual void* Create( const string& Name ) = 0;

		//////////////////////////////////////////////////////////////////////
		/// \brief ������� ������
		///
		/// \param[in] Object ��������� �� ������
		//////////////////////////////////////////////////////////////////////
		virtual void Delete( void* Object ) = 0;
		
		//////////////////////////////////////////////////////////////////////
		/// \brief ���������������� �����
		///
		/// \param[in] Name �������� ������
		/// \param[in] Func_CreateObjectFactory ��������� �� ������� �������� ������� �������
		//////////////////////////////////////////////////////////////////////
		virtual void RegisterClass( const string& Name, Func_CreateObjectFactory_t Func_CreateObjectFactory ) = 0;

		//////////////////////////////////////////////////////////////////////
		/// \brief ����������������� �����
		///
		/// \param[in] Name �������� ������
		//////////////////////////////////////////////////////////////////////
		virtual void UnregisterClass( const string& Name ) = 0;

	protected:
		//////////////////////////////////////////////////////////////////////
		/// \brief ����������
		//////////////////////////////////////////////////////////////////////
		virtual ~IFactory() {}
	};

	//---------------------------------------------------------------------//
}

//---------------------------------------------------------------------//

#endif // !IFACTORY_H
