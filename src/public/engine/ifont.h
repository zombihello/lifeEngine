//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef IFONT_H
#define IFONT_H

#include <string>
using namespace std;

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	//////////////////////////////////////////////////////////////////////
	/// \brief Интерфейс шрифта
	///
	/// Данный интерфейс описывает базовый функционал шрифта для
	/// использования его в других подсистемах движка
	//////////////////////////////////////////////////////////////////////
	class IFont
	{
	public:
		//////////////////////////////////////////////////////////////////////
		/// \brief Деструктор
		//////////////////////////////////////////////////////////////////////
		virtual ~IFont() {}

        //////////////////////////////////////////////////////////////////////
        /// \brief Загрузить шрифт
        ///
        /// \param[in] Route Путь к шрифту
        /// \return True все прошло успешно, иначе false
        //////////////////////////////////////////////////////////////////////
        virtual bool Load( const string& Route ) = 0;

		//////////////////////////////////////////////////////////////////////
		/// \brief Увеличить счетчик ссылок
		//////////////////////////////////////////////////////////////////////
		virtual void IncrementReferenceCount() = 0;

		//////////////////////////////////////////////////////////////////////
		/// \brief Уменьшить счетчик ссылок
		//////////////////////////////////////////////////////////////////////
		virtual void DecrementReferenceCount() = 0;

        //////////////////////////////////////////////////////////////////////
        /// \brief Выгрузить шрифт
        //////////////////////////////////////////////////////////////////////
        virtual void Unload() = 0;

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить количество ссылок
		///
		/// \return Количество ссылок
		//////////////////////////////////////////////////////////////////////
		virtual uint32_t GetReferenceCount() const = 0;

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить название шрифта
		///
		/// \return Название шрифта
		//////////////////////////////////////////////////////////////////////
		virtual const string& GetFamilyName() const = 0;

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить межстрочный интервал
		///
		/// \param[in] CharcterSize Размер текста
		/// \return Межстрочный интервал
		//////////////////////////////////////////////////////////////////////
		virtual float GetLineSpacing( uint32_t CharacterSize ) const = 0;	
	};

	//---------------------------------------------------------------------//
}

#define FONT_INTERFECE_VERSION "Font001"

//---------------------------------------------------------------------//

#endif // !IFONT_H
