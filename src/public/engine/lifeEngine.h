//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef LIFEENGINE_H
#define LIFEENGINE_H

//---------------------------------------------------------------------//

#	if defined( _WIN32 ) || defined( _WIN64 )
#		define PLATFORM_WINDOWS
#	elif defined( __linux__ )
#		define PLATFORM_LINUX
#	else
#		error Unknown platform
#	endif // _WIN32 или _WIN64

//---------------------------------------------------------------------//

#	if defined( LIFEENGINE_EXPORT ) && defined( PLATFORM_WINDOWS )
#		define LIFEENGINE_API				extern "C" __declspec( dllexport )
#	elif defined( LIFEENGINE_EXPORT ) && defined( PLATFORM_LINUX )
#		define LIFEENGINE_API				extern "C"
#	else
#		define LIFEENGINE_API
#	endif // LIFEENGINE_EXPORT

//---------------------------------------------------------------------//

#	if defined( LIFEENGINE_EXPORT )
#		define LIFEENGINE_CORE_API( CoreClass ) \
			namespace le { class ICore; } \
			LIFEENGINE_API le::ICore* LE_CreateCore() { return new CoreClass(); } \
			LIFEENGINE_API void LE_DeleteCore( le::ICore* Object ) { delete static_cast<CoreClass*>( Object ); }

#		define LIFEENGINE_SUBSYSTEM_API( SubsystemClass ) \
			namespace le { class IEngineSubsystem; } \
			LIFEENGINE_API le::IEngineSubsystem* LE_CreateSubsystem() { return new SubsystemClass(); } \
			LIFEENGINE_API void LE_DeleteSubsystem( le::IEngineSubsystem* Object ) { delete static_cast<SubsystemClass*>( Object ); }
										
#		define LIFEENGINE_GAME_API( GameClass ) \
			namespace le { class IGame; } \
			LIFEENGINE_API le::IGame* LE_CreateGame() { return new GameClass(); } \
			LIFEENGINE_API void LE_DeleteGame( le::IGame* Object ) { delete static_cast<GameClass*>( Object ); }
#	endif // LIFEENGINE_EXPORT

//---------------------------------------------------------------------//

#	if defined ( LIFEENGINE_DEBUG )
#		define LIFEENGINE_ASSERT( X, MSG ) \
			if ( !( X ) ) \
			{ \
				std::cout << "\n*** LIFEENGINE_ASSERT ***\nFunction: " << __FUNCTION__ << "\nLine: " << __LINE__ << "\n\nMessage: " << MSG; \
				abort(); \
			}		
#	else
#		define LIFEENGINE_ASSERT( X, MSG )
#	endif // LIFEENGINE_DEBUG

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	class ICore;
	class IEngineSubsystem;
	class IGame;

	//---------------------------------------------------------------------//

	typedef void*				WindowHandle_t;														///< Тип указателя на заголовок окна
	typedef ICore*				( *Func_LE_CreateCore )( );											///< Тип указателя на функцию создания ядра
	typedef void				( *Func_LE_DeleteCore )( ICore* Core );								///< Тип указателя на функцию удаления ядра
	typedef IEngineSubsystem*	( *Func_LE_CreateSubsystem )( );									///< Тип указателя на функцию создания подсистемы движка
	typedef void				( *Func_LE_DeleteSubsystem )( IEngineSubsystem* EngineSubsystem );	///< Тип указателя на функцию удаления подсистемы движка
	typedef IGame*				( *Func_LE_CreateGame )( );											///< Тип указателя на функцию создания игры
	typedef void				( *Func_LE_DeleteGame )( IGame* Game );								///< Тип указателя на функцию удаления игры

	//---------------------------------------------------------------------//
}

//---------------------------------------------------------------------//

#endif // !LIFEENGINE_H
