//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef IGAME_H
#define IGAME_H

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	class ICore;

	//---------------------------------------------------------------------//

	///////////////////////////////////////////////////////////////////////////
	/// \brief Интерфейс игры
	///
	/// Данный интерфейс описывает базовый функционал игры для
	/// функционирования движка с игровой логикой
	////////////////////////////////////////////////////////////////////////////
	class IGame
	{
	public:
		///////////////////////////////////////////////////////////////////////////
		/// \brief Инициализировать логику игры
		///
		/// \param[in] Core Указатель на ядро движка
		/// \return True логика игры успешно инициализирована, иначе false
		////////////////////////////////////////////////////////////////////////////
		virtual bool Initialize( ICore* Core ) = 0;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Обновить логику игры
		/// 
		/// \param[in] DeltaTime Прошедшее время в милисекундах
		//////////////////////////////////////////////////////////////////////////
		virtual void Update( uint32_t DeltaTime ) = 0;

	protected:
		////////////////////////////////////////////////////////////////////////////
		/// \brief Деструктор
		////////////////////////////////////////////////////////////////////////////
		virtual ~IGame() {}
	};

	//---------------------------------------------------------------------//
}

//---------------------------------------------------------------------//

#endif // !IGAME_H
