//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "shadersinfo.h"
#include "shadermanager.h"

#define SHADERS_COUNT		( sizeof( shaderList ) / sizeof( *shaderList ) )

//----------------------------------------------------------------------//

//////////////////////////////////////////////////////////////////////
/// \brief Параметры шейдера
//////////////////////////////////////////////////////////////////////
struct ShaderDescriptor
{
	string				name;				///< Название шейдера
	string				route;				///< Путь к конфигурациям шейдера	
};

//----------------------------------------------------------------------//

static ShaderDescriptor		shaderList[] =			///< Массив всех шейдеров в движке. Порядок шейдеров должен соответствовать порядку в SHADER_TYPE
{
	{ LENGINE_SHADER_MATERIAL,	LENGINE_SHADER_MATERIAL ".manifest" },			///< Описание шейдера материала
	{ LENGINE_SHADER_TEXT,		LENGINE_SHADER_TEXT ".manifest" },				///< Описание шейдера текста
	{ LENGINE_SHADER_SPRITE,	LENGINE_SHADER_SPRITE ".manifest" }				///< Описание шейдера спрайта
};

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------------
 */

le::ShaderManager::~ShaderManager()
{
	DeleteAll();
}

//----------------------------------------------------------------------//

/*
 * Загрузить шейдер с файла
 * ----------------------
 */

le::Shader_t le::ShaderManager::LoadFromFile( const SHADER_TYPE& ShaderType, uint32_t Flags )
{
	if ( shaders.find( ShaderType ) != shaders.end() && shaders[ ShaderType ].find( Flags ) != shaders[ ShaderType ].end() )
		return shaders[ ShaderType ][ Flags ];

	//TODO: [zombiHello] Shader_t( nullptr ) приводит к вылету, исправить

	if ( ShaderType >= SHADERS_COUNT )
	{
		LOG_ERROR( "Invalid shader type [" << ShaderType << "]" );
		return Shader_t( nullptr );
	}

	ShaderDescriptor	shaderDescriptor = shaderList[ ShaderType ];
	Shader_t			shader( new Shader() );

	LOG_INFO( "Load shader [" << shaderDescriptor.name << " | flags: " << Flags << "]" );

	if ( !shader->LoadFromFile( shadersDir + "/" + shaderDescriptor.route, Flags ) )
	{
		LOG_ERROR( "Shader [" << shaderDescriptor.name << " | flags: " << Flags << "] not loaded" );
		return Shader_t( nullptr );
	}

	shaders[ ShaderType ][ Flags ] = shader;
	LOG_INFO( "Loaded shader [" << shaderDescriptor.name << " | flags: " << Flags << "]" );
	return shader;
}

//----------------------------------------------------------------------//

/*
 * Удалить шейдер
 * ----------------------
 */

void le::ShaderManager::Delete( const SHADER_TYPE& ShaderType, uint32_t Flags )
{
	if ( shaders.find( ShaderType ) == shaders.end() && shaders[ ShaderType ].find( Flags ) == shaders[ ShaderType ].end() )
		return;

	shaders[ ShaderType ].erase( Flags );

	if ( shaders[ ShaderType ].empty() )
		shaders.erase( ShaderType );

	LOG_INFO( "Shader with name [" << shaderList[ ShaderType ].name << " | flags: " << Flags << "] deleted" );
}

//----------------------------------------------------------------------//

/*
 * Удалить все шейдеры
 * ----------------------
 */

void le::ShaderManager::DeleteAll()
{
	if ( shaders.empty() ) return;

	LOG_INFO( "All shaders deleted" );
	shaders.clear();
}

//----------------------------------------------------------------------//

/*
 * Получить шейдер
 * ----------------------
 */

le::Shader_t le::ShaderManager::Get( const SHADER_TYPE& ShaderType, uint32_t Flags )
{
	if ( shaders.find( ShaderType ) == shaders.end() && shaders[ ShaderType ].find( Flags ) == shaders[ ShaderType ].end() )
		return Shader_t( nullptr );

	return shaders[ ShaderType ][ Flags ];
}

//----------------------------------------------------------------------//
