//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "common/ray.h"
#include "boundingbox.h"
using namespace std;

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * --------------
 */

le::BoundingBox::BoundingBox()
{}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------
 */

le::BoundingBox::~BoundingBox()
{}

//-------------------------------------------------------------------------//

/*
 * Инициализировать ограничивающее тело
 * ----------------
 */

void le::BoundingBox::Initialize( const glm::vec3& MinXYZ, const glm::vec3& MaxXYZ )
{
	verteces[ 0 ] = glm::vec3( MinXYZ.x, MaxXYZ.y, MaxXYZ.z );
	verteces[ 1 ] = glm::vec3( MaxXYZ.x, MaxXYZ.y, MaxXYZ.z );
	verteces[ 2 ] = glm::vec3( MinXYZ.x, MinXYZ.y, MaxXYZ.z );
	verteces[ 3 ] = glm::vec3( MaxXYZ.x, MinXYZ.y, MaxXYZ.z );
	verteces[ 4 ] = glm::vec3( MinXYZ.x, MaxXYZ.y, MinXYZ.z );
	verteces[ 5 ] = glm::vec3( MaxXYZ.x, MaxXYZ.y, MinXYZ.z );
	verteces[ 6 ] = glm::vec3( MinXYZ.x, MinXYZ.y, MinXYZ.z );
	verteces[ 7 ] = glm::vec3( MaxXYZ.x, MinXYZ.y, MinXYZ.z );
}

//-------------------------------------------------------------------------//

/*
 * Инициализировать ограничивающее тело
 * ----------------
 */

void le::BoundingBox::Initialize( const glm::vec3& Size )
{
	float		halfWidth = Size.x / 2.f;
	float		halfHeight = Size.y / 2.f;
	float		halfDepth = Size.z / 2.f;
	
	verteces[ 0 ] = glm::vec3( -halfWidth, -halfHeight, halfDepth );
	verteces[ 1 ] = glm::vec3( halfWidth, -halfHeight, halfDepth );
	verteces[ 2 ] = glm::vec3( halfWidth, halfHeight, halfDepth );
	verteces[ 3 ] = glm::vec3( -halfWidth, halfHeight, halfDepth );
	verteces[ 4 ] = glm::vec3( -halfWidth, -halfHeight, -halfDepth );
	verteces[ 5 ] = glm::vec3( halfWidth, -halfHeight, -halfDepth );
	verteces[ 6 ] = glm::vec3( halfWidth, halfHeight, -halfDepth );
	verteces[ 7 ] = glm::vec3( -halfWidth, halfHeight, -halfDepth );
}

//-------------------------------------------------------------------------//

/*
 * Получить массив вершин
 * ----------------
 */

const glm::vec3* le::BoundingBox::GetVerteces( uint32_t& Count ) const
{
	Count = 8;
	return verteces;
}

//-------------------------------------------------------------------------//

/*
 * Получить трансформацию объекта
 * ----------------
 */

const le::ITransform& le::BoundingBox::GetTransform() const
{
	return transform;
}

//-------------------------------------------------------------------------//

/*
 * Получить трансформацию объекта
 * ----------------
 */

le::ITransform& le::BoundingBox::GetTransform()
{
	return transform;
}

//-------------------------------------------------------------------------//

/*
 * Пересекает ли луч ограничивающее тело
 * ----------------
 */

bool le::BoundingBox::IsIntersectRay( const Ray& Ray ) const
{	
	glm::vec3			minXYZ;
	glm::vec3			maxXYZ;

	minXYZ = maxXYZ = glm::vec3( transform.GetTransformation() * glm::vec4( verteces[ 0 ], 1.f ) );
	for ( uint32_t index = 0; index < 8; ++index )
	{
		glm::vec3		globalVertex = glm::vec3( transform.GetTransformation() * glm::vec4( verteces[ index ], 1.f ) );

		minXYZ.x = glm::min( minXYZ.x, globalVertex.x );
		minXYZ.y = glm::min( minXYZ.y, globalVertex.y );
		minXYZ.z = glm::min( minXYZ.z, globalVertex.z );
									   
		maxXYZ.x = glm::max( maxXYZ.x, globalVertex.x );
		maxXYZ.y = glm::max( maxXYZ.y, globalVertex.y );
		maxXYZ.z = glm::max( maxXYZ.z, globalVertex.z );
	}
	
	glm::vec3		invDirection = 1.f / Ray.direction;
	float			t1 = invDirection.x * ( minXYZ.x - Ray.origin.x );
	float			t2 = invDirection.x * ( maxXYZ.x - Ray.origin.x );
	float			tmin = glm::min( t1, t2 );
	float			tmax = glm::max( t1, t2 );

	t1 = invDirection.y * ( minXYZ.y - Ray.origin.y );
	t2 = invDirection.y * ( maxXYZ.y - Ray.origin.y );
	tmin = glm::max( tmin, glm::min( t1, t2 ) );
	tmax = glm::min( tmax, glm::max( t1, t2 ) );

	t1 = invDirection.z * ( minXYZ.z - Ray.origin.z );
	t2 = invDirection.z * ( maxXYZ.z - Ray.origin.z );
	tmin = glm::max( tmin, glm::min( t1, t2 ) );
	tmax = glm::min( tmax, glm::max( t1, t2 ) );

	return ( tmin <= tmax ) && ( tmax > 0.f );
}

//-------------------------------------------------------------------------//