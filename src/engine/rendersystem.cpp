//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <SDL2/SDL.h>
#include <codecvt>
#include <algorithm>

#include "dirsinfo.h"
#include "common/configurations.h"
#include "engine/iresourcesystem.h"
#include "engine/icore.h"
#include "engine/iconsolesystem.h"
#include "engine/iconvar.h"
#include "engine/iwindow.h"
#include "engine/imodel.h"
#include "engine/icamera.h"
#include "engine/isprite.h"
#include "core.h"

#include "settingscontext.h"
#include "rendersystem.h"
#include "shadermanager.h"
#include "text.h"
#include "font.h"
#include "material.h"
#include "mesh.h"
#include "modelrenderer.h"
#include "spriterenderer.h"
#include "textrenderer.h"
#include "rendercontext.h"

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	ICore*				core = nullptr;					///< Указатель на ядро
	IResourceSystem*	resourceSystem = nullptr;		///< Указатель на подсистему менеджера ресурсов
	ShaderManager*		shaderManager = nullptr;		///< Указатель на менеджер шейдеров
	RenderSystem*		renderSystem = nullptr;			///< Указатель на систему визуализации	

	IConVar*			con_wireframe = nullptr;		///< Консольная переменная включения каркасного рендера

	//---------------------------------------------------------------------//
}

//---------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------
 */

le::RenderSystem::RenderSystem() :
	renderContext( nullptr ),
	cameraWorld( nullptr ),
	cameraHud( nullptr )
{}

//---------------------------------------------------------------------//

/*
 * Деструктор
 * -----------------
 */

le::RenderSystem::~RenderSystem()
{
	if ( shaderManager )	delete shaderManager;
	if ( renderContext )	delete renderContext;
}

//---------------------------------------------------------------------//

/*
 * Инициализировать систему отрисовки
 * --------------------------------------
 */

bool le::RenderSystem::Initialize( ICore* Core )
{
	// Если в ядре окно не создано (указатель на IWindow nullptr) или
	// заголовок окна nullptr, то выбрасываем ошибку

	if ( !Core->GetWindow() || !Core->GetWindow()->GetHandle() )
	{
		LOG_ERROR( "Window not open or not valid handle" );
		return false;
	}

	core = Core;

	// Создаем контекст OpenGL

	Configurations				configurations = Core->GetConfigurations();
	SettingsContext				settingsContext;
	settingsContext.redBits = 8;
	settingsContext.greenBits = 8;
	settingsContext.blueBits = 8;
	settingsContext.alphaBits = 8;
	settingsContext.depthBits = 24;
	settingsContext.stencilBits = 8;
	settingsContext.majorVersion = 3;
	settingsContext.minorVersion = 3;
	settingsContext.attributeFlags = SettingsContext::CA_CORE;

	renderContext = new RenderContext();
	if ( !renderContext->Create( Core->GetWindow()->GetHandle(), settingsContext ) )
	{
		LOG_ERROR( "Failed created context" );
		SDL_ShowSimpleMessageBox( SDL_MESSAGEBOX_ERROR, "Error", "Failed created context", nullptr );
		return false;
	}

	renderContext->SetVerticalSync( configurations.vsinc );

	// Инициализируем OpenGL

	glEnable( GL_DEPTH_TEST );
	glEnable( GL_CULL_FACE );
	glEnable( GL_TEXTURE_2D );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	// Получаем подсистему менеджера ресурсов, если такого в ядре нет
	// возвращаем ошибку

	resourceSystem = static_cast< IResourceSystem* >( Core->GetSubsystem( EST_RESOURCE_MANAGER ) );
	if ( !resourceSystem )		return false;

	renderSystem = this;
	shaderManager = new ShaderManager();

	shaderManager->SetShadersDir( Core->GetEngineDir() + "/" LENGINE_SHADERS_DIR );
	spriteRenderer.Initialize();
	if ( !textRenderer.Initialize() ) return false;

	// Регестрируем классы в фабрике объектов

	factory = Core->GetFactory();
	factory->RegisterClass(		TEXT_INTERFECE_VERSION,			[]()->void* { return new Text(); }		);
	factory->RegisterClass(		FONT_INTERFECE_VERSION,			[]()->void* { return new Font(); }		);
	factory->RegisterClass(		TEXTURE_INTERFECE_VERSION,		[]()->void* { return new Texture(); }	);
	factory->RegisterClass(		MATERIAL_INTERFECE_VERSION,		[]()->void* { return new Material(); }	);
	factory->RegisterClass(		MESH_INTERFECE_VERSION,			[]()->void* { return new Mesh(); }		);

	// Решестрируем в консоли переменные рендера

	IConsoleSystem*		consoleSystem = static_cast< IConsoleSystem* >( Core->GetSubsystem( EST_CONSOLE_SYSTEM ) );
	if ( !consoleSystem ) return true;

	Log::ConnectConsoleSystem( consoleSystem );

	con_wireframe = ( le::IConVar* ) factory->Create( CONVAR_INTERFECE_VERSION );
	con_wireframe->Initialize( "r_wireframe", "0", le::CVT_BOOL, "Enable wireframe or no", true, 0, true, 1,
							   []( le::IConVar* Var ) 
							   {
								   if ( Var->GetValueBool() )
									   glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
								   else
									   glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
							   } );

	consoleSystem->RegisterVar( con_wireframe );
	return true;
}

//---------------------------------------------------------------------//

/*
 * Изменить размер области вывода кадра
 * --------------------------------------
 */

void le::RenderSystem::ResizeViewport( int X, int Y, uint32_t Width, uint32_t Height )
{
	if ( !renderContext || !renderContext->IsCreated() ) return;
	glViewport( X, Y, Width, Height );
}

//---------------------------------------------------------------------//

// ------------------------------------------------------------------------------------ //
// Добавить модель на отрисовку
// ------------------------------------------------------------------------------------ //
void le::RenderSystem::Draw( IModel* Model, TYPE_LAYER TypeLayer )
{
	if ( !Model ) return;

	vector<RenderObject>* renderObjects = &this->renderObjects[ TypeLayer ];
	Mesh* mesh = static_cast< Mesh* >( Model->GetMesh() );

	if ( !mesh || !mesh->IsCreated() ) return;

	RenderObject							renderObject = {};
	IMaterial* material = nullptr;
	vector<IMaterial*>						materials = mesh->GetMaterials();
	list<IMesh::Surface>					surfaces = mesh->GetSurfaces();
	unordered_map<uint32_t, IMaterial*>		customMaterials = Model->GetCustomMaterials();

	renderObject.vertexArrayObject = &mesh->GetVertexArrayObject();
	renderObject.matrixTransformation = &Model->GetTransformation();
	renderObject.position = Model->GetPosition();
	renderObject.type = RenderObject::OT_STATIC_MODEL;
	renderObject.object = Model;

	switch ( mesh->GetRenderMode() )
	{
	case le::IMesh::RM_TRIANGLE:
		renderObject.renderMode = GL_TRIANGLES;
		break;

	case le::IMesh::RM_LINE:
		renderObject.renderMode = GL_LINES;
		break;
	}

	for ( auto it = surfaces.begin(), itEnd = surfaces.end(); it != itEnd; ++it )
	{
		auto		itMaterial = customMaterials.find( it->materialId );
		if ( itMaterial != customMaterials.end() )
			material = itMaterial->second;
		else
			material = materials[ it->materialId ];	

		renderObject.shader = static_cast< Material* >( material )->GetShader( mesh->GetShaderFlags() | material->GetMaterialFlags() );
		renderObject.material = static_cast< Material* >( material );	
		renderObject.countVertexIndex = it->countVertexIndex;
		renderObject.startVertexIndex = it->startVertexIndex;
		renderObjects->push_back( renderObject );
	}
}

// ------------------------------------------------------------------------------------ //
// Добавить спрайт на отрисовку
// ------------------------------------------------------------------------------------ //
void le::RenderSystem::Draw( ISprite* Sprite, TYPE_LAYER TypeLayer )
{
	if ( !Sprite || !Sprite->GetMaterial() ) return;

	vector<RenderObject>* renderObjects = &this->renderObjects[ TypeLayer ];
	IMaterial* material = Sprite->GetMaterial();
	RenderObject			renderObject;

	renderObject.type = RenderObject::OT_SPRITE;
	renderObject.object = Sprite;
	renderObject.material = static_cast< Material* >( material );
	renderObject.shader = shaderManager->LoadFromFile( ST_SPRITE, renderObject.material->GetMaterialFlags() );
	renderObject.matrixTransformation = &Sprite->GetTransformation();
	renderObject.position = Sprite->GetPosition();
	renderObjects->push_back( renderObject );
}

// ------------------------------------------------------------------------------------ //
// Добавить текст на отрисовку
// ------------------------------------------------------------------------------------ //
void le::RenderSystem::Draw( IText* Text, TYPE_LAYER TypeLayer )
{
	if ( !Text ) return;

	vector<RenderObject>* renderObjects = &this->renderObjects[ TypeLayer ];
	RenderObject			renderObject;

	renderObject.type = RenderObject::OT_TEXT;
	renderObject.object = Text;
	renderObject.position = Text->GetPosition();
	renderObjects->push_back( renderObject );
}

/*
 * Отрисовать кадр
 * --------------------------------------
 */

void le::RenderSystem::Render()
{
	if ( !renderContext || !renderContext->IsCreated() ) return;
	glm::mat4		matrixProjectionView;

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	//TODO: Добавить сортировку по материалам и проход для полупрозрачных объектов

	// Отрисовываем не прозрачные и прозрачные объекты в мире
	if ( cameraWorld )
	{
		matrixProjectionView = cameraWorld->GetProjectionMatrix() * cameraWorld->GetViewMatrix();
		vector<RenderObject>*		renderObjects = &this->renderObjects[ TL_WORLD ];
		
		sort( renderObjects->begin(), renderObjects->end(), [&]( const RenderObject& Value1, const RenderObject& Value2 ) -> bool
			  {
				  return glm::distance( cameraWorld->GetPosition(), Value1.position ) > glm::distance( cameraWorld->GetPosition(), Value2.position );
			  } );

		for ( auto it = renderObjects->begin(), itEnd = renderObjects->end(); it != itEnd; ++it )
			switch ( it->type )
			{
			case RenderObject::OT_STATIC_MODEL:		modelRenderer.Render( *it, matrixProjectionView ); break;
			case RenderObject::OT_SPRITE:			spriteRenderer.Render( *it, matrixProjectionView, cameraWorld->GetViewMatrix() ); break;
			case RenderObject::OT_TEXT:				textRenderer.Render( *it, matrixProjectionView ); break;
			}
	}

	// Отрисовываем не прозрачные и полупрозрачные объекты в HUD'е
	if ( cameraHud )
	{
		matrixProjectionView = cameraHud->GetProjectionMatrix() * cameraHud->GetViewMatrix();
		vector<RenderObject>* renderObjects = &this->renderObjects[ TL_HUD ];

		for ( auto it = renderObjects->begin(), itEnd = renderObjects->end(); it != itEnd; ++it )
			switch ( it->type )
			{
			case RenderObject::OT_STATIC_MODEL:		modelRenderer.Render( *it, matrixProjectionView ); break;
			case RenderObject::OT_SPRITE:			spriteRenderer.Render( *it, matrixProjectionView, cameraHud->GetViewMatrix() ); break;
			case RenderObject::OT_TEXT:				textRenderer.Render( *it, matrixProjectionView ); break;
			}
	}

	renderContext->SwapBuffers();
	renderObjects.clear();
}

//---------------------------------------------------------------------//

/*
 * Задать активную камеру для слоя
 * --------------------------------------
 */

void le::RenderSystem::SetActiveCamera( ICamera* Camera, TYPE_LAYER TypeLayer )
{
	switch ( TypeLayer )
	{
	case TL_WORLD:			cameraWorld = Camera;		break;
	case TL_HUD:			cameraHud = Camera;			break;
	}
}

//---------------------------------------------------------------------//

/*
 * Активировать вертикальную синхронизацию
 * --------------------------------------
 */

void le::RenderSystem::SetVerticalSyncEnabled( bool IsEnable )
{
	if ( !renderContext || !renderContext->IsCreated() ) return;
	renderContext->SetVerticalSync( IsEnable );
}

//---------------------------------------------------------------------//

/*
 * Получить тип подсистемы движка
 * -----------------------
 */

le::ENGINE_SUBSYSTEM_TYPE le::RenderSystem::GetType() const
{
	return EST_RENDER_SYSTEM;
}

//---------------------------------------------------------------------//
