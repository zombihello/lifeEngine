//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "core.h"
#include "engine/ifactory.h"
#include "resourcesystem.h"
#include "fontmanager.h"

//----------------------------------------------------------------------//

/*
 * Конструктор
 * ------------------
 */

le::FontManager::FontManager() :
	directory( "." )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------
 */

le::FontManager::~FontManager()
{
	DeleteAll();
}

//----------------------------------------------------------------------//

/*
 * Загрузить шрифт
 * ------------------
 */

le::IFont* le::FontManager::Load( const string& Route, const string& Name )
{
	string					name = Name;
	if ( Name.empty() )		name = Route;

    if ( fonts.find( name ) == fonts.end() )
    {
        LOG_INFO( "Loading font [" << name << "]" );
        IFont*            font = ( IFont* ) factory->Create( FONT_INTERFECE_VERSION );

        if ( !font || !font->Load( Route ) )
        {
            LOG_ERROR( "Font [" << name << "] not loaded" );
            delete font;
            return nullptr;
        }

        fonts[ name ] = font;
        LOG_INFO( "Loaded font [" << name << "]");
        return font;
    }

    return fonts[ name ];
}

//----------------------------------------------------------------------//

/*
 * Создать шрифт
 * ------------------
 */

le::IFont* le::FontManager::Create( const string& Name )
{
    if ( fonts.find( Name ) == fonts.end() )
    {
		IFont*            font = ( IFont* ) factory->Create( FONT_INTERFECE_VERSION );
		if ( !font )	return nullptr;

		LOG_INFO( "Created font [" << Name << "]" );
        fonts[ Name ] = font;       
        return fonts[ Name ];
    }

    return nullptr;
}

//----------------------------------------------------------------------//

/*
 * Удалить шрифт
 * ------------------
 */

void le::FontManager::Delete( const string& Name )
{
	auto			it = fonts.find( Name );

	if ( it == fonts.end() )
		return;
	
	LOG_INFO( "Font with name [" << Name << "] deleted" );

	factory->Delete( it->second );
	fonts.erase( it );
}

//----------------------------------------------------------------------//

/*
 * Удалить шрифт
 * ------------------
 */

void le::FontManager::Delete( IFont* Font )
{
	for ( auto it = fonts.begin(), itEnd = fonts.end(); it != itEnd; ++it )
		if ( it->second == Font )
		{
			LOG_INFO( "Font with name [" << it->first << "] deleted" );
			factory->Delete( it->second );

			fonts.erase( it );
			return;
		}
}

// ------------------------------------------------------------------------------------ //
// Удалить все ресурсы
// ------------------------------------------------------------------------------------ //
void le::FontManager::DeleteAll()
{
	if ( fonts.empty() ) return;

	for ( auto it = fonts.begin(), itEnd = fonts.end(); it != itEnd; ++it )
		factory->Delete( it->second );

	LOG_INFO( "All font deleted" );
	fonts.clear();
}

//----------------------------------------------------------------------//

/*
 * Задать каталог с материалами
 * ------------------
 */

void le::FontManager::SetDirectory( const string& Directory )
{
	directory = Directory;
}

//----------------------------------------------------------------------//

/*
 * Получить каталог с материалами
 * ------------------
 */

const string& le::FontManager::GetDirectory() const
{
	return directory;
}

//----------------------------------------------------------------------//

/*
 * Получить шрифт по названию
 * ------------------
 */

le::IFont* le::FontManager::Get( const string& Name )
{
	if ( fonts.find( Name ) != fonts.end() )
		return fonts[ Name ];

	return nullptr;
}

//----------------------------------------------------------------------//

