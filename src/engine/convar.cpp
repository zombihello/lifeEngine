//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "convar.h"

// ------------------------------------------------------------------------------------ //
// Конструктор
// ------------------------------------------------------------------------------------ //
le::ConVar::ConVar() :
	isReadOnly( false ),
	hasMin( false ),
	hasMax( false ),
	min( numeric_limits<float>::min() ),
	max( numeric_limits<float>::max() ),
	type( CVT_UNDEFINED ),
	value_int( 0 ),
	value_float( 0.f ),
	value_bool( false ),
	value_string( "" )
{}

// ------------------------------------------------------------------------------------ //
// Деструктор
// ------------------------------------------------------------------------------------ //
le::ConVar::~ConVar()
{}

// ------------------------------------------------------------------------------------ //
// Инициализировать переменную
// ------------------------------------------------------------------------------------ //
void le::ConVar::Initialize( const string& Name, const string& DefaultValue, CONVAR_TYPE Type, const string& HelpString, function<void( IConVar* )> ChangeCallback, bool IsReadOnly )
{
	name = Name;
	isReadOnly = IsReadOnly;
	changeCallback = ChangeCallback;
	defaultValue = DefaultValue;
	helpString = HelpString;

	SetValue( DefaultValue, Type );
}

// ------------------------------------------------------------------------------------ //
// Инициализировать переменную
// ------------------------------------------------------------------------------------ //
void le::ConVar::Initialize( const string& Name, const string& DefaultValue, CONVAR_TYPE Type, const string& HelpString, bool HasMin, float Min, bool HasMax, float Max, function<void( IConVar* )> ChangeCallback, bool IsReadOnly )
{
	name = Name;
	isReadOnly = IsReadOnly;
	changeCallback = ChangeCallback;
	defaultValue = DefaultValue;
	helpString = HelpString;
	hasMin = HasMin;
	hasMax = HasMax;
	min = Min;
	max = Max;

	SetValue( DefaultValue, Type );
}

// ------------------------------------------------------------------------------------ //
// Сбросить значение переменной по-умолчанию
// ------------------------------------------------------------------------------------ //
void le::ConVar::Revert()
{
	SetValue( defaultValue, type );
}

// ------------------------------------------------------------------------------------ //
// Задать Callback-функцию изменения значения
// ------------------------------------------------------------------------------------ //
void le::ConVar::SetChangeCallback( function<void( IConVar* )> ChangeCallback )
{
	changeCallback = ChangeCallback;
}

// ------------------------------------------------------------------------------------ //
// Задать название переменной
// ------------------------------------------------------------------------------------ //
void le::ConVar::SetName( const string& Name )
{
	name = Name;
}

// ------------------------------------------------------------------------------------ //
// Задать значение переменной из строки
// ------------------------------------------------------------------------------------ //
void le::ConVar::SetValue( const string& Value, CONVAR_TYPE Type )
{
	if ( isReadOnly ) return;

	try
	{
		switch ( Type )
		{
		case CVT_INT:
			SetValueInt( stoi( Value ) );
			break;

		case CVT_FLOAT:
			SetValueFloat( stof( Value ) );
			break;

		case CVT_BOOL:
		{
			string			valueLower = Value;
			for ( uint32_t index = 0, count = valueLower.size(); index < count; ++index )
				valueLower[ index ] = tolower( valueLower[ index ] );

			SetValueBool( valueLower == "true" || stoi( valueLower ) ? true : false );
			break;
		}

		case CVT_STRING:
			SetValueString( Value );
			break;

		default: return;
		}
	}
	catch ( exception & Exception )
	{
		LOG_ERROR( name << ": " << Exception.what() << ". Value = " << Value );
	}
}

// ------------------------------------------------------------------------------------ //
// Задать целочисленное значение переменной
// ------------------------------------------------------------------------------------ //
void le::ConVar::SetValueInt( int Value )
{
	if ( isReadOnly ) return;

	type = CVT_INT;

	if ( hasMin && Value < min )
		value_int = min;
	else if ( hasMax && Value > max )
		value_int = max;
	else
		value_int = Value;

	if ( changeCallback )
		changeCallback( this );
}

// ------------------------------------------------------------------------------------ //
// Задать дробное значение переменной
// ------------------------------------------------------------------------------------ //
void le::ConVar::SetValueFloat( float Value )
{
	if ( isReadOnly ) return;

	type = CVT_FLOAT;

	if ( hasMin && Value < min )
		value_float = min;
	else if ( hasMax && Value > max )
		value_float = max;
	else
		value_float = Value;

	if ( changeCallback )
		changeCallback( this );
}

// ------------------------------------------------------------------------------------ //
// Задать логическое значение переменной
// ------------------------------------------------------------------------------------ //
void le::ConVar::SetValueBool( bool Value )
{
	if ( isReadOnly ) return;

	type = CVT_BOOL;

	if ( hasMin && Value < min )
		value_bool = min;
	else if ( hasMax && Value > max )
		value_bool = max;
	else
		value_bool = Value;

	if ( changeCallback )
		changeCallback( this );
}

// ------------------------------------------------------------------------------------ //
// Задать строковое значение переменной
// ------------------------------------------------------------------------------------ //
void le::ConVar::SetValueString( const string& Value )
{
	if ( isReadOnly ) return;

	type = CVT_STRING;

	if ( hasMin && Value.size() < min )
	{
		value_string = Value;
		value_string.append( min - Value.size(), ' ' );
	}
	else if ( hasMax && Value.size() > max )
		value_string = Value.substr( 0, max );
	else
		value_string = Value;

	if ( changeCallback )
		changeCallback( this );
}

// ------------------------------------------------------------------------------------ //
// Получить название переменной
// ------------------------------------------------------------------------------------ //
const string& le::ConVar::GetName() const
{
	return name;
}

// ------------------------------------------------------------------------------------ //
// Получить тип переменной
// ------------------------------------------------------------------------------------ //
le::CONVAR_TYPE le::ConVar::GetType() const
{
	return type;
}

// ------------------------------------------------------------------------------------ //
// Получить целочисленное значение переменной
// ------------------------------------------------------------------------------------ //
int le::ConVar::GetValueInt() const
{
	return value_int;
}

// ------------------------------------------------------------------------------------ //
// Получить дробное значение переменной
// ------------------------------------------------------------------------------------ //
float le::ConVar::GetValueFloat() const
{
	return value_float;
}

// ------------------------------------------------------------------------------------ //
// Получить логическое значение переменной
// ------------------------------------------------------------------------------------ //
bool le::ConVar::GetValueBool() const
{
	return value_bool;
}

// ------------------------------------------------------------------------------------ //
// Получить строковое значение переменной
// ------------------------------------------------------------------------------------ //
const string& le::ConVar::GetValueString() const
{
	return value_string;
}

// ------------------------------------------------------------------------------------ //
// Преобразовать значение переменной в строку
// ------------------------------------------------------------------------------------ //
string le::ConVar::ToValueString() const
{
	if ( type == CVT_STRING )
		return value_string;
	else
		switch ( type )
		{
		case CVT_INT:		return to_string( value_int );
		case CVT_FLOAT:		return to_string( value_float );
		case CVT_BOOL:		return value_bool ? "true" : "fasle";
		default:			return "";
		}
}

// ------------------------------------------------------------------------------------ //
// Получить текст помощи
// ------------------------------------------------------------------------------------ //
const string& le::ConVar::GetHelpText() const
{
	return helpString;
}

// ------------------------------------------------------------------------------------ //
// Получить значение переменной по-умолчанию
// ------------------------------------------------------------------------------------ //
const string& le::ConVar::GetValueDefault() const
{
	return defaultValue;
}

// ------------------------------------------------------------------------------------ //
// Получить минимальный диапазон переменной
// ------------------------------------------------------------------------------------ //
float le::ConVar::GetMin() const
{
	return min;
}

// ------------------------------------------------------------------------------------ //
// Получить максимальный диапазон переменной
// ------------------------------------------------------------------------------------ //
float le::ConVar::GetMax() const
{
	return max;
}

// ------------------------------------------------------------------------------------ //
// Переменная только для чтения ли?
// ------------------------------------------------------------------------------------ //
bool le::ConVar::IsReadOnly() const
{
	return isReadOnly;
}

// ------------------------------------------------------------------------------------ //
// Имеет ли переменная минимальный диапазон
// ------------------------------------------------------------------------------------ //
bool le::ConVar::HasMin() const
{
	return hasMin;
}

// ------------------------------------------------------------------------------------ //
// Имеет ли переменная максимальный диапазон
// ------------------------------------------------------------------------------------ //
bool le::ConVar::HasMax() const
{
	return hasMax;
}
