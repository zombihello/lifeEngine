//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

#include "engine/icore.h"
#include "engine/ifactory.h"
#include "consolesystem.h"
#include "convar.h"

// ------------------------------------------------------------------------------------ //
// Конструктор
// ------------------------------------------------------------------------------------ //
le::ConsoleSystem::ConsoleSystem()
{}

// ------------------------------------------------------------------------------------ //
// Деструктор
// ------------------------------------------------------------------------------------ //
le::ConsoleSystem::~ConsoleSystem()
{
	if ( fileLogs.is_open() )
		fileLogs.close();
}

// ------------------------------------------------------------------------------------ //
// Инициализировать подсистему движка
// ------------------------------------------------------------------------------------ //
bool le::ConsoleSystem::Initialize( ICore* Core )
{
	IFactory* factory = Core->GetFactory();
	factory->RegisterClass(		CONVAR_INTERFECE_VERSION,	[]() -> void* { return new ConVar(); } );

	// Настраиваем вывод логов в файл

	fileLogs.open( "./engine.log" );
	if ( fileLogs.is_open() )
	{
		cout.rdbuf( fileLogs.rdbuf() );
		cerr.rdbuf( fileLogs.rdbuf() );

		cout.setf( ios::dec | ios::showbase );
	}
	
	return true;
}

// ------------------------------------------------------------------------------------ //
// Зарегестрировать консольную переменную
// ------------------------------------------------------------------------------------ //
void le::ConsoleSystem::RegisterVar( IConVar* ConVar )
{
	auto		itVar = consoleVariables.find( ConVar->GetName() );
	if ( itVar != consoleVariables.end() ) return;

	consoleVariables[ ConVar->GetName() ] = static_cast< le::ConVar* >( ConVar );
}

// ------------------------------------------------------------------------------------ //
// Зарегестрировать консольную команду
// ------------------------------------------------------------------------------------ //
void le::ConsoleSystem::RegisterCommand( const string& Name, function<void( const vector<string>& )> ExecCallback )
{
	if ( !ExecCallback ) return;

	auto		itCommand = consoleCommands.find( Name );
	if ( itCommand != consoleCommands.end() ) return;

	consoleCommands[ Name ] = ExecCallback;
}

// ------------------------------------------------------------------------------------ //
// Разрегестрировать консольную переменную
// ------------------------------------------------------------------------------------ //
void le::ConsoleSystem::UnregisterVar( const string& Name )
{
	auto		itVar = consoleVariables.find( Name );
	if ( itVar == consoleVariables.end() ) return;

	consoleVariables.erase( itVar );
}

// ------------------------------------------------------------------------------------ //
// Разрегестрировать консольную команду
// ------------------------------------------------------------------------------------ //
void le::ConsoleSystem::UnregisterCommand( const string& Name )
{
	auto		itCommand = consoleCommands.find( Name );
	if ( itCommand == consoleCommands.end() ) return;

	consoleCommands.erase( itCommand );
}

// ------------------------------------------------------------------------------------ //
// Выполнить комманду
// ------------------------------------------------------------------------------------ //
bool le::ConsoleSystem::Exec( const string& Command )
{
	if ( Command.empty() ) return false;

	// Находим название комманды или переменной в строке
	
	size_t			startIndexName = Command.find_first_not_of( ' ' );
	size_t			endIndexName = Command.find_first_of( ' ', startIndexName );
	string			name = Command.substr( startIndexName, endIndexName - startIndexName );
	string			arguments = endIndexName != string::npos ? Command.substr( endIndexName + 1, Command.size() - endIndexName ) : "";

	// Ищем по названию переменную, если нашли то меняем ее значение

	auto		itVar = consoleVariables.find( name );
	if ( itVar != consoleVariables.end() )
	{
		if ( !arguments.empty() && arguments.find_first_not_of( ' ' ) != string::npos )
			itVar->second->SetValue( arguments, itVar->second->GetType() );
		else
		{
			PrintInfo( name + ": " + itVar->second->GetHelpText() );
			PrintInfo( "Value: " + itVar->second->ToValueString() );
			PrintInfo( "Default value: " + itVar->second->GetValueDefault() );
		}
		
		return true;
	}

	// Ищем по названию комманду, если нашли то выполняем ее

	auto		itCommand = consoleCommands.find( name );
	if ( itCommand != consoleCommands.end() )
	{
		stringstream		strStream( arguments );
		string				tempString;
		vector<string>		arrayArguments;

		while ( !strStream.eof() )
		{
			strStream >> tempString;
			arrayArguments.push_back( tempString );
		}

		itCommand->second( arrayArguments );
		return true;
	}

	PrintError( "Not correct command: \"" + Command + "\"" );
	return false;
}

// ------------------------------------------------------------------------------------ //
// Вывести информационное сообщение
// ------------------------------------------------------------------------------------ //
void le::ConsoleSystem::PrintInfo( const string& Message )
{
	cout << "[Info]  " << Message << endl;
	if ( printConsoleCallback )		printConsoleCallback( Message );
}

// ------------------------------------------------------------------------------------ //
// Вывести предупреждающее сообщение
// ------------------------------------------------------------------------------------ //
void le::ConsoleSystem::PrintWarning( const string& Message )
{
	cout << "[Warning]  " << Message << endl;
	if ( printConsoleCallback )		printConsoleCallback( Message );
}

// ------------------------------------------------------------------------------------ //
// Вывести сообщение об ошибке
// ------------------------------------------------------------------------------------ //
void le::ConsoleSystem::PrintError( const string& Message )
{
	cout << "[Error]  " << Message << endl;
	if ( printConsoleCallback )		printConsoleCallback( Message );
}

// ------------------------------------------------------------------------------------ //
// Задать Callback-функцию вывода сообщения в консоль
// ------------------------------------------------------------------------------------ //
void le::ConsoleSystem::SetPrintConsoleCallback( function<void( const string& )> PrintConsoleCallback )
{
	printConsoleCallback = PrintConsoleCallback;
}

// ------------------------------------------------------------------------------------ //
// Получить тип подсистемы движка
// ------------------------------------------------------------------------------------ //
le::ENGINE_SUBSYSTEM_TYPE le::ConsoleSystem::GetType() const
{
	return EST_CONSOLE_SYSTEM;
}