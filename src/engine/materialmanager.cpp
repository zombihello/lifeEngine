//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "engine/ifactory.h"
#include "core.h"
#include "resourcesystem.h"
#include "materialmanager.h"

// ------------------------------------------------------------------------------------ //
// Конструктор
// ------------------------------------------------------------------------------------ //
le::MaterialManager::MaterialManager() :
	directory( "." )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------
 */

le::MaterialManager::~MaterialManager()
{}

//----------------------------------------------------------------------//

/*
 * Загрузить материал
 * ------------------
 */

le::IMaterial* le::MaterialManager::Load( const string& Route, const string& Name )
{
	string					name = Name;
	if ( Name.empty() )		name = Route;

	if ( materials.find( name ) == materials.end() )
	{
		LOG_INFO( "Loading material [" << name << "]" );
		IMaterial*		material = ( IMaterial* ) factory->Create( MATERIAL_INTERFECE_VERSION );

		if ( !material || !material->Load( Route ) )
		{
			LOG_ERROR( "Material [" << name << "] not loaded" );
			delete material;
			return nullptr;
		}

		materials[ name ] = material;
		LOG_INFO( "Loaded material [" << name << "]" );
		return material;
	}

	return materials[ name ];
}

//----------------------------------------------------------------------//

/*
 * Создать материал
 * ------------------
 */

le::IMaterial* le::MaterialManager::Create( const string& Name )
{
	if ( materials.find( Name ) == materials.end() )
	{
		IMaterial*		material = ( IMaterial* ) factory->Create( MATERIAL_INTERFECE_VERSION );
		if ( !material )	return nullptr;

		LOG_INFO( "Created material [" << Name << "]" );
		materials[ Name ] = material;
		return materials[ Name ];
	}

	return nullptr;
}

//----------------------------------------------------------------------//

/*
 * Удалить материал
 * ------------------
 */

void le::MaterialManager::Delete( const string& Name )
{
	auto			it = materials.find( Name );
	
	if ( it == materials.end() || it->second->GetReferenceCount() > 0 ) return;

	factory->Delete( it->second );
	materials.erase( Name );

	LOG_INFO( "Material with name [" << Name << "] deleted" );
}

//----------------------------------------------------------------------//

/*
 * Удалить материал
 * ------------------
 */

void le::MaterialManager::Delete( IMaterial* Material )
{
	if ( !Material || Material->GetReferenceCount() > 0 ) return;

	for ( auto it = materials.begin(), itEnd = materials.end(); it != itEnd; ++it )
		if ( it->second == Material )
		{
			LOG_INFO( "Material with name [" << it->first << "] deleted" );

			factory->Delete( it->second );
			materials.erase( it );
			return;
		}
}

// ------------------------------------------------------------------------------------ //
// Удалить все ресурсы
// ------------------------------------------------------------------------------------ //
void le::MaterialManager::DeleteAll()
{
	if ( materials.empty() ) return;

	for ( auto it = materials.begin(), itEnd = materials.end(); it != itEnd; ++it )
		factory->Delete( it->second );

	LOG_INFO( "All materials deleted" );
	materials.clear();
}

//----------------------------------------------------------------------//

/*
 * Задать каталог с материалами
 * ------------------
 */

void le::MaterialManager::SetDirectory( const string& Directory )
{
	directory = Directory;
}

//----------------------------------------------------------------------//

/*
 * Получить каталог с материалами
 * ------------------
 */

const string& le::MaterialManager::GetDirectory() const
{
	return directory;
}

//----------------------------------------------------------------------//

/*
 * Получить материал по названию
 * ------------------
 */

le::IMaterial* le::MaterialManager::Get( const string& Name )
{
	if ( materials.find( Name ) != materials.end() )
		return materials[ Name ];

	return nullptr;
}

//----------------------------------------------------------------------//
