cmake_minimum_required( VERSION 3.7 )

#
#   --- Задаем переменные ---
#

file( GLOB SOURCE_FILES "*.h" "*.cpp" "../public/engine/*.h" )
set( MODULE_NAME engine )

set( SDL2_PATH ${EXTLIBS_DIR}/SDL2 CACHE PATH "Path to SDL2" )
set( RAPIDJSON_PATH ${EXTLIBS_DIR} CACHE PATH "Path to RapidJSON" )
set( GLM_PATH ${EXTLIBS_DIR} CACHE PATH "Path to GLM" )
set( FREEIMAGE_PATH ${EXTLIBS_DIR}/FreeImage CACHE PATH "Path to FreeImage" )
set( FREETYPE_PATH ${EXTLIBS_DIR}/freetype CACHE PATH "Path to Freetype" )
set( GLEW_PATH ${EXTLIBS_DIR}/GLEW CACHE PATH "Path to GLEW" )

#
#   --- Название модулей движка ---
#

set( ENGINE_LIB "engine" )
set( GAME_LIB "game" )
set( DIRSINFO dirsinfo.h )

#
#   --- Указываем платформозависимые исходники ---
#

#   Платформозависимые исходники для Windows
if( ${CMAKE_SYSTEM_NAME} MATCHES "Windows" )
    set( SOURCE_FILES ${SOURCE_FILES}
                        win32/wglcontext.cpp
                        win32/wglcontext.h )
	
#   Платформозависимые исходники для Linux    
elseif( ${CMAKE_SYSTEM_NAME} MATCHES "Linux" )
    set( SOURCE_FILES ${SOURCE_FILES}
                        unix/glxcontext.cpp
                        unix/glxcontext.h )
else()
    message( SEND_ERROR "Unknow platform")
endif()


#   Название модулей для Windows
if( ${CMAKE_SYSTEM_NAME} MATCHES "Windows" )
    set( ENGINE_LIB ${ENGINE_LIB}.dll )
    set( GAME_LIB ${GAME_LIB}.dll )

#   Название модулей для Linux    
elseif( ${CMAKE_SYSTEM_NAME} MATCHES "Linux" )
    set( ENGINE_LIB lib${ENGINE_LIB}.so )
    set( GAME_LIB lib${GAME_LIB}.so )

else()
    message( SEND_ERROR "Unknow platform")
endif()

#
#   --- Задаем комманды препроцессора ---
#

add_definitions( -DLIFEENGINE_EXPORT )
add_definitions( -DFREEIMAGE_COLORORDER=FREEIMAGE_COLORORDER_RGB )
#
#   --- Настройки проекта ---
#

configure_file( ${DIRSINFO}.in ${DIRSINFO} )
include_directories( ${CMAKE_BINARY_DIR}/${MODULE_NAME} )
include_directories( ../public/ )
include_directories( ../ )
include_directories( ./ )
add_library( ${MODULE_NAME} SHARED ${SOURCE_FILES} )
target_link_libraries( ${MODULE_NAME} ${PROJECT_COMMON} )
install( TARGETS ${MODULE_NAME} DESTINATION ${BUILD_DIR}/engine )

#
#   --- Ищим и подключаем зависимости ---
#

#---------------
#   OpenGL

find_package( OpenGL REQUIRED )
if( NOT OPENGL_FOUND )
    message( SEND_ERROR "Failed to find OpenGL" )
    return()
else()
    include_directories( ${OPENGL_INCLUDE_DIR} )
    target_link_libraries( ${MODULE_NAME} ${OPENGL_LIBRARIES} )
endif()


#---------------
#   FreeImage

find_package( FreeImage REQUIRED )
if ( NOT FREEIMAGE_FOUND )
    message( SEND_ERROR "Failed to FreeImage" )
    return()
else()
    include_directories( ${FREEIMAGE_INCLUDE} )
	target_link_libraries( ${MODULE_NAME} ${FREEIMAGE_LIB} )
endif()

#---------------
#   SDL2

find_package( SDL2 REQUIRED )
if( NOT SDL2_FOUND )
    message( SEND_ERROR "Failed to find SDL2" )
    return()
else()
    include_directories( ${SDL2_INCLUDE} )
    target_link_libraries( ${MODULE_NAME} ${SDL2_LIB} ${SDL2MAIN_LIB} )
endif()

#---------------
#   RapidJSON

find_package( Rapidjson REQUIRED )
if ( NOT RAPIDJSON_FOUND )
    message( SEND_ERROR "Failed to find RapidJSON" )
    return()
else()
    include_directories( ${RAPIDJSON_INCLUDE} )
endif()

#---------------
#   Freetype

find_package( Freetype REQUIRED )
if( NOT FREETYPE_FOUND )
    message( SEND_ERROR "Failed to find Freetype" )
    return()
else()
    include_directories( ${FREETYPE_INCLUDE} )
    target_link_libraries( ${MODULE_NAME} ${FREETYPE_LIB} )
endif()   

#---------------
#   GLEW

find_package( GLEW REQUIRED )
if( NOT GLEW_FOUND )
    message( SEND_ERROR "Failed to find GLEW" )
    return()
else()
    include_directories( ${GLEW_INCLUDE} )
    target_link_libraries( ${MODULE_NAME} ${GLEW_LIB} )
endif() 

#---------------
#   GLM

find_package( GLM REQUIRED )
if ( NOT GLM_FOUND )
    message( SEND_ERROR "Failed to find GLM" )
    return()
else()
    include_directories( ${GLM_INCLUDE} )
endif()