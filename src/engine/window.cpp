//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

#include "common/log.h"
#include "common/version.h"
#include "common/configurations.h"
#include "common/image.h"
#include "engine/icore.h"
#include "window.h"

//---------------------------------------------------------------------//

/*
 * Конструктор
 * ------------
 */

le::Window::Window() :
	window( nullptr ),
	handle( nullptr ),
	cursor( nullptr ),
	windowInfo( nullptr ),
	isShowCursor( true )
{}

//---------------------------------------------------------------------//

/*
 * Деструктор
 * -------------
 */

le::Window::~Window()
{
	Close();
}

//---------------------------------------------------------------------//

/*
 * Создать окно
 * -----------------
 */

bool le::Window::Create( const char* Title, int Width, int Height, STYLE_WINDOW Style )
{
	if ( window )
		Close();

	uint32_t			flags = SDL_WINDOW_SHOWN;
	if ( Style & SW_FULLSCREEN )
		flags |= SDL_WINDOW_FULLSCREEN;
	else
	{
		if ( Style & SW_RESIZABLE )
			flags |= SDL_WINDOW_RESIZABLE;

		if ( !( Style & SW_DECORATED ) )
			flags |= SDL_WINDOW_BORDERLESS;
	}

	window = SDL_CreateWindow( Title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Width, Height, flags );

	if ( !window )
	{
		LOG_ERROR( "Failed created window" );
		return false;
	}

	LOG_INFO( "Window created (" << Width << "x" << Height << ")" );

	windowInfo = new SDL_SysWMinfo();
	SDL_VERSION( &windowInfo->version );
	SDL_GetWindowWMInfo( window, windowInfo );

#if defined( PLATFORM_WINDOWS )
	handle = windowInfo->info.win.window;
#elif defined( PLATFORM_LINUX )
	handle = &windowInfo->info.x11.window;
#else
	#error Unknown platform
#endif

	windowID = SDL_GetWindowID( window );
	return true;
}

//---------------------------------------------------------------------//

/*
 * Обработка событий окна
 * -----------------
 */

size_t utf8_length( unsigned char c )
{
	c = ( unsigned char ) ( 0xff & c );
	if ( c < 0x80 )
		return 1;
	else if ( ( c >> 5 ) == 0x6 )
		return 2;
	else if ( ( c >> 4 ) == 0xe )
		return 3;
	else if ( ( c >> 3 ) == 0x1e )
		return 4;
	else
		return 0;
}
char* utf8_next( char* p )
{
	size_t len = utf8_length( *p );
	size_t i = 0;
	if ( !len )
		return 0;

	for ( ; i < len; ++i )
	{
		++p;
		if ( !*p )
			return 0;
	}
	return p;
}
#include <codecvt>
typedef std::codecvt_utf8<wchar_t> convert_type;
bool le::Window::PollEvent( Event& Event )
{
	if ( !window )	return false;
	
	SDL_Event			sdlEvent;
	bool				isEndEvent = SDL_PollEvent( &sdlEvent );
	
	switch ( sdlEvent.type )
	{
		// Событие ввода текста
	case SDL_TEXTINPUT:
		Event.type = Event::ET_TEXT_INPUT;
		Event.textInputEvent.text = wstring_convert<convert_type, wchar_t>().from_bytes( sdlEvent.text.text );
		break;

		// Событие нажатия и отжатия клавиш
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		if ( sdlEvent.type == SDL_KEYUP )
			Event.type = Event::ET_KEY_RELEASED;
		else
			Event.type = Event::ET_KEY_PRESSED;
		
		Event.key.isAlt = sdlEvent.key.keysym.mod & KMOD_ALT;
		Event.key.isCapsLock = sdlEvent.key.keysym.mod & KMOD_CAPS;
		Event.key.isControl = sdlEvent.key.keysym.mod & KMOD_CTRL;
		Event.key.isNumLock = sdlEvent.key.keysym.mod & KMOD_NUM;
		Event.key.isShift = sdlEvent.key.keysym.mod & KMOD_SHIFT;
		Event.key.isSuper = sdlEvent.key.keysym.mod & KMOD_GUI;
		Event.key.code = static_cast< KEYBOARD_KEY >( sdlEvent.key.keysym.scancode );
		break;

		// Событие нажатия и отжатия кнопок мыши
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		if ( sdlEvent.type == SDL_MOUSEBUTTONUP )
			Event.type = Event::ET_MOUSE_RELEASED;
		else
			Event.type = Event::ET_MOUSE_PRESSED;

		Event.mouseButton.code = static_cast< MOUSE_KEY >( sdlEvent.button.button );
		Event.mouseButton.x = sdlEvent.button.x;
		Event.mouseButton.y = sdlEvent.button.y;
		break;

		// Событие вращения колесика мыши
	case SDL_MOUSEWHEEL:
		Event.type = Event::ET_MOUSE_WHEEL;
		Event.mouseWheel.x = sdlEvent.wheel.direction == SDL_MOUSEWHEEL_FLIPPED ? sdlEvent.wheel.x * -1 : sdlEvent.wheel.x;
		Event.mouseWheel.y = sdlEvent.wheel.direction == SDL_MOUSEWHEEL_FLIPPED ? sdlEvent.wheel.y * -1 : sdlEvent.wheel.y;
		break;

		// Событие перемещения мышки
	case SDL_MOUSEMOTION:
		Event.type = Event::ET_MOUSE_MOVE;
		Event.mouseMove.x = sdlEvent.motion.x;
		Event.mouseMove.y = sdlEvent.motion.y;
		Event.mouseMove.xDirection = sdlEvent.motion.xrel;
		Event.mouseMove.yDirection = sdlEvent.motion.yrel;
		break;

		// События окна
	case SDL_WINDOWEVENT:
		//if ( windowID != sdlEvent.window.windowID )
		//	break;

		switch ( sdlEvent.window.event )
		{
			// Событие закрытия окна
		case SDL_WINDOWEVENT_CLOSE:
			Event.type = Event::ET_WINDOW_CLOSE;
			break;

			// Событие изменения размеров окна
		case SDL_WINDOWEVENT_SIZE_CHANGED:
		case SDL_WINDOWEVENT_RESIZED:
			Event.type = Event::ET_WINDOW_RESIZE;
			Event.windowResize.width = sdlEvent.window.data1;
			Event.windowResize.height = sdlEvent.window.data2;
			break;

			// Событие получения фокуса 
		case SDL_WINDOWEVENT_FOCUS_GAINED:
			Event.type = Event::ET_WINDOW_FOCUS_GAINED;
			if ( !isShowCursor ) SDL_SetRelativeMouseMode( SDL_TRUE );
			break;

			// Событие потери фокуса
		case SDL_WINDOWEVENT_FOCUS_LOST:
			Event.type = Event::ET_WINDOW_FOCUS_LOST;
			if ( !isShowCursor ) SDL_SetRelativeMouseMode( SDL_FALSE );
			break;
		}
		break;

	default:		Event.type = Event::ET_NONE;		break;
	}

	return isEndEvent;
}

//---------------------------------------------------------------------//

/*
 * Закрыть окно
 * -----------------
 */

void le::Window::Close()
{
	if ( !window )	return;
	else SDL_DestroyWindow( window );

	if ( cursor ) SDL_FreeCursor( cursor );
	if ( !isShowCursor ) SDL_SetRelativeMouseMode( SDL_FALSE );
	if ( windowInfo ) delete windowInfo;

	LOG_INFO( "Window closed" );

	window = nullptr;
	handle = nullptr;
	cursor = nullptr;
	windowInfo = nullptr;
	isShowCursor = true;
	windowID = 0;
}

//---------------------------------------------------------------------//

/*
 * Задать иконку окна
 * -----------------
 */

void le::Window::SetIcon( Image& Image )
{
	if ( !window || !Image.data ) return;

	SDL_Surface*		surface = SDL_CreateRGBSurfaceFrom( Image.data, Image.width, Image.height, Image.depth, Image.pitch,
															Image.rmask, Image.gmask, Image.bmask, Image.amask );
	SDL_SetWindowIcon( window, surface );
	SDL_FreeSurface( surface );
}

//---------------------------------------------------------------------//

/*
 * Задать иконку окна
 * -----------------
 */

void le::Window::SetCursor( Image& Image )
{
	if ( !Image.data ) return;
	if ( cursor ) SDL_FreeCursor( cursor );

	SDL_Surface*		surface = SDL_CreateRGBSurfaceFrom( Image.data, Image.width, Image.height, Image.depth, Image.pitch,
															Image.rmask, Image.gmask, Image.bmask, Image.amask );
	cursor = SDL_CreateColorCursor( surface, 0, 0 );
	SDL_SetCursor( cursor );

	SDL_FreeSurface( surface );
}

//---------------------------------------------------------------------//

/*
 * Сбросить иконку курсора до стандартной
 * -----------------
 */

void le::Window::ResetCursor()
{
	if ( !cursor ) return;

	SDL_FreeCursor( cursor );
	cursor = SDL_CreateSystemCursor( SDL_SYSTEM_CURSOR_ARROW );
	SDL_SetCursor( cursor );
}

//---------------------------------------------------------------------//

/*
 * Задать отображение курсора
 * -----------------
 */

void le::Window::SetShowCursor( bool IsShow )
{
	if ( IsShow )
		SDL_SetRelativeMouseMode( SDL_FALSE );
	else
		SDL_SetRelativeMouseMode( SDL_TRUE );

	isShowCursor = IsShow;
}

//---------------------------------------------------------------------//

/*
 * Задать заголовок окна
 * -----------------
 */

void le::Window::SetHandle( WindowHandle_t WindowHandle )
{
	if ( window ) return;
	handle = WindowHandle;
}

 //---------------------------------------------------------------------//

/*
 * Задать название окна
 * -----------------
 */

void le::Window::SetTitle( const char* Title )
{
	if ( !window ) return;
	SDL_SetWindowTitle( window, Title );
}

//---------------------------------------------------------------------//

/*
 * Задать размер окна
 * -----------------
 */

void le::Window::SetSize( uint32_t Width, uint32_t Height )
{
	if ( !window ) return;

	SDL_SetWindowSize( window, Width, Height );
	SDL_SetWindowPosition( window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED );
}

//---------------------------------------------------------------------//

/*
 * Получить размер окна
 * -----------------
 */

const glm::ivec2 le::Window::GetSize() const
{
	if ( !window )		return glm::ivec2( 0, 0 );

	glm::ivec2			windowSize;
	SDL_GetWindowSize( window, &windowSize.x, &windowSize.y );

	return windowSize;
}

//---------------------------------------------------------------------//

/*
 * Получить заголовок окна
 * -----------------
 */

le::WindowHandle_t le::Window::GetHandle() const
{
	return handle;
}

//---------------------------------------------------------------------//

/*
 * Открыто ли окно
 * -----------------
 */

bool le::Window::IsOpen()
{
	return window;
}

//----------------------------------------------------------------------//