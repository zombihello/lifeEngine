//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

#include "common/configurations.h"
#include "common/event.h"
#include "engine/icore.h"
#include "inputsystem.h"

//---------------------------------------------------------------------//

/*
 * Конструктор
 * -------------
 */

le::InputSystem::InputSystem() :
	mouseSensitivity( 0.15f ),
	mouseWheel( MW_NONE ),
	mousePosition( 0.f ),
	mouseOffset( 0.f )
{
	for ( uint32_t index = 0; index < KK_KEY_COUNT; ++index )
		keyboardKeyEvents[ index ] = Event::ET_NONE;

	for ( uint32_t index = 0; index < MK_KEY_COUNT; ++index )
		mouseButtonEvents[ index ] = Event::ET_NONE;
}

//---------------------------------------------------------------------//

/*
 * Деструктор
 * -------------
 */

le::InputSystem::~InputSystem()
{}

//---------------------------------------------------------------------//

/*
 * Инициализировать подсистему движка
 * ----------------------------
 */

bool le::InputSystem::Initialize( ICore* Core )
{
	mouseSensitivity = Core->GetConfigurations().sensitivityMouse;

	return true;
}

//---------------------------------------------------------------------//

/*
 * Обработать событие
 * -------------------------------------
 */

void le::InputSystem::ApplyEvent( Event& Event )
{
	switch ( Event.type )
	{
	case Event::ET_KEY_PRESSED:
	case Event::ET_KEY_RELEASED:
		keyboardKeyEvents[ Event.key.code ] = Event.type;
		break;

	case Event::ET_MOUSE_PRESSED:
	case Event::ET_MOUSE_RELEASED:
		mouseButtonEvents[ Event.mouseButton.code ] = Event.type;
		break;

	case Event::ET_MOUSE_MOVE:
		mouseOffset.x = Event.mouseMove.xDirection;
		mouseOffset.y = Event.mouseMove.yDirection;
		mousePosition.x = Event.mouseMove.x;
		mousePosition.y = Event.mouseMove.y;
		break;

	case Event::ET_MOUSE_WHEEL:
		mouseWheel = Event.mouseWheel.y > 0 ? MW_UP : Event.mouseWheel.y < 0 ? MW_DOWN : MW_NONE;
		break;

	case Event::ET_TEXT_INPUT:
		inputText = Event.textInputEvent.text;
		break;
	}
}

//---------------------------------------------------------------------//

/*
 * Очистить систему ввода
 * -----------------------------------
 */

void le::InputSystem::Clear()
{
	for ( uint32_t index = 0; index < KK_KEY_COUNT; ++index )
		if ( keyboardKeyEvents[ index ] == Event::ET_KEY_RELEASED )
		keyboardKeyEvents[ index ] = Event::ET_NONE;

	for ( uint32_t index = 0; index < MK_KEY_COUNT; ++index )
		if ( mouseButtonEvents[ index ] == Event::ET_KEY_RELEASED )
		mouseButtonEvents[ index ] = Event::ET_NONE;

	mouseWheel = MW_NONE;
	mouseOffset = glm::vec2( 0.f );
	inputText.clear();
}

//---------------------------------------------------------------------//

/*
 * Нажата ли клавиша
 * -----------------------------------
 */

bool le::InputSystem::IsKeyDown( KEYBOARD_KEY Key )
{
	LIFEENGINE_ASSERT( Key < KK_KEY_COUNT, "le::KEYBOARD_KEY (" << Key << ") more le::KK_KEY_COUNT (" << KK_KEY_COUNT << ")" );
	return keyboardKeyEvents[ Key ] == Event::ET_KEY_PRESSED;
}

//---------------------------------------------------------------------//

/*
 * Отпущена ли клавиша
 * -----------------------------------
 */

bool le::InputSystem::IsKeyUp( KEYBOARD_KEY Key )
{
	LIFEENGINE_ASSERT( Key < KK_KEY_COUNT, "le::KEYBOARD_KEY (" << Key << ") more le::KK_KEY_COUNT (" << KK_KEY_COUNT << ")" );
	return keyboardKeyEvents[ Key ] == Event::ET_KEY_RELEASED;
}

//---------------------------------------------------------------------//

/*
 * Нажата ли клавиша мышки
 * -----------------------------------
 */

bool le::InputSystem::IsMouseKeyDown( MOUSE_KEY MouseKey )
{
	LIFEENGINE_ASSERT( MouseKey < MK_KEY_COUNT, "le::MOUSE_KEY (" << MouseKey << ") more le::MK_KEY_COUNT (" << MK_KEY_COUNT << ")" );
	return mouseButtonEvents[ MouseKey ] == Event::ET_MOUSE_PRESSED;
}

//---------------------------------------------------------------------//

/*
 * Отпущена ли клавиша мышки
 * -----------------------------------
 */

bool le::InputSystem::IsMouseKeyUp( MOUSE_KEY MouseKey )
{
	LIFEENGINE_ASSERT( MouseKey < MK_KEY_COUNT, "le::MOUSE_KEY (" << MouseKey << ") more le::MK_KEY_COUNT (" << MK_KEY_COUNT << ")" );
	return mouseButtonEvents[ MouseKey ] == Event::ET_MOUSE_RELEASED;
}

//---------------------------------------------------------------------//

/*
 * Вращается ли колесико мышки
 * -----------------------------------
 */

bool le::InputSystem::IsMouseWheel( MOUSE_WHEEL MouseWheel )
{
	return mouseWheel == MouseWheel;
}

//---------------------------------------------------------------------//

/*
 * Вводится ли текст
 * -----------------------------------
 */

bool le::InputSystem::IsInputText() const
{
	return !inputText.empty();
}

//---------------------------------------------------------------------//

/*
 * Получить позицию мышки
 * -----------------------------------
 */

const glm::vec2& le::InputSystem::GetMousePosition() const
{
	return mousePosition;
}

//---------------------------------------------------------------------//

/*
 * Получить смещение мышки
 * -----------------------------------
 */

const glm::vec2& le::InputSystem::GetMouseOffset() const
{
	return mouseOffset;
}

//---------------------------------------------------------------------//

/*
 * Получить чувствительность мышки
 * -----------------------------------
 */

float le::InputSystem::GetMouseSensitivity() const
{
	return mouseSensitivity;
}

//---------------------------------------------------------------------//

/*
 * Получить тип подсистемы движка
 * -----------------------
 */

le::ENGINE_SUBSYSTEM_TYPE le::InputSystem::GetType() const
{
	return EST_INPUT_SYSTEM;
}

//---------------------------------------------------------------------//

/*
 * Получить введенный текст
 * -----------------------
 */

const wstring& le::InputSystem::GetInputText() const
{
	return inputText;
}

 //---------------------------------------------------------------------//