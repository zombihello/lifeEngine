//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "engine/iresourcesystem.h"
#include "engine/imaterial.h"
#include "scene.h"
#include "model.h"

//----------------------------------------------------------------------//

/*
 * Конструктор
 * -----------------
 */

le::Model::Model() :
	mesh( nullptr ),
	matrix_Position( 1.f ),
	matrix_Rotation( 1.f ),
	matrix_Scale( 1.f ),
	matrix_Transformation( 1.f ),
	position( 0.f ),
	rotation( 1.f, 0.f, 0.f, 0.f ),
	scale( 1.f )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------
 */

le::Model::~Model()
{
	SetMesh( nullptr );
	ClearCustomMaterials();
}

//----------------------------------------------------------------------//

/*
 * Добавить кастомный материал
 * ------------------------
 */

void le::Model::AddCustomMaterial( uint32_t MaterialId, IMaterial* Material )
{
	LIFEENGINE_ASSERT( MaterialId >= 0, "Material id < 0" );
	LIFEENGINE_ASSERT( Material, "Material is null" );

	Material->IncrementReferenceCount();
	customMaterials[ MaterialId ] = Material;
}

//----------------------------------------------------------------------//

/*
 * Удалить кастомный материал
 * ------------------------
 */

void le::Model::RemoveCustomMaterial( uint32_t MaterialId )
{
	LIFEENGINE_ASSERT( MaterialId >= 0, "Material id < 0" );

	auto		it = customMaterials.find( MaterialId );
	if ( it == customMaterials.end() ) return;

	it->second->DecrementReferenceCount();

	if ( it->second->GetReferenceCount() <= 0 )
		resourceSystem->GetMaterialManager()->Delete( it->second );

	customMaterials.erase( it );
}

//----------------------------------------------------------------------//

/*
 * Очистить массив кастомных материалов
 * ------------------------
 */

void le::Model::ClearCustomMaterials()
{
	for ( auto it = customMaterials.begin(), itEnd = customMaterials.end(); it != itEnd; ++it )
	{
		it->second->DecrementReferenceCount();

		if ( it->second->GetReferenceCount() <= 0 )
			resourceSystem->GetMaterialManager()->Delete( it->second );
	}

	customMaterials.clear();
}

//----------------------------------------------------------------------//

/*
 * Сместить модель
 * ------------------------
 */

void le::Model::Move( const glm::vec3& FactorMove )
{
	glm::mat4			matrix_Temp = glm::translate( FactorMove );

	position += FactorMove;
	matrix_Position *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

//-------------------------------------------------------------------------//

/*
 * Отмасштабировать модель
 * ------------------------
 */

void le::Model::Scale( const glm::vec3& FactorScale )
{
	glm::mat4			matrix_Temp = glm::scale( FactorScale );

	scale += FactorScale;
	matrix_Scale *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

//-------------------------------------------------------------------------//

/*
 * Повернуть модель
 * ------------------------
 */

void le::Model::Rotate( const glm::vec3& FactorRotate )
{
	glm::vec3			axis( sin( FactorRotate.x / 2 ), sin( FactorRotate.y / 2 ), sin( FactorRotate.z / 2 ) );
	glm::vec3			rotations( cos( FactorRotate.x / 2 ), cos( FactorRotate.y / 2 ), cos( FactorRotate.z / 2 ) );

	glm::quat			rotateX( rotations.x, axis.x, 0, 0 );
	glm::quat			rotateY( rotations.y, 0, axis.y, 0 );
	glm::quat			rotateZ( rotations.z, 0, 0, axis.z );

	glm::quat			rotation = rotateX * rotateY * rotateZ;
	glm::mat4			matrix_Temp = glm::mat4_cast( rotation );

	this->rotation *= rotation;
	matrix_Rotation *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

//-------------------------------------------------------------------------//

/*
 * Повернуть модель
 * ------------------------
 */

void le::Model::Rotate( const glm::quat& FactorRotate )
{
	glm::mat4			matrix_Temp = glm::mat4_cast( FactorRotate );
	
	rotation *= FactorRotate;
	matrix_Rotation *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

//-------------------------------------------------------------------------//

/*
 * Задать меш модели
 * ------------------------
 */

void le::Model::SetMesh( IMesh* Mesh )
{
	if ( mesh == Mesh ) return;

	if ( mesh )
	{
		mesh->DecrementReferenceCount();

		if ( mesh->GetReferenceCount() <= 0 )
			resourceSystem->GetMeshManager()->Delete( mesh );

		mesh = nullptr;
		if ( !Mesh ) return;
	}

	Mesh->IncrementReferenceCount();
	mesh = Mesh;
}

//-------------------------------------------------------------------------//

/*
 * Задать позицию модели
 * ------------------------
 */

void le::Model::SetPosition( const glm::vec3& Position )
{
	position = Position;
	matrix_Position = glm::translate( Position );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

//-------------------------------------------------------------------------//

/*
 * Задать вращения модели
 * ------------------------
 */

void le::Model::SetRotation( const glm::vec3& Rotation )
{
	glm::vec3				axis( sin( Rotation.x / 2 ), sin( Rotation.y / 2 ), sin( Rotation.z / 2 ) );
	glm::vec3				rotations( cos( Rotation.x / 2 ), cos( Rotation.y / 2 ), cos( Rotation.z / 2 ) );

	glm::quat				rotateX( rotations.x, axis.x, 0, 0 );
	glm::quat				rotateY( rotations.y, 0, axis.y, 0 );
	glm::quat				rotateZ( rotations.z, 0, 0, axis.z );
	glm::quat				rotation = rotateX * rotateY * rotateZ;

	this->rotation = rotation;
	matrix_Rotation = glm::mat4_cast( rotation );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

//-------------------------------------------------------------------------//

/*
 * Задать вращения модели
 * ------------------------
 */

void le::Model::SetRotation( const glm::quat& Rotation )
{
	rotation = Rotation;
	matrix_Rotation = glm::mat4_cast( Rotation );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

//-------------------------------------------------------------------------//

/*
 * Задать масштаб модели
 * ------------------------
 */

void le::Model::SetScale( const glm::vec3& Scale )
{
	scale = Scale;
	matrix_Scale = glm::scale( Scale );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

//-------------------------------------------------------------------------//

/*
 * Получить меш модели
 * ------------------------
 */

le::IMesh* le::Model::GetMesh() const
{
    return mesh;
}

//-------------------------------------------------------------------------//

/*
 * Получить массив кастомных материалов модели
 * ------------------------
 */

const unordered_map<uint32_t, le::IMaterial*>& le::Model::GetCustomMaterials() const
{
	return customMaterials;
}

//-------------------------------------------------------------------------//

/*
 * Получить количество кастомных материалов
 * ------------------------
 */

uint32_t le::Model::GetCountCustomMaterials() const
{
	return customMaterials.size();
}

//-------------------------------------------------------------------------//