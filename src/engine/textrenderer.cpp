//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (��������� �����) ***
//				Copyright (C) 2018-2019
//
// ����������� ������:  https://gitlab.com/zombihello/lifeEngine
// ������:				���� �������� (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "engine/icamera.h"
#include "engine/configurations.h"
#include "rendersystem.h"
#include "text.h"
#include "textrenderer.h"
#include "renderobject.h"

//----------------------------------------------------------------------//

/*
* �����������
* --------------------------------------
*/

le::TextRenderer::TextRenderer()
{}

//----------------------------------------------------------------------//

/*
* ����������
* --------------------------------------
*/

le::TextRenderer::~TextRenderer()
{}

//----------------------------------------------------------------------//

/*
* ���������������� ������� ������������ ���������
* --------------------------------------
*/

bool le::TextRenderer::Initialize()
{
	shader_Text = shaderManager->LoadFromFile( ST_TEXT );
	return shader_Text != nullptr;
}

//----------------------------------------------------------------------//

/*
* ���������� ��������� �� �������
* --------------------------------------
*/

void le::TextRenderer::Render( const RenderObject& RenderObject, const glm::mat4& ProjectionView )
{
	shader_Text->Bind();
	shader_Text->SetUniform( "matrix_Projection", ProjectionView );

	//FIXME: При пустом тексте происходит вылет

	static_cast< Text* >( RenderObject.object )->Bind( shader_Text );
	glDrawArrays( GL_TRIANGLES, 0, 6 * static_cast< Text* >( RenderObject.object )->GetText().size() );
}

//----------------------------------------------------------------------//