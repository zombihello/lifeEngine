//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef TEXT_H
#define TEXT_H

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <string>
#include <locale>
#include <codecvt>
using namespace std;

#include "engine/itext.h"
#include "shadermanager.h"
#include "font.h"
#include "vertexarrayobject.h"
#include "vertexbufferobject.h"

//-------------------------------------------------------------------------//

namespace le
{
	//-------------------------------------------------------------------------//
	
	//////////////////////////////////////////////////////////////////////
	/// \brief Текст
	///
	/// Данный класс отвечает за работу с текстом в системе
	/// визуализации
	//////////////////////////////////////////////////////////////////////
	class Text : public IText
	{
	public:
		//////////////////////////////////////////////////////////////////////
		/// \brief Конструктор
		//////////////////////////////////////////////////////////////////////
		Text();

		//////////////////////////////////////////////////////////////////////
		/// \brief Деструктор
		//////////////////////////////////////////////////////////////////////
		~Text();

		//////////////////////////////////////////////////////////////////////
		/// \brief Сместить текст
		///
		/// \param[in] FactorMove Фактор смещения
		//////////////////////////////////////////////////////////////////////
		inline void Move( const glm::vec3& FactorMove )
		{
			position += FactorMove;
			matrix_Transformation *= glm::translate( FactorMove );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Повернуть текст
		///
		/// \param[in] FactorRotate Углы поворота по XYZ
		//////////////////////////////////////////////////////////////////////
		void Rotate( const glm::vec3& FactorRotate );

		//////////////////////////////////////////////////////////////////////
		/// \brief Повернуть текст
		///
		/// \param[in] FactorRotate Кватернион вращения
		//////////////////////////////////////////////////////////////////////
		inline void Rotate( const glm::quat& FactorRotate )
		{
			rotation *= FactorRotate;
			matrix_Transformation *= glm::mat4_cast( FactorRotate );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Отмасштабировать текст
		///
		/// \param[in] FactorScale Фактор масштаба
		//////////////////////////////////////////////////////////////////////
		inline void Scale( const glm::vec3& FactorScale )
		{
			scale += FactorScale;
			matrix_Transformation *= glm::scale( FactorScale );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Активировать текст для визуализации
		///
		/// \param[in] Shader Шейдер
		//////////////////////////////////////////////////////////////////////
		inline void Bind( Shader_t& Shader )
		{
			if ( font )
			{
				Update();

				if ( vertexArray.IsCreate() && textureFont )
				{
					vertexArray.Bind();
					textureFont->Bind();

					Shader->SetUniform( "textColor", color );
					Shader->SetUniform( "matrix_Transformation", matrix_Transformation );
				}
			}
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Отвязать текст от визуализации
		//////////////////////////////////////////////////////////////////////
		static inline void Unbind()
		{
			VertexArrayObject::Unbind();
			TextureAtlas::Unbind();
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать шрифт текста
		///
		/// \param[in] Font Шрифт текста
		//////////////////////////////////////////////////////////////////////
		inline void SetFont( IFont* Font )
		{
			font = static_cast<le::Font*>( Font );
			isUpdateGeometry = true;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать размер шрифта
		///
		/// \param[in] CharacterSize Размер шрифта
		//////////////////////////////////////////////////////////////////////
		inline void SetCharacterSize( uint32_t CharacterSize )
		{
			characterSize = CharacterSize;
			isUpdateGeometry = true;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать содержимое текста
		///
		/// \param[in] Text Содержимое текста
		//////////////////////////////////////////////////////////////////////
		inline void SetText( const string& Text )
		{
			wstring_convert<codecvt_utf8<wchar_t>>		converter;
			text = converter.from_bytes( Text );

			isUpdateGeometry = true;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать содержимое текста
		///
		/// \param[in] Text Содержимое текста
		//////////////////////////////////////////////////////////////////////
		inline void SetText( const wstring& Text )
		{
			text = Text;
			isUpdateGeometry = true;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать цвет текста
		///
		/// \param[in] Text Цвет текста
		//////////////////////////////////////////////////////////////////////
		inline void SetColor( const glm::vec3& Color )
		{
			color = Color;
		}
		
		//////////////////////////////////////////////////////////////////
		/// \brief Задать позицию текста
		///
		/// \param[in] Position Позиция текста
		//////////////////////////////////////////////////////////////////////
		inline void SetPosition( const glm::vec3& Position )
		{
			position = Position;
			isUpdateTransformation = true;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать поворот текста
		///
		/// \param[in] Rotation Углы поворота по XYZ
		//////////////////////////////////////////////////////////////////////
		void SetRotation( const glm::vec3& Rotation );

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать углы поворота модели
		///
		/// \param[in] Rotation Кватернион вращения
		//////////////////////////////////////////////////////////////////////
		inline void SetRotation( const glm::quat& Rotation )
		{
			rotation = Rotation;
			isUpdateTransformation = true;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать масштаб модели
		///
		/// \param[in] Scale Масштаб модели
		//////////////////////////////////////////////////////////////////////
		inline void SetScale( const glm::vec3& Scale )
		{
			scale = Scale;
			isUpdateTransformation = true;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать межбуквеный интервал
		///
		/// \param[in] LetterSpacingFactor Межбуквеный интервал
		//////////////////////////////////////////////////////////////////////
		inline void SetLetterSpacingFactor( float LetterSpacingFactor )
		{
			letterSpacingFactor = LetterSpacingFactor;
			isUpdateGeometry = true;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать межстрочный интервал
		///
		/// \param[in] LineSpacingFactor Межстрочный интервал
		//////////////////////////////////////////////////////////////////////
		inline void SetLineSpacingFactor( float LineSpacingFactor )
		{
			lineSpacingFactor = LineSpacingFactor;
			isUpdateGeometry = true;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Активировать текст для отрисовки
		///
		/// \param[in] IsEnable Отрисовывать ли текст
		//////////////////////////////////////////////////////////////////////
		inline void SetEnable( bool IsEnable = true )
		{
			isEnable = IsEnable;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить шрифт текста
		///
		/// \return Идентификатор шрифта текста, если его нет - строка пустая
		//////////////////////////////////////////////////////////////////////
		const string& GetFont() const
		{
			return fontName;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить размер шрифта
		///
		/// \return Размер шрифта
		//////////////////////////////////////////////////////////////////////
		inline const uint32_t& GetCharacterSize() const
		{
			return characterSize;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить содержимое текста
		///
		/// \return Содержимое текста
		//////////////////////////////////////////////////////////////////////
		inline const wstring& GetText() const
		{
			return text;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить цвет текста
		///
		/// \return Цвет текста
		//////////////////////////////////////////////////////////////////////
		inline const glm::vec3& GetColor() const
		{
			return color;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить позицию текста
		///
		/// \return Позиция текста
		//////////////////////////////////////////////////////////////////////
		inline const glm::vec3& GetPosition() const
		{
			return position;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить поворот текста
		///
		/// \return Поворот текста
		//////////////////////////////////////////////////////////////////////
		inline const glm::quat& GetRotation() const
		{
			return rotation;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить масштаб текста
		///
		/// \return Получить масштаб текста
		//////////////////////////////////////////////////////////////////////
		inline const glm::vec3& GetScale() const
		{
			return scale;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить межбуквеный интервал
		///
		/// \return Межбуквеный интервал
		//////////////////////////////////////////////////////////////////////
		inline float GetLetterSpacingFactor() const
		{
			return letterSpacingFactor;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить межстрочный интервал
		///
		/// \return Межстрочный интервал
		//////////////////////////////////////////////////////////////////////
		inline float GetLineSpacingFactor() const
		{
			return lineSpacingFactor;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Включен ли текст
		///
		/// \return True текст включен для визуализации, иначе false
		//////////////////////////////////////////////////////////////////////
		inline bool IsEnabled() const
		{
			return isEnable;
		}

	private:
		//////////////////////////////////////////////////////////////////////
		/// \brief Обновить текст
		//////////////////////////////////////////////////////////////////////
		void Update();

		bool				isUpdateGeometry;		///< Нужно ли обновить геометрию текста
		bool				isUpdateTransformation;	///< Нужно ли обновить матрицу трансформации
		bool				isEnable;				///< Нужно ли визуализировать текст

		Font*				font;					///< Шрифт
		TextureAtlas_t		textureFont;			///< Текстура шрифта
		wstring				text;					///< Содержимое текста
		string				fontName;				///< Идентификатор шрифта
		uint32_t			characterSize;			///< Размер шрифта
		float				letterSpacingFactor;	///< Межбуквеный интервал
		float				lineSpacingFactor;		///< Межстрочный интервал

		glm::ivec2			sizeTextureFont;		///< Размер текстуры шрифта					
		glm::vec3			color;					///< Цвет текста
		glm::vec3			position;				///< Позиция текста
		glm::quat			rotation;				///< Поворот текста
		glm::vec3			scale;					///< Масштаб текста

		glm::mat4			matrix_Transformation;	///< Матрица трансормации

		VertexArrayObject	vertexArray;			///< Массив буферов
		VertexBufferObject	vertexBuffer;			///< Вершиный буфер
	};

	//-------------------------------------------------------------------------//
}

//-------------------------------------------------------------------------//

#endif // !TEXT_H
