//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "engine/iresourcesystem.h"
#include "engine/itexture.h"
#include "engine/imaterial.h"
#include "scene.h"
#include "sprite.h"

// ------------------------------------------------------------------------------------ //
// Конструктор
// ------------------------------------------------------------------------------------ //
le::Sprite::Sprite() :
	type( ST_SPRITE_STATIC ),
	material( nullptr ),
	matrix_Position( 1.f ),
	matrix_Rotation( 1.f ),
	matrix_Scale( 1.f ),
	matrix_Transformation( 1.f ),
	position( 0.f ),
	size( 0.f ),
	scale( 1.f ),
	rotation( 1.f, 0.f, 0.f, 0.f )
{}

// ------------------------------------------------------------------------------------ //
// Деструктор
// ------------------------------------------------------------------------------------ //
le::Sprite::~Sprite()
{
	SetMaterial( nullptr );
}

// ------------------------------------------------------------------------------------ //
// Сместить спрайт
// ------------------------------------------------------------------------------------ //
void le::Sprite::Move( const glm::vec3& FactorMove )
{
	glm::mat4			matrix_Temp = glm::translate( FactorMove );

	position += FactorMove;
	matrix_Position *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

// ------------------------------------------------------------------------------------ //
// Отмасштабировать спрайт
// ------------------------------------------------------------------------------------ //
void le::Sprite::Scale( const glm::vec3& FactorScale )
{
	glm::mat4			matrix_Temp = glm::scale( FactorScale );

	scale += FactorScale;
	matrix_Scale *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

// ------------------------------------------------------------------------------------ //
// Повернуть спрайт
// ------------------------------------------------------------------------------------ //
void le::Sprite::Rotate( const glm::vec3& FactorRotate )
{
	glm::vec3			axis( sin( FactorRotate.x / 2 ), sin( FactorRotate.y / 2 ), sin( FactorRotate.z / 2 ) );
	glm::vec3			rotations( cos( FactorRotate.x / 2 ), cos( FactorRotate.y / 2 ), cos( FactorRotate.z / 2 ) );

	glm::quat			rotateX( rotations.x, axis.x, 0, 0 );
	glm::quat			rotateY( rotations.y, 0, axis.y, 0 );
	glm::quat			rotateZ( rotations.z, 0, 0, axis.z );

	glm::quat			rotation = rotateX * rotateY * rotateZ;
	glm::mat4			matrix_Temp = glm::mat4_cast( rotation );

	this->rotation *= rotation;
	matrix_Rotation *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

// ------------------------------------------------------------------------------------ //
// Повернуть спрайт
// ------------------------------------------------------------------------------------ //
void le::Sprite::Rotate( const glm::quat& FactorRotate )
{
	glm::mat4			matrix_Temp = glm::mat4_cast( FactorRotate );

	rotation *= FactorRotate;
	matrix_Rotation *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

// ------------------------------------------------------------------------------------ //
// Задать тип спрайта
// ------------------------------------------------------------------------------------ //
void le::Sprite::SetType( SPRITE_TYPE SpriteType )
{
	type = SpriteType;
}

// ------------------------------------------------------------------------------------ //
// Задать прямоугольник текстуры, который будет отображать спрайт
// ------------------------------------------------------------------------------------ //
void le::Sprite::SetTextureRect( const FloatRect& Rect )
{
	textureRect = Rect;

	if ( material )
	{
		ITexture*		texture = material->GetTexture( 0 );
		if ( !texture ) return;

		glm::vec3		size = texture->GetSize();
		textureRect.top = ( size.y - textureRect.top ) - textureRect.height;

		textureRect.left /= size.x;
		textureRect.width /= size.x;
		textureRect.top /= size.y;
		textureRect.height /= size.y;
	}
}

// ------------------------------------------------------------------------------------ //
// Задать размер спрайта
// ------------------------------------------------------------------------------------ //
void le::Sprite::SetSize( const glm::vec2& Size )
{
	size = Size;
}

// ------------------------------------------------------------------------------------ //
// Задать материал спрайта
// ------------------------------------------------------------------------------------ //
void le::Sprite::SetMaterial( IMaterial* Material )
{
	if ( material == Material ) return;

	if ( material )
	{
		material->DecrementReferenceCount();

		if ( material->GetReferenceCount() <= 0 )
			resourceSystem->GetMaterialManager()->Delete( material );

		material = nullptr;
		if ( !Material ) return;
	}

	Material->IncrementReferenceCount();
	material = Material;

	ITexture* texture = material->GetTexture( 0 );
	if ( texture ) textureRect = FloatRect( 0, 0, 1, 1 );
}

// ------------------------------------------------------------------------------------ //
// Задать позицию спрайта
// ------------------------------------------------------------------------------------ //
void le::Sprite::SetPosition( const glm::vec3& Position )
{
	position = Position;
	matrix_Position = glm::translate( Position );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

// ------------------------------------------------------------------------------------ //
// Задать вращение спрайта
// ------------------------------------------------------------------------------------ //
void le::Sprite::SetRotation( const glm::vec3& Rotation )
{
	glm::vec3				axis( sin( Rotation.x / 2 ), sin( Rotation.y / 2 ), sin( Rotation.z / 2 ) );
	glm::vec3				rotations( cos( Rotation.x / 2 ), cos( Rotation.y / 2 ), cos( Rotation.z / 2 ) );

	glm::quat				rotateX( rotations.x, axis.x, 0, 0 );
	glm::quat				rotateY( rotations.y, 0, axis.y, 0 );
	glm::quat				rotateZ( rotations.z, 0, 0, axis.z );
	glm::quat				rotation = rotateX * rotateY * rotateZ;

	this->rotation = rotation;
	matrix_Rotation = glm::mat4_cast( rotation );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

// ------------------------------------------------------------------------------------ //
// Задать вращение спрайта
// ------------------------------------------------------------------------------------ //
void le::Sprite::SetRotation( const glm::quat& Rotation )
{
	rotation = Rotation;
	matrix_Rotation = glm::mat4_cast( Rotation );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

// ------------------------------------------------------------------------------------ //
// Задать масштаб спрайта
// ------------------------------------------------------------------------------------ //
void le::Sprite::SetScale( const glm::vec3& Scale )
{
	scale = Scale;
	matrix_Scale = glm::scale( Scale );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

// ------------------------------------------------------------------------------------ //
// Получить прямоугольник текстуры, который отображает спрайт
// ------------------------------------------------------------------------------------ //
const le::FloatRect& le::Sprite::GetTextureRect() const
{
	return textureRect;
}

// ------------------------------------------------------------------------------------ //
// Получить материал спрайта
// ------------------------------------------------------------------------------------ //
le::IMaterial* le::Sprite::GetMaterial() const
{
	return material;
}

// ------------------------------------------------------------------------------------ //
// Получить тип спрайта
// ------------------------------------------------------------------------------------ //
le::SPRITE_TYPE le::Sprite::GetType() const
{
	return type;
}

// ------------------------------------------------------------------------------------ //
// Получить размер спрайта
// ------------------------------------------------------------------------------------ //
const glm::vec2& le::Sprite::GetSize() const
{
	return size;
}

// ------------------------------------------------------------------------------------ //
// Получить позицию спрайта
// ------------------------------------------------------------------------------------ //
const glm::vec3& le::Sprite::GetPosition() const
{
	return position;
}

// ------------------------------------------------------------------------------------ //
// Получить кватернион вращения спрайта
// ------------------------------------------------------------------------------------ //
const glm::quat& le::Sprite::GetRotation() const
{
	return rotation;
}

// ------------------------------------------------------------------------------------ //
// Получить масштаб спрайта
// ------------------------------------------------------------------------------------ //
const glm::vec3& le::Sprite::GetScale() const
{
	return scale;
}

// ------------------------------------------------------------------------------------ //
// Получить матрицу трансформации спрайта
// ------------------------------------------------------------------------------------ //
const glm::mat4& le::Sprite::GetTransformation() const
{
	return matrix_Transformation;
}