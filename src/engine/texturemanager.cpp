//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "engine/ifactory.h"
#include "core.h"
#include "resourcesystem.h"
#include "texturemanager.h"

// ------------------------------------------------------------------------------------ //
// Конструктор
// ------------------------------------------------------------------------------------ //
le::TextureManager::TextureManager() :
	directory( "." )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------
 */

le::TextureManager::~TextureManager()
{}

//----------------------------------------------------------------------//

/*
 * Задать каталог с текстурами
 * ------------------
 */

void le::TextureManager::SetDirectory( const string& Directory )
{
	directory = Directory;
}

//----------------------------------------------------------------------//

/*
 * Получить каталог с текстурами
 * ------------------
 */

const string& le::TextureManager::GetDirectory() const
{
	return directory;
}

//----------------------------------------------------------------------//

/*
 * Загрузить текстуру
 * ------------------
 */

le::ITexture* le::TextureManager::Load( const string& Route, const string& Name )
{
	string					name = Name;
	if ( Name.empty() )		name = Route;

	if ( textures.find( name ) == textures.end() )
	{
		LOG_INFO( "Loading texture [" << name << "]" );
		ITexture*            texture = ( ITexture* ) factory->Create( TEXTURE_INTERFECE_VERSION );

		if ( !texture || !texture->Load( Route ) )
		{
			LOG_ERROR( "Texture [" << name << "] not loaded" );
			delete texture;
			return nullptr;
		}

		textures[ name ] = texture;
		LOG_INFO( "Loaded texture [" << name << "]" );
		return texture;
	}

	return textures[ name ];
}

//----------------------------------------------------------------------//

/*
 * Создать текстуру
 * ------------------
 */

le::ITexture* le::TextureManager::Create( const string& Name )
{
	if ( textures.find( Name ) == textures.end() )
	{
		ITexture*            texture = ( ITexture* ) factory->Create( TEXTURE_INTERFECE_VERSION );
		if ( !texture )		return nullptr;
		
		LOG_INFO( "Created texture [" << Name << "]" );
		textures[ Name ] = texture;		
		return textures[ Name ];
	}

	return nullptr;
}

//----------------------------------------------------------------------//

/*
 * Удалить текстуру
 * ------------------
 */

void le::TextureManager::Delete( const string& Name )
{
	auto			it = textures.find( Name );

	if ( it == textures.end() || it->second->GetReferenceCount() > 0 ) return;

	factory->Delete( it->second );
	textures.erase( Name );

	LOG_INFO( "Texture with name [" << Name << "] deleted" );
}

//----------------------------------------------------------------------//

/*
 * Удалить текстуру
 * ------------------
 */

void le::TextureManager::Delete( ITexture* Texture )
{
	if ( !Texture || Texture->GetReferenceCount() > 0 ) return;

	for ( auto it = textures.begin(), itEnd = textures.end(); it != itEnd; ++it )
		if ( it->second == Texture )
		{
			LOG_INFO( "Texture with name [" << it->first << "] deleted" );

			factory->Delete( it->second );
			textures.erase( it );
			return;
		}
}

// ------------------------------------------------------------------------------------ //
// Удалить все ресурсы
// ------------------------------------------------------------------------------------ //
void le::TextureManager::DeleteAll()
{
	if ( textures.empty() ) return;

	for ( auto it = textures.begin(), itEnd = textures.end(); it != itEnd; ++it )
		factory->Delete( it->second );

	LOG_INFO( "All textures deleted" );
	textures.clear();
}

//----------------------------------------------------------------------//

/*
 * Получить текстуру
 * ------------------
 */

le::ITexture* le::TextureManager::Get( const string& Name )
{
	if ( textures.find( Name ) != textures.end() )
		return textures[ Name ];

	return nullptr;
}

//----------------------------------------------------------------------//
