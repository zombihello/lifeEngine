//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "engine/icamera.h"
#include "modelrenderer.h"
#include "rendersystem.h"
#include "mesh.h"
#include "material.h"
#include "renderobject.h"

//----------------------------------------------------------------------//

/*
 * Конструктор
 * --------------------------------------
 */

le::ModelRenderer::ModelRenderer()
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * --------------------------------------
 */

le::ModelRenderer::~ModelRenderer()
{}

//----------------------------------------------------------------------//

/*
 * Отрисовать геометрию из очереди
 * --------------------------------------
 */

void le::ModelRenderer::Render( const RenderObject& RenderObject, const glm::mat4& ProjectionView )
{
	RenderObject.shader->Bind();
	RenderObject.material->Bind( RenderObject.shader );
	RenderObject.vertexArrayObject->Bind();

	RenderObject.shader->SetUniform( "matrix_Projection", ProjectionView );
	RenderObject.shader->SetUniform( "matrix_Transformation", ( *RenderObject.matrixTransformation ) );

	glDrawElements( RenderObject.renderMode, RenderObject.countVertexIndex, GL_UNSIGNED_INT, ( void* ) ( RenderObject.startVertexIndex * sizeof( unsigned int ) ) );
}

//----------------------------------------------------------------------//