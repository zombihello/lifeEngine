//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "engine/icamera.h"
#include "engine/isprite.h"
#include "engine/imesh.h"
#include "common/configurations.h"
#include "spriterenderer.h"
#include "rendersystem.h"
#include "material.h"
#include "renderobject.h"

//----------------------------------------------------------------------//

/*
* Конструктор
* --------------------------------------
*/

le::SpriteRenderer::SpriteRenderer()
{}

//----------------------------------------------------------------------//

/*
* Деструктор
* --------------------------------------
*/

le::SpriteRenderer::~SpriteRenderer()
{
	vertexArrayObject.Delete();
	vertexBufferObject.Delete();
	indexBufferObject.Delete();
}

//----------------------------------------------------------------------//

/*
* Инициализировать систему визуализации геометрии
* --------------------------------------
*/

void le::SpriteRenderer::Initialize()
{
	// Массив вершин и индексов, который зальется в GPU

	float x0 = -( float ) 1.f, y0 = -( float ) 1.f;
	float x1 = ( float ) 1.f, y1 = ( float ) 1.f;

	IMesh::Vertex		arrayVertexes[] =
	{
		{ glm::vec3( x0, y0, 1.0f ), glm::vec3( x0, y0, 0.0f ), glm::vec2( 0.0f, 0.0f ) },
		{ glm::vec3( x0, y1, 1.0f ), glm::vec3( x0, y1, 0.0f ), glm::vec2( 0.0f, 1.0f ) },
		{ glm::vec3( x1, y1, 1.0f ), glm::vec3( x1, y1, 0.0f ), glm::vec2( 1.0f, 1.0f ) },
		{ glm::vec3( x1, y0, 1.0f ), glm::vec3( x1, y0, 0.0f ), glm::vec2( 1.0f, 0.0f ) },
	};

	uint32_t			arrayIndices[] =
	{ 3, 2, 1, 0 };

	// Формируем буфер вершин для визуализации спрайтов

	vertexArrayObject.Create();
	vertexBufferObject.Create();
	indexBufferObject.Create();

	vertexBufferObject.Bind();
	vertexBufferObject.Allocate( arrayVertexes, 4 * sizeof( IMesh::Vertex ) );

	indexBufferObject.Bind();
	indexBufferObject.Allocate( arrayIndices, 6 * sizeof( uint32_t ) );

	VertexBufferLayout				vertexBufferLayout;
	vertexBufferLayout.PushFloat( 3 );			// Позиция вершины
	vertexBufferLayout.PushFloat( 3 );			// Нормаль вершины
	vertexBufferLayout.PushFloat( 2 );			// Текстурная координата вершины
	vertexBufferLayout.PushFloat( 3 );			// Тангент вершины
	vertexBufferLayout.PushFloat( 3 );			// Битангент вершины

	vertexArrayObject.Bind();
	vertexArrayObject.AddBuffer( vertexBufferObject, vertexBufferLayout );
	vertexArrayObject.AddBuffer( indexBufferObject );

	vertexArrayObject.Unbind();
	vertexBufferObject.Unbind();
	indexBufferObject.Unbind();
}

//----------------------------------------------------------------------//

/*
* Отрисовать геометрию из очереди
* --------------------------------------
*/

void le::SpriteRenderer::Render( const RenderObject& RenderObject, const glm::mat4& ProjectionView, const glm::mat4& View )
{
	glm::mat4					model( 1.f );
	vertexArrayObject.Bind();

	RenderObject.shader->Bind();
	RenderObject.material->Bind( RenderObject.shader );

	switch ( static_cast<ISprite*>( RenderObject.object )->GetType() )
	{
	case ST_SPRITE_STATIC:
		model = *RenderObject.matrixTransformation;
		break;

	case ST_SPRITE_ROTATING:
	case ST_SPRITE_ROTATING_ONLY_VERTICAL:
		model = glm::translate( static_cast< ISprite* >( RenderObject.object )->GetPosition() );
		model[ 0 ].x = View[ 0 ].x;
		model[ 0 ].y = View[ 1 ].x;
		model[ 0 ].z = View[ 2 ].x;

		if ( static_cast< ISprite* >( RenderObject.object )->GetType() == ST_SPRITE_ROTATING_ONLY_VERTICAL )
		{
			model[ 1 ].x = 0;
			model[ 1 ].y = 1;
			model[ 1 ].z = 0;
		}
		else
		{
			model[ 1 ].x = View[ 0 ].y;
			model[ 1 ].y = View[ 1 ].y;
			model[ 1 ].z = View[ 2 ].y;
		}

		model[ 2 ].x = View[ 0 ].z;
		model[ 2 ].y = View[ 1 ].z;
		model[ 2 ].z = View[ 2 ].z;

		model *= glm::mat4_cast( static_cast< ISprite* >( RenderObject.object )->GetRotation() );
		model *= glm::scale( static_cast< ISprite* >( RenderObject.object )->GetScale() );
		break;
	}

	RenderObject.shader->SetUniform( "textureRect", static_cast< ISprite* >( RenderObject.object )->GetTextureRect() );
	RenderObject.shader->SetUniform( "spriteSize", static_cast< ISprite* >( RenderObject.object )->GetSize() );
	RenderObject.shader->SetUniform( "matrix_Projection", ProjectionView );
	RenderObject.shader->SetUniform( "matrix_Transformation", model );

	glDrawElements( GL_TRIANGLE_FAN, 4, GL_UNSIGNED_INT, NULL );
}

//----------------------------------------------------------------------//
