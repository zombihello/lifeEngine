//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "transform.h"

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * --------------
 */

le::Transform::Transform() :
	matrix_Position( 1.f ),
	matrix_Rotation( 1.f ),
	matrix_Scale( 1.f ),
	matrix_Transformation( 1.f ),
	position( 0.f ),
	scale( 1.f )
{}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * --------------
 */

le::Transform::~Transform()
{}

//-------------------------------------------------------------------------//


/*
 * Сместить объект
 * ------------------------
 */

void le::Transform::Move( const glm::vec3& FactorMove )
{
	glm::mat4			matrix_Temp = glm::translate( FactorMove );

	position += FactorMove;
	matrix_Position *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

//-------------------------------------------------------------------------//

/*
 * Отмасштабировать объект
 * ------------------------
 */

void le::Transform::Scale( const glm::vec3& FactorScale )
{
	glm::mat4			matrix_Temp = glm::scale( FactorScale );

	scale += FactorScale;
	matrix_Scale *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

//-------------------------------------------------------------------------//

/*
 * Повернуть объект
 * ------------------------
 */

void le::Transform::Rotate( const glm::vec3& FactorRotate )
{
	glm::vec3			axis( sin( FactorRotate.x / 2 ), sin( FactorRotate.y / 2 ), sin( FactorRotate.z / 2 ) );
	glm::vec3			rotations( cos( FactorRotate.x / 2 ), cos( FactorRotate.y / 2 ), cos( FactorRotate.z / 2 ) );

	glm::quat			rotateX( rotations.x, axis.x, 0, 0 );
	glm::quat			rotateY( rotations.y, 0, axis.y, 0 );
	glm::quat			rotateZ( rotations.z, 0, 0, axis.z );

	glm::quat			rotation = rotateX * rotateY * rotateZ;
	glm::mat4			matrix_Temp = glm::mat4_cast( rotation );

	this->rotation *= rotation;
	matrix_Rotation *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

//-------------------------------------------------------------------------//

/*
 * Повернуть объекь
 * ------------------------
 */

void le::Transform::Rotate( const glm::quat& FactorRotate )
{
	glm::mat4			matrix_Temp = glm::mat4_cast( FactorRotate );

	rotation *= FactorRotate;
	matrix_Rotation *= matrix_Temp;
	matrix_Transformation *= matrix_Temp;
}

//-------------------------------------------------------------------------//

/*
 * Задать позицию объекта
 * ------------------------
 */

void le::Transform::SetPosition( const glm::vec3& Position )
{
	position = Position;
	matrix_Position = glm::translate( Position );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

//-------------------------------------------------------------------------//

/*
 * Задать вращения объекта
 * ------------------------
 */

void le::Transform::SetRotation( const glm::vec3& Rotation )
{
	glm::vec3				axis( sin( Rotation.x / 2 ), sin( Rotation.y / 2 ), sin( Rotation.z / 2 ) );
	glm::vec3				rotations( cos( Rotation.x / 2 ), cos( Rotation.y / 2 ), cos( Rotation.z / 2 ) );

	glm::quat				rotateX( rotations.x, axis.x, 0, 0 );
	glm::quat				rotateY( rotations.y, 0, axis.y, 0 );
	glm::quat				rotateZ( rotations.z, 0, 0, axis.z );
	glm::quat				rotation = rotateX * rotateY * rotateZ;

	this->rotation = rotation;
	matrix_Rotation = glm::mat4_cast( rotation );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

//-------------------------------------------------------------------------//

/*
 * Задать вращения объекта
 * ------------------------
 */

void le::Transform::SetRotation( const glm::quat& Rotation )
{
	rotation = Rotation;
	matrix_Rotation = glm::mat4_cast( Rotation );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

//-------------------------------------------------------------------------//

/*
 * Задать масштаб объекта
 * ------------------------
 */

void le::Transform::SetScale( const glm::vec3& Scale )
{
	scale = Scale;
	matrix_Scale = glm::scale( Scale );
	matrix_Transformation = matrix_Position * matrix_Rotation * matrix_Scale;
}

//-------------------------------------------------------------------------//