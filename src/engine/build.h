//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef BUILD_H
#define BUILD_H

//----------------------------------------------------------------------//

namespace le
{
	//----------------------------------------------------------------------//

	//////////////////////////////////////////////////////////////////////////
	/// \brief Посчитать номер сборки
	///
	/// \param[in] GoldDate Золотая дата в днях (начиная с 1900 года)
	/// \return Номер сборки
	//////////////////////////////////////////////////////////////////////////
	int ComputeBuildNumber( int GoldDate );

	//----------------------------------------------------------------------//
}

//----------------------------------------------------------------------//

#endif // !BUILD_H
