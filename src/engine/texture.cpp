//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "engine/image.h"
#include "engine/icore.h"
#include "engine/iresourcesystem.h"
#include "rendersystem.h"
#include "texture.h"

//----------------------------------------------------------------------//

vector<GLuint>				le::Texture::currentTargets;
vector<GLuint>				le::Texture::currentTexturesID;

//----------------------------------------------------------------------//

/*
 * Конструктор
 * ------------------
 */

le::Texture::Texture() :
	textureID( 0 ),
	target( 0 ),
	referenceCount( 0 ),
	size( 0.f )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------
 */

le::Texture::~Texture()
{
    Unload();
}

//----------------------------------------------------------------------//

/*
 * Загрузить текстуру
 * ------------------
 */

bool le::Texture::Load( const string& Route )
{
	if ( textureID > 0 )
        Unload();

	Image		image;
	if ( !core->LoadImage( resourceSystem->GetTextureManager()->GetDirectory() + "/" + Route, image, false, true ) )
		return false;

	glGenTextures( 1, &textureID );
	
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, textureID );

	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, image.amask != 0 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, image.data );
	glGenerateMipmap( GL_TEXTURE_2D );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	glBindTexture( GL_TEXTURE_2D, 0 );

	target = GL_TEXTURE_2D;
	size = glm::vec3( image.width, image.height, 0 );

	core->UnloadImage( image );
	return true;
}

//----------------------------------------------------------------------//

/*
 * Выгрузить текстуру
 * ------------------
 */

void le::Texture::Unload()
{
	if ( textureID == 0 ) return;

	Unbind();
	glDeleteTextures( 1, &textureID );

	textureID = 0;
	target = 0;
}

//----------------------------------------------------------------------//

/*
 * Увеличить счетчик ссылок
 * ------------------
 */

void le::Texture::IncrementReferenceCount()
{
	++referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Уменьшить счетчик ссылок
 * ------------------
 */

void le::Texture::DecrementReferenceCount()
{
	--referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Получить количество ссылок
 * ------------------
 */

uint32_t le::Texture::GetReferenceCount() const
{
	return referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Получить размер текстуры
 * ------------------
 */

const glm::vec3& le::Texture::GetSize() const
{
	return size;
}
