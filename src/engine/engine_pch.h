//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef ENGINE_PCH_H
#define ENGINE_PCH_H

//---------------------------------------------------------------------//

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
using namespace rapidjson;

#include <FreeImage/FreeImage.h>

#include <SDL2/SDL.h>

#include "common/log.h"
#include "engine/lifeEngine.h"
#include "dirsinfo.h"
#include "build.h"

//---------------------------------------------------------------------//

#endif // !ENGINE_PCH_H
