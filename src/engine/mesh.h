//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//	
//////////////////////////////////////////////////////////////////////////

#ifndef MESH_H
#define MESH_H

#include <glm/glm.hpp>

#include "engine/imesh.h"
#include "vertexbufferobject.h"
#include "indexbufferobject.h"
#include "vertexarrayobject.h"

//----------------------------------------------------------------------//

namespace le
{
	//----------------------------------------------------------------------//

	class IMaterial;

	//----------------------------------------------------------------------//

	//////////////////////////////////////////////////////////////////////
	/// \brief Меш
	///
	/// Данный класс обеспечивает работу с мешем в движке
	//////////////////////////////////////////////////////////////////////
    class Mesh : public IMesh
	{
	public:     
		//////////////////////////////////////////////////////////////////////
		/// \brief Конструктор
		//////////////////////////////////////////////////////////////////////
		Mesh();

		//////////////////////////////////////////////////////////////////////
		/// \brief Деструктор
		//////////////////////////////////////////////////////////////////////
		~Mesh();

        //////////////////////////////////////////////////////////////////////
        /// \brief Загрузить меш
        ///
        /// \param[in] Route Путь к мешу
        /// \return True меш загружен, иначе false
        //////////////////////////////////////////////////////////////////////
        bool Load( const string& Route );

        //////////////////////////////////////////////////////////////////////
        /// \brief Создать меш
        ///
        /// \param[in] GeometryDescriptor Информация об меше
        /// \param[in] RenderMode Режим визуализации меша
        //////////////////////////////////////////////////////////////////////
        void Create( const GeometryDescriptor& GeometryDescriptor, RENDER_MODE RenderMode = RM_TRIANGLE );

        //////////////////////////////////////////////////////////////////////
        /// \brief Очистить меш
        //////////////////////////////////////////////////////////////////////
        void Clear();

		//////////////////////////////////////////////////////////////////////
		/// \brief Увеличить счетчик ссылок
		//////////////////////////////////////////////////////////////////////
		void IncrementReferenceCount();

		//////////////////////////////////////////////////////////////////////
		/// \brief Уменьшить счетчик ссылок
		//////////////////////////////////////////////////////////////////////
		void DecrementReferenceCount();

        //////////////////////////////////////////////////////////////////////
        /// \brief Обновить геометрию меша
        ///
        /// \param[in] Verteces Обновляемая часть вершин
        /// \param[in] Offset Смещение в буфере вершин (в байтах)
        //////////////////////////////////////////////////////////////////////
        void Update( const vector<Vertex>& Verteces, uint32_t Offset );

        //////////////////////////////////////////////////////////////////////
        /// \brief Задать режим визуализации меша
        ///
        /// \param[in] RenderMode Режим визуализации меша
        //////////////////////////////////////////////////////////////////////
        void SetRenderMode( RENDER_MODE RenderMode );

		//////////////////////////////////////////////////////////////////////
        /// \brief Получить список поверхностей
		///
        /// \return Список подмешей
		//////////////////////////////////////////////////////////////////////
        inline const list<Surface>& GetSurfaces() const
		{
            return surfaces;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить массив буферов
		///
		/// \return Массив буферов
		//////////////////////////////////////////////////////////////////////
		inline VertexArrayObject& GetVertexArrayObject()
		{
			return vertexArrayObject;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить вершиный буфер
		///
		/// \return Вершиный буфер
		//////////////////////////////////////////////////////////////////////
		inline VertexBufferObject& GetVertexBufferObject()
		{
			return vertexBufferObject;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить индексный буфер
		///
		/// \return Индексный буфер
		//////////////////////////////////////////////////////////////////////
		inline IndexBufferObject& GetIndexBufferObject()
		{
			return indexBufferObject;
		}

		//////////////////////////////////////////////////////////////////////
        /// \brief Получить флаги техник шейдера
		///
        /// \return Флаги техник шейдера
		//////////////////////////////////////////////////////////////////////
        inline uint32_t GetShaderFlags() const
		{
			return vertexShaderFlags;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить минимальные XYZ в меше
		///
		/// \return Минимальные XYZ в меше
		//////////////////////////////////////////////////////////////////////
		inline const glm::vec3& GetMinXYZ() const
		{
			return minXYZ;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить максимальные XYZ в меше
		///
		/// \return Максимальные XYZ в меше
		//////////////////////////////////////////////////////////////////////
		inline const glm::vec3& GetMaxXYZ() const
		{
			return maxXYZ;
		}

        //////////////////////////////////////////////////////////////////////
        /// \brief Создан ли меш
        ///
        /// \return True меш создан иначе false
        //////////////////////////////////////////////////////////////////////
        bool IsCreated() const;

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить массив материалов
		///
		/// \return Массив материалов
		//////////////////////////////////////////////////////////////////////
		const vector<IMaterial*>& GetMaterials() const;

        //////////////////////////////////////////////////////////////////////
        /// \brief Получить режим визуализации меша
        ///
        /// \return Режим визуализации меша
        //////////////////////////////////////////////////////////////////////
        RENDER_MODE GetRenderMode() const;

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить количество ссылок
		///
		/// \return Количество ссылок
		//////////////////////////////////////////////////////////////////////
		uint32_t GetReferenceCount() const;

	private:
        bool                    isCreated;              ///< Меш создан ли
        uint32_t                vertexShaderFlags;		///< Флаги техник которые необходимые в вершином шейдере
		uint32_t				referenceCount;			///< Количество ссылок
		RENDER_MODE             renderMode;             ///< Режим визуализации (отрисовка треугольниками, линиями, т.д)
        
		VertexArrayObject       vertexArrayObject;		///< Массив буферов
        VertexBufferObject      vertexBufferObject;		///< Вершиный буфер
        IndexBufferObject       indexBufferObject;		///< Индексный буфер
		
		glm::vec3				minXYZ;					///< Минимальные XYZ в меше
		glm::vec3				maxXYZ;					///< Максимальные XYZ в меше

		vector<IMaterial*>		materials;				///< Массив материалов
        list<Surface>           surfaces;				///< Список поверхностей
	};

	//----------------------------------------------------------------------//
}

//----------------------------------------------------------------------//

#endif // !MESH_H
