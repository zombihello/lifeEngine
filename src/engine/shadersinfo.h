//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef SHADERS_INFO_H
#define SHADERS_INFO_H

//---------------------------------------------------------------------//

#define LENGINE_SHADER_MATERIAL				"material"
#define LENGINE_SHADER_TEXT					"text"
#define LENGINE_SHADER_SPRITE				"sprite"

//---------------------------------------------------------------------//

#endif // !SHADERS_INFO_H
