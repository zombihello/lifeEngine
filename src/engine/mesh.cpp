//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <fstream>
using namespace std;

#include "engine/iresourcesystem.h"
#include "rendersystem.h"
#include "material.h"
#include "mesh.h"

#define ID_FORMAT					"LMD"
#define VERSION_FORMAT				( unsigned short ) 2

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ------------------
 */

le::Mesh::Mesh() :
	vertexShaderFlags( SF_NONE ),
	renderMode( RM_TRIANGLE ),
	isCreated( false ),
	referenceCount( 0 )
{}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------
 */

le::Mesh::~Mesh()
{
	Clear();
}

//-------------------------------------------------------------------------//

/*
 * Загрузить меш
 * ------------------
 */

bool le::Mesh::Load( const string& Route )
{
	if ( isCreated )
		Clear();

	ifstream					fileModel( resourceSystem->GetMeshManager()->GetDirectory() + "/" + Route, ios::binary );

	if ( !fileModel.is_open() )
	{
		LOG_ERROR( "File [" << Route << "] not found" );
		return false;
	}

	// Читаем заголовок файла
	// **************************

	char							strId[ 3 ];
	unsigned short					version = 0;

	fileModel.read( strId, 3 );
	fileModel.read( ( char* ) &version, sizeof( unsigned short ) );

	if ( string( strId, 3 ) != string( ID_FORMAT, 3 ) || version != VERSION_FORMAT )
	{
		LOG_ERROR( "Not support format LMD [ID: " << strId << " Version: " << version << "]" );
		return false;
	}

	// Читаем все материалы модели
	// **************************

	uint32_t					sizeString = 0;
	uint32_t					sizeArrayMaterials = 0;
	string*						routeMaterial = nullptr;
	vector<string>				arrayRouteMaterials;

	fileModel.read( ( char* ) &sizeArrayMaterials, sizeof( uint32_t ) );
	arrayRouteMaterials.resize( sizeArrayMaterials );

	for ( size_t index = 0; index < sizeArrayMaterials; ++index )
	{
		routeMaterial = &arrayRouteMaterials[ index ];

		fileModel.read( ( char* ) &sizeString, sizeof( uint32_t ) );
		routeMaterial->resize( sizeString );

		fileModel.read( &( *routeMaterial )[ 0 ], sizeString );
	}

	// Загружаем материалы
	// **************************

	for ( uint32_t index = 0; index < sizeArrayMaterials; ++index )
	{
		Material*			material = static_cast< Material* >( resourceSystem->GetMaterialManager()->Load( arrayRouteMaterials[ index ] ) );
		if ( !material )	continue;

		material->IncrementReferenceCount();
		materials.push_back( material );
	}

	// Читаем все вершины модели
	// **************************

	uint32_t					sizeArrayVertexes;
	fileModel.read( ( char* ) &sizeArrayVertexes, sizeof( uint32_t ) );

	vector<Vertex>			arrayVertexes( sizeArrayVertexes );

	if ( sizeArrayVertexes > 0 )
		fileModel.read( ( char* ) &arrayVertexes[ 0 ], sizeArrayVertexes * sizeof( Vertex ) );
	else
	{
		LOG_ERROR( "In model not exists segment with vertex array" );
		return false;
	}

	// Находим минимальные и максимальные XYZ в меше	
	// TODO: Перенести minXYZ и maxXYZ в сам формат модели
	// ***********************************************

	minXYZ = maxXYZ = arrayVertexes[ 0 ].position;

	for ( uint32_t index = 0; index < sizeArrayVertexes; ++index )
	{
		minXYZ.x = glm::min( minXYZ.x, arrayVertexes[ index ].position.x );
		minXYZ.y = glm::min( minXYZ.y, arrayVertexes[ index ].position.y );
		minXYZ.z = glm::min( minXYZ.z, arrayVertexes[ index ].position.z );

		maxXYZ.x = glm::max( maxXYZ.x, arrayVertexes[ index ].position.x );
		maxXYZ.y = glm::max( maxXYZ.y, arrayVertexes[ index ].position.y );
		maxXYZ.z = glm::max( maxXYZ.z, arrayVertexes[ index ].position.z );
	}

	// Читаем все индексы модели
	// **************************

	uint32_t						sizeArrayIndices;
	fileModel.read( ( char* ) &sizeArrayIndices, sizeof( uint32_t ) );

	vector<unsigned int>		arrayIndices( sizeArrayIndices );

	if ( sizeArrayIndices > 0 )
		fileModel.read( ( char* ) &arrayIndices[ 0 ], sizeArrayIndices * sizeof( uint32_t ) );
	else
	{
		LOG_ERROR( "In model not exists segment with indices array" );
		return false;
	}

	// Читаем все сетки модели
	// **************************

	uint32_t						sizeArrayMeshes;
	fileModel.read( ( char* ) &sizeArrayMeshes, sizeof( uint32_t ) );

	if ( sizeArrayMeshes == 0 )
	{
		LOG_ERROR( "In model not exists segment with surfaces array" );
		return false;
	}

	Surface               surface;

	for ( size_t index = 0; index < sizeArrayMeshes; ++index )
	{
		fileModel.read( ( char* ) &surface.materialId, sizeof( uint32_t ) );
		fileModel.read( ( char* ) &surface.startVertexIndex, sizeof( uint32_t ) );
		fileModel.read( ( char* ) &surface.countVertexIndex, sizeof( uint32_t ) );
		surfaces.push_back( surface );
	}

	if ( surfaces.empty() )
	{
		LOG_ERROR( "Fail loaded surfaces array" );
		return false;
	}

	// Загружаем информацию о меше в GPU
	// **************************

	vertexArrayObject.Create();
	vertexBufferObject.Create();
	indexBufferObject.Create();

	vertexBufferObject.Bind();
	vertexBufferObject.Allocate( arrayVertexes.data(), arrayVertexes.size() * sizeof( Vertex ) );

	indexBufferObject.Bind();
	indexBufferObject.Allocate( arrayIndices.data(), arrayIndices.size() * sizeof( uint32_t ) );

	VertexBufferLayout				vertexBufferLayout;
	vertexBufferLayout.PushFloat( 3 );			// Позиция вершины
	vertexBufferLayout.PushFloat( 3 );			// Нормаль вершины
	vertexBufferLayout.PushFloat( 2 );			// Текстурная координата вершины
	vertexBufferLayout.PushFloat( 3 );			// Тангент вершины
	vertexBufferLayout.PushFloat( 3 );			// Битангент вершины

	vertexArrayObject.Bind();
	vertexArrayObject.AddBuffer( vertexBufferObject, vertexBufferLayout );
	vertexArrayObject.AddBuffer( indexBufferObject );

	vertexArrayObject.Unbind();
	vertexBufferObject.Unbind();
	indexBufferObject.Unbind();

	isCreated = true;
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Создать меш
 * ----------------
 */

void le::Mesh::Create( const GeometryDescriptor& GeometryDescriptor, RENDER_MODE RenderMode )
{
	if ( isCreated )
		Clear();

	// Запоминаем материалы
	// **************************

	for ( uint32_t index = 0, count = GeometryDescriptor.materials.size(); index < count; ++index )
	{
		IMaterial*			material = GeometryDescriptor.materials[ index ];
		LIFEENGINE_ASSERT( material, "Material is null" );

		material->IncrementReferenceCount();
		materials.push_back( material );
	}

	// Запоминаем поверхности
	// **************************

	for ( uint32_t index = 0, count = GeometryDescriptor.surfaces.size(); index < count; ++index )
		surfaces.push_back( GeometryDescriptor.surfaces[ index ] );

	// Находим минимальные и максимальные XYZ в меше	
	// ***********************************************

	if ( !GeometryDescriptor.verteces.empty() )
	{
		minXYZ = maxXYZ = GeometryDescriptor.verteces[ 0 ].position;

		for ( uint32_t index = 0, count = GeometryDescriptor.verteces.size(); index < count; ++index )
		{
			minXYZ.x = glm::min( minXYZ.x, GeometryDescriptor.verteces[ index ].position.x );
			minXYZ.y = glm::min( minXYZ.y, GeometryDescriptor.verteces[ index ].position.y );
			minXYZ.z = glm::min( minXYZ.z, GeometryDescriptor.verteces[ index ].position.z );

			maxXYZ.x = glm::max( maxXYZ.x, GeometryDescriptor.verteces[ index ].position.x );
			maxXYZ.y = glm::max( maxXYZ.y, GeometryDescriptor.verteces[ index ].position.y );
			maxXYZ.z = glm::max( maxXYZ.z, GeometryDescriptor.verteces[ index ].position.z );
		}
	}

	// Загружаем информацию о меше в GPU
	// **************************

	vertexArrayObject.Create();
	vertexBufferObject.Create();
	indexBufferObject.Create();

	vertexBufferObject.Bind();
	vertexBufferObject.Allocate( GeometryDescriptor.verteces.data(), GeometryDescriptor.verteces.size() * sizeof( Vertex ) );

	indexBufferObject.Bind();
	indexBufferObject.Allocate( GeometryDescriptor.indeces.data(), GeometryDescriptor.indeces.size() * sizeof( unsigned int ) );

	VertexBufferLayout				vertexBufferLayout;
	vertexBufferLayout.PushFloat( 3 );			// Позиция вершины
	vertexBufferLayout.PushFloat( 3 );			// Нормаль вершины
	vertexBufferLayout.PushFloat( 2 );			// Текстурная координата вершины
	vertexBufferLayout.PushFloat( 3 );			// Тангент вершины
	vertexBufferLayout.PushFloat( 3 );			// Битангент вершины

	vertexArrayObject.Bind();
	vertexArrayObject.AddBuffer( vertexBufferObject, vertexBufferLayout );
	vertexArrayObject.AddBuffer( indexBufferObject );

	vertexArrayObject.Unbind();
	vertexBufferObject.Unbind();
	indexBufferObject.Unbind();

	renderMode = RenderMode;
	isCreated = true;
}

//-------------------------------------------------------------------------//

/*
 * Очистить меш
 * ----------------
 */

void le::Mesh::Clear()
{
	isCreated = false;
	vertexShaderFlags = SF_NONE;
	renderMode = RM_TRIANGLE;

	for ( uint32_t index = 0, count = materials.size(); index < count; ++index )
	{
		Material*		material = static_cast< Material* >( materials[ index ] );
		material->DecrementReferenceCount();

		if ( material->GetReferenceCount() <= 0 )
			resourceSystem->GetMaterialManager()->Delete( material );
	}

	vertexArrayObject.Delete();
	vertexBufferObject.Delete();
	indexBufferObject.Delete();
	materials.clear();
	surfaces.clear();
}

//-------------------------------------------------------------------------//

/*
 * Обновить геометрию меша
 * ----------------
 */

void le::Mesh::Update( const vector<Vertex>& Verteces, uint32_t Offset )
{
	if ( !isCreated || !vertexBufferObject.IsCreate() ) return;

	vertexBufferObject.Bind();
	vertexBufferObject.Update( Verteces.data(), Verteces.size(), Offset );
	vertexBufferObject.Unbind();
}

//-------------------------------------------------------------------------//

/*
 * Задать режим визуализации меша
 * ----------------
 */

void le::Mesh::SetRenderMode( RENDER_MODE RenderMode )
{
	renderMode = RenderMode;
}

//-------------------------------------------------------------------------//

/*
 * Получить режим визуализации меша
 * ----------------
 */

le::IMesh::RENDER_MODE le::Mesh::GetRenderMode() const
{
	return renderMode;
}

//-------------------------------------------------------------------------//

/*
 * Создан ли меш
 * ----------------
 */

bool le::Mesh::IsCreated() const
{
	return isCreated;
}

//-------------------------------------------------------------------------//

/*
 * Получить массив материалов
 * ------------------
 */

const vector<le::IMaterial*>& le::Mesh::GetMaterials() const
{
	return materials;
}

//-------------------------------------------------------------------------//

/*
 * Увеличить счетчик ссылок
 * ------------------
 */

void le::Mesh::IncrementReferenceCount()
{
	++referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Уменьшить счетчик ссылок
 * ------------------
 */

void le::Mesh::DecrementReferenceCount()
{
	--referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Получить количество ссылок
 * ------------------
 */

uint32_t le::Mesh::GetReferenceCount() const
{
	return referenceCount;
}

//----------------------------------------------------------------------//
