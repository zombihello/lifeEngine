//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef CORE_H
#define CORE_H

#include <unordered_map>
using namespace std;

#include "common/configurations.h"
#include "common/gamemanifest.h"
#include "engine/icore.h"
#include "objectfactory.h"
#include "consolesystem.h"

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	class IGame;
	class Window;

	//---------------------------------------------------------------------//

	///////////////////////////////////////////////////////////////////////////
	/// \brief Заголовок подсистемы движка
	////////////////////////////////////////////////////////////////////////////
	struct EngineSubsystem
	{
		//////////////////////////////////////////////////////////////////////////
		/// \brief Конструктор
		///
		/// \param[in] EngineSubsystem Указатель на подсистему движка
		/// \param[in] HandleLib Заголовок загруженной динамической библиотеки
		//////////////////////////////////////////////////////////////////////////
		EngineSubsystem( IEngineSubsystem* EngineSubsystem = nullptr, void* HandleLib = nullptr ) :
			engineSubsystem( EngineSubsystem ),
			handleLib( HandleLib )
		{}

		IEngineSubsystem*			engineSubsystem;		///< Указатель на подсистему движка
		void*						handleLib;				///< Заголовок загруженной динамической библиотеки
	};

	//---------------------------------------------------------------------//

	///////////////////////////////////////////////////////////////////////////
	/// \brief Ядро движка
	///
	/// Ядро движка содержит в себе подсистемы и отвечает за их взаимодействие 
	/// между собой и осуществляет главный цикл игры
	////////////////////////////////////////////////////////////////////////////
	class Core : public ICore
	{
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Конструктор
		//////////////////////////////////////////////////////////////////////////
		Core();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Деструктор
		//////////////////////////////////////////////////////////////////////////
		~Core();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Загрузить информацию об игре
		///
		/// \param[in] GameDir Путь к каталогу игры
		/// \return True - удалось загрузить манифест игры, иначе false
		//////////////////////////////////////////////////////////////////////////
		bool LoadGameManifest( const string& GameDir );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Загрузить конфигурации движка
		///
		/// \param[in] Route Путь к файлу конфигураций движка
		/// \return True - удалось загрузить конфигурации, иначе false
		//////////////////////////////////////////////////////////////////////////
		bool LoadConfig( const string& Route );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Сохранить конфигурации движка
		///
		/// \param[in] Configurations Конфигурации движка
		//////////////////////////////////////////////////////////////////////////
		void SaveConfig( const string& Route );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Загрузить изображение
		///
		/// \param[in] Route Путь к изображению
		/// \param[out] Image Изображение
		/// \param[in] IsFlipVertical Перевернуть ли изображение по вертикали
		/// \param[in] IsSwitchRedAndBlueChannels Поменять ли R и B каналы местами
		/// \return True - удалось загрузить изображение, иначе false
		//////////////////////////////////////////////////////////////////////////
		bool LoadImage( const string& Route, Image& Image, bool IsFlipVertical = false, bool IsSwitchRedAndBlueChannels = false );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Выгрузить изображение
		///
		/// \param[in] Image Изображение
		//////////////////////////////////////////////////////////////////////////
		void UnloadImage( const Image& Image );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Загрузить логику игры
		/// \warning Данный метод необходимо вызывать после инициализации движка
		/// и загрузки манифеста игры
		///
		/// \return True логика игры успешно загружена, иначе false
		//////////////////////////////////////////////////////////////////////////
		bool LoadGame();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Выгрузить логику игры		
		//////////////////////////////////////////////////////////////////////////
		void UnloadGame();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Инициализировать движок
		///
		/// \param[in] WindowHandle Заголовок окна, если он nullptr то ядро создаст свое окно
		/// \return True инициализация движка прошла успешно, иначе false
		//////////////////////////////////////////////////////////////////////////
		bool InitializeEngine( WindowHandle_t WindowHandle = nullptr );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Запустить симуляцию игры
		//////////////////////////////////////////////////////////////////////////
		void RunSimulation();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Остановить симуляцию игры
		//////////////////////////////////////////////////////////////////////////
		void StopSimulation();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Задать конфигурации движка
		///
		/// \param[in] Configurations Конфигурации движка
		//////////////////////////////////////////////////////////////////////////
		void SetConfigurations( const Configurations& Configurations );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Задать информацию об игре
		///
		/// \param[in] GameManifest Информация об игре
		//////////////////////////////////////////////////////////////////////////
		void SetGameManifest( const GameManifest& GameManifest );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Задать каталог движка
		///
		/// \param[in] EngineDir Каталог с ресурсами движка
		//////////////////////////////////////////////////////////////////////////
		void SetEngineDir( const string& EngineDir );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Получить подсистему движка
		///
		/// \param[in] EngineSubsystemType Тип подсистемы движка
		/// \return Указатель на подсистему движка, если в ядре ее нет - nullptr
		//////////////////////////////////////////////////////////////////////////
		IEngineSubsystem* GetSubsystem( ENGINE_SUBSYSTEM_TYPE EngineSubsystemType ) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Получить фабрику объектов движка
		///
		/// \return Указатель на фабрику объектов движка
		//////////////////////////////////////////////////////////////////////////
		IFactory* GetFactory() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Получить окно приложения
		///
		/// \return Указатель на окно приложения
		//////////////////////////////////////////////////////////////////////////
		IWindow* GetWindow() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Получить конфигурации движка
		///
		/// \return Конфигурации движка
		//////////////////////////////////////////////////////////////////////////
		const Configurations& GetConfigurations() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Получить информацию об игре
		///
		/// \return Информация об игре
		//////////////////////////////////////////////////////////////////////////
		const GameManifest& GetGameManifest() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Получить каталог движка
		///
		/// \return Каталог с ресурсами движка
		//////////////////////////////////////////////////////////////////////////
		const string& GetEngineDir() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Получить версию lifeEngine
		///
		/// \return Версия lifeEngine
		//////////////////////////////////////////////////////////////////////////
		Version GetVersion() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Запущена ли симуляция игры
		///
		/// \return True - запущена, иначе false
		//////////////////////////////////////////////////////////////////////////
		bool IsRunSimulation() const;

	private:		
		//////////////////////////////////////////////////////////////////////////
		/// \brief Консольная команда "Выход"
		///
		/// \param[in] Arguments Аргементы команды
		//////////////////////////////////////////////////////////////////////////
		void ConsoleCommand_Exit( const vector<string>& Arguments );

		//////////////////////////////////////////////////////////////////////////
		/// \brief Консольная команда "Версия движка"
		///
		/// \param[in] Arguments Аргементы команды
		//////////////////////////////////////////////////////////////////////////
		void ConsoleCommand_Version( const vector<string>& Arguments );
		
		//////////////////////////////////////////////////////////////////////////
		/// \brief Инициализировать подсистему движка
		///
		/// \param[in] EngineSubsystemType Тип подсистемы движка
		/// \return True подсистема удачно инициализирована, иначе false
		//////////////////////////////////////////////////////////////////////////
		bool InitSubsystem( ENGINE_SUBSYSTEM_TYPE EngineSubsystemType );

		bool														isInitEngine;			///< Инициализирован ли движок
		bool														isRunSimulation;		///< Запущена ли симуляция игры

		string														engineDir;				///< Каталог движка
		Configurations												configurations;			///< Конфигурации движка
		GameManifest												gameManifest;			///< Информация об игре
		ObjectFactory												objectFactory;			///< Фабрика объектов движка
		Window*														window;					///< Окно приложения

		void*														handleGameLib;			///< Заголовок загруженной динамической библиотеки с логикой игры
		IGame*														game;					///< Логика игры		

		unordered_map<ENGINE_SUBSYSTEM_TYPE, EngineSubsystem>		engineSubsystems;		///< Подсистемы движка
	};

	//---------------------------------------------------------------------//

	extern IFactory*		factory;				///< Указатель на фабрику объектов движка
}

//---------------------------------------------------------------------//

#endif // !ENGINE_H
