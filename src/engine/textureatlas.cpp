//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "engine/lifeEngine.h"
#include "textureatlas.h"

//----------------------------------------------------------------------//

/*
 * Конструктор
 * ------------------
 */

le::TextureAtlas::TextureAtlas() :
	textureID( 0 ),
	format( 0 )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------
 */

le::TextureAtlas::~TextureAtlas()
{
	Delete();
}

//----------------------------------------------------------------------//

/*
 * Очистить текстурный атлас
 * ------------------
 */

void le::TextureAtlas::Clear()
{
	if ( textureID == 0 ) return;

	glBindTexture( GL_TEXTURE_2D, textureID );
	glTexImage2D( GL_TEXTURE_2D, 0, format, size.x, size.y, 0, format, GL_UNSIGNED_BYTE, nullptr );
	glBindTexture( GL_TEXTURE_2D, 0 );
}

//----------------------------------------------------------------------//

/*
 * Удалить текстурный атлас
 * ------------------
 */

void le::TextureAtlas::Delete()
{
	if ( textureID == 0 ) return;

	glDeleteTextures( 1, &textureID );

	size = glm::ivec2( 0 );
	textureID = 0;
	format = 0;
}

//----------------------------------------------------------------------//

/*
 * Изменить размер текстурного атласа
 * ------------------
 */

void le::TextureAtlas::Resize( uint32_t NewWidth, uint32_t NewHeight, bool IsSavePixels )
{
	if ( textureID == 0 ) return;

	// Если нам не нужно сохранять данные - просто пересоздаем текстуру
	if ( !IsSavePixels )
	{
		glBindTexture( GL_TEXTURE_2D, textureID );
		glTexImage2D( GL_TEXTURE_2D, 0, format, NewWidth, NewHeight, 0, format, GL_UNSIGNED_BYTE, nullptr );
		glBindTexture( GL_TEXTURE_2D, 0 );

		size = glm::ivec2( NewWidth, NewHeight );
		return;
	}

	// Сохраняем текущии биндинги буферов кадров, чтоб потом востановить
	GLint		readFramebuffer = 0;
	GLint		drawFramebuffer = 0;

	glGetIntegerv( GL_READ_FRAMEBUFFER_BINDING, &readFramebuffer );
	glGetIntegerv( GL_DRAW_FRAMEBUFFER_BINDING, &drawFramebuffer );

	// Создаем буферы кадров
	GLuint		sourceFramebuffer = 0;
	GLuint		destFramebuffer = 0;

	glGenFramebuffers( 1, &sourceFramebuffer );
	glGenFramebuffers( 1, &destFramebuffer );

	if ( !sourceFramebuffer || !destFramebuffer )
	{
		LOG_ERROR( "Cannot copy texture, failed to create a frame buffer object" );
		return;
	}

	// Запоминаем идентификатор старой текстуры и создаем новую
	GLuint			oldTexture = textureID;

	glGenTextures( 1, &textureID );
	glBindTexture( GL_TEXTURE_2D, textureID );
	glTexImage2D( GL_TEXTURE_2D, 0, format, NewWidth, NewHeight, 0, format, GL_UNSIGNED_BYTE, nullptr );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glBindTexture( GL_TEXTURE_2D, 0 );

	// Соединяем исходную текстуру с исходным буфером кадра
	glBindFramebuffer( GL_READ_FRAMEBUFFER, sourceFramebuffer );
	glFramebufferTexture2D( GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, oldTexture, 0 );

	// Соединяем текстуру назначения с буфером кадра назначения
	glBindFramebuffer( GL_DRAW_FRAMEBUFFER, destFramebuffer );
	glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureID, 0 );

	// Финальная проверка на корректность создания буферов
	GLenum			sourceStatus;
	sourceStatus = glCheckFramebufferStatus( GL_READ_FRAMEBUFFER );

	GLenum			destStatus;
	destStatus = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );

	if ( sourceStatus == GL_FRAMEBUFFER_COMPLETE && destStatus == GL_FRAMEBUFFER_COMPLETE )
	{
		// Рисует содержимое текстуры из источника в место назначения текстуры
		glBlitFramebuffer(
			0, 0, size.x, size.y,		// Исходный прямоугольник
			0, 0, size.x, size.y,		// Прямоугольник назначения
			GL_COLOR_BUFFER_BIT, GL_NEAREST
		);
	}
	else
		LOG_ERROR( "Cannot copy texture, failed to link texture to frame buffer" );

	// Востанавливаем буфера
	glBindFramebuffer( GL_READ_FRAMEBUFFER, readFramebuffer );
	glBindFramebuffer( GL_DRAW_FRAMEBUFFER, drawFramebuffer );

	// Удаляем буфера которые были созданы и старую текстуру
	glDeleteFramebuffers( 1, &sourceFramebuffer );
	glDeleteFramebuffers( 1, &destFramebuffer );
	glDeleteTextures( 1, &oldTexture );

	size = glm::ivec2( NewWidth, NewHeight );
}

//----------------------------------------------------------------------//

/*
 * Создать текстурный атлас
 * ------------------
 */

bool le::TextureAtlas::Create( uint32_t Width, uint32_t Height, GLint Format )
{
	// Если ранее текстура не была создана - генерируем текстурный ID
	if ( !textureID )
		glGenTextures( 1, &textureID );
	
	glBindTexture( GL_TEXTURE_2D, textureID );
	glTexImage2D( GL_TEXTURE_2D, 0, Format, Width, Height, 0, Format, GL_UNSIGNED_BYTE, nullptr );
	
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glBindTexture( GL_TEXTURE_2D, 0 );

	size = glm::ivec2( Width, Height );
	format = Format;
	return true;
}

//----------------------------------------------------------------------//

/*
 * Обновить данные в текстурном атласе
 * ------------------
 */

void le::TextureAtlas::Update( uint8_t* PixelData, uint32_t X, uint32_t Y, uint32_t Width, uint32_t Height )
{
	LIFEENGINE_ASSERT( X + Width <= size.x, "Width updateing rectangle big (" << X << "+" << Width << " > " << size.x << ")" );
	LIFEENGINE_ASSERT( Y + Height <= size.y, "Height updateing rectangle big (" << Y << "+" << Height << " > " << size.y << ")" );

	// Если текстура не создана - выходим
	if ( textureID == 0 ) return;

	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
	glBindTexture( GL_TEXTURE_2D, textureID );

	glTexSubImage2D( GL_TEXTURE_2D, 0, X, Y, Width, Height, format, GL_UNSIGNED_BYTE, PixelData );

	glBindTexture( GL_TEXTURE_2D, 0 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );
}

//----------------------------------------------------------------------//
