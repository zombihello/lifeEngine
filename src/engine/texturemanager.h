//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <string>
#include <memory>
#include <unordered_map>
using namespace std;

#include "engine/iresourcemanager.h"
#include "engine/itexture.h"

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	//////////////////////////////////////////////////////////////////////
	/// \brief Менеджер текстур
	//////////////////////////////////////////////////////////////////////
	class TextureManager : public IResourceManager<ITexture>
	{
	public:
		//////////////////////////////////////////////////////////////////////
		/// \brief Конструктор
		//////////////////////////////////////////////////////////////////////
		TextureManager();

		//////////////////////////////////////////////////////////////////////
		/// \brief Деструктор
		//////////////////////////////////////////////////////////////////////
		~TextureManager();

		//////////////////////////////////////////////////////////////////////
		/// \brief Создать текстуру
		///
		/// \param[in] Name Название текстуры с которым она будет ассоциирована
		/// \return Указатель на текстуру, если название такое имеется - nullptr
		//////////////////////////////////////////////////////////////////////
		ITexture* Create( const string& Name );

        //////////////////////////////////////////////////////////////////////
        /// \brief Загрузить текстуру
        ///
        /// \param[in] Route Путь к текстуре
        /// \param[in] Name Название текстуры с которым она будет ассоциирована. По-умолчанию Name = Route
        /// \return Указатель на текстуру, если текстура с таким названием
        /// уже создана, то вернет указатель на нее, а при неудачной загрузке - nullptr
        //////////////////////////////////////////////////////////////////////
		ITexture* Load( const string& Route, const string& Name = "" );

        //////////////////////////////////////////////////////////////////////
        /// \brief Удалить текстуру
        ///
        /// \param[in] Name Название текстуры с которым она ассоциирована
        //////////////////////////////////////////////////////////////////////
        void Delete( const string& Name );

        //////////////////////////////////////////////////////////////////////
        /// \brief Удалить текстуру
        ///
        /// \param[in] Texture Указатель на текстуру
        //////////////////////////////////////////////////////////////////////
        void Delete( ITexture* Texture );

		//////////////////////////////////////////////////////////////////////
		/// \brief Удалить все ресурсы
		//////////////////////////////////////////////////////////////////////
		void DeleteAll();

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать каталог с текстурами
		///
		/// \param[in] Directory Путь к каталогу с текстурами
		//////////////////////////////////////////////////////////////////////
		void SetDirectory( const string& Directory );

        //////////////////////////////////////////////////////////////////////
        /// \brief Получить каталог с текстурами
        ///
        /// \return Путь к каталогу с текстурами
        //////////////////////////////////////////////////////////////////////
		const string& GetDirectory() const;

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить текстуру
		///
		/// \param[in] Name Название текстуры
		/// \return Указатель на текстуру, если произошла ошибка - nullptr
		//////////////////////////////////////////////////////////////////////
		ITexture* Get( const string& Name );

	private:
        string									directory;		///< Каталог с текстурами
        unordered_map<string, ITexture*>		textures;		///< Массив текстур
	};

	//---------------------------------------------------------------------//
}

//---------------------------------------------------------------------//


#endif // !TEXTURE_MANAGER_H
