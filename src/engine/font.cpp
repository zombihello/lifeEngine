//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "engine/iresourcesystem.h"
#include "rendersystem.h"
#include "font.h"

#define DEFAULT_SIZE_TEXTURE_ATLAS		512

#include "ft2build.h"
#include FT_FREETYPE_H
#include FT_GLYPH_H

//----------------------------------------------------------------------//

/*
 * Конструктор
 * ------------------
 */

le::Font::Font() :
	ftLibrary( nullptr ),
	ftFace( nullptr ),
	isLoaded( false ),
	referenceCount( 0 )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------
 */

le::Font::~Font()
{
    Unload();
}

//----------------------------------------------------------------------//

/*
 * Загрузить шрифт
 * ------------------
 */

bool le::Font::Load( const string& Route )
{
	// Если шрифт ранее был загружен - удаляем
	if ( ftFace )
        Unload();

	// Инициазируем FreeType
	FT_Library			ftLibrary;
	if ( FT_Init_FreeType( &ftLibrary ) )
	{
		LOG_ERROR( "Could not init FreeType Library" );
		return false;
	}
	this->ftLibrary = ftLibrary;

	// Загружаем шрифт
	FT_Face				ftFace;
    if ( FT_New_Face( static_cast< FT_Library >( ftLibrary ), ( resourceSystem->GetFontManager()->GetDirectory() + "/" + Route ).c_str(), 0, &ftFace ) )
	{
		LOG_ERROR( "Failed to load font" );
		return false;
	}

	// Выбераем карту символов Юникода
	if ( FT_Select_Charmap( ftFace, FT_ENCODING_UNICODE ) )
	{
		LOG_ERROR( "Failed to set the Unicode charcter set" );
		FT_Done_Face( ftFace );
		return false;
	}

	// Запоминаем новый указатель на шрифт и его название
	this->ftFace = ftFace;
	familyName = ftFace->family_name ? ftFace->family_name : "";
	isLoaded = true;

	return true;
}

//----------------------------------------------------------------------//

/*
 * Увеличить счетчик ссылок
 * ------------------
 */

void le::Font::IncrementReferenceCount()
{
	++referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Уменьшить счетчик ссылок
 * ------------------
 */

void le::Font::DecrementReferenceCount()
{
	--referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Выгрузить шрифт
 * ------------------
 */

void le::Font::Unload()
{
	if ( ftFace )		FT_Done_Face( static_cast<FT_Face>( ftFace ) );
	if ( ftLibrary )	FT_Done_FreeType( static_cast<FT_Library>( ftLibrary ) );

	isLoaded = false;
	ftFace = nullptr;
	ftLibrary = nullptr;
	familyName.clear();
	pages.clear();
}

//----------------------------------------------------------------------//

/*
 * Получить глиф шрифта
 * ------------------
 */

const le::Font::Glyph& le::Font::GetGlyph( uint32_t CodePoint, uint32_t CharacterSize )
{
	// Получаем страницу которая соответствует заданому размеру
	unordered_map<uint32_t, Glyph>&				glyphs = pages[ CharacterSize ].glyphs;

	// Получаем индекс глифа на основе Юникод-символе
	uint32_t		key = FT_Get_Char_Index( static_cast< FT_Face >( ftFace ), CodePoint );

	// Ищем глиф в кэше
	auto		itGlyph = glyphs.find( key );
	if ( itGlyph != glyphs.end() )
		return itGlyph->second;

	// Если не нашли, то загружаем из FreeType
	Glyph		newGlyph = LoadGlyph( CodePoint, CharacterSize );
	return glyphs.insert( make_pair( key, newGlyph ) ).first->second;
}

//----------------------------------------------------------------------//

/*
 * Загрузить глиф и сохранить его в кэш
 * ------------------
 */

le::Font::Glyph le::Font::LoadGlyph( uint32_t CodePoint, uint32_t CharacterSize )
{
	Glyph			glyph;

	SetCurrentSize( CharacterSize );

	// Загружаем глиф символа
	if ( FT_Load_Char( static_cast< FT_Face >( ftFace ), CodePoint, FT_LOAD_RENDER ) )
	{
		LOG_ERROR( "Failed to load glyph for char with code [" << CodePoint << "]" );
		return glyph;
	}

	// Запоменаем смещение до следующего глифа
	glyph.advance = static_cast< float >( static_cast< FT_Face >( ftFace )->glyph->advance.x >> 6 );

	// Получаем размер глифа
	int				width = static_cast< FT_Face >( ftFace )->glyph->bitmap.width;
	int				height = static_cast< FT_Face >( ftFace )->glyph->bitmap.rows;

	if ( width <= 0 || height <= 0 ) return glyph;

	// Оставляем небольшой отсуп вокруг символов, чтоб фильтрация не
	// загрезняла их соседними пикселями
	const unsigned int			padding = 1;
	
	width	+= 2 * padding;
	height	+= 2 * padding;

	// Получаем страницу шрифта для нужного размера символов
	Page&			page = pages[ CharacterSize ];
	
	// Если текстура в странице пустая - создаем текстуру с минимальным размером
	if ( !page.textureAtlas->IsCreated() )
		page.textureAtlas->Create( 1, 1, GL_RED );

	// Находим позицию куда поместить новый глиф
	glyph.textureRect = FindGlyphRect( page, width, height );

	glyph.textureRect.left		+= padding;
	glyph.textureRect.top		+= padding;
	glyph.textureRect.width		-= 2 * padding;
	glyph.textureRect.height	-= 2 * padding;

	// Вычислить ограничительную рамку глифа
	glyph.bounds.left = static_cast< float >( static_cast< FT_Face >( ftFace )->glyph->bitmap_left );
	glyph.bounds.top = static_cast< float >( static_cast< FT_Face >( ftFace )->glyph->bitmap_top );
	glyph.bounds.width = static_cast< float >( static_cast< FT_Face >( ftFace )->glyph->bitmap.width );
	glyph.bounds.height = static_cast< float >( static_cast< FT_Face >( ftFace )->glyph->bitmap.rows );

	// Записываем глиф в текстурный атлас
	page.textureAtlas->Update(
		&static_cast< FT_Face >( ftFace )->glyph->bitmap.buffer[ 0 ],
		glyph.textureRect.left,
		glyph.textureRect.top,
		width - 2 * padding,
		height - 2 * padding
	);

	return glyph;

	// ****************************
	// Тут покоется самооценка Егора (zombiHello), который пытался написать рендер текста
	// и приуныл из-за груды кода и двух часов кодинга без компиляции :)
	//
	// Помним, любим, скорбим...
	// ******************************
}

//----------------------------------------------------------------------//

/*
 * Найти место для глифа
 * ------------------
 */

le::IntRect le::Font::FindGlyphRect( Page& Page, uint32_t Width, uint32_t Height )
{
	// Ищем линию, которая хорошо вписывается в глиф
	Row*		row = nullptr;
	float		bestRation = 0;

	for ( auto it = Page.rows.begin(), itEnd = Page.rows.end(); it != itEnd && !row; ++it )
	{
		float		ratio = static_cast< float >( Height ) / it->height;

		// Игнорируем строки, которые слишком малы или слишком высоки
		if ( ( ratio < 0.7f ) || ( ratio > 1.f ) )
			continue;

		// Проверяем, достаточно ли горизонтального пространства осталось в строке
		if ( Width > Page.textureAtlas->GetSize().x - it->width )
			continue;

		// Убедимся, что эта новая строка является лучшим найденным до сих пор
		if ( ratio < bestRation )
			continue;

		// Текущая строка прошла все тесты: мы можем выбрать ее
		row = &*it;
		bestRation = ratio;
	}

	// Если мы не нашли соответствующую строку, создаем новую текстуру (на 10% больше)
	if ( !row )
	{
		int			rowHeight = Height + Height / 10;
		while ( Page.nextRow + rowHeight >= ( uint32_t ) Page.textureAtlas->GetSize().y || Width >= ( uint32_t ) Page.textureAtlas->GetSize().x )
		{
			// Недостаточно места: изменяем размер текстуры, если это возможно
			glm::ivec2			textureSize = Page.textureAtlas->GetSize();

			if ( ( uint32_t ) textureSize.x * 2 <= Texture::GetMaximumSize() && ( uint32_t ) textureSize.y * 2 <= Texture::GetMaximumSize() )
				Page.textureAtlas->Resize( textureSize.x * 2, textureSize.y * 2, true );
			else
			{
				LOG_ERROR( "Failed to add a new character to the font: the maximum texture size has been reached" );
				return IntRect( 0, 0, 2, 2 );
			}
		}

		// Теперь мы можем создать новую строку
		Page.rows.push_back( Row( Page.nextRow, rowHeight ) );
		Page.nextRow += rowHeight;
		row = &Page.rows.back();
	}

	// Вычисляем прямоугольник глифа в выбранной строке
	IntRect			rect( row->width, row->top, Width, Height );

	// Обновляем длину строки
	row->width += Width;

	return rect;
}

//----------------------------------------------------------------------//

/*
 * Задать размер символа
 * ------------------
 */

bool le::Font::SetCurrentSize( uint32_t CharacterSize ) const
{
	FT_UShort			currentSize = static_cast< FT_Face >( ftFace )->size->metrics.x_ppem;

	if ( CharacterSize != currentSize )
	{
		FT_Error		result = FT_Set_Pixel_Sizes( static_cast< FT_Face >( ftFace ), 0, CharacterSize );

		if ( result == FT_Err_Invalid_Pixel_Size )
		{
			// В случае растровых шрифтов, изменение размера может быть
			// сбой, если запрошенный размер недоступен
			if ( !FT_IS_SCALABLE( static_cast< FT_Face >( ftFace ) ) )
			{
				LOG_ERROR( "Failed to set bitmap font size to " << CharacterSize );
				LOG_ERROR( "Available sizes are: " );

				for ( int index = 0; index < static_cast< FT_Face >( ftFace )->num_fixed_sizes; ++index )
					LOG_ERROR( ( ( static_cast< FT_Face >( ftFace )->available_sizes[ index ].y_ppem + 32 ) >> 6 ) );
			}
			else
				LOG_ERROR( "Failed to set font size to " << CharacterSize );

			return result == FT_Err_Ok;
		}
	}

	return true;
}

//----------------------------------------------------------------------//

/*
 * Получить межстрочный интервал
 * ------------------
 */

float le::Font::GetLineSpacing( uint32_t CharacterSize ) const
{
	FT_Face			face = static_cast< FT_Face >( ftFace );

	if ( face && SetCurrentSize( CharacterSize ) )
		return static_cast< float >( face->size->metrics.height ) / static_cast< float >( 1 << 6 );

	return 0.f;
}

//----------------------------------------------------------------------//

/*
 * Получить количество ссылок
 * ------------------
 */

uint32_t le::Font::GetReferenceCount() const
{
	return referenceCount;
}

//----------------------------------------------------------------------//