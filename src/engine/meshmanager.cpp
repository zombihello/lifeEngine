//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "core.h"
#include "engine/ifactory.h"
#include "resourcesystem.h"
#include "meshmanager.h"

// ------------------------------------------------------------------------------------ //
// Конструктор
// ------------------------------------------------------------------------------------ //
le::MeshManager::MeshManager() :
	directory( "." )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------
 */

le::MeshManager::~MeshManager()
{}

//----------------------------------------------------------------------//

/*
 * Загрузить меш
 * ------------------
 */

le::IMesh* le::MeshManager::Load( const string& Route, const string& Name )
{
	string					name = Name;
	if ( Name.empty() )		name = Route;

	if ( meshes.find( name ) == meshes.end() )
	{
		LOG_INFO( "Loading mesh [" << name << "]" );
		IMesh*            mesh = ( IMesh* ) factory->Create( MESH_INTERFECE_VERSION );

		if ( !mesh || !mesh->Load( Route ) )
		{
			LOG_ERROR( "Mesh [" << name << "] not loaded" );
			delete mesh;
			return nullptr;
		}

		meshes[ name ] = mesh;
		LOG_INFO( "Loaded mesh [" << name << "]" );
		return mesh;
	}

	return meshes[ name ];
}

//----------------------------------------------------------------------//

/*
 * Создать меш
 * ------------------
 */

le::IMesh* le::MeshManager::Create( const string& Name )
{
	if ( meshes.find( Name ) == meshes.end() )
	{
		IMesh*            mesh = ( IMesh* ) factory->Create( MESH_INTERFECE_VERSION );
		if ( !mesh )	return nullptr;

		LOG_INFO( "Created mesh [" << Name << "]" );
		meshes[ Name ] = mesh;
		return meshes[ Name ];
	}

	return nullptr;
}

//----------------------------------------------------------------------//

/*
 * Удалить меш
 * ------------------
 */

void le::MeshManager::Delete( const string& Name )
{
	auto			it = meshes.find( Name );

	if ( it == meshes.end() || it->second->GetReferenceCount() > 0 ) return;

	factory->Delete( it->second );
	meshes.erase( Name );

	LOG_INFO( "Mesh with name [" << Name << "] deleted" );
}

//----------------------------------------------------------------------//

/*
 * Удалить меш
 * ------------------
 */

void le::MeshManager::Delete( IMesh* Mesh )
{
	if ( !Mesh || Mesh->GetReferenceCount() > 0 ) return;

	for ( auto it = meshes.begin(), itEnd = meshes.end(); it != itEnd; ++it )
		if ( it->second == Mesh )
		{
			LOG_INFO( "Mesh with name [" << it->first << "] deleted" );

			factory->Delete( it->second );
			meshes.erase( it );
			return;
		}
}

// ------------------------------------------------------------------------------------ //
// Удалить все ресурсы
// ------------------------------------------------------------------------------------ //
void le::MeshManager::DeleteAll()
{
	if ( meshes.empty() ) return;

	for ( auto it = meshes.begin(), itEnd = meshes.end(); it != itEnd; ++it )
		factory->Delete( it->second );

	LOG_INFO( "All meshes deleted" );
	meshes.clear();
}

//----------------------------------------------------------------------//

/*
 * Задать каталог с материалами
 * ------------------
 */

void le::MeshManager::SetDirectory( const string& Directory )
{
	directory = Directory;
}

//----------------------------------------------------------------------//

/*
 * Получить каталог с материалами
 * ------------------
 */

const string& le::MeshManager::GetDirectory() const
{
	return directory;
}

//----------------------------------------------------------------------//

/*
 * Получить материал по названию
 * ------------------
 */

le::IMesh* le::MeshManager::Get( const string& Name )
{
	if ( meshes.find( Name ) != meshes.end() )
		return meshes[ Name ];

	return nullptr;
}

//----------------------------------------------------------------------//
