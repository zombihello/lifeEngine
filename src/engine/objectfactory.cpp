//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "objectfactory.h"

// ------------------------------------------------------------------------------------ //
// Конструктор
// ------------------------------------------------------------------------------------ //
le::ObjectFactory::ObjectFactory()
{}

// ------------------------------------------------------------------------------------ //
// Создать объект
// ------------------------------------------------------------------------------------ //
void* le::ObjectFactory::Create( const string& Name )
{
	auto		itClass = classObjects.find( Name );
	if ( itClass == classObjects.end() )
	{
		LOG_ERROR( "Class with name [" << Name << "] not registered in factory" );
		return nullptr;
	}

	return itClass->second();
}

// ------------------------------------------------------------------------------------ //
// Удалить объект
// ------------------------------------------------------------------------------------ //
void le::ObjectFactory::Delete( void* Object )
{
	delete Object;
}

// ------------------------------------------------------------------------------------ //
// Зарегестрировать класс
// ------------------------------------------------------------------------------------ //
void le::ObjectFactory::RegisterClass( const string& Name, Func_CreateObjectFactory_t Func_CreateObjectFactory )
{
	if ( classObjects.find( Name ) != classObjects.end() )
		return;

	LOG_INFO( "Class with name [" << Name << "] registered in factory" );
	classObjects[ Name ] = Func_CreateObjectFactory;
}

// ------------------------------------------------------------------------------------ //
// Разрегистрировать класс
// ------------------------------------------------------------------------------------ //
void le::ObjectFactory::UnregisterClass( const string& Name )
{
	auto		itClass = classObjects.find( Name );
	if ( itClass == classObjects.end() )
		return;

	LOG_INFO( "Class with name [" << Name << "] unregistered in factory" );
	classObjects.erase( itClass );
}