//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"
#include "engine/icore.h"
#include "engine/irendersystem.h"
#include "engine/iresourcesystem.h"
#include "engine/imodel.h"
#include "engine/ifactory.h"
#include "scene.h"

#include "boundingbox.h"
#include "camera.h"
#include "model.h"
#include "sprite.h"

//----------------------------------------------------------------------//

/*
* Конструктор
* ---------------
*/

le::Scene::Scene()
{}

//----------------------------------------------------------------------//

/*
* Деструктор
* ---------------
*/

le::Scene::~Scene()
{
	Clear();
}

//----------------------------------------------------------------------//

/*
* Инициализировать подсистему движка
* --------------------------
*/

bool le::Scene::Initialize( ICore* Core )
{
	renderSystem = static_cast< IRenderSystem* >( Core->GetSubsystem( EST_RENDER_SYSTEM ) );
	if ( !renderSystem )		return false;

	IFactory*		factory = Core->GetFactory();
	factory->RegisterClass( BOUNDINGBOX_INTERFECE_VERSION, []() -> void* { return new BoundingBox(); } );
	factory->RegisterClass( CAMERA_INTERFECE_VERSION, []() -> void* { return new Camera(); } );
	factory->RegisterClass( MODEL_INTERFECE_VERSION, []() -> void* { return new Model(); } );
	factory->RegisterClass( SPRITE_INTERFECE_VERSION, []() -> void* { return new Sprite(); } );

	return true;
}

//----------------------------------------------------------------------//

/*
* Добавить модель на сцену
* --------------------------
*/

void le::Scene::AddModel( IModel* Model )
{
	models.push_back( Model );
}

//----------------------------------------------------------------------//

/*
* Удалить модель со сцены
* --------------------------
*/

void le::Scene::RemoveModel( IModel* Model )
{
	for ( auto it = models.begin(), itEnd = models.end(); it != itEnd; ++it )
		if ( ( *it ) == Model )
		{
			models.erase( it );
			return;
		}
}

//----------------------------------------------------------------------//

/*
* Очистить сцену
* --------------------------
*/

void le::Scene::Clear()
{
	models.clear();
}

//----------------------------------------------------------------------//

/*
* Обновить сцену
* --------------------------
*/

void le::Scene::Update()
{
	for ( auto model = models.begin(), modelEnd = models.end(); model != modelEnd; ++model )
		renderSystem->Draw( *model, TL_WORLD );
}

//----------------------------------------------------------------------//

/*
* Получить тип подсистемы движка
* --------------------------
*/

le::ENGINE_SUBSYSTEM_TYPE le::Scene::GetType() const
{
	return le::EST_SCENE_MANAGER;
}

//----------------------------------------------------------------------//