//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (��������� �����) ***
//				Copyright (C) 2018-2019
//
// ����������� ������:  https://gitlab.com/zombihello/lifeEngine
// ������:				���� �������� (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef SETTINGS_CONTEXT_H
#define SETTINGS_CONTEXT_H

#include <stdint.h>
using namespace std;

//----------------------------------------------------------------------//

namespace le
{
	//----------------------------------------------------------------------//

	///////////////////////////////////////////////////////////////////////////
	/// \brief ��������� ���������
	///////////////////////////////////////////////////////////////////////////
	struct SettingsContext
	{
		//----------------------------------------------------------------------//

		////////////////////////////////////////////////////////////
		/// \brief ������������ ��������� ���������
		////////////////////////////////////////////////////////////
		enum CONTEXT_ATTRIBUTE
		{
			CA_DEFAULT	= 0,					///< �������� �������������
			CA_CORE		= 1 << 0,				///< �������� ����
			CA_DEBUG	= 1 << 2				///< �������� �������
		};

		//----------------------------------------------------------------------//
		
		////////////////////////////////////////////////////////////
		/// \brief �����������
		///
		/// \param[in] RedBits Количество бит на красный канал
		/// \param[in] GreenBits Количество бит на зеленый канал
		/// \param[in] BlueBits Количество бит на синий канал
		/// \param[in] AlphaBits Количество бит на альфа канал
		/// \param[in] DepthBits ���������� ��� �� ����� �������
		/// \param[in] StencilBits ���������� ��� �� ����� ���������
		/// \param[in] MajorVersion �������� ������ OpenGL
		/// \param[in] MinorVersion �������� ������ OpenGL
		/// \param[in] AttributeFlags ����� ��������� ���������
		////////////////////////////////////////////////////////////
		SettingsContext( uint32_t RedBits = 8, uint32_t GreenBits = 8, uint32_t BlueBits = 8, uint32_t AlphaBits = 8, uint32_t DepthBits = 8, uint32_t StencilBits = 0, uint32_t MajorVersion = 1, uint32_t MinorVersion = 1, uint32_t AttributeFlags = CA_DEFAULT ) :
			redBits( RedBits ),
			greenBits( GreenBits ),
			blueBits( BlueBits ),
			alphaBits( AlphaBits ),
			depthBits( DepthBits ),
			stencilBits( StencilBits ),
			majorVersion( MajorVersion ),
			minorVersion( MinorVersion ),
			attributeFlags( AttributeFlags )
		{}

		uint32_t			redBits;			///< Количество бит на красный канал
		uint32_t			greenBits;			///< Количество бит на зеленый канал
		uint32_t			blueBits;			///< Количество бит на синий канал
		uint32_t			alphaBits;			///< Количество бит на альфа канал
		uint32_t			depthBits;			///< ���������� ��� �� ����� �������
		uint32_t			stencilBits;		///< ���������� ��� �� ����� ���������
		uint32_t			majorVersion;		///< �������� ������ OpenGL
		uint32_t			minorVersion;		///< �������� ������ OpenGL
		uint32_t			attributeFlags;		///< ����� ��������� ���������
	};

	//----------------------------------------------------------------------//
}

//----------------------------------------------------------------------//

#endif // !SETTINGS_CONTEXT_H