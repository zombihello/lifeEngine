//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "text.h"

//----------------------------------------------------------------------//

namespace le
{
	//-------------------------------------------------------------------------//

	//////////////////////////////////////////////////////////////////////
	/// \brief Вершина текста
	//////////////////////////////////////////////////////////////////////
	struct VertexText
	{
		VertexText( const glm::vec3& Position = glm::vec3( 0.f ), const glm::vec2& TexCoords = glm::vec2( 0.f ) ) :
			position( Position ),
			texCoords( TexCoords )
		{}

		glm::vec3		position;		///< Позиция вершины
		glm::vec2		texCoords;		///< Текстурные координаты
	};

	//-------------------------------------------------------------------------//
}

//----------------------------------------------------------------------//

/*
 * Конструктор
 * ------------------
 */

le::Text::Text() :
	isUpdateGeometry( false ),
	isUpdateTransformation( false ),
	isEnable( true ),
	font( nullptr ),
	textureFont( nullptr ),
	characterSize( 45 ),
	lineSpacingFactor( 1.f ),
	letterSpacingFactor( 1.f ),
	color( 255.f ),
	scale( 1.f ),
	rotation( 0.f, 0.f, 0.f, 0.f ),
	matrix_Transformation( 1.f )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------
 */

le::Text::~Text()
{}

//----------------------------------------------------------------------//

/*
 * Повернуть текст
 * ------------------
 */

void le::Text::Rotate( const glm::vec3& FactorRotate )
{
	glm::vec3			axis( sin( FactorRotate.x / 2 ), sin( FactorRotate.y / 2 ), sin( FactorRotate.z / 2 ) );
	glm::vec3			rotations( cos( FactorRotate.x / 2 ), cos( FactorRotate.y / 2 ), cos( FactorRotate.z / 2 ) );

	glm::quat			rotateX( rotations.x, axis.x, 0, 0 );
	glm::quat			rotateY( rotations.y, 0, axis.y, 0 );
	glm::quat			rotateZ( rotations.z, 0, 0, axis.z );

	glm::quat			rotation = rotateX * rotateY * rotateZ;

	this->rotation *= rotation;
	matrix_Transformation *= glm::mat4_cast( rotation );
}

//----------------------------------------------------------------------//

/*
 * Задать поворот текста
 * ------------------
 */

void le::Text::SetRotation( const glm::vec3& Rotation )
{
	glm::vec3				axis( sin( Rotation.x / 2 ), sin( Rotation.y / 2 ), sin( Rotation.z / 2 ) );
	glm::vec3				rotations( cos( Rotation.x / 2 ), cos( Rotation.y / 2 ), cos( Rotation.z / 2 ) );

	glm::quat				rotateX( rotations.x, axis.x, 0, 0 );
	glm::quat				rotateY( rotations.y, 0, axis.y, 0 );
	glm::quat				rotateZ( rotations.z, 0, 0, axis.z );

	glm::quat				rotation = rotateX * rotateY * rotateZ;

	this->rotation = rotation;
	isUpdateTransformation = true;
}

//----------------------------------------------------------------------//

/*
 * Обновить текст
 * ------------------
 */

void le::Text::Update()
{
	// Если шрифт не указан - выходим
	if ( !font )
		return;

	// Если шрифт был очищен - удаляем VAO/VBO и выходим
	if ( !font->IsLoaded() )
	{
		if ( vertexArray.IsCreate() )
		{
			vertexArray.Delete();
			vertexBuffer.Delete();
		}

		return;
	}

	// Если текст трансформировался - обновляем матрицу трансформаций
	if ( isUpdateTransformation )
	{
		matrix_Transformation = glm::translate( position ) * glm::mat4_cast( rotation ) * glm::scale( scale );
		isUpdateTransformation = false;
	}

	// Если геометрия текста с размером текстуры не менялись, то ничего не делаем
	if ( !isUpdateGeometry && textureFont && sizeTextureFont == textureFont->GetSize() )
		return;

	vector<VertexText>			buffer;
	float						lineSpacing = font->GetLineSpacing( characterSize ) * lineSpacingFactor;
	float						whitespaceWidth = font->GetGlyph( ' ', characterSize ).advance;
	float						letterSpacing = ( whitespaceWidth / 3.f ) * ( letterSpacingFactor - 1.f );
	float						localPositionX = 0;
	float						localPositionY = 0;

	whitespaceWidth += letterSpacing;

	// Обновляем геометрию текста
	for ( uint32_t index = 0, count = text.size(); index < count; ++index )
	{
		switch ( text[ index ] )
		{
		case ' ':
			localPositionX += whitespaceWidth;
			continue;

		case '\t':
			localPositionX += whitespaceWidth * 4.f;
			continue;

		case '\n':
			localPositionY -= lineSpacing;
			localPositionX = 0;
			continue;
		}

		Font::Glyph		glyph = font->GetGlyph( text[ index ], characterSize );
		
		float			xPosition = localPositionX + glyph.bounds.left;
		float			yPosition = localPositionY - ( glyph.bounds.height - glyph.bounds.top );

		float			u1 = static_cast< float >( glyph.textureRect.left );
		float			v1 = static_cast< float >( glyph.textureRect.top );
		float			u2 = static_cast< float >( glyph.textureRect.left + glyph.textureRect.width ) ;
		float			v2 = static_cast< float >( glyph.textureRect.top + glyph.textureRect.height );

		buffer.push_back( VertexText( glm::vec3( xPosition,							yPosition + glyph.bounds.height,	1 ), glm::vec2( u1, v1 ) ) );
		buffer.push_back( VertexText( glm::vec3( xPosition,							yPosition,							1 ), glm::vec2( u1, v2 ) ) );
		buffer.push_back( VertexText( glm::vec3( xPosition + glyph.bounds.width,	yPosition,							1 ), glm::vec2( u2, v2 ) ) );
		buffer.push_back( VertexText( glm::vec3( xPosition,							yPosition + glyph.bounds.height,	1 ), glm::vec2( u1, v1 ) ) );
		buffer.push_back( VertexText( glm::vec3( xPosition + glyph.bounds.width,	yPosition,							1 ), glm::vec2( u2, v2 ) ) );
		buffer.push_back( VertexText( glm::vec3( xPosition + glyph.bounds.width,	yPosition + glyph.bounds.height,	1 ), glm::vec2( u2, v1 ) ) );

		localPositionX += glyph.advance + letterSpacing;
	}

	// Если нет текстуры шрифта, то берем ее и запоминаем размер текстуры
	if ( !textureFont )
	{
		textureFont = font->GetTextureAtlas( characterSize );
		sizeTextureFont = textureFont->GetSize();
	}
	else if ( sizeTextureFont != textureFont->GetSize() )		// Если размеры текстуры менялись - запоминаем новые размеры
		sizeTextureFont = textureFont->GetSize();

	// Проходимся по буферу и нормализуем текстурные координаты в пределах [0..1]
	for ( uint32_t index = 0, count = buffer.size(); index < count; ++index )
		buffer[ index ].texCoords /= sizeTextureFont;

	// Если ранее буфер текста был создан - обновляем данные, иначе создаем
	if ( vertexArray.IsCreate() )
	{
		vertexBuffer.Bind();
		vertexBuffer.Allocate( buffer.data(), buffer.size() * sizeof( VertexText ) );
		vertexBuffer.Unbind();
	}
	else
	{
		// Формируем новый вершиный буфер
		VertexBufferLayout				vertexBufferLayout;
		vertexBufferLayout.PushFloat( 3 );
		vertexBufferLayout.PushFloat( 2 );

		vertexArray.Create();
		vertexBuffer.Create();

		vertexBuffer.Bind();
		vertexBuffer.SetTypeUsage( TUB_DYNAMIC );
		vertexBuffer.Allocate( buffer.data(), buffer.size() * sizeof( VertexText ) );

		vertexArray.Bind();
		vertexArray.AddBuffer( vertexBuffer, vertexBufferLayout );

		vertexArray.Unbind();
		vertexBuffer.Unbind();
	}
	
	isUpdateGeometry = false;
}

//----------------------------------------------------------------------//
