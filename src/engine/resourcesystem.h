//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef RESOURCE_SYSTEM_H
#define RESOURCE_SYSTEM_H

#include "engine/iresourcesystem.h"
#include "fontmanager.h"
#include "materialmanager.h"
#include "meshmanager.h"
#include "texturemanager.h"

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	class ITexture;
	class IMaterial;
	class IMesh;
	class IFont;
	class IFactory;

	//---------------------------------------------------------------------//

	//////////////////////////////////////////////////////////////////////////
	/// \brief Система менеджера ресурсов
	///
	/// Система менеджера ресурсов отвечает за менеджмент всех загружаемых ресурсов
	//////////////////////////////////////////////////////////////////////////
	class ResourceSystem : public IResourceSystem
	{
	public:
		///////////////////////////////////////////////////////////////////////////
		/// \brief Конструктор
		////////////////////////////////////////////////////////////////////////////
		ResourceSystem();
		
		///////////////////////////////////////////////////////////////////////////
		/// \brief Деструктор
		////////////////////////////////////////////////////////////////////////////
		~ResourceSystem();

		///////////////////////////////////////////////////////////////////////////
		/// \brief Инициализировать подсистему движка
		///
		/// \param[in] Core Указатель на ядро движка
		/// \return True подсистема движка успешно инициализирована, иначе false
		////////////////////////////////////////////////////////////////////////////
		bool Initialize( ICore* Core );

		//////////////////////////////////////////////////////////////////////
		/// \brief Выгрузить все загруженные ресурсы всех менеджеров
		//////////////////////////////////////////////////////////////////////
		void Clear();

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить менеджер шрифтов
		///
		/// \return Указатель на менеджер шрифтов
		//////////////////////////////////////////////////////////////////////
		IResourceManager<IFont>* GetFontManager() const;

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить менеджер текстур
		///
		/// \return Указатель на менеджер текстур
		//////////////////////////////////////////////////////////////////////
		IResourceManager<ITexture>* GetTextureManager() const;

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить менеджер материалов
		///
		/// \return Указатель на менеджер материалов
		//////////////////////////////////////////////////////////////////////
		IResourceManager<IMaterial>* GetMaterialManager() const;

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить менеджер мешей
		///
		/// \return Указатель на менеджер мешей
		//////////////////////////////////////////////////////////////////////
		IResourceManager<IMesh>* GetMeshManager() const;

		///////////////////////////////////////////////////////////////////////////
		/// \brief Получить тип подсистемы движка
		///
		/// \return Тип подсистемы движка
		////////////////////////////////////////////////////////////////////////////
		ENGINE_SUBSYSTEM_TYPE GetType() const;

	private:
		FontManager			fontManager;			///< Менеджер шрифтов
		TextureManager		textureManager;			///< Менеджер текстур
		MaterialManager		materialManager;		///< Менеджер материалов
		MeshManager			meshManager;			///< Менеджер мешей
	};

	//---------------------------------------------------------------------//
}

//---------------------------------------------------------------------//

#endif // !RESOURCE_SYSTEM_H
