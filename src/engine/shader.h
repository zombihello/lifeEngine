//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

#include "common/rect.h"
#include "engine/lifeEngine.h"

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	//////////////////////////////////////////////////////////////////////                      
	/// \brief Шейдерная программа
	///
	/// Данный класс является обверткой над OpenGL функциями по работе с
	/// шейдерами
	//////////////////////////////////////////////////////////////////////
	class Shader
	{
	public:

		//-------------------------------------------------------------------------//

		friend				class ShaderManager;

		//-------------------------------------------------------------------------//

		//////////////////////////////////////////////////////////////////////
		/// \brief Конструктор
		//////////////////////////////////////////////////////////////////////
		Shader();

		//////////////////////////////////////////////////////////////////////
		/// \brief Деструктор
		//////////////////////////////////////////////////////////////////////
		~Shader();

		//////////////////////////////////////////////////////////////////////
		/// \brief Активировать шейдер
		//////////////////////////////////////////////////////////////////////
		inline void Bind()
		{
			if ( currentProgramID == programID ) return;

			glUseProgram( programID );
			currentProgramID = programID;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Деактивировать шейдер
		//////////////////////////////////////////////////////////////////////
		static inline void Unbind()
		{
			if ( currentProgramID == 0 ) return;

			glUseProgram( 0 );
			currentProgramID = 0;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, bool Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniform1i( GetUniformLocation( Name ), static_cast< int >( Value ) );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, int Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniform1i( GetUniformLocation( Name ), Value );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, float Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniform1f( GetUniformLocation( Name ), Value );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, unsigned int Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniform1i( GetUniformLocation( Name ), Value );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, const glm::mat4& Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniformMatrix4fv( GetUniformLocation( Name ), 1, GL_FALSE, glm::value_ptr( Value ) );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, const glm::vec2& Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniform2f( GetUniformLocation( Name ), Value.x, Value.y );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, const glm::vec3& Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniform3f( GetUniformLocation( Name ), Value.x, Value.y, Value.z );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, const glm::vec4& Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniform4f( GetUniformLocation( Name ), Value.x, Value.y, Value.z, Value.w );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, const IntRect& Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniform4i( GetUniformLocation( Name ), Value.left, Value.top, Value.width, Value.height );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Задать значение юниформ-переменной
		///
		/// \param[in] Name Название переменной в шейдере
		/// \param[in] Value Значение переменной
		//////////////////////////////////////////////////////////////////////
		inline void SetUniform( const string& Name, const FloatRect& Value )
		{
			if ( programID == 0 ) return;

			LIFEENGINE_ASSERT( currentProgramID == programID, "Shader not bind" );
			glUniform4f( GetUniformLocation( Name ), Value.left, Value.top, Value.width, Value.height );
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить ID шейдера
		///
		/// \return ID шейдера
		//////////////////////////////////////////////////////////////////////
		inline const GLuint& GetHandle()
		{
			return programID;
		}

		//////////////////////////////////////////////////////////////////////
		/// \brief Скомпилирован ли шейдер
		///
		/// \return true - да, false - нет
		//////////////////////////////////////////////////////////////////////
		inline bool IsCompile()
		{
			return programID > 0;
		}

	private:
		//////////////////////////////////////////////////////////////////////
		/// \brief Загрузить шейдер с файла
		///
		/// \param[in] ShaderRoute Путь к манифесту шейдера
		/// \param[in] Flags Флаги техник шейдера
		/// \return true - все прошло удачно, false - была ошибка
		//////////////////////////////////////////////////////////////////////
		bool LoadFromFile( const string& ShaderRoute, uint32_t Flags );

		//////////////////////////////////////////////////////////////////////
		/// \brief Удалить шейдер
		//////////////////////////////////////////////////////////////////////
		void Delete();

		//////////////////////////////////////////////////////////////////////
		/// \brief Скомпилировать шейдер
		///
		/// \param[in] VertexShader Код вершиного шейдера
		/// \param[in] GeometryShader Код геометрического шейдера
		/// \param[in] ComputeShader Код вычислительного шейдера
		/// \param[in] FragmentShader Код фрагментного шейдера		
		/// \param[in] Flags Флаги техник шейдера
		/// \return true - все прошло удачно, false - была ошибка
		//////////////////////////////////////////////////////////////////////
		bool Compile( const char* VertexShader, const char* GeometryShader, const char* ComputeShader, const char* FragmentShader, uint32_t Flags );

		//////////////////////////////////////////////////////////////////////
		/// \brief Скомпилировать вершиный шейдер
		///
		/// \param[in] ShaderCode Код шейдера
		/// \return true - все прошло удачно, false - была ошибка
		//////////////////////////////////////////////////////////////////////
		bool CompileVertexShader( const char* ShaderCode );

		//////////////////////////////////////////////////////////////////////
		/// \brief Скомпилировать геометрический шейдер
		///
		/// \param[in] ShaderCode Код шейдера
		/// \return true - все прошло удачно, false - была ошибка
		//////////////////////////////////////////////////////////////////////
		bool CompileGeometryShader( const char* ShaderCode );

		//////////////////////////////////////////////////////////////////////
		/// \brief Скомпилировать вычеслительный шейдер
		///
		/// \param[in] ShaderCode Код шейдера
		/// \return true - все прошло удачно, false - была ошибка
		//////////////////////////////////////////////////////////////////////
		bool CompileComputeShader( const char* ShaderCode );

		//////////////////////////////////////////////////////////////////////
		/// \brief Скомпилировать фрагментный шейдер
		///
		/// \param[in] ShaderCode Код шейдера
		/// \return true - все прошло удачно, false - была ошибка
		//////////////////////////////////////////////////////////////////////
		bool CompileFragmentShader( const char* ShaderCode );

		//////////////////////////////////////////////////////////////////////
		/// \brief Существует ли юниформ-переменная в шейдере
		///
		/// \param[in] NameUniform Название перменной
		/// \return True - присутствует, иначе false
		//////////////////////////////////////////////////////////////////////
		bool IsUniformExists( const string& NameUniform );

		//////////////////////////////////////////////////////////////////////
		/// \brief Получить идентификатор расположения юниформ-переменной
		///
		/// \param[in] NameUniform Название перменной
		/// \return Идентификатор расположения юниформ-переменной. 
		/// Если юниформ-переменной нет вернет -1
		//////////////////////////////////////////////////////////////////////
		int GetUniformLocation( const string& NameUniform );

		GLuint							vertexID;			///< Идентификатор вершиного шейдера
		GLuint							geometryID;			///< Идентификатор геометрического шейдера
		GLuint							computeID;			///< Идентификатор вычислительного шейдера
		GLuint							fragmentID;			///< Идентификатор фрагментного шейдера
		GLuint							programID;			///< Идентификатор шейдерной программы

		static GLuint					currentProgramID;	///< Идентификатор активированой шейдерной программы

		unordered_map<string, int>		uniforms;			///< Ассоциативный массив расположения юниформ-переменных в шейдере
	};

	//---------------------------------------------------------------------//
}

//---------------------------------------------------------------------//

#endif // !SHADER_MANAGER_H
