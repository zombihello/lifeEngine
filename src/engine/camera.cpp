//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (��������� �����) ***
//				Copyright (C) 2018-2019
//
// ����������� ������:  https://gitlab.com/zombihello/lifeEngine
// ������:				���� �������� (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/ray.h"
#include "engine/iinputsystem.h"
#include "camera.h"

//----------------------------------------------------------------------//

/*
 * �����������
 * ---------------
 */

le::Camera::Camera() :
    isEnable( true ),
    isUpdate( true ),
    up( 0.f, 1.f, 0.f ),
    localUp( 0.f, 1.f, 0.f ),
    position( 0.f, 0.f, 0.f ),
    targetDirection( 0.f, 0.f, -1.f ),
    localTargetDirection( 0.f, 0.f, -1.f ),
    quatRotation( 1.f, 0.f, 0.f, 0.f ),
    eulerRotation( 0.f, 0.f, 0.f ),
    matrixProjection( 1.f ),
    matrixView( 1.f )
{}

//----------------------------------------------------------------------//

/*
 * ����������
 * ----------------
 */

le::Camera::~Camera()
{}

//----------------------------------------------------------------------//

/*
 * ���������������� ������� ��������
 * ----------------
 */

void le::Camera::InitProjection_Perspective( float FOV, float Aspect, float Near, float Far )
{
    matrixProjection = glm::perspective( glm::radians( FOV ), Aspect, Near, Far );
}

//----------------------------------------------------------------------//

/*
 * ���������������� ������� ��������
 * ----------------
 */

void le::Camera::InitProjection_Ortho( float Left, float Right, float Bottom, float Top, float Near, float Far )
{
    matrixProjection = glm::ortho( Left, Right, Bottom, Top, Near, Far );
}

//----------------------------------------------------------------------//

/*
 * �������� ������
 * ----------------
 */

void le::Camera::Move( float FactorX, float FactorY, float FactorZ )
{
    position.x += FactorX;
    position.y += FactorY;
    position.z += FactorZ;

    isUpdate = true;
}

//----------------------------------------------------------------------//

/*
 * ��������� ������
 * ----------------
 */

void le::Camera::Rotate( float FactorX, float FactorY, float FactorZ )
{
    // 6.28319 ������ = 360 ��������

    if ( FactorZ != 0.f )
    {
        eulerRotation.z += glm::radians( FactorZ );
        if ( eulerRotation.z < -6.28319f || eulerRotation.z > 6.28319f )
            eulerRotation.z = 0.f;
    }

    if ( FactorX != 0.f )
    {
        eulerRotation.x += glm::radians( FactorX );
        if ( eulerRotation.x < -6.28319f || eulerRotation.x > 6.28319f )
            eulerRotation.x = 0.f;
    }

    if ( FactorY != 0.f )
    {
        eulerRotation.y += glm::radians( FactorY );
        if ( eulerRotation.y < -6.28319f || eulerRotation.y > 6.28319f )
            eulerRotation.y = 0.f;
    }

    quatRotation =
            glm::angleAxis( eulerRotation.z, glm::vec3( 0.f, 0.f, 1.f ) ) *
            glm::angleAxis( eulerRotation.x, glm::vec3( 1.f, 0.f, 0.f ) ) *
            glm::angleAxis( eulerRotation.y, glm::vec3( 0.f, 1.f, 0.f ) );

    isUpdate = true;
}

//----------------------------------------------------------------------//

/*
 * ��������� ������ � ������� �����
 * ----------------
 */

void le::Camera::RotateByMouse( float MouseOffsetX, float MouseOffsetY, float MouseSensitivit, bool ConstrainYaw )
{
    // 1.5708 ������ = 90 ��������
    // 6.28319 ������ = 360 ��������

    if ( MouseOffsetX != 0 )
    {
        eulerRotation.y += glm::radians( MouseOffsetX * MouseSensitivit );

        if ( eulerRotation.x < -6.28319f || eulerRotation.x > 6.28319f )
            eulerRotation.x = 0.f;
    }

    if ( MouseOffsetY != 0 )
    {
        eulerRotation.x += glm::radians( MouseOffsetY * MouseSensitivit );

        if ( ConstrainYaw )
        {
            if ( eulerRotation.x > 1.5708f )
                eulerRotation.x = 1.5708f;

            if ( eulerRotation.x < -1.5708f )
                eulerRotation.x = -1.5708f;
        }
        else if ( eulerRotation.x < -6.28319f || eulerRotation.x > 6.28319f )
            eulerRotation.x = 0.f;
    }

    quatRotation =
            glm::angleAxis( eulerRotation.z, glm::vec3( 0.f, 0.f, 1.f ) ) *
            glm::angleAxis( eulerRotation.x, glm::vec3( 1.f, 0.f, 0.f ) ) *
            glm::angleAxis( eulerRotation.y, glm::vec3( 0.f, 1.f, 0.f ) );

    isUpdate = true;
}

//----------------------------------------------------------------------//

/*
 * �������� ������ � ������ �� ��������
 * ----------------
 */

void le::Camera::Move( CAMERA_SIDE_MOVE SideMove, float MoveSpeed )
{
    switch ( SideMove )
    {
    case CSM_FORWARD:
        position += targetDirection * MoveSpeed;
        break;

    case CSM_BACKWARD:
        position -= targetDirection * MoveSpeed;
        break;

    case CSM_LEFT:
        position -= right * MoveSpeed;
        break;

    case CSM_RIGHT:
        position += right * MoveSpeed;
        break;
    }

    isUpdate = true;
}

//----------------------------------------------------------------------//

/*
 * ������ ������� ������
 * ----------------
 */

void le::Camera::SetPosition( float X, float Y, float Z )
{
    position.x = X;
    position.y = Y;
    position.z = Z;

    isUpdate = true;
}

//----------------------------------------------------------------------//

/*
 * ������ ���� �������� ������
 * ----------------
 */

void le::Camera::SetRotation( float X, float Y, float Z )
{
    // 6.28319 ������ = 360 ��������

    eulerRotation = glm::vec3( glm::radians( X ), glm::radians( Y ), glm::radians( Z ) );

    if ( eulerRotation.z < -6.28319f || eulerRotation.z > 6.28319f )
        eulerRotation.z = 0.f;

    if ( eulerRotation.x < -6.28319f || eulerRotation.x > 6.28319f )
        eulerRotation.x = 0.f;

    if ( eulerRotation.y < -6.28319f || eulerRotation.y > 6.28319f )
        eulerRotation.y = 0.f;

    quatRotation =
            glm::angleAxis( eulerRotation.z, glm::vec3( 0.f, 0.f, 1.f ) ) *
            glm::angleAxis( eulerRotation.x, glm::vec3( 1.f, 0.f, 0.f ) ) *
            glm::angleAxis( eulerRotation.y, glm::vec3( 0.f, 1.f, 0.f ) );
	
    isUpdate = true;
}

//----------------------------------------------------------------------//

/*
 * ������ ����������� ������� ������
 * ----------------
 */

void le::Camera::SetTargetDirection( float X, float Y, float Z )
{
    localTargetDirection.x = X;
    localTargetDirection.y = Y;
    localTargetDirection.z = Z;

    isUpdate = true;
}

//----------------------------------------------------------------------//

/*
 * ������ ������ �����
 * ----------------
 */

void le::Camera::SetUp( float X, float Y, float Z )
{
    localUp.x = X;
    localUp.y = Y;
    localUp.z = Z;

    isUpdate = true;
}

//----------------------------------------------------------------------//

/*
 * ������������ ������
 * ----------------
 */

void le::Camera::SetEnable( bool IsEnable )
{
    isEnable = IsEnable;
}

//----------------------------------------------------------------------//

/*
 * Преобразовать экранные координаты в мировые
 * ------------------------------------------------
 */

le::Ray le::Camera::ScreenToWorld( const glm::vec2& Coords, const glm::vec2& ViewportSize )
{
    glm::vec3       farPlane = glm::unProject( glm::vec3( Coords.x, ViewportSize.y - Coords.y, 1.f ), matrixView, matrixProjection, glm::vec4( 0, 0, ViewportSize.x, ViewportSize.y ) );
    glm::vec3		nearPlane = glm::unProject( glm::vec3( Coords.x, ViewportSize.y - Coords.y, 0.f ), matrixView, matrixProjection, glm::vec4( 0, 0, ViewportSize.x, ViewportSize.y ) );

	return le::Ray( position, glm::normalize( farPlane - position ) );
}

//----------------------------------------------------------------------//

/*
 * Преобразовать мировые координаты в экранные
 * ------------------------------------------------
 */

glm::vec3 le::Camera::WorldToScreen( const glm::vec3& Coords, const glm::vec2& ViewportSize )
{
	return glm::project( Coords, matrixView, matrixProjection, glm::vec4( 0, 0, ViewportSize.x, ViewportSize.y ) );
}

//----------------------------------------------------------------------//

/*
 * �������� ������� ������
 * ----------------
 */

const glm::vec3& le::Camera::GetPosition() const
{
    return position;
}

// ------------------------------------------------------------------------------------ //
// Получить вектор вверх камеры
// ------------------------------------------------------------------------------------ //
const glm::vec3& le::Camera::GetUp() const
{
	return up;
}

// ------------------------------------------------------------------------------------ //
// Получить вектор вправо камеры
// ------------------------------------------------------------------------------------ //
const glm::vec3& le::Camera::GetRight() const
{
	return right;
}

//----------------------------------------------------------------------//

/*
 * �������� ���������� ������
 * ----------------
 */

const glm::quat& le::Camera::GetQuatRotation() const
{
    return quatRotation;
}

//----------------------------------------------------------------------//

/*
 * �������� ���� ������ ������
 * ----------------
 */

const glm::vec3& le::Camera::GetEulerRotation() const
{
    return eulerRotation;
}

//----------------------------------------------------------------------//

/*
 * �������� ����������� �������
 * ----------------
 */

const glm::vec3& le::Camera::GetTargetDirection() const
{
    return targetDirection;
}

// ------------------------------------------------------------------------------------ //
// Получить матрицу вида
// ------------------------------------------------------------------------------------ //
const glm::mat4& le::Camera::GetViewMatrix()
{
	if ( isUpdate )
	{
		targetDirection = localTargetDirection * quatRotation;
		up = localUp * quatRotation;
		right = glm::normalize( glm::cross( targetDirection, glm::vec3( 0.f, 1.f, 0.f ) ) );
		matrixView = glm::lookAt( position, position + targetDirection, up );

		isUpdate = false;
	}

	return matrixView;
}

// ------------------------------------------------------------------------------------ //
// Получить матрицу проекции
// ------------------------------------------------------------------------------------ //
const glm::mat4& le::Camera::GetProjectionMatrix() const
{
	return matrixProjection;
}

//----------------------------------------------------------------------//

/*
 * �������� �� ������
 * ----------------
 */

bool le::Camera::IsEnabled() const
{
    return isEnable;
}

//----------------------------------------------------------------------//
