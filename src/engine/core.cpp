//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <algorithm>
#include <time.h>

#include "engine_pch.h"
#include "common/image.h"
#include "common/gamemanifest.h"
#include "common/configurations.h"
#include "common/version.h"
#include "engine/igame.h"
#include "core.h"
#include "rendersystem.h"
#include "guisystem.h"
#include "window.h"
#include "scene.h"
#include "consolesystem.h"
#include "inputsystem.h"
#include "resourcesystem.h"

#define GOLD_DATE						42318			// Dec 12 2016 (начало разработки lifeEngine)
#define DEFAULT_WINDOW_TITLE			"lifeEngine"
#define LIFEENGINE_VERSION_MAJOR		0
#define LIFEENGINE_VERSION_MINOR		1
#define LIFEENGINE_VERSION_PATCH		0

//-------------------------------------------------------------------------//

LIFEENGINE_CORE_API( le::Core )

//-------------------------------------------------------------------------//

namespace le
{
	IFactory*		factory = nullptr;				///< Указатель на фабрику объектов движка
}

//-------------------------------------------------------------------------//

/*
 * Тип подсистемы перевести в строку
 * ---------------
 */

	const string EngineSubsystemTypeToString( le::ENGINE_SUBSYSTEM_TYPE EngineSubsystemType )
{
	switch ( EngineSubsystemType )
	{
	case le::EST_GUI_SYSTEM:			return "gui system";
	case le::EST_INPUT_SYSTEM:			return "input system";
	case le::EST_SCENE_MANAGER:			return "scene manager";
	case le::EST_RESOURCE_MANAGER:		return "resource manager";
	case le::EST_RENDER_SYSTEM:			return "render system";
	case le::EST_CONSOLE_SYSTEM:		return "console system";
	default:							return "unknown";
	}
}

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------
 */

le::Core::Core() :
	window( nullptr ),
	handleGameLib( nullptr ),
	game( nullptr ),
	isInitEngine( false ),
	isRunSimulation( false )
{
	configurations.frameLimit = 0;
	configurations.isFullscreen = false;
	configurations.vsinc = false;
	configurations.music = true;
	configurations.sound = true;
	configurations.isBumpMapping = true;
	configurations.isSpecularMapping = true;
	configurations.fov = 75.f;
	configurations.volumeMusic = 100.f;
	configurations.volumeSound = 100.f;
	configurations.sensitivityMouse = 35.f;
	configurations.windowWidth = 800;
	configurations.windowHeight = 600;

	factory = &objectFactory;

	IConsoleSystem*			consoleSystem = new ConsoleSystem();
	consoleSystem->Initialize( this );

	engineSubsystems[ EST_CONSOLE_SYSTEM ] = EngineSubsystem( consoleSystem, nullptr );	
	Log::ConnectConsoleSystem( consoleSystem );
}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ---------------
 */

le::Core::~Core()
{
	UnloadGame();
	Log::DisconnectConsoleSystem();

	if ( window )
	{
		if ( window->IsOpen() ) window->Close();
		delete window;
	}
}

//-------------------------------------------------------------------------//

/*
 * Загрузить информацию об игре
 * ---------------
 */

bool le::Core::LoadGameManifest( const string& GameDir )
{
	ifstream						file( GameDir + "/" + LENGINE_GAME_MANIFEST );

	LOG_INFO( "Loading game manifest [" << GameDir << "/" << LENGINE_GAME_MANIFEST << "]" );

	if ( !file.is_open() )
	{
		LOG_ERROR( "File game manifest [" << GameDir << "/" << LENGINE_GAME_MANIFEST << "] not found" );
		return false;
	}

	Document						document;
	string							strBuffer;

	getline( file, strBuffer, '\0' );
	document.Parse( strBuffer.c_str() );

	if ( document.HasParseError() )
	{
		LOG_ERROR( "Fail parse game manifest [" << GameDir << "/" << LENGINE_GAME_MANIFEST << "]" );
		return false;
	}

	for ( auto it = document.MemberBegin(), itEnd = document.MemberEnd(); it != itEnd; ++it )
	{
		if ( !it->value.IsObject() )
			continue;

		for ( auto itObject = it->value.MemberBegin(), itObjectEnd = it->value.MemberEnd(); itObject != itObjectEnd; ++itObject )
		{
			// ***********************
			// Информация об игре

			if ( strcmp( it->name.GetString(), "gameinfo" ) == 0 )
			{
				// *** Название игры ***
				if ( strcmp( itObject->name.GetString(), "game" ) == 0 && itObject->value.IsString() )
					gameManifest.game = itObject->value.GetString();

				// *** Иконка игры ***
				if ( strcmp( itObject->name.GetString(), "icon" ) == 0 && itObject->value.IsString() )
					gameManifest.icon = GameDir + "/" + itObject->value.GetString();
			}

			// ***********************
			// Информация об каталогах

			if ( strcmp( it->name.GetString(), "directories" ) == 0 )
			{
				// *** Каталог текстур ***
				if ( strcmp( itObject->name.GetString(), "textures" ) == 0 && itObject->value.IsString() )
					gameManifest.texturesDir = GameDir + "/" + itObject->value.GetString();

				// *** Каталог материалов ***
				if ( strcmp( itObject->name.GetString(), "materials" ) == 0 && itObject->value.IsString() )
					gameManifest.materialsDir = GameDir + "/" + itObject->value.GetString();

				// *** Каталог моделей ***
				if ( strcmp( itObject->name.GetString(), "models" ) == 0 && itObject->value.IsString() )
					gameManifest.modelsDir = GameDir + "/" + itObject->value.GetString();

				// *** Каталог карт ***
				if ( strcmp( itObject->name.GetString(), "maps" ) == 0 && itObject->value.IsString() )
					gameManifest.mapsDir = GameDir + "/" + itObject->value.GetString();

				// *** Каталог музыки ***
				if ( strcmp( itObject->name.GetString(), "musics" ) == 0 && itObject->value.IsString() )
					gameManifest.musicsDir = GameDir + "/" + itObject->value.GetString();

				// *** Каталог звуков ***
				if ( strcmp( itObject->name.GetString(), "sounds" ) == 0 && itObject->value.IsString() )
					gameManifest.soundsDir = GameDir + "/" + itObject->value.GetString();

				// *** Каталог сохранений ***
				if ( strcmp( itObject->name.GetString(), "save" ) == 0 && itObject->value.IsString() )
					gameManifest.saveDir = GameDir + "/" + itObject->value.GetString();

				// *** Каталог шрифтов ***
				if ( strcmp( itObject->name.GetString(), "fonts" ) == 0 && itObject->value.IsString() )
					gameManifest.fontsDir = GameDir + "/" + itObject->value.GetString();
			}
		}
	}

	gameManifest.gameDir = GameDir;

	LOG_INFO( "Loaded game manifest [" << GameDir << "/" << LENGINE_GAME_MANIFEST << "]" );
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Загрузить конфигурации движка
 * ---------------
 */

bool le::Core::LoadConfig( const string& Route )
{
	LOG_INFO( "Loading file configurations [" << Route << "]" );

	ifstream						file( Route );

	if ( !file.is_open() )
	{
		LOG_ERROR( "File configurations [" << Route << "] not found" );
		return false;
	}

	Document						document;
	string							strBuffer;

	getline( file, strBuffer, '\0' );
	document.Parse( strBuffer.c_str() );

	if ( document.HasParseError() )
	{
		LOG_ERROR( "Fail parse file configurations [" << Route << "]" );
		return false;
	}

	for ( auto it = document.MemberBegin(), itEnd = document.MemberEnd(); it != itEnd; ++it )
	{
		if ( !it->value.IsObject() )
			continue;

		for ( auto itObject = it->value.MemberBegin(), itObjectEnd = it->value.MemberEnd(); itObject != itObjectEnd; ++itObject )
		{
			// ***********************
			// Окно

			if ( strcmp( it->name.GetString(), "window" ) == 0 )
			{
				// *** Длина окна ***
				if ( strcmp( itObject->name.GetString(), "width" ) == 0 && itObject->value.IsNumber() )
					configurations.windowWidth = itObject->value.GetInt();

				// *** Ширина окна ***
				else if ( strcmp( itObject->name.GetString(), "height" ) == 0 && itObject->value.IsNumber() )
					configurations.windowHeight = itObject->value.GetInt();

				// *** Полноэкраный ли режим ***
				else if ( strcmp( itObject->name.GetString(), "fullscreen" ) == 0 && itObject->value.IsBool() )
					configurations.isFullscreen = itObject->value.GetBool();

				// *** Чувствительность мыши ***
				else if ( strcmp( itObject->name.GetString(), "sensitivityMouse" ) == 0 && itObject->value.IsNumber() )
					configurations.sensitivityMouse = itObject->value.GetFloat();
			}

			// ***********************
			// Графика

			else if ( strcmp( it->name.GetString(), "graphics" ) == 0 )
			{
				// *** Ограничение кадров ***
				if ( strcmp( itObject->name.GetString(), "frameLimit" ) == 0 && itObject->value.IsNumber() )
					configurations.frameLimit = itObject->value.GetInt();

				// *** Включена ли вертикальная синхронизация ***
				else if ( strcmp( itObject->name.GetString(), "vsinc" ) == 0 && itObject->value.IsBool() )
					configurations.vsinc = itObject->value.GetBool();

				// *** Угол обзора ***
				else if ( strcmp( itObject->name.GetString(), "fov" ) == 0 && itObject->value.IsNumber() )
					configurations.fov = itObject->value.GetFloat();

				// *** Использовать ли карту нормалей ***
				else if ( strcmp( itObject->name.GetString(), "bumpMapping" ) == 0 && itObject->value.IsBool() )
					configurations.isBumpMapping = itObject->value.GetBool();

				// *** Использовать ли карту блеска ***
				else if ( strcmp( itObject->name.GetString(), "specularMapping" ) == 0 && itObject->value.IsBool() )
					configurations.isSpecularMapping = itObject->value.GetBool();
			}

			// ***********************
			// Аудио

			else if ( strcmp( it->name.GetString(), "audio" ) == 0 )
			{
				// *** Влючена ли музыка ***
				if ( strcmp( itObject->name.GetString(), "music" ) == 0 && itObject->value.IsBool() )
					configurations.music = itObject->value.GetBool();

				// *** Включен ли звук ***
				else if ( strcmp( itObject->name.GetString(), "sound" ) == 0 && itObject->value.IsBool() )
					configurations.sound = itObject->value.GetBool();

				// *** Громкость музыки ***
				else if ( strcmp( itObject->name.GetString(), "volumeMusic" ) == 0 && itObject->value.IsNumber() )
					configurations.volumeMusic = itObject->value.GetFloat();

				// *** Громеость звука ***
				else if ( strcmp( itObject->name.GetString(), "volumeSound" ) == 0 && itObject->value.IsNumber() )
					configurations.volumeSound = itObject->value.GetFloat();
			}
		}
	}

	LOG_INFO( "Loaded file configurations [" << Route << "]" );
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Сохранить конфигурации движка
 * ---------------
 */

void le::Core::SaveConfig( const string& Route )
{
	LOG_INFO( "Saving file configurations [" << Route << "]" );

	ofstream		fileSave( Route );

	if ( !fileSave.is_open() )
	{
		LOG_ERROR( "Fail saving file configurations [" << Route << "]. File not open" );
		return;
	}

	fileSave <<
		"{\n\
	\"window\": {\n\
		\"width\": " << configurations.windowWidth << ",\n\
		\"height\" : " << configurations.windowHeight << ",\n\
		\"fullscreen\" : " << ( configurations.isFullscreen ? "true" : "false" ) << ",\n\
		\"sensitivityMouse\" : " << configurations.sensitivityMouse << "\n\
	},\n\
	\n\
	\"graphics\": {\n\
		\"frameLimit\": " << configurations.frameLimit << ",\n\
		\"vsinc\" : " << ( configurations.vsinc ? "true" : "false" ) << ",\n\
		\"fov\" : " << configurations.fov << ",\n\
		\"bumpMapping\" : " << ( configurations.isBumpMapping ? "true" : "false" ) << ",\n\
		\"specularMapping\" : " << ( configurations.isSpecularMapping ? "true" : "false" ) << "\n\
	},\n\
	\n\
	\"audio\": {\n\
		\"music\": " << ( configurations.music ? "true" : "false" ) << ",\n\
		\"sound\" : " << ( configurations.sound ? "true" : "false" ) << ",\n\
		\"volumeMusic\" : " << configurations.volumeMusic << ",\n\
		\"volumeSound\" : " << configurations.volumeSound << "\n\
	}\n\
}";

	LOG_INFO( "Saved file configurations [" << Route << "]" );
}

//-------------------------------------------------------------------------//

/*
 * Загрузить изображение
 * ---------------
 */

bool le::Core::LoadImage( const string& Route, Image& Image, bool IsFlipVertical, bool IsSwitchRedAndBlueChannels )
{
	FREE_IMAGE_FORMAT		imageFormat = FIF_UNKNOWN;
	imageFormat = FreeImage_GetFileType( Route.c_str(), 0 );

	if ( imageFormat == FIF_UNKNOWN )
		imageFormat = FreeImage_GetFIFFromFilename( Route.c_str() );

	FIBITMAP* bitmap = FreeImage_Load( imageFormat, Route.c_str(), 0 );
	LOG_INFO( Route );
	if ( !bitmap )
	{
		LOG_ERROR( "Game icon for window not loaded. Maybe this file not found or not supported format (supported formats: bmp, png, tga, jpg, gif, psd, hdr and pic)" );
		return false;
	}


	if ( IsFlipVertical )				FreeImage_FlipVertical( bitmap );
	if ( IsSwitchRedAndBlueChannels )
	{
		auto		red = FreeImage_GetChannel( bitmap, FREE_IMAGE_COLOR_CHANNEL::FICC_RED );
		auto		blue = FreeImage_GetChannel( bitmap, FREE_IMAGE_COLOR_CHANNEL::FICC_BLUE );

		FreeImage_SetChannel( bitmap, blue, FREE_IMAGE_COLOR_CHANNEL::FICC_RED );
		FreeImage_SetChannel( bitmap, red, FREE_IMAGE_COLOR_CHANNEL::FICC_BLUE );

		FreeImage_Unload( red );
		FreeImage_Unload( blue );
	}

	Image.handle = bitmap;
	Image.data = FreeImage_GetBits( bitmap );
	Image.width = FreeImage_GetWidth( bitmap );
	Image.height = FreeImage_GetHeight( bitmap );
	Image.depth = FreeImage_GetBPP( bitmap );
	Image.pitch = FreeImage_GetPitch( bitmap );

	Image.rmask = 0x00ff0000;
	Image.gmask = 0x0000ff00;
	Image.bmask = 0x000000ff;
	Image.amask = ( Image.depth == 24 ) ? 0 : 0xff000000;

	return true;
}

//-------------------------------------------------------------------------//

/*
 * Выгрузить изображение
 * ---------------
 */

void le::Core::UnloadImage( const Image& Image )
{
	if ( !Image.handle ) return;
	FreeImage_Unload( static_cast< FIBITMAP* >( Image.handle ) );
}

//-------------------------------------------------------------------------//

/*
 * Инициализировать движок
 * ---------------
 */

bool le::Core::InitializeEngine( WindowHandle_t WindowHandle )
{
	LOG_INFO( "Initialization lifeEngine" );

	// Выводим системную информацию

	SDL_version		sdlVersion;
	SDL_GetVersion( &sdlVersion );

	LOG_INFO( "*** System info start ****" );
	LOG_INFO( "  Base path: " << SDL_GetBasePath() );
	LOG_INFO( "" );
	LOG_INFO( "  lifeEngine " << LIFEENGINE_VERSION_MAJOR << "." << LIFEENGINE_VERSION_MINOR << "." << LIFEENGINE_VERSION_PATCH << " (build " << ComputeBuildNumber( GOLD_DATE ) << ")" );
	LOG_INFO( "  SDL version: " << ( uint32_t ) sdlVersion.major << "." << ( uint32_t ) sdlVersion.minor << "." << ( uint32_t ) sdlVersion.patch );
	LOG_INFO( "  Platform: " << SDL_GetPlatform() );
	LOG_INFO( "  CPU cache L1 size: " << SDL_GetCPUCacheLineSize() << " bytes" );
	LOG_INFO( "  CPU cores: " << SDL_GetCPUCount() );
	LOG_INFO( "  RAM: " << SDL_GetSystemRAM() << " MB" );
	LOG_INFO( "  Has 3DNow: " << ( SDL_Has3DNow() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has AVX: " << ( SDL_HasAVX() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has AVX2: " << ( SDL_HasAVX2() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has AltiVec: " << ( SDL_HasAltiVec() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has MMX: " << ( SDL_HasMMX() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has RDTSC: " << ( SDL_HasRDTSC() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has SSE: " << ( SDL_HasSSE() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has SSE2: " << ( SDL_HasSSE2() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has SSE3: " << ( SDL_HasSSE3() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has SSE41: " << ( SDL_HasSSE41() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "  Has SSE42: " << ( SDL_HasSSE42() == SDL_TRUE ? "true" : "false" ) );
	LOG_INFO( "*** System info end ****" );

	// Инициализируем окно приложения. Если заголовок на окно пуст, 
	// то создаем свое окно, а иначе запоминаем заголовок окна

	window = new Window();

	if ( !WindowHandle )
		if ( !window->Create( DEFAULT_WINDOW_TITLE, configurations.windowWidth, configurations.windowHeight, configurations.isFullscreen ? SW_FULLSCREEN : SW_DEFAULT ) )
			throw "Fail create window";
		else
			window->SetHandle( WindowHandle );

	try
	{
		// Загружаем подсистемы движка
		
		engineSubsystems[ EST_SCENE_MANAGER ] = EngineSubsystem( new Scene(), nullptr );
		engineSubsystems[ EST_INPUT_SYSTEM ] = EngineSubsystem( new InputSystem(), nullptr );
		engineSubsystems[ EST_RESOURCE_MANAGER ] = EngineSubsystem( new ResourceSystem(), nullptr );
		engineSubsystems[ EST_GUI_SYSTEM ] = EngineSubsystem( new GuiSystem(), nullptr );
		engineSubsystems[ EST_RENDER_SYSTEM ] = EngineSubsystem( new RenderSystem(), nullptr );

		// Инициализируем подсистемы движка

		if ( 
			 !InitSubsystem( EST_INPUT_SYSTEM ) ||
			 !InitSubsystem( EST_RENDER_SYSTEM ) ||
			 !InitSubsystem( EST_RESOURCE_MANAGER ) ||
			 !InitSubsystem( EST_GUI_SYSTEM ) ||
			 !InitSubsystem( EST_SCENE_MANAGER ) )
			throw;
	}
	catch ( const string & Message )
	{
		LOG_ERROR( Message );
		return false;
	}
	catch ( ... )
	{
		return false;
	}

	// Регестрируем консольные команды и переменные

	IConsoleSystem*			consoleSystem = static_cast< le::IConsoleSystem* >( GetSubsystem( EST_CONSOLE_SYSTEM ) );
	if ( consoleSystem )
	{
		consoleSystem->RegisterCommand( "exit", bind( &Core::ConsoleCommand_Exit, this, placeholders::_1 ) );
		consoleSystem->RegisterCommand( "version", bind( &Core::ConsoleCommand_Version, this, placeholders::_1 ) );
	}

	LOG_INFO( "Initialized lifeEngine" );
	isInitEngine = true;
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Запустить симуляцию игры
 * ---------------
 */

void le::Core::RunSimulation()
{
	if ( !game )
	{
		LOG_ERROR( "Game not loaded" );
		return;
	}

	IInputSystem* inputSystem = static_cast< IInputSystem* >( GetSubsystem( EST_INPUT_SYSTEM ) );
	if ( !inputSystem )				throw "Subsystem " + EngineSubsystemTypeToString( EST_INPUT_SYSTEM ) + " not loaded";

	IRenderSystem* renderSystem = static_cast< IRenderSystem* >( GetSubsystem( EST_RENDER_SYSTEM ) );
	if ( !renderSystem )			throw "Subsystem " + EngineSubsystemTypeToString( EST_RENDER_SYSTEM ) + " not loaded";

	// Запускаем игровой цикл

	LOG_INFO( "*** Game logic start ***" );

	uint32_t	startTime = 0;
	uint32_t	deltaTime = 0;
	bool		isFocus = true;
	isRunSimulation = true;

	Event		event;

	while ( isRunSimulation )
	{
		inputSystem->Clear();

		// Обрабатываем события окна

		while ( window->PollEvent( event ) )
		{
			switch ( event.type )
			{
			case Event::ET_WINDOW_CLOSE:
				StopSimulation();
				break;

			case Event::ET_WINDOW_FOCUS_GAINED:
				isFocus = true;
				break;

			case Event::ET_WINDOW_FOCUS_LOST:
				isFocus = false;
				break;

			case Event::ET_KEY_PRESSED:
			case Event::ET_KEY_RELEASED:
			case Event::ET_MOUSE_MOVE:
			case Event::ET_MOUSE_PRESSED:
			case Event::ET_MOUSE_RELEASED:
			case Event::ET_MOUSE_WHEEL:
			case Event::ET_TEXT_INPUT:
				inputSystem->ApplyEvent( event );
				break;

			case Event::ET_WINDOW_RESIZE:
				renderSystem->ResizeViewport( 0, 0, event.windowResize.width, event.windowResize.height );
				break;

			default: break;
			}
		}

		// Если окно в фокусе, то обновляем кадр

		if ( isFocus )
		{
			startTime = SDL_GetTicks();

			game->Update( deltaTime );
			renderSystem->Render();

			deltaTime = ( SDL_GetTicks() - startTime );
		}
	}

	isRunSimulation = false;
	LOG_INFO( "*** Game logic end ***" );
}

//-------------------------------------------------------------------------//

/*
 * Остановить симуляцию игры
 * ---------------
 */

void le::Core::StopSimulation()
{
	isRunSimulation = false;

	// TODO: добавить очистку менеджеров ресурсов и объектов (моделей, камер и прочего)
}

// ------------------------------------------------------------------------------------ //
// Консольная команда "Выход"
// ------------------------------------------------------------------------------------ //
void le::Core::ConsoleCommand_Exit( const vector<string>& Arguments )
{
	StopSimulation();
}

// ------------------------------------------------------------------------------------ //
// Консольная команда "Версия движка"
// ------------------------------------------------------------------------------------ //
void le::Core::ConsoleCommand_Version( const vector<string>& Arguments )
{
	Version		version = GetVersion();

	LOG_INFO( "lifeEngine " << version.major << "." << version.minor << "." << version.path 
			  << " (build " << version.build << "  " << version.date << " " << version.time << ")" );
}

// ------------------------------------------------------------------------------------ //
// Задать конфигурации движка
// ------------------------------------------------------------------------------------ //
void le::Core::SetConfigurations( const Configurations& Configurations )
{
	configurations = Configurations;
}

// ------------------------------------------------------------------------------------ //
// Задать информацию об игре
// ------------------------------------------------------------------------------------ //
void le::Core::SetGameManifest( const GameManifest& GameManifest )
{
	gameManifest = GameManifest;
}

//-------------------------------------------------------------------------//

/*
 * Получить подсистему движка
 * ---------------
 */

le::IEngineSubsystem* le::Core::GetSubsystem( ENGINE_SUBSYSTEM_TYPE EngineSubsystemType ) const
{
	auto		itSubsystem = engineSubsystems.find( EngineSubsystemType );
	if ( itSubsystem == engineSubsystems.end() )		return nullptr;

	return itSubsystem->second.engineSubsystem;
}

// ------------------------------------------------------------------------------------ //
// Получить фабрику объектов движка
// ------------------------------------------------------------------------------------ //
le::IFactory* le::Core::GetFactory() const
{
	return ( le::IFactory* ) & objectFactory;
}

//-------------------------------------------------------------------------//

/*
 * Получить версию lifeEngine
 * ---------------
 */

le::Version le::Core::GetVersion() const
{
	return Version( LIFEENGINE_VERSION_MAJOR, LIFEENGINE_VERSION_MINOR, LIFEENGINE_VERSION_PATCH, ComputeBuildNumber( GOLD_DATE ), __DATE__, __TIME__ );
}

//-------------------------------------------------------------------------//

// ------------------------------------------------------------------------------------ //
// Инициализировать подсистему движка
// ------------------------------------------------------------------------------------ //
bool le::Core::InitSubsystem( ENGINE_SUBSYSTEM_TYPE EngineSubsystemType )
{
	try
	{
		auto			itSubsystem = engineSubsystems.find( EngineSubsystemType );
		if ( itSubsystem == engineSubsystems.end() )
			throw "Subsystem " + EngineSubsystemTypeToString( EngineSubsystemType ) + " not found in core";

		if ( !itSubsystem->second.engineSubsystem->Initialize( this ) )
			throw "Fail initialize subsystem " + EngineSubsystemTypeToString( EngineSubsystemType );
	}
	catch ( const string& Message )
	{
		LOG_ERROR( Message );
		return false;
	}

	LOG_INFO( "Subsystem " << EngineSubsystemTypeToString( EngineSubsystemType ) << " initialized" );
	return true;
}

/*
 * Загрузить логику игры
 * ---------------
 */

bool le::Core::LoadGame()
{
	LOG_INFO( "Loading game " << gameManifest.gameDir );

	try
	{
		// Если движок не инициализирован - выходим с ошибкой

		if ( !isInitEngine )
			throw "Engine not initialized";

		// Если ранее логика была загружена - завершаем прошлую игру
		// и выгружаем дин. библиотеку ее

		if ( game )		UnloadGame();

		// Загружаем динамическую библиотеку с логикой игры

		handleGameLib = SDL_LoadObject( ( gameManifest.gameDir + "/" LENGINE_GAME_LIB ).c_str() );
		if ( !handleGameLib )			throw SDL_GetError();

		Func_LE_CreateGame		LE_CreateGame = ( Func_LE_CreateGame ) SDL_LoadFunction( handleGameLib, "LE_CreateGame" );
		if ( !LE_CreateGame )		throw SDL_GetError();

		// Инициализируем окно игры (меняем название окна и его иконку)
		// а также настраиваем менеджеры ресурсов

		window->SetTitle( gameManifest.game.c_str() );

		if ( !gameManifest.icon.empty() )
		{
			Image			icon;

			if ( LoadImage( gameManifest.icon, icon, true ) )
			{
				window->SetIcon( icon );
				UnloadImage( icon );
			}
		}

		IResourceSystem* resourceSystem = static_cast< IResourceSystem* >( GetSubsystem( EST_RESOURCE_MANAGER ) );
		if ( !resourceSystem )	throw "Subsystem " + EngineSubsystemTypeToString( EST_RESOURCE_MANAGER ) + " not found";

		resourceSystem->GetFontManager()->SetDirectory( gameManifest.fontsDir );
		resourceSystem->GetMaterialManager()->SetDirectory( gameManifest.materialsDir );
		resourceSystem->GetMeshManager()->SetDirectory( gameManifest.modelsDir );
		resourceSystem->GetTextureManager()->SetDirectory( gameManifest.texturesDir );

		// Создаем и инициализируем игровую логику

		game = LE_CreateGame();

		if ( !game->Initialize( this ) )
			throw "Fail initialize game";
	}
	catch ( const char* Message )
	{
		LOG_ERROR( Message );
		return false;
	}

	LOG_INFO( "Loaded game" );
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Выгрузить логику игры
 * ---------------
 */

void le::Core::UnloadGame()
{
	if ( !game )		return;
	static_cast< IConsoleSystem* >( GetSubsystem( EST_CONSOLE_SYSTEM ) )->SetPrintConsoleCallback( nullptr );

	try
	{
		// Загружаем из библиотеки ф-ц для удаления логики игры

		Func_LE_DeleteGame		LE_DeleteGame = ( Func_LE_DeleteGame ) SDL_LoadFunction( handleGameLib, "LE_DeleteGame" );
		if ( !LE_DeleteGame )	throw SDL_GetError();

		// Останавливаем симуляцию игры и удаляем ее

		StopSimulation();
		LE_DeleteGame( game );
		SDL_UnloadObject( handleGameLib );
	}
	catch ( const char* Message )
	{
		LOG_ERROR( Message );
	}
}

//-------------------------------------------------------------------------//

/*
 * Задать каталог движка
 * ---------------
 */

void le::Core::SetEngineDir( const string& EngineDir )
{
	engineDir = EngineDir;
}

//-------------------------------------------------------------------------//

/*
 * Получить каталог движка
 * ---------------
 */

const string& le::Core::GetEngineDir() const
{
	return engineDir;
}

//-------------------------------------------------------------------------//

/*
 * Получить окно приложения
 * ---------------
 */

le::IWindow* le::Core::GetWindow() const
{
	return window;
}

// ------------------------------------------------------------------------------------ //
// Получить информацию об игре
// ------------------------------------------------------------------------------------ //
const le::Configurations& le::Core::GetConfigurations() const
{
	return configurations;
}

// ------------------------------------------------------------------------------------ //
// Получить информацию об игре
// ------------------------------------------------------------------------------------ //
const le::GameManifest& le::Core::GetGameManifest() const
{
	return gameManifest;
}

//-------------------------------------------------------------------------//

/*
 * Запущена ли симуляция игры
 * ---------------
 */

bool le::Core::IsRunSimulation() const
{
	return isRunSimulation;
}

//-------------------------------------------------------------------------//