//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <fstream>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
using namespace rapidjson;
using namespace std;

#include "engine/iresourcesystem.h"
#include "shadersinfo.h"
#include "rendersystem.h"
#include "texture.h"
#include "material.h"

#undef GetObject

//----------------------------------------------------------------------//

static unordered_map<string, le::SHADER_TYPE>			shadersName =		///< Ассоциативный массив где по названию шейдера получаем его тип
{
    { LENGINE_SHADER_MATERIAL, le::SHADER_TYPE::ST_MATERIAL }
};

//----------------------------------------------------------------------//

/*
 * Перевести JSON массив в glm::vec4
 * -----------------------------
 */

glm::vec4 JsonArrayToVec4( Value::Array& Array )
{
    try
    {
        if ( Array.Size() < 4 ) throw;

        for ( size_t index = 0, count = Array.Size(); index < count; ++index )
            if ( !Array[ index ].IsNumber() )
                throw;
    }
    catch ( ... )
    {
        return glm::vec4( 1.f, 1.f, 1.f, 1.f );
    }

    return glm::vec4( Array[ 0 ].GetFloat(), Array[ 1 ].GetFloat(), Array[ 2 ].GetFloat(), Array[ 3 ].GetFloat() );
}

//----------------------------------------------------------------------//

/*
 * Конструктор
 * -----------------------------
 */

le::Material::Material() :
    diffuseColor( 1.f, 1.f, 1.f, 1.f ),
    specular( 0 ),
    shininess( 1.f ),
    fragmentShaderFlags( SHADER_FLAGS::SF_NONE ),
    shaderType( SHADER_TYPE::ST_NONE ),
	referenceCount( 0 )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * -----------------------------
 */

le::Material::~Material()
{
    Clear();
}

//----------------------------------------------------------------------//

/*
 * Очистить материал
 * -----------------------------
 */

void le::Material::Clear()
{
    ambientColor = emissionColor = glm::vec4();
    diffuseColor = glm::vec4( 1.f, 1.f, 1.f, 1.f );
    specular = 0.f;
    shininess = 1.f;
    fragmentShaderFlags = SHADER_FLAGS::SF_NONE;
    shaderType = SHADER_TYPE::ST_NONE;

	for ( auto it = textureMaps.begin(), itEnd = textureMaps.end(); it != itEnd; ++it )
	{
		it->second->DecrementReferenceCount();

		if ( it->second->GetReferenceCount() <= 0 )
			resourceSystem->GetTextureManager()->Delete( it->second );
	}

    shadersCache.clear();
    textureMaps.clear();
}

//----------------------------------------------------------------------//

/*
 * Загрузить материал
 * -----------------------------
 */

bool le::Material::Load( const string& Route )
{
    ifstream			file( resourceSystem->GetMaterialManager()->GetDirectory() + "/" + Route );

    if ( !file.is_open() )
    {
        LOG_ERROR( "File material [" << Route << "] not found" );
        return false;
    }

    Document						document;
    string							strBuffer;

    getline( file, strBuffer, '\0' );
    document.Parse( strBuffer.c_str() );

    if ( document.HasParseError() )
    {
        LOG_ERROR( "Fail parse material [" << Route << "]" );
        return false;
    }

    // ******************
    // Считываем информацию о текстурах

    if ( document.HasMember( "textures" ) )
        if ( document[ "textures" ].IsArray() )
        {
            auto		array = document[ "textures" ].GetArray();

            for ( auto it = array.Begin(), itEnd = array.End(); it != itEnd; ++it )
            {
                if ( !it->IsObject() ) continue;

                string					route;
                auto					object = it->GetObject();
                int						layer = -1;

                if ( object.HasMember( "layer" ) && object[ "layer" ].IsInt() )
                    layer = object[ "layer" ].GetInt();

                if ( object.HasMember( "route" ) && object[ "route" ].IsString() )
                    route = object[ "route" ].GetString();

                if ( layer != -1 )
                {
					Texture*            texture = static_cast< Texture* >( resourceSystem->GetTextureManager()->Load( route ) );
                    if ( !texture )     continue;

					texture->IncrementReferenceCount();
                    textureMaps[ layer ] = texture;
                }
                else
                    LOG_WARNING( "Texture [" << route << "] not exists value [layer]" );
            }
        }
        else
            LOG_WARNING( "Member [textures] not array" );

    // ******************
    // Считываем константы материала

    if ( document.HasMember( "constants" ) )
        if ( document[ "constants" ].IsObject() )
        {
            auto			object = document[ "constants" ].GetObject();

            if ( object.HasMember( "diffuse" ) && object[ "diffuse" ].IsArray() )
            {
                auto		array = object[ "diffuse" ].GetArray();
                diffuseColor = JsonArrayToVec4( array );
            }

            if ( object.HasMember( "ambient" ) && object[ "ambient" ].IsArray() )
            {
                auto		array = object[ "ambient" ].GetArray();
                ambientColor = JsonArrayToVec4( array );
            }

            if ( object.HasMember( "emission" ) && object[ "emission" ].IsArray() )
            {
                auto		array = object[ "emission" ].GetArray();
                emissionColor = JsonArrayToVec4( array );
            }

            if ( object.HasMember( "specular" ) && object[ "specular" ].IsNumber() )
                specular = object[ "specular" ].GetFloat();

            if ( object.HasMember( "shininess" ) && object[ "shininess" ].IsNumber() )
                shininess = object[ "shininess" ].GetFloat();
        }
        else
            LOG_WARNING( "Member [constants] not object" );

    // ******************
    // Считываем информацию о шейдере

    if ( document.HasMember( "shader" ) )
        if ( document[ "shader" ].IsObject() )
        {
            auto			object = document[ "shader" ].GetObject();

            if ( object.HasMember( "flags" ) && object[ "flags" ].IsNumber() )
                fragmentShaderFlags = object[ "flags" ].GetUint();

            if ( object.HasMember( "name" ) && object[ "name" ].IsString() )
            {
                const char*			shaderName = object[ "name" ].GetString();

                if ( shadersName.find( shaderName ) == shadersName.end() )
                {
                    LOG_ERROR( "Invalid shader name [" << shaderName << "]" );
                    return false;
                }

                shaderType = shadersName[ shaderName ];
            }
        }
        else
        {
            LOG_ERROR( "Member [shader] not object" );
            return false;
        }

    if ( shaderType == SHADER_TYPE::ST_NONE )
    {
        LOG_ERROR( "In material [" << Route << "] not selected shader" );
        return false;
    }

    return true;
}

//----------------------------------------------------------------------//

/*
 * Скопировать материал
 * -----------------------------
 */

void le::Material::Copy( IMaterial* Material )
{
	if ( !Material )	return;
	le::Material*		material = static_cast<le::Material*>( Material );
	
	fragmentShaderFlags = material->fragmentShaderFlags;
	shaderType = material->shaderType;
	ambientColor = material->ambientColor;
	diffuseColor = material->diffuseColor;
	emissionColor = material->emissionColor;
	specular = material->specular;
	shininess = material->shininess;
	shadersCache = material->shadersCache;
	textureMaps = material->textureMaps;

	for ( auto it = textureMaps.begin(), itEnd = textureMaps.end(); it != itEnd; ++it )
		it->second->IncrementReferenceCount();
}

//----------------------------------------------------------------------//

/*
 * Добавить текстуру на слой
 * -----------------------------
 */

void le::Material::AddTexture( ITexture* Texture, uint32_t Layer )
{
    if ( !Texture->IsLoaded() ) return;
    textureMaps[ Layer ] = static_cast<le::Texture*>( Texture );
}

//----------------------------------------------------------------------//

/*
 * Удалить текстуру со слоя
 * -----------------------------
 */

void le::Material::DeleteTexture( uint32_t Layer )
{
    if ( textureMaps.find( Layer ) != textureMaps.end() )
        textureMaps.erase( Layer );
}

//----------------------------------------------------------------------//

/*
 * Задать тип шейдера
 * -----------------------------
 */

void le::Material::SetShaderType( SHADER_TYPE ShaderType, uint32_t MaterialFlags )
{
    shadersCache.clear();
    shaderType = ShaderType;
    fragmentShaderFlags = MaterialFlags;
}

//----------------------------------------------------------------------//

/*
 * Задать флаги техник материала в шейдере
 * -----------------------------
 */

void le::Material::SetMaterialFlags( uint32_t MaterialFlags )
{
    fragmentShaderFlags = MaterialFlags;
}

//----------------------------------------------------------------------//

/*
 * Задать цвет рассеянного отражения
 * -----------------------------
 */

void le::Material::SetDiffuseColor( const glm::vec4& DiffuseColor )
{
    diffuseColor = DiffuseColor;
}

//----------------------------------------------------------------------//

/*
 * Задать цвет фонового отражения
 * -----------------------------
 */

void le::Material::SetAmbientColor( const glm::vec4& AmbientColor )
{
    ambientColor = AmbientColor;
}

//----------------------------------------------------------------------//

/*
 * Задать цвет собственного излучения
 * -----------------------------
 */

void le::Material::SetEmissionColor( const glm::vec4& EmissionColor )
{
    emissionColor = EmissionColor;
}

//----------------------------------------------------------------------//

/*
 * Задать фактор зеркального отражения
 * -----------------------------
 */

void le::Material::SetSpecular( float Specular )
{
    specular = Specular;
}

//----------------------------------------------------------------------//

/*
 * Задать фактор блеска отражения
 * -----------------------------
 */

void le::Material::SetShininess( float Shininess )
{
    shininess = Shininess;
}

//----------------------------------------------------------------------//

/*
 * Получить цвет рассеянного отражения
 * -----------------------------
 */

const glm::vec4& le::Material::GetDiffuseColor() const
{
    return diffuseColor;
}

//----------------------------------------------------------------------//

/*
 * Получить цвет фонового отражения
 * -----------------------------
 */

const glm::vec4& le::Material::GetAmbientColor() const
{
    return ambientColor;
}

//----------------------------------------------------------------------//

/*
 * Получить цвет cобственного излученияя
 * -----------------------------
 */

const glm::vec4& le::Material::GetEmissionColor() const
{
    return emissionColor;
}

//----------------------------------------------------------------------//

/*
 * Получить фактор зеркального отражения
 * -----------------------------
 */

float le::Material::GetSpecular() const
{
    return specular;
}

//----------------------------------------------------------------------//

/*
 * Получить фактор блеска отражения
 * -----------------------------
 */

float le::Material::GetShininess() const
{
    return shininess;
}

//----------------------------------------------------------------------//

/*
 * Получить тип шейдера для материала
 * -----------------------------
 */

le::SHADER_TYPE le::Material::GetShaderType() const
{
    return shaderType;
}

//----------------------------------------------------------------------//

/*
 * Получить флаги техник материала в шейдере
 * -----------------------------
 */

uint32_t le::Material::GetMaterialFlags() const
{
    return fragmentShaderFlags;
}

//----------------------------------------------------------------------//

/*
 * Получить шейдер материала
 * -----------------------------
 */

le::Shader_t le::Material::GetShader( uint32_t Flags )
{
    auto        it = shadersCache.find( Flags );
    if ( it != shadersCache.end() )
        return it->second;

    Shader_t    shader = shaderManager->LoadFromFile( shaderType, Flags );
    shadersCache[ Flags ] = shader;
    return shader;
}

//----------------------------------------------------------------------//

/*
 * Увеличить счетчик ссылок
 * ------------------
 */

void le::Material::IncrementReferenceCount()
{
	++referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Уменьшить счетчик ссылок
 * ------------------
 */

void le::Material::DecrementReferenceCount()
{
	--referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Получить количество ссылок
 * ------------------
 */

uint32_t le::Material::GetReferenceCount() const
{
	return referenceCount;
}

//----------------------------------------------------------------------//

/*
 * Получить текстуру из слоя
 * ------------------
 */

le::ITexture* le::Material::GetTexture( uint32_t Layer )
{
	auto		itTexture = textureMaps.find( Layer );
	if ( itTexture == textureMaps.end() ) return nullptr;

	return itTexture->second;
}

//----------------------------------------------------------------------//