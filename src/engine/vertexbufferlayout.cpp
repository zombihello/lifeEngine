//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

#include "engine/lifeEngine.h"
#include "vertexbufferlayout.h"

//----------------------------------------------------------------------//

/*
 * Конструктор
 * -----------------------------
 */

le::VertexBufferLayout::VertexBufferLayout() :
	stride( 0 )
{}

//----------------------------------------------------------------------//

/*
 * Получить размер типа в байтах
 * -----------------------------
 */

uint32_t le::VertexBufferElement::GetSizeOfType( uint32_t Type )
{
	switch ( Type )
	{
		case GL_FLOAT: 
		case GL_UNSIGNED_INT:	return 4;

		case GL_UNSIGNED_BYTE:	return 1;
	}

	LIFEENGINE_ASSERT( false, "Not correct type" );
	return 0;
}

//----------------------------------------------------------------------//

/*
 * Получить шаг в буфере вершин в байтах
 * -----------------------------
 */

const uint32_t& le::VertexBufferLayout::GetStride()
{
	return stride;
}

//----------------------------------------------------------------------//

/*
 * Получить массив элементов буфера вершин
 * -----------------------------
 */

const vector<le::VertexBufferElement>& le::VertexBufferLayout::GetElements()
{
	return elements;
}

//----------------------------------------------------------------------//