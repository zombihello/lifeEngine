//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <fstream>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
using namespace rapidjson;
using namespace std;

#include "common/log.h"
#include "shadermanager.h"
#include "shader.h"

//----------------------------------------------------------------------//

GLuint				le::Shader::currentProgramID = 0;

//----------------------------------------------------------------------//

//////////////////////////////////////////////////////////////////////
/// \brief Конфигурации шейдера
///
/// Данная структура хранит конфигурации 
/// шейдера (путь к шейдерам, значения по-умолчанию и т.д)
//////////////////////////////////////////////////////////////////////
struct ShaderConfig
{
	string							vertexShader;		///< Вершиный шейдер
	string							geometryShader;		///< Геометрический шейдер
	string							computeShader;		///< Вычеслительный шейдер
	string							fragmentShader;		///< Фрагментный шейдер
	unordered_map<string, int>		textureLayers;		///< Слои текстур
};

//----------------------------------------------------------------------//

/*
 * Загрузить конфигурации шейдера
 * ----------------------
 */

bool LoadShaderConfig( const string& Route, ShaderConfig& ShaderConfig )
{
	ifstream						file( Route );

	if ( !file.is_open() )
	{
		LOG_ERROR( "File shader config [" << Route << "] not found" );
		return false;
	}

	Document						document;
	string							strBuffer;

	// Считываем строки из файла и заносим их в буфер

	while ( !file.eof() )
	{
		string 		tempString;
		file >> tempString;
		strBuffer += tempString;
	}

	document.Parse( strBuffer.c_str() );

	if ( document.HasParseError() )
	{
		LOG_ERROR( "Fail parse file shader config [" << Route << "]" );
		return false;
	}

	for ( auto it = document.MemberBegin(), itEnd = document.MemberEnd(); it != itEnd; ++it )
	{
		if ( !it->value.IsObject() )
			continue;

		for ( auto itObject = it->value.MemberBegin(), itObjectEnd = it->value.MemberEnd(); itObject != itObjectEnd; ++itObject )
		{
			// ***********************
			// Пути к шейдерам

			if ( strcmp( it->name.GetString(), "shaders" ) == 0 )
			{
				// *** Путь к вершиному шейдеру ***
				if ( strcmp( itObject->name.GetString(), "vertex" ) == 0 && itObject->value.IsString() )
				{
					ShaderConfig.vertexShader = itObject->value.GetString();
					ShaderConfig.vertexShader = le::shaderManager->GetShadersDir() + "/" + ShaderConfig.vertexShader;
				}

				// *** Путь к геометрическому шейдеру ***
				else if ( strcmp( itObject->name.GetString(), "geometry" ) == 0 && itObject->value.IsString() )
				{
					ShaderConfig.geometryShader = itObject->value.GetString();
					ShaderConfig.geometryShader = le::shaderManager->GetShadersDir() + "/" + ShaderConfig.geometryShader;
				}

				// *** Путь к вычислительному шейдеру ***
				else if ( strcmp( itObject->name.GetString(), "compute" ) == 0 && itObject->value.IsString() )
				{
					ShaderConfig.computeShader = itObject->value.GetString();
					ShaderConfig.computeShader = le::shaderManager->GetShadersDir() + "/" + ShaderConfig.computeShader;
				}

				// *** Путь к фрагментному шейдеру ***
				else if ( strcmp( itObject->name.GetString(), "fragment" ) == 0 && itObject->value.IsString() )
				{
					ShaderConfig.fragmentShader = itObject->value.GetString();
					ShaderConfig.fragmentShader = le::shaderManager->GetShadersDir() + "/" + ShaderConfig.fragmentShader;
				}
			}

			// ***********************
			// Параметры текстурных слоев

			else if ( strcmp( it->name.GetString(), "textureLayers" ) == 0 )
			{
				if ( itObject->value.IsNumber() )
					ShaderConfig.textureLayers[ itObject->name.GetString() ] = itObject->value.GetInt();
			}
		}
	}

	return true;
}

//----------------------------------------------------------------------//

/*
 * Считать код шейдера с файла
 * -----------------------------
 */

bool GetShaderCode( const string& RouteToShader, string& ShaderCode )
{
	ifstream			shaderFile( RouteToShader );

	if ( !shaderFile.is_open() )
		return false;

	getline( shaderFile, ShaderCode, '\0' );
	shaderFile.close();
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Скомпилировать вершиный шейдер
 * -----------------------------
 */

bool le::Shader::CompileVertexShader( const char* ShaderCode )
{
	int						errorCompilation = 0;

	vertexID = glCreateShader( GL_VERTEX_SHADER );
	glShaderSource( vertexID, 1, &ShaderCode, NULL );
	glCompileShader( vertexID );

	// *********************************
	// Проверяем результат компиляции	

	glGetShaderiv( vertexID, GL_COMPILE_STATUS, &errorCompilation );

	if ( errorCompilation != GL_TRUE )
	{
		int				lengthLog = 0;
		char* errorMessage;

		glGetShaderiv( vertexID, GL_INFO_LOG_LENGTH, &lengthLog );
		errorMessage = new char[ lengthLog ];

		glGetShaderInfoLog( vertexID, lengthLog, &lengthLog, errorMessage );

		LOG_ERROR( "**** Shader error ****" );
		LOG_ERROR( "Failed to compile vertex shader:" );
		LOG_ERROR( "Messagee:\n" << errorMessage );
		LOG_ERROR( "**** Shader error end ****" );

		delete[] errorMessage;
		Delete();

		return false;
	}

	glAttachShader( programID, vertexID );
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Скомпилировать геометрический шейдер
 * -----------------------------
 */

bool le::Shader::CompileGeometryShader( const char* ShaderCode )
{
	int						errorCompilation = 0;

	geometryID = glCreateShader( GL_GEOMETRY_SHADER );
	glShaderSource( geometryID, 1, &ShaderCode, NULL );
	glCompileShader( geometryID );

	// *********************************
	// Проверяем результат компиляции	

	glGetShaderiv( geometryID, GL_COMPILE_STATUS, &errorCompilation );

	if ( errorCompilation != GL_TRUE )
	{
		int					lengthLog = 0;
		char* errorMessage;

		glGetShaderiv( geometryID, GL_INFO_LOG_LENGTH, &lengthLog );
		errorMessage = new char[ lengthLog ];

		glGetShaderInfoLog( geometryID, lengthLog, &lengthLog, errorMessage );

		LOG_ERROR( "**** Shader error ****" );
		LOG_ERROR( "Failed to compile geometry shader:" );
		LOG_ERROR( "Messagee:\n" << errorMessage );
		LOG_ERROR( "**** Shader error end ****" );

		delete[] errorMessage;
		Delete();

		return false;
	}

	glAttachShader( programID, geometryID );
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Скомпилировать вычеслительный шейдер
 * -----------------------------
 */

bool le::Shader::CompileComputeShader( const char* ShaderCode )
{
	int					errorCompilation = 0;

	computeID = glCreateShader( GL_COMPUTE_SHADER );
	glShaderSource( computeID, 1, &ShaderCode, NULL );
	glCompileShader( computeID );

	// *********************************
	// Проверяем результат компиляции	

	glGetShaderiv( computeID, GL_COMPILE_STATUS, &errorCompilation );

	if ( errorCompilation != GL_TRUE )
	{
		int					lengthLog = 0;
		char* errorMessage;

		glGetShaderiv( computeID, GL_INFO_LOG_LENGTH, &lengthLog );
		errorMessage = new char[ lengthLog ];

		glGetShaderInfoLog( computeID, lengthLog, &lengthLog, errorMessage );

		LOG_ERROR( "**** Shader error ****" );
		LOG_ERROR( "Failed to compile compute shader:" );
		LOG_ERROR( "Messagee:\n" << errorMessage );
		LOG_ERROR( "**** Shader error end ****" );

		delete[] errorMessage;
		Delete();

		return false;
	}

	glAttachShader( programID, computeID );
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Скомпилировать фрагментный шейдер
 * -----------------------------
 */

bool le::Shader::CompileFragmentShader( const char* ShaderCode )
{
	int					errorCompilation = 0;

	fragmentID = glCreateShader( GL_FRAGMENT_SHADER );
	glShaderSource( fragmentID, 1, &ShaderCode, NULL );
	glCompileShader( fragmentID );

	// *********************************
	// Проверяем результат компиляции	

	glGetShaderiv( fragmentID, GL_COMPILE_STATUS, &errorCompilation );

	if ( errorCompilation != GL_TRUE )
	{
		int					lengthLog = 0;
		char* errorMessage;

		glGetShaderiv( fragmentID, GL_INFO_LOG_LENGTH, &lengthLog );
		errorMessage = new char[ lengthLog ];

		glGetShaderInfoLog( fragmentID, lengthLog, &lengthLog, errorMessage );

		LOG_ERROR( "**** Shader error ****" );
		LOG_ERROR( "Failed to compile fragment shader:" );
		LOG_ERROR( "Messagee:\n" << errorMessage );
		LOG_ERROR( "**** Shader error end ****" );

		delete[] errorMessage;
		Delete();

		return false;
	}

	glAttachShader( programID, fragmentID );
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ----------------------
 */

le::Shader::Shader() :
	vertexID( 0 ),
	geometryID( 0 ),
	computeID( 0 ),
	fragmentID( 0 ),
	programID( 0 )
{}

//----------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------------
 */

le::Shader::~Shader()
{
	Delete();
}

//----------------------------------------------------------------------//

/*
 * Загрузить шейдер с файла
 * ----------------------
 */

bool le::Shader::LoadFromFile( const string& ShaderRoute, uint32_t Flags )
{
	ShaderConfig		shaderConfig = {};
	if ( !LoadShaderConfig( ShaderRoute, shaderConfig ) )
		return false;

	string				vertexShader_Code;
	string				geometryShader_Code;
	string				computeShader_Code;
	string				fragmentShader_Code;

	if ( !shaderConfig.vertexShader.empty() && !GetShaderCode( shaderConfig.vertexShader, vertexShader_Code ) )
	{
		LOG_ERROR( "Vertex shader [" << shaderConfig.vertexShader << "] not found" );
		return false;
	}

	if ( !shaderConfig.geometryShader.empty() && !GetShaderCode( shaderConfig.geometryShader, geometryShader_Code ) )
	{
		LOG_ERROR( "Geometry shader [" << shaderConfig.geometryShader << "] not found" );
		return false;
	}

	if ( !shaderConfig.computeShader.empty() && !GetShaderCode( shaderConfig.computeShader, computeShader_Code ) )
	{
		LOG_ERROR( "Compute shader [" << shaderConfig.computeShader << "] not found" );
		return false;
	}

	if ( !shaderConfig.fragmentShader.empty() && !GetShaderCode( shaderConfig.fragmentShader, fragmentShader_Code ) )
	{
		LOG_ERROR( "Fragment shader [" << shaderConfig.fragmentShader << "] not found" );
		return false;
	}

	if ( !Compile(
		!vertexShader_Code.empty() ? vertexShader_Code.c_str() : nullptr,
		!geometryShader_Code.empty() ? geometryShader_Code.c_str() : nullptr,
		!computeShader_Code.empty() ? computeShader_Code.c_str() : nullptr,
		!fragmentShader_Code.empty() ? fragmentShader_Code.c_str() : nullptr,
		Flags ) )
		return false;

	if ( !shaderConfig.textureLayers.empty() )
	{
		Bind();

		for ( auto it = shaderConfig.textureLayers.begin(), itEnd = shaderConfig.textureLayers.end(); it != itEnd; ++it )
		{
			if ( IsUniformExists( it->first ) )
				SetUniform( it->first, it->second );
		}

		Unbind();
	}

	return true;
}

//----------------------------------------------------------------------//

/*
 * Удалить шейдер
 * ----------------------
 */

void le::Shader::Delete()
{
	if ( programID == 0 ) return;

	glDeleteShader( vertexID );
	glDeleteShader( geometryID );
	glDeleteShader( computeID );
	glDeleteShader( fragmentID );
	glDeleteProgram( programID );
	uniforms.clear();
}

//----------------------------------------------------------------------//

/*
 * Скомпилировать шейдер
 * ----------------------
 */

bool le::Shader::Compile( const char* VertexShader, const char* GeometryShader, const char* ComputeShader, const char* FragmentShader, uint32_t Flags )
{
	string					defineCode;

	if ( Flags > SF_NONE )
	{
		if ( Flags & SF_DIFFUSE_MAP )
			defineCode += "#define DIFFUSE_MAP\n";

		if ( Flags & SF_EMISSION_MAP )
			defineCode += "#define EMISSION_MAP\n";

		if ( Flags & SF_NORMAL_MAP )
			defineCode += "#define NORMAL_MAP\n";

		if ( Flags & SF_SPECULAR_MAP )
			defineCode += "#define SPECULAR_MAP\n";
	}

	// Удаляем шейдер, если был уже создан
	if ( programID != 0 )
		Delete();

	programID = glCreateProgram();

	// *************************
	// Создаем вершинный шейдер

	if ( VertexShader )
	{
		if ( !defineCode.empty() )
		{
			string				codeShader = VertexShader;
			size_t				versionIndex = codeShader.find( "#version" );

			if ( versionIndex == -1 )
				LOG_WARNING( "Missing #version xxx in vertex shader" );
			else
			{
				size_t				defineInsertPoint = codeShader.find( "\n", versionIndex ) + 1;
				codeShader.insert( defineInsertPoint, defineCode );
			}

			if ( !CompileVertexShader( codeShader.c_str() ) )
				return false;
		}
		else if ( !CompileVertexShader( VertexShader ) )
			return false;
	}

	// *************************
	// Создаем геометрический шейдер

	if ( GeometryShader )
	{
		if ( !defineCode.empty() )
		{
			string				codeShader = GeometryShader;
			size_t				versionIndex = codeShader.find( "#version" );

			if ( versionIndex == -1 )
				LOG_WARNING( "Missing #version xxx in geometry shader" );
			else
			{
				size_t				defineInsertPoint = codeShader.find( "\n", versionIndex ) + 1;
				codeShader.insert( defineInsertPoint, defineCode );
			}

			if ( !CompileGeometryShader( codeShader.c_str() ) )
				return false;
		}
		else if ( !CompileGeometryShader( GeometryShader ) )
			return false;
	}

	// *************************
	// Создаем вычеслительный шейдер

	if ( ComputeShader )
	{
		if ( !defineCode.empty() )
		{
			string				codeShader = ComputeShader;
			size_t				versionIndex = codeShader.find( "#version" );

			if ( versionIndex == -1 )
				LOG_WARNING( "Missing #version xxx in compute shader" );
			else
			{
				size_t				defineInsertPoint = codeShader.find( "\n", versionIndex ) + 1;
				codeShader.insert( defineInsertPoint, defineCode );
			}

			if ( !CompileComputeShader( codeShader.c_str() ) )
				return false;
		}
		else if ( !CompileComputeShader( ComputeShader ) )
			return false;
	}

	// *************************
	// Создаем фрагментный шейдер

	if ( FragmentShader )
	{
		if ( !defineCode.empty() )
		{
			string				codeShader = FragmentShader;
			size_t				versionIndex = codeShader.find( "#version" );

			if ( versionIndex == -1 )
				LOG_WARNING( "Missing #version xxx in fragment shader" );
			else
			{
				size_t				defineInsertPoint = codeShader.find( "\n", versionIndex ) + 1;
				codeShader.insert( defineInsertPoint, defineCode );
			}

			if ( !CompileFragmentShader( codeShader.c_str() ) )
				return false;
		}
		else if ( !CompileFragmentShader( FragmentShader ) )
			return false;
	}

	// *********************************
	// Линкуем программу и проверяем на ошибки

	glLinkProgram( programID );

	int			errorLink = 0;
	glGetProgramiv( programID, GL_LINK_STATUS, &errorLink );

	if ( errorLink != GL_TRUE )
	{
		int				lengthLog = 0;
		char* errorMessage;

		glGetProgramiv( programID, GL_INFO_LOG_LENGTH, &lengthLog );
		errorMessage = new char[ lengthLog ];

		glGetProgramInfoLog( programID, lengthLog, &lengthLog, errorMessage );

		LOG_ERROR( "**** Shader error ****" );
		LOG_ERROR( "Failed to link shader" );
		LOG_ERROR( "Messagee:\n" << errorMessage );
		LOG_ERROR( "**** Shader error end ****" );

		delete[] errorMessage;
		Delete();

		return false;
	}

	return true;
}

//----------------------------------------------------------------------//

/*
 * Получить идентификатор расположения юниформ-переменной
 * ----------------------
 */

int le::Shader::GetUniformLocation( const string& NameUniform )
{
	// Провиряем кеш юниформов
	auto it = uniforms.find( NameUniform );

	// Нашли в кеше - возвращаем позицию с него
	if ( it != uniforms.end() )
		return it->second;
	else
	{
		// В кеше не нашли, то тогда запрашиваем позицию в OpenGL'е
		int			location = glGetUniformLocation( programID, NameUniform.c_str() );
		uniforms.insert( make_pair( NameUniform, location ) );

		if ( location == -1 )
			LOG_ERROR( "Uniform [" << NameUniform << "] not found in shader" );

		return location;
	}
}

//----------------------------------------------------------------------//

/*
 * Существует ли юниформ-переменная в шейдере
 * ----------------------
 */

bool le::Shader::IsUniformExists( const string& NameUniform )
{
	// Провиряем кеш юниформов
	auto it = uniforms.find( NameUniform );

	// Нашли в кеше - возвращаем true если позиция больше -1
	if ( it != uniforms.end() )
		return it->second > -1;
	else
	{
		// В кеше не нашли, то тогда запрашиваем позицию в OpenGL'е
		int			location = glGetUniformLocation( programID, NameUniform.c_str() );
		uniforms.insert( make_pair( NameUniform, location ) );

		return location > -1;
	}
}

//----------------------------------------------------------------------//
