//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (��������� �����) ***
//				Copyright (C) 2018-2019
//
// ����������� ������:  https://gitlab.com/zombihello/lifeEngine
// ������:				���� �������� (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <mutex>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <Windows.h>
#include <GL/wglew.h>

#include "engine/lifeEngine.h"
#include "engine/settingscontext.h"
#include "engine/rendercontext.h"
#include "common/log.h"
#include "wglcontext.h"

//---------------------------------------------------------------------//

namespace le
{
	//---------------------------------------------------------------------//

	///////////////////////////////////////////////////////////////////////////
	/// \brief ���������� ���������
	///////////////////////////////////////////////////////////////////////////
	struct ContextDescriptor
	{
		///////////////////////////////////////////////////////////////////////////
		/// \brief �����������
		///////////////////////////////////////////////////////////////////////////
		ContextDescriptor( HDC DeviceContext, HGLRC RenderContext ) :
			deviceContext( DeviceContext ),
			renderContext( RenderContext )
		{}

		HDC			deviceContext;			///< �������� ����������
		HGLRC		renderContext;			///< �������� ������������
	};

	//---------------------------------------------------------------------//
}

//---------------------------------------------------------------------//

static le::ContextDescriptor*				currentContext = nullptr;			///< ������� ������������� ��������

//---------------------------------------------------------------------//


//---------------------------------------------------------------------//

/*
 * ������� �������� ������������
 * -----------------
 */

bool le::WinGL_CreateContext( WindowHandle_t WindowHandle, const SettingsContext& SettingsContext, ContextDescriptor_t& ContextDescriptor, ContextDescriptor_t* ShareContext )
{
	PIXELFORMATDESCRIPTOR				pixelFormatDesc = {};
	pixelFormatDesc.nSize = sizeof( PIXELFORMATDESCRIPTOR );
	pixelFormatDesc.nVersion = 1;
	pixelFormatDesc.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;
	pixelFormatDesc.iLayerType = PFD_MAIN_PLANE;
	pixelFormatDesc.cColorBits = SettingsContext.redBits + SettingsContext.greenBits + SettingsContext.blueBits + SettingsContext.alphaBits;
	pixelFormatDesc.cDepthBits = SettingsContext.depthBits;
	pixelFormatDesc.cStencilBits = SettingsContext.stencilBits;

	HDC						deviceContext = GetDC( static_cast< HWND >( WindowHandle ) );
	HGLRC					renderContext = nullptr;

	le::ContextDescriptor*	shareContext = ShareContext ? static_cast< le::ContextDescriptor* >( *ShareContext ) : nullptr;
	int						pixelFormat = ChoosePixelFormat( deviceContext, &pixelFormatDesc );
	bool					isSetProfile = false;

	try
	{
		// ������� ���������� ������ ��� ������ ��������

		if ( pixelFormat != 0 )
		{
			PIXELFORMATDESCRIPTOR			bestMatch_PixelFormatDescd;
			DescribePixelFormat( deviceContext, pixelFormat, sizeof( PIXELFORMATDESCRIPTOR ), &bestMatch_PixelFormatDescd );

			if ( bestMatch_PixelFormatDescd.cDepthBits < pixelFormatDesc.cDepthBits )
				LOG_WARNING( "Depth bits: " << pixelFormatDesc.cDepthBits << " not supported, seted to " << bestMatch_PixelFormatDescd.cDepthBits );

			if ( SetPixelFormat( deviceContext, pixelFormat, &pixelFormatDesc ) == FALSE )
				throw "Failed seted pixel fromat";
		}
		else throw "Failed choose pixel format";

		renderContext = wglCreateContext( deviceContext );
		if ( !renderContext )
			throw "Creating temporary render context fail. Code error: " + to_string( GetLastError() );

		if ( !wglMakeCurrent( deviceContext, renderContext ) )
			throw "Selecting temporary render context fail. Code error: " + to_string( GetLastError() );

		// ��������� ���������� OpenGL

		glewExperimental = GL_TRUE;
		if ( glewInit() != GLEW_OK )	throw "OpenGL context is broken";

		// ������� ���������� �� OpenGL

		int			numberExtensions = 0;
		glGetIntegerv( GL_NUM_EXTENSIONS, &numberExtensions );

		LOG_INFO( "*** OpenGL info ***" );
		LOG_INFO( "  OpenGL version: " << glGetString( GL_VERSION ) );
		LOG_INFO( "  OpenGL vendor: " << glGetString( GL_VENDOR ) );
		LOG_INFO( "  OpenGL renderer: " << glGetString( GL_RENDERER ) );
		LOG_INFO( "  OpenGL GLSL version: " << glGetString( GL_SHADING_LANGUAGE_VERSION ) );
		LOG_INFO( "  OpenGL extensions:" );

		for ( uint32_t index = 0; index < numberExtensions; ++index )
		{
			const uint8_t*		extension = glGetStringi( GL_EXTENSIONS, index );
			LOG_INFO( extension );
		}

		LOG_INFO( "*** OpenGL info end ***" );

		// ���� ���� ��������� wglCreateContextAttribsARB - ���������� ��,
		// ����� ����� �������� � ������� �����������

		if ( wglewIsSupported( "WGL_ARB_create_context" ) == 1 )
		{
			vector<int>				attributes;

			// ��������� ��� �� �������� OpenGL ������ 1.1

			if ( SettingsContext.majorVersion > 1 || ( SettingsContext.majorVersion == 1 && SettingsContext.minorVersion > 1 ) )
			{
				attributes.push_back( WGL_CONTEXT_MAJOR_VERSION_ARB );
				attributes.push_back( SettingsContext.majorVersion );
				attributes.push_back( WGL_CONTEXT_MINOR_VERSION_ARB );
				attributes.push_back( SettingsContext.minorVersion );
			}

			// ���������, �������������� �� ��������� �������

			if ( wglewIsSupported( "WGL_ARB_create_context_profile" ) == 1 )
			{
				int				profile = ( SettingsContext.attributeFlags & le::SettingsContext::CA_CORE ) ? WGL_CONTEXT_CORE_PROFILE_BIT_ARB : WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB;
				int				debug = ( SettingsContext.attributeFlags & le::SettingsContext::CA_DEBUG ) ? WGL_CONTEXT_DEBUG_BIT_ARB : 0;

				attributes.push_back( WGL_CONTEXT_PROFILE_MASK_ARB );
				attributes.push_back( profile );
				attributes.push_back( WGL_CONTEXT_FLAGS_ARB );
				attributes.push_back( debug );

				isSetProfile = true;
			}
			else if ( ( SettingsContext.attributeFlags & le::SettingsContext::CA_CORE ) || ( SettingsContext.attributeFlags & le::SettingsContext::CA_DEBUG ) )
					LOG_WARNING( "Selecting a profile during context creation is not supported, disabling comptibility and debug" );

			// ��������� ����������� 0

			attributes.push_back( 0 );
			attributes.push_back( 0 );

			if ( shareContext )
			{
				static mutex		mutex;
				mutex.lock();

				if ( shareContext == currentContext )
				{
					if ( !wglMakeCurrent( shareContext->deviceContext, nullptr ) )
						throw "Failed to deactivate shared context before sharing. Code error: " + to_string( GetLastError() );

					currentContext = nullptr;
				}

				mutex.unlock();
			}

			HGLRC			oldContext = renderContext;
			renderContext = wglCreateContextAttribsARB( deviceContext, shareContext ? shareContext->renderContext : nullptr, &attributes[ 0 ] );

			if ( renderContext )
				wglDeleteContext( oldContext );
			else
				renderContext = oldContext;
		}
		else if ( shareContext )
		{
			static mutex		mutex;
			mutex.lock();

			if ( shareContext == currentContext )
			{
				if ( !wglMakeCurrent( shareContext->deviceContext, nullptr ) )
					throw "Failed to deactivate shared context before sharing. Code error: " + to_string( GetLastError() );

				currentContext = nullptr;
			}

			if ( !wglShareLists( shareContext->renderContext, renderContext ) )
				throw "Failed to share the OpenGL context. Code error: " + to_string( GetLastError() );

			mutex.unlock();
		}

	}
	catch ( const string& Message )
	{
		LOG_ERROR( Message );
		SDL_ShowSimpleMessageBox( SDL_MESSAGEBOX_ERROR, "Error", Message.c_str(), nullptr );
		return false;
	}

	le::ContextDescriptor*			contextDescriptor = new le::ContextDescriptor( deviceContext, renderContext );
	ContextDescriptor = contextDescriptor;
	WinGL_MakeCurrentContext( contextDescriptor );

	LOG_INFO( "Context OpenGL " << glGetString( GL_VERSION ) << 
			  ( isSetProfile && ( SettingsContext.attributeFlags & le::SettingsContext::CA_CORE ) ? " core" : "" ) <<
			  ( isSetProfile && ( SettingsContext.attributeFlags & le::SettingsContext::CA_DEBUG ) ? " debug" : "" ) <<
			  " created" );

	return true;
}

//---------------------------------------------------------------------//

/*
 * ������� �������� ��������
 * -----------------
 */

bool le::WinGL_MakeCurrentContext( const ContextDescriptor_t& ContextDescriptor )
{
	if ( static_cast< le::ContextDescriptor* >( ContextDescriptor ) == currentContext ) return true;

	if ( !wglMakeCurrent( static_cast< le::ContextDescriptor* >( ContextDescriptor )->deviceContext, static_cast< le::ContextDescriptor* >( ContextDescriptor )->renderContext ) )
		return false;

	currentContext = static_cast< le::ContextDescriptor* >( ContextDescriptor );
	return true;
}

//---------------------------------------------------------------------//

/*
 * ������� ��������
 * -----------------
 */

void le::WinGL_DeleteContext( ContextDescriptor_t& ContextDescriptor )
{
	if ( static_cast< le::ContextDescriptor* >( ContextDescriptor ) == currentContext )
	{
		wglMakeCurrent( nullptr, nullptr );
		currentContext = nullptr;
	}

	wglDeleteContext( static_cast< le::ContextDescriptor* >( ContextDescriptor )->renderContext );
	DeleteDC( static_cast< le::ContextDescriptor* >( ContextDescriptor )->deviceContext );
	delete static_cast< le::ContextDescriptor* >( ContextDescriptor );
	ContextDescriptor = nullptr;
}

//---------------------------------------------------------------------//

/*
 * �������� ������ � ���������
 * -----------------
 */

void le::WinGL_SwapBuffers( const ContextDescriptor_t& ContextDescriptor )
{
	if ( static_cast< le::ContextDescriptor* >( ContextDescriptor ) != currentContext ) return;
	SwapBuffers( static_cast< le::ContextDescriptor* >( ContextDescriptor )->deviceContext );
}

//---------------------------------------------------------------------//

/*
 * ������������ ������������ �������������
 * -----------------
 */

void le::WinGL_SetVerticalSync( bool IsEnable )
{		
	if ( wglewIsSupported( "WGL_EXT_swap_control" ) == 1 )
	{
		int			interval = 0;

		if ( IsEnable )
			interval = wglewIsSupported( "WGL_EXT_swap_control_tear" ) == 1 ? -1 : 1;

		if ( !wglSwapIntervalEXT( interval ) )
			LOG_ERROR( "Setting vertical sync failed: " << to_string( GetLastError() ) );
	}
	else
	{
		static bool			isWarned = false;

		if ( isWarned )
		{
			LOG_WARNING( "Setting vertical sync not supported" );
			isWarned = true;
		}
	}
}

//---------------------------------------------------------------------//