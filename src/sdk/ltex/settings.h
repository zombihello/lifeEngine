//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>
#include <list>
#include <Compressonator/Compressonator.h>
using namespace std;

//---------------------------------------------------------------------//

struct File
{
	File( const string& Path, const string& Name, const string& Format ) :
		path( Path ),
		name( Name ),
		format( Format )
	{}

	string			name;		// Название файла
	string			format;		// Формат файла
	string			path;		// Путь к файлу
};

//---------------------------------------------------------------------//

struct Settings
{
	static bool				isGenerateMipmaps;	// Генерировать ли мипмапы
	static CMP_FORMAT		format;				// Формат выходных текстур

	static string			outDirectory;		// Выходной каталог
	static string			srcDirectory;		// Исходный каталог
	static list<File>		srcFiles;			// Исходные файлы
};

//---------------------------------------------------------------------//

#endif // !SETTINGS_H
