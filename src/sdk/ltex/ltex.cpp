//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <filesystem>
#include <FreeImage/FreeImage.h>
#include <Compressonator/Compressonator.h>
#include <gli/gli.hpp>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include "settings.h"
namespace fs = std::experimental::filesystem;
using namespace std;

#define GOLD_DATE				43376			// Oct 5 2019 (начало разработки ltex)
#define LTEX_VERSION_MAJOR		0
#define LTEX_VERSION_MINOR		1
#define LTEX_VERSION_PATCH		0

//---------------------------------------------------------------------//

/*
* Высчитать номер сборки
* -----------------
*/

int ComputeBuildNumber( int GoldDate )
{
	const char* date = __DATE__;

	const char* month[ 12 ] =
	{ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	const char		month_days[ 12 ] =
	{ 31,    28,    31,    30,    31,    30,    31,    31,    30,    31,    30,    31 };

	int				buildNumber = 0;
	int				months = 0;
	int				days = 0;
	int				years = 0;

	for ( months = 0; months < 11; ++months )
	{
		if ( strncmp( &date[ 0 ], month[ months ], 3 ) == 0 )
			break;

		days += month_days[ months ];
	}

	days += atoi( &date[ 4 ] ) - 1;
	years = atoi( &date[ 7 ] ) - 1900;

	buildNumber = days + static_cast< int >( ( years - 1 ) * 365.25f );

	if ( ( years % 4 == 0 ) && months > 1 )
		++buildNumber;

	buildNumber -= GoldDate;
	return buildNumber;
}

//---------------------------------------------------------------------//

/*
* Соответсвует ли строка маске
* ------------------------------
*/

bool IsMatchMask( const string& Name, const string& Mask )
{
	uint32_t		indexMask = 0;
	uint32_t		indexName = 0;
	uint32_t		countMask = Mask.size();
	uint32_t		countName = Name.size();

	while ( indexName < countName || indexMask < countMask )
	{
		if ( indexName + 1 < countName && Name[ indexName ] == '.' &&
			( Name[ indexName + 1 ] == '/' || Name[ indexName + 1 ] == '\\' ) )
		{
			indexName += 2;
			continue;
		}

		else if ( Mask[ indexMask ] == '*' )
		{
			++indexMask;
			if ( indexMask + 1 >= countMask ) break;

			while ( indexName < countName )
			{
				if ( Name[ indexName ] == Mask[ indexMask ] )
					break;

				++indexName;
			}

			continue;
		}

		else if ( Mask[ indexMask ] != '/' && Mask[ indexMask ] != '\\' &&
				  Name[ indexName ] != '/' && Name[ indexName ] != '\\' &&
				  Name[ indexName ] != Mask[ indexMask ] )
			return false;

		++indexName;
		++indexMask;
	}

	return true;
}

//---------------------------------------------------------------------//

/*
* Вывод использования утилиты
* ------------------------------
*/

void PrintUsage()
{
	cout << "-------------------------\n"
		<< "lifeEngine Texture " << LTEX_VERSION_MAJOR << "." << LTEX_VERSION_MINOR << "." << LTEX_VERSION_PATCH << " (build " << ComputeBuildNumber( GOLD_DATE ) << ") by Egor Pogulyaka\n"
		<< "Converter textures to KTX format\n"
		<< "-------------------------\n\n"
		<< "Usage: ltex [options] [files]\n"
		<< "Example: ltex -outdir outDir *.tga monster1/*.jpg\n\n"
		<< "-h | -help\t->\tshow this message\n"
		<< "-od | -outdir\t->\tset out directory for textures\n"
		<< "-sd | -srcdir\t->\tset source directory with textures\n"
		<< "-mm | -mipmap\t->\tmipmaps generate\n"
		<< "-f | -format\t->\tset out format textures\n"
		<< "Supported formats: RED, RG, RGBA, BC1, BC2, BC3, BC4, BC5, BC6H, BC6HSF\n"
		<< "Default format: RGBA\n";
}

//---------------------------------------------------------------------//

/*
* Пропарсить аргументы запуска
* ------------------------------
*/

void ParseArgs( int argc, char** argv )
{
	uint32_t		lastIndexArg = 1;

	// Парсим аргументы запуска

	for ( uint32_t index = 1; index < argc; ++index )
	{
		// Вывод информации об использовании утилиты

		if ( strstr( argv[ index ], "-h" ) || strstr( argv[ index ], "-help" ) )
		{
			PrintUsage();
			exit( 0 );
		}

		// Выходной каталог для текстур

		else if ( ( strstr( argv[ index ], "-od" ) || strstr( argv[ index ], "-outdir" ) ) && index + 1 < argc )
		{
			Settings::outDirectory = argv[ index + 1 ];
			++index;
		}

		// Исходный каталог с текстурами

		else if ( ( strstr( argv[ index ], "-sd" ) || strstr( argv[ index ], "-srcdir" ) ) && index + 1 < argc )
		{
			Settings::srcDirectory = argv[ index + 1 ];
			++index;
		}

		// Генерировать ли мипмап уровни

		else if ( strstr( argv[ index ], "-mm" ) || strstr( argv[ index ], "-mipmap" ) )
			Settings::isGenerateMipmaps = true;

		// Формат выходных текстур

		else if ( ( strstr( argv[ index ], "-f" ) || strstr( argv[ index ], "-format" ) ) && index + 1 < argc )
		{
			if ( strcmp( argv[ index + 1 ], "RED" ) == 0 )
				Settings::format = CMP_FORMAT_R_8;

			else if ( strcmp( argv[ index + 1 ], "RG" ) == 0 )
				Settings::format = CMP_FORMAT_RG_8;

			else if ( strcmp( argv[ index + 1 ], "RGBA" ) == 0 )
				Settings::format = CMP_FORMAT_RGBA_8888;

			else if ( strcmp( argv[ index + 1 ], "BC1" ) == 0 )
				Settings::format = CMP_FORMAT_BC1;

			else if ( strcmp( argv[ index + 1 ], "BC2" ) == 0 )
				Settings::format = CMP_FORMAT_BC2;

			else if ( strcmp( argv[ index + 1 ], "BC3" ) == 0 )
				Settings::format = CMP_FORMAT_BC3;

			else if ( strcmp( argv[ index + 1 ], "BC4" ) == 0 )
				Settings::format = CMP_FORMAT_BC4;

			else if ( strcmp( argv[ index + 1 ], "BC5" ) == 0 )
				Settings::format = CMP_FORMAT_BC5;

			else if ( strcmp( argv[ index + 1 ], "BC6H" ) == 0 )
				Settings::format = CMP_FORMAT_BC6H;

			else if ( strcmp( argv[ index + 1 ], "BC6HSF" ) == 0 )
				Settings::format = CMP_FORMAT_BC6H_SF;

			else cout << "Unknown format, defaults to RGBA\n";

			++index;
		}

		// Иначе продолжем парсить
		else continue;

		// Если мы нашли аргумент, то запоминаем следующий индекс.
		// Там могут быть пути к текстурам
		lastIndexArg = index + 1;
	}

	// Забираем из аргументов запуска пути к текстурам

	string		srcFile;
	string		srcDir;

	for ( uint32_t index = lastIndexArg; index < argc; ++index )
	{
		srcFile = argv[ index ];
		srcDir.clear();

		// Заменяем в пути к текстуре '\' на '/'

		uint32_t	indexSlash = srcFile.find_first_of( '\\' );
		while ( indexSlash != string::npos )
		{
			srcFile[ indexSlash ] = '/';
			indexSlash = srcFile.find_first_of( '\\' );
		}

		indexSlash = srcFile.find_last_of( '/' );
		if ( indexSlash != string::npos )
		{
			srcDir = srcFile;
			srcDir.erase( indexSlash, srcFile.size() );
		}

		for ( const auto& file : fs::directory_iterator( srcDir.empty() ? Settings::srcDirectory : srcDir ) )
		{
			if ( !fs::is_regular_file( file.status() ) )
				continue;

			string fileName = file.path().u8string();

			if ( !IsMatchMask( fileName, srcFile ) )
				continue;

			string		path = file.path().u8string();
			string		name = file.path().filename().u8string();
			string		format = file.path().extension().u8string();

			// Убираем формат в названии файла

			if ( name.find( format ) != string::npos )
				name.erase( name.find( format ), format.size() );

			Settings::srcFiles.push_back( File( path, name, format ) );
		}
	}
}

//---------------------------------------------------------------------//

/*
* Вывод прогресса сжатия
* ------------------------------
*/

bool CompressionCallback( float Progress, CMP_DWORD_PTR User1, CMP_DWORD_PTR User2 )
{
	printf( "\rCompression progress = %3.0f", Progress );
	return false;
}

//---------------------------------------------------------------------//

/*
* Поменять красный и синий канал местами
* --------------------------------------------
*/

void Texture_SwitchRedAndBlueChannels( FIBITMAP* Bitmap )
{
	auto		red = FreeImage_GetChannel( Bitmap, FREE_IMAGE_COLOR_CHANNEL::FICC_RED );
	auto		blue = FreeImage_GetChannel( Bitmap, FREE_IMAGE_COLOR_CHANNEL::FICC_BLUE );

	FreeImage_SetChannel( Bitmap, blue, FREE_IMAGE_COLOR_CHANNEL::FICC_RED );
	FreeImage_SetChannel( Bitmap, red, FREE_IMAGE_COLOR_CHANNEL::FICC_BLUE );

	FreeImage_Unload( red );
	FreeImage_Unload( blue );
}

//---------------------------------------------------------------------//

void Texture_GenerateMipmaps( gli::texture2d& Texture2d, CMP_FORMAT TextureFormat, const gli::extent2d& Size, uint32_t MipmapLevels )
{
	GLuint			glTexture = 0;
	GLuint			format = GL_RGBA;

	switch ( TextureFormat )
	{
	case CMP_FORMAT_R_8:		format = GL_RED;	break;
	case CMP_FORMAT_RG_8:		format = GL_RG;		break;
	case CMP_FORMAT_RGBA_8888:
	default:					format = GL_RGBA;	break;
	}

	glGenTextures( 1, &glTexture );
	glBindTexture( GL_TEXTURE_2D, glTexture );

	glTexImage2D( GL_TEXTURE_2D, 0, format, Size.x, Size.y, 0, format, GL_UNSIGNED_BYTE, Texture2d.data() );
	glGenerateMipmap( GL_TEXTURE_2D );

	for ( int level = 0; level < MipmapLevels; ++level )
		glGetTexImage( GL_TEXTURE_2D, level, format, GL_UNSIGNED_BYTE, Texture2d.data( 0, 0, level ) );

	glDeleteTextures( 1, &glTexture );
	glBindTexture( GL_TEXTURE_2D, 0 );
}

//---------------------------------------------------------------------//

/*
* Конвертировать текстуру
* -----------------
*/

CMP_ERROR Texture_Convert( CMP_FORMAT FormatConvert, const gli::texture2d& SrcTexture, gli::texture2d& DstTexture, uint32_t MipmapLevels, const CMP_CompressOptions& CMP_Options )
{
	gli::extent2d		srcSize;
	gli::extent2d		dstSize;

	CMP_ERROR			cmp_Status;
	CMP_Texture			cmp_Texture = { 0 };
	cmp_Texture.dwSize = sizeof( CMP_Texture );
	cmp_Texture.format = CMP_FORMAT_RGBA_8888;

	CMP_Texture			cmp_TextureCompress = { 0 };
	cmp_TextureCompress.dwSize = sizeof( CMP_Texture );
	cmp_TextureCompress.format = FormatConvert;

	for ( int level = 0; level < MipmapLevels; ++level )
	{
		srcSize = SrcTexture.extent( level );
		dstSize = DstTexture.extent( level );

		cmp_Texture.dwWidth = srcSize.x;
		cmp_Texture.dwHeight = srcSize.y;
		cmp_Texture.dwDataSize = CMP_CalculateBufferSize( &cmp_Texture );
		cmp_Texture.pData = ( CMP_BYTE* ) SrcTexture.data( 0, 0, level );

		cmp_TextureCompress.dwWidth = dstSize.x;
		cmp_TextureCompress.dwHeight = dstSize.y;
		cmp_TextureCompress.dwDataSize = CMP_CalculateBufferSize( &cmp_TextureCompress );
		cmp_TextureCompress.pData = ( CMP_BYTE* ) DstTexture.data( 0, 0, level );

		cmp_Status = CMP_ConvertTexture( &cmp_Texture, &cmp_TextureCompress, &CMP_Options, &CompressionCallback, 0, 0 );

		if ( cmp_Status != CMP_OK )
		{
			cout << "\rMipmap level: " << level + 1 << ". Compression fail, status: " << cmp_Status << endl;
			break;
		}
		else
			cout << "\rMipmap level: " << level + 1 << " compressed\n";
	}

	return cmp_Status;
}

//---------------------------------------------------------------------//

/*
* Точка входа
* ------------------------------
*/

int main( int argc, char** argv )
{
	if ( argc > 1 )
	{
		// Парсим аргументы запуска

		ParseArgs( argc, argv );

		// Нашли ли мы текстуры, которые нужно конвертировать?

		if ( Settings::srcFiles.empty() )
		{
			cout << "Source files not found or not set\n";
			return 1;
		}

		if ( !Settings::outDirectory.empty() && !fs::exists( Settings::outDirectory ) && !fs::create_directory( Settings::outDirectory ) )
		{
			cout << "Fail created directory [" << Settings::outDirectory << "]\n";
			return 1;
		}

		// Нужно ли генерировать мипмап уровни, если да
		// то с помощью SDL2 создаем контекст OpenGL'a
		// через который будем на GPU генерировать их

		SDL_Window*			window = nullptr;
		SDL_GLContext		glContext = nullptr;

		if ( Settings::isGenerateMipmaps )
		{
			try
			{
				SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
				SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 );
				SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

				window = SDL_CreateWindow( "", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1, 1, SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN );
				if ( !window ) throw SDL_GetError();

				glContext = SDL_GL_CreateContext( window );
				if ( !glContext ) throw SDL_GetError();

				if ( glewInit() != GLEW_OK )
					throw "OpenGL context is broken";
			}
			catch ( const char* Message )
			{
				SDL_ShowSimpleMessageBox( SDL_MESSAGEBOX_ERROR, "Error", Message, nullptr );
				return 1;
			}
		}

		bool								isNeedCompress = Settings::format != CMP_FORMAT_R_8 && Settings::format != CMP_FORMAT_RG_8 && Settings::format != CMP_FORMAT_RGBA_8888;
		gli::format							dstFormat;
		gli::extent2d						size;
		gli::texture2d						srcTexture;
		gli::texture2d						dstTexture;
		string								outFileName;

		CMP_CompressOptions					cmp_Options = { 0 };
		cmp_Options.dwSize = sizeof( CMP_CompressOptions );
		cmp_Options.fquality = 8;
		cmp_Options.bDisableMultiThreading = true;

		switch ( Settings::format )
		{
		case CMP_FORMAT_R_8:			dstFormat = gli::FORMAT_R8_UNORM_PACK8;				break;
		case CMP_FORMAT_RG_8:			dstFormat = gli::FORMAT_RG8_UNORM_PACK8;			break;
		case CMP_FORMAT_RGBA_8888:		dstFormat = gli::FORMAT_RGBA8_UNORM_PACK8;			break;
		case CMP_FORMAT_BC1:			dstFormat = gli::FORMAT_RGBA_DXT1_UNORM_BLOCK8;		break;
		case CMP_FORMAT_BC2:			dstFormat = gli::FORMAT_RGBA_DXT3_UNORM_BLOCK16;	break;
		case CMP_FORMAT_BC3:			dstFormat = gli::FORMAT_RGBA_DXT5_UNORM_BLOCK16;	break;
		case CMP_FORMAT_BC4:			dstFormat = gli::FORMAT_R_ATI1N_UNORM_BLOCK8;		break;
		case CMP_FORMAT_BC5:			dstFormat = gli::FORMAT_RG_ATI2N_UNORM_BLOCK16;		break;
		case CMP_FORMAT_BC6H:			dstFormat = gli::FORMAT_RGB_BP_UFLOAT_BLOCK16;		break;
		case CMP_FORMAT_BC6H_SF:		dstFormat = gli::FORMAT_RGB_BP_SFLOAT_BLOCK16;		break;
		}

		// Проходимся по всем файлам и конвертируем

		for ( auto it = Settings::srcFiles.begin(), itEnd = Settings::srcFiles.end(); it != itEnd; ++it )
		{
			File*		file = &*it;

			cout << "\n\nTexture: " << it->path << endl;
			cout << "----------------\n";

			// Получаем типа файла

			FREE_IMAGE_FORMAT		imageFormat = FIF_UNKNOWN;
			imageFormat = FreeImage_GetFileType( it->path.c_str(), 0 );

			if ( imageFormat == FIF_UNKNOWN )
				imageFormat = FreeImage_GetFIFFromFilename( it->path.c_str() );

			// Загружаем изображение

			FIBITMAP*		bitmap = FreeImage_Load( imageFormat, it->path.c_str(), 0 );
			if ( !bitmap )
			{
				cout << "Loading fail\n";
				continue;
			}
			else
				cout << "Loaded file\n";

			// Конвертируем изображение в 32 битную глубину

			FIBITMAP*		temp = bitmap;
			bitmap = FreeImage_ConvertTo32Bits( bitmap );
			FreeImage_Unload( temp );

			// Если конвертируем в RGBA, то меняем местами красный и синий,
			// ибо FreeImape по-умолчанию грузит в BGRA

			if ( Settings::format == CMP_FORMAT_RGBA_8888 )
				Texture_SwitchRedAndBlueChannels( bitmap );

			outFileName = Settings::outDirectory + "/" + file->name + ".ktx";
			size = gli::extent2d( FreeImage_GetWidth( bitmap ), FreeImage_GetHeight( bitmap ) );
			uint32_t			mipmapLevels = Settings::isGenerateMipmaps ? gli::levels( size ) : 1;

			srcTexture = gli::texture2d( gli::FORMAT_RGBA8_UNORM_PACK8, size, mipmapLevels );
			dstTexture = gli::texture2d( dstFormat, size, mipmapLevels );
			memcpy( srcTexture.data(), FreeImage_GetBits( bitmap ), size.x * size.y * 4 );

			// Если нам ненужна компрессия, то
			// просто конвертируем формат и по необходимости
			// генерируем мипмап уровни

			if ( !isNeedCompress )
			{
				if ( Texture_Convert( Settings::format, srcTexture, dstTexture, mipmapLevels, cmp_Options ) != CMP_OK )
					continue;

				if ( Settings::isGenerateMipmaps )
					Texture_GenerateMipmaps( dstTexture, Settings::format, size, mipmapLevels );
			}

			// Иначе, если нужна компрессия, то генерируем мипмап
			// уровни (если нужны) и сжимает текстуру

			else
			{
				if ( Settings::isGenerateMipmaps )
					Texture_GenerateMipmaps( srcTexture, CMP_FORMAT_RGBA_8888, size, mipmapLevels );

				if ( Texture_Convert( Settings::format, srcTexture, dstTexture, mipmapLevels, cmp_Options ) != CMP_OK )
					continue;
			}

			gli::save( dstTexture, outFileName );
			cout << "\rFile saved\n";

			FreeImage_Unload( bitmap );
		}
	}
	else
		PrintUsage();

	return 0;
}

//---------------------------------------------------------------------//