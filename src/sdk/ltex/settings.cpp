//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "settings.h"

//---------------------------------------------------------------------//

bool				Settings::isGenerateMipmaps = false;
CMP_FORMAT			Settings::format = CMP_FORMAT_RGBA_8888;

string				Settings::outDirectory = "./";
string				Settings::srcDirectory = "./";
list<File>			Settings::srcFiles;

//---------------------------------------------------------------------//