//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <qwidget.h>
#include <qlibrary.h>
#include <qtimer.h>

#include "engine/icamera.h"

//-------------------------------------------------------------------------//

class Viewport : public QWidget
{
	Q_OBJECT

public:
	/* Конструктор */
	Viewport( QWidget *parent = nullptr );

	/* Деструктор */
	~Viewport();

	/* Событие отображения виджета на экране */
	void showEvent( QShowEvent* Event );

	/* Событие отрисовки виджета */
    void paintEvent( QPaintEvent* Event );

	/* Событие изменения размеров виджета */
	void resizeEvent( QResizeEvent* Event );

	/* Получить движок отрисовки виджета */
    QPaintEngine* paintEngine() const;

signals:
	/* Сигнал об обновлении кадра */
	void Update( float DeltaTime );

	/* Сигнал об изменении размеров вьюпорта */
	void Resize( const QSize& NewSize );

private slots:
    /* Метод обновления виджета */
    void Render();

private:
    bool            isInit;
    float           deltaTime;

    QTimer          timerUpdate;
};

//-------------------------------------------------------------------------//

#endif // !VIEWPORT_H
