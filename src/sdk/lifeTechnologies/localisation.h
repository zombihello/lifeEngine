//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef LOCALISATION_H
#define LOCALISATION_H

#include "local.h"
#include <vector>
using namespace std;

//-------------------------------------------------------------------------//

class Localisation
{
public:
    /* Загрузить файл локализаций */
   static bool LoadFromFile( const QString& DirLocalisations, const QString& FileName = "localisations.json" );

   /* Получить перевод текста */
   static QString Translate( const QString& Key );

   /* Получить перевод текста */
   static QString Translate( const QString& Key, vector<QString>& Arguments );

   /* Задать текущую локализацию */
   static bool SetLocale( const QString& Key );

   /* Задать текущую локализацию */
   static bool SetLocale( unsigned int Index );

   /* Получить название локализации */
   static inline const QString& GetLocale( unsigned int Index )
   {
	   if ( Index >= localSettings.size() ) return "";
	   return localSettings[ Index ].key;
   }

   /* Получить ID локализации */
   static inline uint32_t GetLocale( const QString& Key )
   {
	   for ( uint32_t index = 0, count = localSettings.size(); index < count; ++index )
		   if ( localSettings[ index ].key == Key )
			   return index;

	   return 0;
   }
   
   /* Получить название текущей локализации */
   static inline QString GetNameCurrentLocale()
   {
	   if ( idCurrentLocale >= localSettings.size() ) return "";
	   return localSettings[ idCurrentLocale ].key;
   }

   /* Получить ID текущей локализации */
   static inline uint32_t GetIDCurrentLocale()
   {
	   return idCurrentLocale;
   }

   /* Получить директорию с локализацией */
   static inline const QString& GetDirLocalisations()
   {
       return dirLocalisations;
   }

   /* Получить массив настроек локолизации */
   static inline const vector<LocaleSettings>& GetLocaleSettings()
   {
       return localSettings;
   }

private:
	static uint32_t							idCurrentLocale;
	static QString							dirLocalisations;
	static Local							locale;
	static vector<LocaleSettings>			localSettings;
};

//-------------------------------------------------------------------------//

#endif // !LOCALISATION_H
