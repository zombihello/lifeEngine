//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "tooladdmodel.h"
#include "engineapi.h"
#include "brush.h"
#include "window_editorscene.h"

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------------
 */

ToolAddModel::ToolAddModel() :
	activeModel( nullptr ),
	isOnScene( false )
{}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ---------------------
 */

ToolAddModel::~ToolAddModel()
{
	HideAddableObject();
}

//-------------------------------------------------------------------------//

/*
 * Добавить объект на сцену
 * ---------------------
 */

void ToolAddModel::AddObject( const le::Ray& Ray )
{
	if ( !activeModel ) return;

	float t = -Ray.origin.y / Ray.direction.y;
	float worldX = Ray.origin.x + Ray.direction.x * t;
	float worldZ = Ray.origin.z + Ray.direction.z * t;

	Brush* brush = level->CreateBrush();
	brush->GetModel()->SetMesh( activeModel->GetMesh() );
	brush->SetPosition( glm::vec3( worldX, 0, worldZ ) );
}

//-------------------------------------------------------------------------//

/*
 * Показать добавляемый объект
 * ---------------------
 */

void ToolAddModel::ShowAddableObject( const QString& Route )
{
	if ( !engineAPI.GetRenderSystem() || !engineAPI.GetScene() )	return;
	if ( !activeModel )		activeModel = engineAPI.GetRenderSystem()->CreateModel();

	if ( !isOnScene )
	{
		engineAPI.GetScene()->AddModel( activeModel );
		isOnScene = true;
	}

	if ( !activeModel->Load( Route.toStdString(), Route.toStdString() ) )
		HideAddableObject();
	else
	{
		vector<le::IMaterial*>			materials = activeModel->GetMesh()->GetMaterials();
		for ( uint32_t index = 0, count = materials.size(); index < count; ++index )
		{
			le::IMaterial*			material = engineAPI.GetMaterialManager()->Create( "add_" + to_string( index ) );
			if ( !material )		material = engineAPI.GetMaterialManager()->Get( "add_" + to_string( index ) );

			material->Copy( materials[ index ] );
			material->SetDiffuseColor( glm::vec4( 0, 1, 0, 0.5 ) );
			activeModel->AddCustomMaterial( index, material );
		}
	}
}

//-------------------------------------------------------------------------//

/*
 * Скрыть добавляемый объект
 * ---------------------
 */

void ToolAddModel::HideAddableObject()
{
	if ( !activeModel || !engineAPI.GetScene() ) return;

	engineAPI.GetScene()->RemoveModel( activeModel );
	engineAPI.GetRenderSystem()->DeleteModel( activeModel );

	activeModel = nullptr;
	isOnScene = false;
}

//-------------------------------------------------------------------------//