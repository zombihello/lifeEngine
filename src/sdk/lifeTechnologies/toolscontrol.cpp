//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qdebug.h>
#include "toolscontrol.h"

//-------------------------------------------------------------------------//

ToolsControl			toolsControl;

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ----------------------------------------
 */

ToolsControl::ToolsControl() :
	selectTypeTool( TT_NONE ),
	isInitialize( false )
{}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------------------------------
 */

ToolsControl::~ToolsControl()
{}

//-------------------------------------------------------------------------//

/*
 * Инициализировать контролы для работы
 * ----------------------------------------
 */

void ToolsControl::Initialize()
{
	toolMove.Initialize();
	isInitialize = true;
}

//-------------------------------------------------------------------------//

/*
 * Добавить метод обратного вызова
 * ----------------------------------------
 */

void ToolsControl::AddCallback( QObject* Object, ObjectCallback_t ObjectCallback )
{
	callbacks.insert( Object, ObjectCallback );
}

//-------------------------------------------------------------------------//

/*
 * Удалить метод обратного вызова
 * ----------------------------------------
 */

void ToolsControl::RemoveCallback( QObject* Object )
{
	callbacks.remove( Object );
}

//-------------------------------------------------------------------------//

/*
 * Задать тип выбранного инструмента
 * ----------------------------------------
 */

void ToolsControl::SetSelectTypeTool( TOOLS_TYPE ToolType )
{
	// Если контроллер инструментов не был
	// инициализирован, то выходим

	if ( !isInitialize )
	{
		static bool			isShowError = false;

		if ( !isShowError )
		{
			qCritical() << "Tools control not initialized";
			isShowError = true;
		}

		return;
	}

	// Деактивируем старые инструменты 

	switch ( selectTypeTool )
	{
	case TT_SELECT:
	case TT_MOVE:
	case TT_ROTATE:
	case TT_SCALE:
		if ( ToolType == TT_ADD_MODEL ||
			 ToolType == TT_ADD_ENTITY )
			toolSelect.UnselectObject();

		if ( selectTypeTool == TT_MOVE )		toolMove.HideControl();
		break;

	case TT_ADD_MODEL:
		toolAddModel.HideAddableObject();
		break;
	}

	// Рассылаем сообщение об смене типа инструмента

	for ( auto it = callbacks.begin(), itEnd = callbacks.end(); it != itEnd; ++it )
		( it.key()->*it.value() )( ToolType );

	selectTypeTool = ToolType;

	// Если нам манипуляторы для трансформации не нужны или объект не выбран, то выходим

	if ( !toolSelect.GetSelectedObject() ||
		 ToolType == TT_SELECT ||
		 ToolType == TT_ADD_MODEL ||
		 ToolType == TT_ADD_ENTITY )
		return;

	// Иначе показываем контрол в зависимости от выбраного инструмента

	switch ( ToolType )
	{
	case TT_MOVE:
		toolMove.ShowControl();
		break;

	case TT_ROTATE:
	case TT_SCALE:
		break;
	}
}

//-------------------------------------------------------------------------//