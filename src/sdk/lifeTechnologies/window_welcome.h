//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef WINDOW_WELCOME_H
#define WINDOW_WELCOME_H

#include <qdialog.h>
#include "ui_window_welcome.h"
#include "configuration.h"
#include "settings.h"

//-------------------------------------------------------------------------//

class Window_Welcome : public QDialog
{
	Q_OBJECT

public:
	enum DIALOG_CODE
	{
		DC_EXIT,
		DC_OPEN_EDITOR
	};

	/* Конструктор */
	Window_Welcome( Settings& Settings, QWidget* parent = Q_NULLPTR );

	/* Деструктор */
	~Window_Welcome();

	/* Получить конфигурацию редактора */
	static DIALOG_CODE GetConfiguration( Configuration& Configuration, Settings& Settings );

private slots:
	/* Событие смены языка */
	void on_comboBox_language_currentIndexChanged( int Index );

	/* Событие выбора конфигурации */
	void on_comboBox_game_currentIndexChanged( int Index );

	/* Событие нажатия кнопки "Открыть редактор" */
	void on_pushButton_startEditor_clicked();
	
	/* Событие нажатия кнопки "Настроить конфигурацию редактора" */
	void on_pushButton_configureEditor_clicked();

	/* Событие нажатия кнопки "Выход" */
	void on_pushButton_exit_clicked();

private:
	/* Обновить локализацию окна */
	void UpdateLocalisation();

	Settings*						settings;
	Configuration					selectConfiguration;
	Ui::Window_Welcome				ui;
};

//-------------------------------------------------------------------------//

#endif // !WINDOW_WELCOME_H