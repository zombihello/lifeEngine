//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "toolselect.h"
#include "window_editorscene.h"
#include "engineapi.h"

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------------
 */

ToolSelect::ToolSelect() :
	selectBrush( nullptr ),
	isSelectedObject( false )
{}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ---------------------
 */

ToolSelect::~ToolSelect()
{}

//-------------------------------------------------------------------------//

/*
 * Выбрать объект
 * ---------------------
 */

bool ToolSelect::SelectObject( const le::Ray& Ray, const glm::vec3& Position )
{
	if ( !engineAPI.GetRenderSystem() ) return false;
	if ( selectBrush )		UnselectObject();

	// Проходимся по всем объектам проверяя
	// пересечение луча с ними

	QHash<QUuid, Brush*>			brushes = level->GetBrushes();
	QList<Brush*>					intersectBrushes;

	for ( auto it = brushes.begin(), itEnd = brushes.end(); it != itEnd; ++it )
		if ( it.value()->IsIntersectRay( Ray ) )
			intersectBrushes.append( it.value() );

	// Если ни с кем луч не пересекся - выходим

	if ( intersectBrushes.empty() )		return false;

	// Если луч пересекся с объектами, то берем самый ближайший
	// объект

	float			minDistance = 0;
	selectBrush = *intersectBrushes.begin();
	minDistance = glm::distance( Position, selectBrush->GetPosition() );

	for ( auto it = intersectBrushes.begin(), itEnd = intersectBrushes.end(); it != itEnd; ++it )
	{
		float		distance = glm::distance( Position, ( *it )->GetPosition() );

		if ( distance < minDistance )
		{
			minDistance = distance;
			selectBrush = *it;
		}
	}

	if ( !selectBrush )		return false;

	// Объект выбран? Теперь подсвечиваем его красным цветом

	vector<le::IMaterial*>			materials = selectBrush->GetModel()->GetMesh()->GetMaterials();
	for ( uint32_t index = 0, count = materials.size(); index < count; ++index )
	{
		le::IMaterial*			material = engineAPI.GetMaterialManager()->Create( "select_" + to_string( index ) );
		if ( !material )		material = engineAPI.GetMaterialManager()->Get( "select_" + to_string( index ) );

		material->Copy( materials[ index ] );
		material->SetDiffuseColor( glm::vec4( 1, 0, 0, 1 ) );
		selectBrush->GetModel()->AddCustomMaterial( index, material );
	}

	// Показываем контрол в зависимости от его типа

	switch ( toolsControl.GetSelectTypeTool() )
	{
	case TT_MOVE:
		toolsControl.GetToolMove().ShowControl();
		break;
	}

	isSelectedObject = true;
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Выбрать объект
 * ---------------------
 */

void ToolSelect::UnselectObject()
{
	if ( !selectBrush ) return;

	// Убираем выделение с объекта

	selectBrush->GetModel()->ClearCustomMaterials();

	// Прячем контроллеры в зависимости от типа

	switch ( toolsControl.GetSelectTypeTool() )
	{
	case TT_MOVE:
		toolsControl.GetToolMove().HideControl();
		break;
	}

	selectBrush = nullptr;
	isSelectedObject = false;
}

//-------------------------------------------------------------------------//