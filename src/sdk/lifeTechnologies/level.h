//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef LEVEL_H
#define LEVEL_H

#include <qhash.h>
#include <quuid.h>

#include "brush.h"

//-------------------------------------------------------------------------//

class Level
{
public:
    /* Консруктор */
    Level();

    /* Деструктор */
    ~Level();

    /* Создать браш на уровень */
    Brush* CreateBrush( Brush::BRUSH_TYPE BrushType = Brush::BT_NONE );

    /* Удалить браш из уровня */
    void DeleteBrush( Brush* Brush );

	/* Очистить уровень */
	void Clear();

	/* Получить массив брашей */
	inline const QHash<QUuid, Brush*>& GetBrushes() const
	{
		return brushes;
	}

private:
	QHash<QUuid, Brush*>			brushes;
};

//-------------------------------------------------------------------------//

#endif // LEVEL_H
