//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "version.h"
#include <string>
using namespace std;

//-------------------------------------------------------------------------//

int ComputeBuildNumber( int GoldDate );

//-------------------------------------------------------------------------//

unsigned short			Version::major = 0;
unsigned short			Version::minor = 1;
unsigned short			Version::patch = 1;
unsigned int			Version::build = ComputeBuildNumber( 43217 ); // Apr 29 2019 (начало разработки lifeTechnologies)

//-------------------------------------------------------------------------//

const char*		date = __DATE__;

const char*		month[ 12 ] =
{ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

const char		month_days[ 12 ] =
{ 31,    28,    31,    30,    31,    30,    31,    31,    30,    31,    30,    31 };

//-------------------------------------------------------------------------//

/*
 * Высчитать номер сборки
 * -----------------
 */

int ComputeBuildNumber( int GoldDate )
{
	int				buildNumber = 0;
	int				months = 0;
	int				days = 0;
	int				years = 0;

	for ( months = 0; months < 11; ++months )
	{
		if ( strncmp( &date[ 0 ], month[ months ], 3 ) == 0 )
			break;

		days += month_days[ months ];
	}

	days += atoi( &date[ 4 ] ) - 1;
	years = atoi( &date[ 7 ] ) - 1900;

	buildNumber = days + static_cast< int >( ( years - 1 ) * 365.25f );

	if ( ( years % 4 == 0 ) && months > 1 )
		++buildNumber;

	buildNumber -= GoldDate;

	return buildNumber;
}

//-------------------------------------------------------------------------//

/*
 * Перевести в строку
 * ---------------------
 */

QString Version::ToString()
{
	return QString( QString::number( Version::major ) + "." + QString::number( Version::minor ) + "." + QString::number( Version::patch ) + " (build " + QString::number( Version::build ) + ")" );
}

//-------------------------------------------------------------------------//
