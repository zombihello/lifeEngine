//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (��������� �����) ***
//				Copyright (C) 2018-2019
//
// ����������� ������:  https://gitlab.com/zombihello/lifeEngine
// ������:				���� �������� (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "viewport.h"
#include "engine/logger.h"
#include "engine/version.h"
#include "engine/icamera.h"
#include "engine/ifont.h"
#include "engine/imaterial.h"
#include "engine/imesh.h"
#include "engine/dirsinfo.h"

#include "localisation.h"
#include "localkeys.h"
#include "level.h"
#include "brush.h"
#include "window_editorscene.h"
#include "engineapi.h"
#include "toolscontrol.h"

#include <qmessagebox.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <qaction.h>
#include <qdebug.h>
#include <qevent.h>
using namespace std;

le::ICamera*			uiCamera = nullptr;
le::IFont*				Font = nullptr;
le::IText*				text = nullptr;
le::IModel*                 lines = nullptr;
le::IMesh*                  meshLines = nullptr;
le::IMaterial*              material = nullptr;

//-------------------------------------------------------------------------//

/*
 * �����������
 * ---------------------
 */

Viewport::Viewport( QWidget *parent ) :
	QWidget( parent ),
	isInit( false ),
	deltaTime( 0.f )
{
	setAttribute( Qt::WA_PaintOnScreen );
}

//-------------------------------------------------------------------------//

/*
 * ����������
 * ---------------------
 */

Viewport::~Viewport()
{
	if ( uiCamera )
	{
		engineAPI.GetRenderSystem()->DeleteCamera( uiCamera );
		uiCamera = nullptr;
	}

	if ( text )
	{
		engineAPI.GetRenderSystem()->DeleteText( text );
		text = nullptr;
	}

	if ( Font )
	{
		engineAPI.GetFontManager()->Delete( Font );
		Font = nullptr;
	}

	if ( lines )
	{
		engineAPI.GetRenderSystem()->DeleteModel( lines );
		lines = nullptr;
	}

	isInit = false;

	disconnect( &timerUpdate, &QTimer::timeout, this, &Viewport::Render );
	timerUpdate.stop();
}

//-------------------------------------------------------------------------//

/*
 * ������� ����������� ������� �� ������
 * ---------------------
 */

void Viewport::showEvent( QShowEvent* Event )
{
	if ( !isInit )
	{	
		uiCamera = engineAPI.GetRenderSystem()->CreateCamera();
		text = engineAPI.GetRenderSystem()->CreateText();
		
		uiCamera->InitProjection_Ortho( 0.0f, ( float ) size().width(), 0.0f, ( float ) size().height(), -150.f, 150.f );

		Font = engineAPI.GetFontManager()->Create( "ui" );
		Font->Load( "ui.ttf" );

		engineAPI.GetRenderSystem()->SetActiveCamera( uiCamera, le::TL_UI );

		text->SetCharacterSize( 45 );
		text->SetFont( Font );
		text->SetText( "lifeEngine" );
		text->SetPosition( glm::vec3( 25, 25, 0 ) );

		lines = engineAPI.GetRenderSystem()->CreateModel();
		meshLines = engineAPI.GetMeshManager()->Create( "lines" );
		material = engineAPI.GetMaterialManager()->Create( "red" );
		le::IMesh::GeometryDescriptor		geometryDescriptor;
		le::IMesh::Surface					surface;

		material->SetShaderType( le::ST_MATERIAL, le::SF_NONE );
		material->SetDiffuseColor( glm::vec4( 122 / 255.f, 124 / 255.f, 126 / 255.f, 1 ) );

		for ( int i = -2500; i <= 2500; i += 32 )
		{
			le::IMesh::Vertex          vertex_1x = {};
			le::IMesh::Vertex          vertex_2x = {};
			le::IMesh::Vertex          vertex_1z = {};
			le::IMesh::Vertex          vertex_2z = {};

			vertex_1x.position = glm::vec3( i, 0, 2500 );
			vertex_2x.position = glm::vec3( i, 0, -2500 );
			vertex_1z.position = glm::vec3( 2500, 0, i );
			vertex_2z.position = glm::vec3( -2500, 0, i );

			geometryDescriptor.indeces.push_back( geometryDescriptor.verteces.size() );
			geometryDescriptor.indeces.push_back( geometryDescriptor.verteces.size() + 1 );
			geometryDescriptor.indeces.push_back( geometryDescriptor.verteces.size() + 2 );
			geometryDescriptor.indeces.push_back( geometryDescriptor.verteces.size() + 3 );

			geometryDescriptor.verteces.push_back( vertex_1x );
			geometryDescriptor.verteces.push_back( vertex_2x );
			geometryDescriptor.verteces.push_back( vertex_1z );
			geometryDescriptor.verteces.push_back( vertex_2z );
		}

		surface.materialId = 0;
		surface.startVertexIndex = 0;
		surface.countVertexIndex = geometryDescriptor.indeces.size();

		geometryDescriptor.materials.push_back( material );
		geometryDescriptor.surfaces.push_back( surface );
		meshLines->Create( geometryDescriptor, le::IMesh::RM_LINE );

		lines->SetMesh( meshLines );
		engineAPI.GetScene()->AddModel( lines );

		connect( &timerUpdate, &QTimer::timeout, this, &Viewport::Render );

		timerUpdate.start( 0 );
		isInit = true;
	}

	QWidget::showEvent( Event );
}

//-------------------------------------------------------------------------//

/*
 * ������� ��������� �������� �������
 * ---------------------
 */

void Viewport::resizeEvent( QResizeEvent* Event )
{
	if ( isInit )
	{
		engineAPI.GetRenderSystem()->ResizeViewport( 0, 0, width(), height() );
		uiCamera->InitProjection_Ortho( 0.0f, ( float ) size().width(), 0.0f, ( float ) size().height(), -150.f, 150.f );

		emit Resize( size() );
	}

	QWidget::resizeEvent( Event );
}

//-------------------------------------------------------------------------//

/*
 * ������� ��������� �������
 * ---------------------
 */

void Viewport::paintEvent( QPaintEvent* Event )
{}

//-------------------------------------------------------------------------//

/*
 * ������������ �����
 * ---------------------
 */

void Viewport::Render()
{
	if ( !isInit )		return;
	float				startTime = clock();

	if ( isEnabled() )
	{
		emit Update( deltaTime );
		engineAPI.GetTextRenderer()->Draw( text, le::TL_UI );		
	}

	engineAPI.GetRenderSystem()->Render();
	deltaTime = ( clock() - startTime ) / CLOCKS_PER_SEC;
}

//-------------------------------------------------------------------------//

/*
 * �������� ������ ��������� �������
 * ---------------------
 */

QPaintEngine* Viewport::paintEngine() const
{
	return nullptr;
}

//-------------------------------------------------------------------------//
