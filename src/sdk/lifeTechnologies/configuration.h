//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <qstring.h>

//-------------------------------------------------------------------------//

struct Configuration
{
	/* Конструктор */
	Configuration( const QString& Name = "", const QString GameDir = "" ) :
		name( Name ),
		gameDir( GameDir )
	{}

	QString				name;
	QString				gameDir;
};

//-------------------------------------------------------------------------//

#endif // !CONFIGURATION_H
