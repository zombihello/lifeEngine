//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qdebug.h>
#include <qmimedata.h>
#include <QMouseEvent>

#include "window_editorscene.h"
#include "exploer3dmodels.h"
#include "engineapi.h"
#include "toolscontrol.h"

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ----------------------------------------
 */

Exploer3DModels::Exploer3DModels( QWidget* parent ) :
	QTreeView( parent )
{
	fileSystemModel.setFilter( QDir::QDir::AllEntries | QDir::QDir::NoDotAndDotDot | QDir::QDir::NoSymLinks );
	fileSystemModel.setRootPath( "" );

	setModel( &fileSystemModel );
	setIconSize( QSize( 16, 16 ) );

	// *** Убираем в проводнике все колонки (тип файла, размер, дата создания) и оставляем только название файла

	for ( int index = 1, count = fileSystemModel.columnCount(); index < count; ++index )
		hideColumn( index );
}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------------------------------
 */

Exploer3DModels::~Exploer3DModels()
{}

//-------------------------------------------------------------------------//

/*
 * Задать каталог проекта
 * ------------------------------
 */

void Exploer3DModels::SetRootDir( const QDir &RootDir )
{
	rootDir = RootDir;
	setRootIndex( fileSystemModel.index( rootDir.path() ) );
}

//-------------------------------------------------------------------------//

/*
 * Получить модель файловой системы
 * ----------------------------------------
 */

QFileSystemModel& Exploer3DModels::GetFileSystemModel()
{
	return fileSystemModel;
}

//-------------------------------------------------------------------------//

/*
 * Получить каталог проекта
 * ------------------------------
 */

const QDir& Exploer3DModels::GetRootDir() const
{
	return rootDir;
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатий кнопки мыши
 * ------------------------------
 */

void Exploer3DModels::mousePressEvent( QMouseEvent* Event )
{
	QModelIndex			indexSelect = indexAt( Event->pos() );

	if ( !indexSelect.isValid() )
	{
		setCurrentIndex( QModelIndex() );

		if ( toolsControl.GetSelectTypeTool() == TT_ADD_MODEL )
			toolsControl.SetSelectTypeTool( TT_SELECT );

		return;
	}

	if ( currentIndex() == indexSelect )		return;

	QFileInfo			fileInfo = fileSystemModel.fileInfo( indexSelect );
	if ( fileInfo.isDir() )		return;

	QString				route = fileInfo.absoluteFilePath();
	route.replace( "\\", "/" );
	route.remove( 0, route.lastIndexOf( "/" ) + 1 );

	if ( toolsControl.GetSelectTypeTool() != TT_ADD_MODEL )
		toolsControl.SetSelectTypeTool( TT_ADD_MODEL );

	toolsControl.GetToolAddModel().ShowAddableObject( route );
	QTreeView::mousePressEvent( Event );
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия клавиши
 * ---------------------
 */

void Exploer3DModels::keyPressEvent( QKeyEvent* Event )
{
	if ( toolsControl.GetSelectTypeTool() == TT_ADD_MODEL && Event->key() == Qt::Key_Escape )
	{
		toolsControl.SetSelectTypeTool( TT_SELECT );
		setCurrentIndex( QModelIndex() );
	}
}

//-------------------------------------------------------------------------//