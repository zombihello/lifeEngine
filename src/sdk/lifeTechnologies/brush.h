//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef BRUSH_H
#define BRUSH_H

#include <iostream>
#include <glm/glm.hpp>
#include <qstring.h>
#include <quuid.h>
using namespace std;

#include "engine/lifeEngine.h"
#include "engine/ray.h"
#include "engine/imaterial.h"
#include "engine/imodel.h"

//-------------------------------------------------------------------------//

class Brush
{
public:
	enum BRUSH_TYPE
	{
		BT_NONE,
		BT_BOX
	};

    /* Конструктор */
    Brush();

    /* Деструктор */
    ~Brush();

    /* Создать браш */
    void Create( BRUSH_TYPE BrushType = BT_BOX );

    /* Удалить браш */
    void Delete();

    /* Задать позицию браша */
	inline void SetPosition( const glm::vec3& Position )
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		model->SetPosition( Position );
	}

	/* Задать поворот браша */
	inline void SetRotation( const glm::vec3& Rotation )
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		model->SetRotation( Rotation );
	}

	/* Задать поворот браша */
	inline void SetRotation( const glm::quat& Rotation )
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		model->SetRotation( Rotation );
	}

	/* Задать масштаб браша */
	inline void SetScale( const glm::vec3& Scale )
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		model->SetScale( Scale );
	}

	/* Задать активный материал */
	static inline void SetActiveMaterial( le::IMaterial* Material )
	{
		activeMaterial = Material;
	}

	/* Сместить позицию браша */
	inline void Move( const glm::vec3& FactorMove )
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		model->Move( FactorMove );
	}

	/* Повернуть браш */
	inline void Rotate( const glm::vec3& FactorRotate )
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		model->Rotate( FactorRotate );
	}

	/* Повернуть браш */
	inline void Rotate( const glm::quat& FactorRotate )
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		model->Rotate( FactorRotate );
	}

	/* Отмасштабировать модель */
	inline void Scale( const glm::vec3& FactorScale )
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		model->Scale( FactorScale );
	}

	/* Получить позицию браша */
	inline const glm::vec3& GetPosition() const
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		return model->GetPosition();
	}

	/* Получить поворот браша */
	inline const glm::quat& GetRotation() const
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		return model->GetRotation();
	}

	/* Получить масштаб браша */
	inline const glm::vec3& GetScale() const
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		return model->GetScale();
	}

	/* Получить модель браша */
	inline le::IModel* GetModel() const
	{
		return model;
	}

	/* Получить уникальный идентификатор браша */
	inline QUuid GetId() const
	{
		return id;
	}

	/* Получить активный материал */
	static inline le::IMaterial* GetActiveMaterial()
	{
		return activeMaterial;
	}

	/* Пересекает ли луч браш */
	inline bool IsIntersectRay( const le::Ray& Ray ) const
	{
		LIFEENGINE_ASSERT( model, "Model brush not created" );
		return model->IsIntersectRay( Ray );
	}

private:
	static le::IMaterial*		activeMaterial;
    le::IModel*					model;	
	QUuid						id;
};

//-------------------------------------------------------------------------//

#endif // !BRUSH_H
