//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (��������� �����) ***
//				Copyright (C) 2018-2019
//
// ����������� ������:  https://gitlab.com/zombihello/lifeEngine
// ������:				���� �������� (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef WINDOW_EDITOR_SCENE_H
#define WINDOW_EDITOR_SCENE_H

#include <qmainwindow.h>
#include <qlibrary.h>

#include "ui_window_editorscene.h"
#include "level.h"
#include "engine/lifeEngine.h"
#include "engine/irendersystem.h"
#include "engine/irenderer.h"
#include "engine/iscene.h"
#include "engine/imodel.h"
#include "engine/itext.h"
#include "toolscontrol.h"

//-------------------------------------------------------------------------//

struct Configuration;
class Settings;

//-------------------------------------------------------------------------//

class Window_EditorScene : public QMainWindow
{
    Q_OBJECT

public:
    /* Конструктор */
    Window_EditorScene( const Configuration& Configuration, const Settings& Settings, QWidget* parent = nullptr );

    /* Деструктор */
    ~Window_EditorScene();

	/* Событие нажатия кнопки на виджете */
	void keyPressEvent( QKeyEvent* Event );

	/* Событие отпускания кнопки на виджете */
	void keyReleaseEvent( QKeyEvent* Event );

	/* Событие клика мышки в окне */
	void mousePressEvent( QMouseEvent* Event );

	/* Фильтр событий */
	bool eventFilter( QObject* Object, QEvent* Event );

private slots:
    /* Событие нажатия на кнопку "Выход" */
    void on_action_exit_triggered();

    /* Событие нажатия на кнопку "Об Qt" */
    void on_action_aboutQt_triggered();

    /* Событие нажатия на кнопку "Об lifeTehcnologies" */
    void on_action_aboutlifeTehcnologies_triggered();

    /* Событие нажатия на кнопку "Новый файл" */
    void on_action_createFile_triggered();

    /* Событие нажатия на кнопку "Закрыть файл" */
    void on_action_closeFile_triggered();

	/* Событие выбора инструмента "Выбор объектов" */
	void on_action_select_triggered();

	/* Событие выбора инструмента "Перемещение объектов" */
	void on_action_move_triggered();

	/* Событие выбора инструмента "Вращение объектов" */
	void on_action_rotate_triggered();

	/* Событие выбора инструмента "Масштабирование объектов" */
	void on_action_scale_triggered();

	/* Событие обновления кадра во вьюпорте */
	void on_viewport_Update( float DeltaTime );

	/* Событие изменения размеров вьюпорта */
	void on_viewport_Resize( const QSize& NewSize );

private:
    /* Событие закрытия окна */
    void closeEvent( QCloseEvent* Event );

    /* Обновить локализацию окна */
    void UpdateLocalisation();

	/* Обратный вызов выбора/смены инструмента редактирования */
	void Callback_SelectTool( TOOLS_TYPE ToolType );

	bool								isActiveCamera;
	uint32_t							moveFlags;

	le::ICamera*						camera;
    Ui::Window_EditorScene              ui;
};

//-------------------------------------------------------------------------//

extern Level*                           level;

//-------------------------------------------------------------------------//

#endif // !WINDOW_EDITOR_SCENE_H
