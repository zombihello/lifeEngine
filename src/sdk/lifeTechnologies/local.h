//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef LOCAL_H
#define LOCAL_H

#include <qstring.h>
#include <map>
using namespace std;

//-------------------------------------------------------------------------//

struct LocaleSettings
{
    QString         route;
    QString         key;
    QString         name;
    QString         icon;
};

//-------------------------------------------------------------------------//

class Local
{
public:
    /* Конструктор */
    Local();

    /* Загрузить файл локализации */
    bool LoadFromFile( const QString& Route );

    /* Получить локализацию строки */
    const QString& operator[]( const QString& Key );

private:
    bool						isLoaded;
    map<QString, QString>		translates;
};

//-------------------------------------------------------------------------//

#endif // !LOCAL_H
