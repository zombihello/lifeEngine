//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef TOOLADDMODEL_H
#define TOOLADDMODEL_H

#include <glm/glm.hpp>
#include <iostream>
#include <qpoint.h>
#include <qstring.h>
#include <qvector2d.h>
#include <qdebug.h>

#include "engine/ray.h"
#include "engine/imodel.h"
#include "engineapi.h"


//-------------------------------------------------------------------------//

class ToolAddModel
{
public:
	/* Конструктор */
	ToolAddModel();

	/* Деструктор */
	~ToolAddModel();

	/* Добавить объект на сцену */
	void AddObject( const le::Ray& Ray );

	/* Показать добавляемый объект */
	void ShowAddableObject( const QString& Route );

	/* Скрыть добавляемый объект */
	void HideAddableObject();
	
	inline void SetPosition_AddableObject( const glm::vec3& Position )
	{
		if ( !activeModel )			return;
		activeModel->SetPosition( Position );
	}

	/* Находится ли активная модель на сцене */
	inline bool IsOnScene() const
	{
		return isOnScene;
	}

private:
	bool					isOnScene;
	le::IModel*				activeModel;
};

//-------------------------------------------------------------------------//

#endif // !TOOLADDMODEL_H