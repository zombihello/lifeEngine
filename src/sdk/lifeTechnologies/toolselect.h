//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef TOOLSELECT_H
#define TOOLSELECT_H

#include <qpoint.h>
#include <qvector2d.h>

#include "engine/icamera.h"
#include "brush.h"

//-------------------------------------------------------------------------//

class ToolSelect
{
public:
	/* Конструктор */
	ToolSelect();

	/* Деструктор */
	~ToolSelect();

	/* Выбрать объект */
	bool SelectObject( const le::Ray& Ray, const glm::vec3& Position );

	/* Сбросить выделенный объкт */
	void UnselectObject();

	/* Выбран ли объект */
	inline bool IsSelectedObject() const
	{
		return isSelectedObject;
	}

	/* Получить выбранный объект */
	inline Brush* GetSelectedObject() const
	{
		return selectBrush;
	}

private:
	bool			isSelectedObject;
	Brush*			selectBrush;
};

//-------------------------------------------------------------------------//

#endif // !TOOLSELECT_H