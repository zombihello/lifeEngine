//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef SETTINGS_H
#define SETTINGS_H

#include <vector>
using namespace std;

#include <qstring.h>
#include "configuration.h"

//-------------------------------------------------------------------------//

class Settings
{
public:
	/* Конструктор */
	Settings();

	/* Загрузить найстроки */
	bool LoadFromFile( const QString& Route );

	/* Сохранить найстроки */
	void SaveToFile( const QString& Route ) const;

	/* Задать последнию выбранную локализацию */
	inline void SetLastLanguage( const QString& Language )
	{
		lastLanguage = Language;
	}

	/* Задать последнию выбранную конфигурацию */
	inline void SetLastConfiguration( int IdConfiguration )
	{
		if ( configurations.empty() || IdConfiguration >= configurations.size() )
		{
			lastIDConfiguration = configurations.size() - 1;
			return;
		}

		lastIDConfiguration = IdConfiguration;
	}

	/* Задать массив конфигураций */
	inline void SetConfigurations( const vector<Configuration>& Configurations )
	{
		configurations = Configurations;

		if ( configurations.empty() || lastIDConfiguration >= configurations.size() )
			lastIDConfiguration = configurations.size() - 1;
	}

	/* Получить конфигурацию редактора */
	inline Configuration GetConfiguration( int IdConfiguration ) const
	{
		if ( IdConfiguration == -1 || configurations.empty() || ( IdConfiguration >= configurations.size() ) )
			return ::Configuration();

		return configurations[ IdConfiguration ];
	}

	/* Получить конфигурации редактора */
	inline const vector<Configuration>& GetConfigurations() const
	{
		return configurations;
	}

	/* Получить последнию выбранную локализацию */
	inline const QString& GetLastLanguage() const
	{
		return lastLanguage;
	}

	/* Получить последнию выбранную ID конфигурации */
	inline int GetLastIDConfiguration() const
	{
		return lastIDConfiguration;
	}

	/* Получить последнию выбранную конфигурацию */
	inline Configuration GetLastConfiguration() const
	{
		return GetConfiguration( lastIDConfiguration );
	}

private:
	vector<Configuration>			configurations;
	int 							lastIDConfiguration;
	QString							lastLanguage;
};

//-------------------------------------------------------------------------//

#endif // !SETTINGS_H
