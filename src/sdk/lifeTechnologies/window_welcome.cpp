//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qdebug.h>
#include <qmessagebox.h>

#include "window_welcome.h"
#include "window_configuregame.h"
#include "localisation.h"
#include "localkeys.h"
#include "configuration.h"
#include "routes.h"
#include "settings.h"

bool			isInit = false;

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ----------------------------------------
 */

Window_Welcome::Window_Welcome( Settings& Settings, QWidget* parent ) :
	QDialog( parent ),
	settings( &Settings )
{
	ui.setupUi( this );

	// Загружаем все конфигурации редактора

	vector<Configuration>		array_configurations = Settings.GetConfigurations();
	for ( auto it = array_configurations.begin(), itEnd = array_configurations.end(); it != itEnd; ++it )
		ui.comboBox_game->addItem( it->name );

	// Локализируем окно и загружаем все языки для выбора

	vector<LocaleSettings>          array_localeSettings = Localisation::GetLocaleSettings();
	for ( size_t index = 0, count = array_localeSettings.size(); index < count; ++index )
	{
		LocaleSettings*             localeSettings = &array_localeSettings[ index ];
		ui.comboBox_language->addItem( QIcon( Localisation::GetDirLocalisations() + "/" + localeSettings->icon ), localeSettings->name );
	}

	UpdateLocalisation();
	ui.comboBox_language->setCurrentIndex( Localisation::GetIDCurrentLocale() );
	ui.comboBox_game->setCurrentIndex( Settings.GetLastIDConfiguration() );
	selectConfiguration = Settings.GetLastConfiguration();
	isInit = true;
}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------------------------------
 */

Window_Welcome::~Window_Welcome()
{}

//-------------------------------------------------------------------------//

/*
 * Получить информацию об игре
 * ----------------------------------------
 */

Window_Welcome::DIALOG_CODE Window_Welcome::GetConfiguration( Configuration& Configuration, Settings& Settings )
{
	Window_Welcome				window_Welcome( Settings );
	int							dialogCode = window_Welcome.exec();

	if ( dialogCode == DC_EXIT )
		return DC_EXIT;

	Configuration = window_Welcome.selectConfiguration;
	return DC_OPEN_EDITOR;
}

//-------------------------------------------------------------------------//

/*
 * Обновить локализацию окна
 * ----------------------------------------
 */

void Window_Welcome::UpdateLocalisation()
{
	setWindowTitle( Localisation::Translate( LOCALISATION_WINDOW_WELCOME )  );

	ui.label_game->setText( Localisation::Translate( LOCALISATION_LABEL_GAME ) + ":" );
	ui.label_language->setText( Localisation::Translate( LOCALISATION_LABEL_LANGUAGE ) + ":" );

	ui.pushButton_configureEditor->setText( Localisation::Translate( LOCALISATION_BUTTON_CONFIGURE_EDITOR ) );
	ui.pushButton_startEditor->setText( Localisation::Translate( LOCALISATION_BUTTON_START_EDITOR ) );
	ui.pushButton_exit->setText( Localisation::Translate( LOCALISATION_BUTTON_EXIT ) );
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия кнопки "Открыть редактор"
 * ----------------------------------------
 */

void Window_Welcome::on_pushButton_startEditor_clicked()
{
	if ( ui.comboBox_game->currentIndex() == -1 )
	{
		QMessageBox::critical( this, Localisation::Translate( LOCALISATION_WINDOW_ERROR ), Localisation::Translate( LOCALISATION_LABEL_NOT_SELECTED_CONFIGURATION_GAME ), QMessageBox::Ok );
		return;
	}

	settings->SetLastConfiguration( ui.comboBox_game->currentIndex() );
	settings->SaveToFile( ROUTE_EDITOR_CONFIGURATIONS );
	done( DC_OPEN_EDITOR );
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия кнопки "Настроить конфигурацию редактора"
 * ----------------------------------------
 */

void Window_Welcome::on_pushButton_configureEditor_clicked()
{
	Window_ConfigureGame		window_configureGame( settings->GetConfigurations(), this );
	int							dialogCode = window_configureGame.exec();

	if ( dialogCode == Window_ConfigureGame::DC_OK )
	{
		vector<Configuration>			configurations = window_configureGame.GetConfigurations();
		settings->SetConfigurations( configurations );
		settings->SaveToFile( ROUTE_EDITOR_CONFIGURATIONS );

		ui.comboBox_game->clear();

		for ( auto it = configurations.begin(), itEnd = configurations.end(); it != itEnd; ++it )
			ui.comboBox_game->addItem( it->name );

		ui.comboBox_game->setCurrentIndex( settings->GetLastIDConfiguration() );
	}
}

//-------------------------------------------------------------------------//

/*
 * Событие смены языка
 * ----------------------------------------
 */

void Window_Welcome::on_comboBox_language_currentIndexChanged( int Index )
{
	if ( !isInit ) return;

	Localisation::SetLocale( Index );
	UpdateLocalisation();

	settings->SetLastLanguage( Localisation::GetNameCurrentLocale() );
	settings->SaveToFile( ROUTE_EDITOR_CONFIGURATIONS );
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия кнопки "Выход"
 * ----------------------------------------
 */

void Window_Welcome::on_pushButton_exit_clicked()
{
	done( DC_EXIT );
}

//-------------------------------------------------------------------------//

/*
 * Событие выбора конфигурации
 * ----------------------------------------
 */

void Window_Welcome::on_comboBox_game_currentIndexChanged( int Index )
{
	selectConfiguration = settings->GetLastConfiguration();
}

//-------------------------------------------------------------------------//