//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (��������� �����) ***
//				Copyright (C) 2018-2019
//
// ����������� ������:  https://gitlab.com/zombihello/lifeEngine
// ������:				���� �������� (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qinputdialog.h>
#include <qfiledialog.h>
#include <qmessagebox.h>
#include <qstring.h>

#include "engine/dirsinfo.h"
#include "window_configuregame.h"
#include "localisation.h"
#include "localkeys.h"

//-------------------------------------------------------------------------//

/*
 * �����������
 * ----------------------------------------
 */

Window_ConfigureGame::Window_ConfigureGame( const vector<Configuration>& Configurations, QWidget *parent ) :
	QDialog( parent ),
	configurations( Configurations )
{
	ui.setupUi( this );

	// �������� ��� ������������

	for ( auto it = configurations.begin(), itEnd = configurations.end(); it != itEnd; ++it )
		ui.comboBox_game->addItem( it->name );

	if ( ui.comboBox_game->currentIndex() == -1 )
	{
		ui.pushButton_removeConfigure->setEnabled( false );
		ui.pushButton_selectDir->setEnabled( false );
	}

	UpdateLocalisation();
}

//-------------------------------------------------------------------------//

/*
 * ����������
 * ----------------------------------------
 */

Window_ConfigureGame::~Window_ConfigureGame()
{}

//-------------------------------------------------------------------------//

/*
 * �������� ����������� ����
 * ----------------------------------------
 */

void Window_ConfigureGame::UpdateLocalisation()
{
	setWindowTitle( Localisation::Translate( LOCALISATION_WINDOW_CONFIGURE_GAME ) );

	ui.label_game->setText( Localisation::Translate( LOCALISATION_LABEL_GAME ) + ":" );
	ui.label_gameDir->setText( Localisation::Translate( LOCALISATION_LABEL_GAME_DIRECTORY ) + ":" );

	ui.pushButton_selectDir->setText( Localisation::Translate( LOCALISATION_BUTTON_SELECT ) );
	ui.pushButton_addConfigure->setText( Localisation::Translate( LOCALISATION_BUTTON_ADD_CONFIGURE_GAME ) );
	ui.pushButton_removeConfigure->setText( Localisation::Translate( LOCALISATION_BUTTON_REMOVE_CONFIGURE_GAME ) );
	ui.pushButton_cancel->setText( Localisation::Translate( LOCALISATION_BUTTON_CANCEL ) );
	ui.pushButton_ok->setText( Localisation::Translate( LOCALISATION_BUTTON_OK ) );
}

//-------------------------------------------------------------------------//

/*
 * ������� ������ ������������
 * ----------------------------------------
 */

void Window_ConfigureGame::on_comboBox_game_currentIndexChanged( int Index )
{
	if ( configurations.empty() || ( Index >= configurations.size() ) ) return;
	if ( !ui.pushButton_removeConfigure->isEnabled() ) ui.pushButton_removeConfigure->setEnabled( true );
	if ( !ui.pushButton_selectDir->isEnabled() ) ui.pushButton_selectDir->setEnabled( true );

	ui.lineEdit_gameDir->setText( configurations[ Index ].gameDir );
}

//-------------------------------------------------------------------------//

/*
 * ������� ������� ������ "�������� ������������ ����"
 * ----------------------------------------
 */

void Window_ConfigureGame::on_pushButton_addConfigure_clicked()
{
	QString					configurationName = QInputDialog::getText( this, Localisation::Translate( LOCALISATION_WINDOW_INPUT ), Localisation::Translate( LOCALISATION_LABEL_ENTER_NAME_CONFIGURATION_GAME ) );
	if ( !configurationName.isEmpty() )
	{
		configurations.push_back( Configuration( configurationName ) );
		ui.comboBox_game->addItem( configurationName );
		ui.comboBox_game->setCurrentIndex( ui.comboBox_game->count() - 1 );
	}
}

//-------------------------------------------------------------------------//

/*
 * ������� ������� ������ "�������"
 * ----------------------------------------
 */

void Window_ConfigureGame::on_pushButton_selectDir_clicked()
{
	QString				gameDir = QFileDialog::getOpenFileName( this, Localisation::Translate( LOCALISATION_WINDOW_SELECT_GAME_MANIFEST ), QDir::currentPath(), "Game manifest (" LENGINE_GAME_MANIFEST ")" );
	if ( !gameDir.isEmpty() )
	{
		gameDir.remove( gameDir.lastIndexOf( "/" ), gameDir.size() );
		ui.lineEdit_gameDir->setText( gameDir );
		configurations[ ui.comboBox_game->currentIndex() ].gameDir = gameDir;
	}
}

//-------------------------------------------------------------------------//

/*
 * ������� ������� ������ "������� ������������ ����"
 * ----------------------------------------
 */

void Window_ConfigureGame::on_pushButton_removeConfigure_clicked()
{
	configurations.erase( ui.comboBox_game->currentIndex() + configurations.begin() );
	ui.comboBox_game->removeItem( ui.comboBox_game->currentIndex() );

	if ( ui.comboBox_game->currentIndex() == -1 )
	{
		ui.lineEdit_gameDir->setText( "" );
		ui.pushButton_removeConfigure->setEnabled( false );
		ui.pushButton_selectDir->setEnabled( false );
	}
}

//-------------------------------------------------------------------------//

/*
 * ������� ������� ������ "������"
 * ----------------------------------------
 */

void Window_ConfigureGame::on_pushButton_cancel_clicked()
{
	done( DC_CANCEL );
}

//-------------------------------------------------------------------------//

/*
 * ������� ������� ������ "��"
 * ----------------------------------------
 */

void Window_ConfigureGame::on_pushButton_ok_clicked()
{
	done( DC_OK );
}

//-------------------------------------------------------------------------//
