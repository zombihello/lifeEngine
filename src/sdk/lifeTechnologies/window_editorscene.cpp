//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qtextstream.h>
#include <qfile.h>
#include <qlabel.h>
#include <qdir.h>
#include <qmessagebox.h>
#include <qevent.h>
#include <qdebug.h>
#include <qfiledialog.h>
#include <qfilesystemmodel.h>

#include "engineapi.h"
#include "engine/lifeEngine.h"
#include "engine/logger.h"
#include "engine/version.h"
#include "engine/dirsinfo.h"
#include "engine/configurations.h"
#include "engine/gamemanifest.h"
#include "engine/irendersystem.h"
#include "engine/ray.h"
#include "routes.h"
#include "viewport.h"
#include "window_editorscene.h"
#include "configuration.h"
#include "localisation.h"
#include "localkeys.h"
#include "brush.h"

//-------------------------------------------------------------------------//

Level*                           level = nullptr;

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ----------------------------------------
 */
#include "engine/iboundingbox.h"
Window_EditorScene::Window_EditorScene( const Configuration& Configuration, const Settings& Settings, QWidget *parent ) :
	QMainWindow( parent ),
	moveFlags( le::CSM_NONE ),
	isActiveCamera( false )
{
	ui.setupUi( this );
	setWindowTitle( "lifeTechnologies - [" + Configuration.name + "]" );

	// Указываем иконки для кнопок инструментов

	QIcon       icon_select;
	icon_select.addPixmap( QPixmap( ROUTE_ACTION_ICON_SELECT ) );
	ui.action_select->setIcon( icon_select );

	QIcon       icon_move;
	icon_move.addPixmap( QPixmap( ROUTE_ACTION_ICON_MOVE ) );
	ui.action_move->setIcon( icon_move );

	QIcon       icon_rotate;
	icon_rotate.addPixmap( QPixmap( ROUTE_ACTION_ICON_ROTATE ) );
	ui.action_rotate->setIcon( icon_rotate );

	QIcon       icon_scale;
	icon_scale.addPixmap( QPixmap( ROUTE_ACTION_ICON_SCALE ) );
	ui.action_scale->setIcon( icon_scale );

	// Локализируем весь текст в окне
	UpdateLocalisation();

	// Загружаем модуль рендера движка

	try
	{
		if ( !engineAPI.LoadCoreEngine( "./" LENGINE_ENGINE_LIB ) )
			throw engineAPI.GetErrorMessage();

		if ( !engineAPI.LoadRenderSystem( "./" LENGINE_RENDER_SYSTEM_LIB ) )
			throw engineAPI.GetErrorMessage();

		if ( !engineAPI.LoadGameManifest( Configuration.gameDir ) )
			throw engineAPI.GetErrorMessage();

		le::Configurations		configurations = {};
		configurations.fov = 75.f;
		configurations.sensitivityMouse = 0.15f;
		configurations.vsinc = false;
		configurations.windowWidth = 800;
		configurations.windowHeight = 600;

		le::WindowHandle_t	windowHandle = nullptr;
		ui.treeView_exploer3DModels->SetRootDir( QDir( ( engineAPI.GetGameManifest().modelsDir ).c_str() ) );

#if defined( PLATFORM_WINDOWS )
		windowHandle = ( le::WindowHandle_t ) ui.viewport->winId();
#elif defined( PLATFORM_LINUX )
		WId                 winid = ui.viewport->winId();
		windowHandle = &winid;
#else
#	error	Not supported platform
#endif

		if ( !engineAPI.InitRenderSystem( windowHandle, configurations ) )
			throw engineAPI.GetErrorMessage();
	}
	catch ( const QString& Message )
	{
		QMessageBox::critical( nullptr, "Error", Message, QMessageBox::StandardButton::Ok );
		exit( 1 );
	}

	// Инициализируем контролы редактора
	// для трансформации объектов

	toolsControl.Initialize();
	toolsControl.AddCallback( this, ( ToolsControl::ObjectCallback_t ) &Window_EditorScene::Callback_SelectTool );

	// Создаем камеру и делаем ее активной

	camera = engineAPI.GetRenderSystem()->CreateCamera();
	camera->SetPosition( -37.8417, 42.4907, -35.7351 );
	camera->SetRotation( 45, 180, 0 );
	engineAPI.GetRenderSystem()->SetActiveCamera( camera, le::TL_WORLD );

	resize( 800, 600 );
	level = new Level();
}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------------------------------
 */

Window_EditorScene::~Window_EditorScene()
{
	delete level;
	delete ui.viewport;
	
	engineAPI.UnloadRenderSystem();
	engineAPI.UnloadCoreEngine();
}

//-------------------------------------------------------------------------//

/*
 * Событие закрытия окна
 * ----------------------------------------
 */

void Window_EditorScene::closeEvent( QCloseEvent* Event )
{
	Q_UNUSED( Event );
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия на кнопку "Выход"
 * ----------------------------------------
 */

void Window_EditorScene::on_action_exit_triggered()
{
	QApplication::quit();
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия на кнопку "Об Qt"
 * ----------------------------------------
 */

void Window_EditorScene::on_action_aboutQt_triggered()
{
	QMessageBox::aboutQt( this, Localisation::Translate( LOCALISATION_WINDOW_ABOUT_QT ) );
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия на кнопку "Об lifeTehcnologies"
 * ----------------------------------------
 */

void Window_EditorScene::on_action_aboutlifeTehcnologies_triggered()
{
	QMessageBox::about( this, Localisation::Translate( LOCALISATION_WINDOW_ABOUT_LIFETECHNOLOGIES ), "умный текст" );
}

//-------------------------------------------------------------------------//

/*
 * Обновить локализацию окна
 * ----------------------------------------
 */

void Window_EditorScene::UpdateLocalisation()
{
	ui.menu_file->setTitle( Localisation::Translate( LOCALISATION_MENU_FILE ) );
	ui.menu_edit->setTitle( Localisation::Translate( LOCALISATION_MENU_EDIT ) );
	ui.menu_map->setTitle( Localisation::Translate( LOCALISATION_MENU_MAP ) );
	ui.menu_tools->setTitle( Localisation::Translate( LOCALISATION_MENU_TOOLS ) );
	ui.menu_help->setTitle( Localisation::Translate( LOCALISATION_MENU_HELP ) );

	ui.dockWidget_objectCreation->setWindowTitle( Localisation::Translate( LOCALISATION_DOCKWIDGET_OBJECT_CREATION ) );
	ui.dockWidget_objects->setWindowTitle( Localisation::Translate( LOCALISATION_DOCKWIDGET_OBJECTS ) );
	ui.dockWidget_properties->setWindowTitle( Localisation::Translate( LOCALISATION_DOCKWIDGET_PROPERTIES ) );

	ui.tabWidget_objectCreation->setTabText( 0, Localisation::Translate( LOCALISATION_TAB_3D_MODELS ) );
	ui.tabWidget_objectCreation->setTabText( 1, Localisation::Translate( LOCALISATION_TAB_ENTITIES ) );

	ui.action_createFile->setText( Localisation::Translate( LOCALISATION_MENU_FILE_NEW_FILE ) );
	ui.action_openFile->setText( Localisation::Translate( LOCALISATION_MENU_FILE_OPEN_FILE ) );
	ui.action_saveFile->setText( Localisation::Translate( LOCALISATION_MENU_FILE_SAVE_FILE ) );
	ui.action_saveFileAs->setText( Localisation::Translate( LOCALISATION_MENU_FILE_SAVE_FILE_AS ) );
	ui.action_closeFile->setText( Localisation::Translate( LOCALISATION_MENU_FILE_CLOSE_FILE ) );
	ui.action_exit->setText( Localisation::Translate( LOCALISATION_MENU_FILE_EXIT ) );
	ui.action_undo->setText( Localisation::Translate( LOCALISATION_MENU_EDIT_UNDO ) );
	ui.action_redo->setText( Localisation::Translate( LOCALISATION_MENU_EDIT_REDO ) );
	ui.action_settingsMap->setText( Localisation::Translate( LOCALISATION_MENU_MAP_SETTINGS_MAP ) );
	ui.action_settingsEditor->setText( Localisation::Translate( LOCALISATION_MENU_TOOLS_SETTINGS_EDITOR ) );
	ui.action_aboutQt->setText( Localisation::Translate( LOCALISATION_MENU_HELP_ABOUT_QT ) );
	ui.action_aboutlifeTehcnologies->setText( Localisation::Translate( LOCALISATION_MENU_HELP_ABOUT_LIFETECHNOLOGIES ) );

	ui.action_select->setToolTip( Localisation::Translate( LOCALISATION_TOOLTIP_CURSOR ) );
	ui.action_move->setToolTip( Localisation::Translate( LOCALISATION_TOOLTIP_MOVE ) );
	ui.action_rotate->setToolTip( Localisation::Translate( LOCALISATION_TOOLTIP_ROTATE ) );
	ui.action_scale->setToolTip( Localisation::Translate( LOCALISATION_TOOLTIP_SCALE ) );
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия на кнопку "Новый файл"
 * ----------------------------------------
 */

void Window_EditorScene::on_action_createFile_triggered()
{
	ui.scene->setEnabled( true );
	ui.viewport->setEnabled( true );

	ui.action_saveFile->setEnabled( true );
	ui.action_saveFileAs->setEnabled( true );
	ui.action_closeFile->setEnabled( true );
	ui.action_undo->setEnabled( true );
	ui.action_redo->setEnabled( true );
	ui.action_settingsMap->setEnabled( true );
	ui.action_settingsEditor->setEnabled( true );

	ui.dockWidget_objectCreation->setEnabled( true );
	ui.dockWidget_objects->setEnabled( true );
	ui.dockWidget_properties->setEnabled( true );

	ui.toolBar->setEnabled( true );
	toolsControl.SetSelectTypeTool( TT_SELECT );
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия на кнопку "Закрыть файл"
 * ----------------------------------------
 */

void Window_EditorScene::on_action_closeFile_triggered()
{
	ui.scene->setEnabled( false );
	ui.viewport->setEnabled( false );

	ui.action_saveFile->setEnabled( false );
	ui.action_saveFileAs->setEnabled( false );
	ui.action_closeFile->setEnabled( false );
	ui.action_undo->setEnabled( false );
	ui.action_redo->setEnabled( false );
	ui.action_settingsMap->setEnabled( false );
	ui.action_settingsEditor->setEnabled( false );

	ui.dockWidget_objectCreation->setEnabled( false );
	ui.dockWidget_objects->setEnabled( false );
	ui.dockWidget_properties->setEnabled( false );

	ui.toolBar->setEnabled( false );
	toolsControl.SetSelectTypeTool( TT_NONE );
	level->Clear();
}

//-------------------------------------------------------------------------//

/*
 * Событие клика мышки в окне
 * ----------------------------------------
 */

void Window_EditorScene::mousePressEvent( QMouseEvent* Event )
{
	switch ( toolsControl.GetSelectTypeTool() )
	{
	case TT_SELECT:
	case TT_MOVE:
	case TT_SCALE:
	case TT_ROTATE:
	{
		if ( !QRect( ui.viewport->mapToGlobal( ui.viewport->pos() ), ui.viewport->size() ).contains( Event->screenPos().toPoint() ) )
			return;

		QPoint			cursorPosition = ui.viewport->mapFromGlobal( Event->globalPos() );
		
		toolsControl.GetToolSelect().SelectObject( camera->ScreenToWorld(
			glm::vec2( cursorPosition.x(), cursorPosition.y() ),
			glm::vec2( ui.viewport->width(), ui.viewport->height() ) ), camera->GetPosition() );
	}
	break;

	case TT_ADD_MODEL:
		if ( QRect( ui.viewport->mapToGlobal( ui.viewport->pos() ), ui.viewport->size() ).contains( Event->screenPos().toPoint() ) &&
			 Event->button() == Qt::LeftButton )
		{
			QPoint			cursorPosition = ui.viewport->mapFromGlobal( Event->globalPos() );

			toolsControl.GetToolAddModel().AddObject( camera->ScreenToWorld(
				glm::vec2( cursorPosition.x(), cursorPosition.y() ),
				glm::vec2( ui.viewport->width(), ui.viewport->height() ) ) );
		}
		break;
	}

	QWidget::mousePressEvent( Event );
}

//-------------------------------------------------------------------------//

/*
 * Событие выбора инструмента "Выбор объектов"
 * ----------------------------------------
 */

void Window_EditorScene::on_action_select_triggered()
{
	toolsControl.SetSelectTypeTool( TT_SELECT );
}

//-------------------------------------------------------------------------//

/*
 * Событие выбора инструмента "Перемещение объектов"
 * ----------------------------------------
 */

void Window_EditorScene::on_action_move_triggered()
{
	toolsControl.SetSelectTypeTool( TT_MOVE );
}

//-------------------------------------------------------------------------//

/*
 * Событие выбора инструмента "Вращение объектов"
 * ----------------------------------------
 */

void Window_EditorScene::on_action_rotate_triggered()
{
	toolsControl.SetSelectTypeTool( TT_ROTATE );
}

//-------------------------------------------------------------------------//

/*
 * Событие выбора инструмента "Масштабирование объектов"
 * ----------------------------------------
 */

void Window_EditorScene::on_action_scale_triggered()
{
	toolsControl.SetSelectTypeTool( TT_SCALE );
}

//-------------------------------------------------------------------------//

/*
 * Обратный вызов выбора/смены инструмента редактирования
 * ----------------------------------------
 */

void Window_EditorScene::Callback_SelectTool( TOOLS_TYPE ToolType )
{
	QIcon		icon;

	// Убираем выделение с прошлого инструмента

	switch ( toolsControl.GetSelectTypeTool() )
	{
	case TT_SELECT:
		icon = ui.action_select->icon();
		icon.addPixmap( QPixmap( ROUTE_ACTION_ICON_SELECT ) );
		ui.action_select->setIcon( icon );
		break;

	case TT_MOVE:
		icon = ui.action_move->icon();
		icon.addPixmap( QPixmap( ROUTE_ACTION_ICON_MOVE ) );
		ui.action_move->setIcon( icon );
		break;

	case TT_ROTATE:
		icon = ui.action_rotate->icon();
		icon.addPixmap( QPixmap( ROUTE_ACTION_ICON_ROTATE ) );
		ui.action_rotate->setIcon( icon );
		break;

	case TT_SCALE:
		icon = ui.action_scale->icon();
		icon.addPixmap( QPixmap( ROUTE_ACTION_ICON_SCALE ) );
		ui.action_scale->setIcon( icon );
		break;
	}

	// Выделяем выбраный инструмент

	switch ( ToolType )
	{
	case TT_SELECT:
		icon = ui.action_select->icon();
		icon.addPixmap( QPixmap( ROUTE_ACTION_ICON_SELECT_FOCUS ) );
		ui.action_select->setIcon( icon );
		break;

	case TT_MOVE:
		icon = ui.action_move->icon();
		icon.addPixmap( QPixmap( ROUTE_ACTION_ICON_MOVE_FOCUS ) );
		ui.action_move->setIcon( icon );
		break;

	case TT_ROTATE:
		icon = ui.action_rotate->icon();
		icon.addPixmap( QPixmap( ROUTE_ACTION_ICON_ROTATE_FOCUS ) );
		ui.action_rotate->setIcon( icon );
		break;

	case TT_SCALE:
		icon = ui.action_scale->icon();
		icon.addPixmap( QPixmap( ROUTE_ACTION_ICON_SCALE_FOCUS ) );
		ui.action_scale->setIcon( icon );
		break;
	}
}

//-------------------------------------------------------------------------//

/*
 * Событие нажатия кнопки на виджете
 * ------------------------------
 */

void Window_EditorScene::keyPressEvent( QKeyEvent* Event )
{
	switch ( Event->nativeVirtualKey() )
	{
	case Qt::Key_Space:
	{
		QCursor		tempCursor = cursor();
		isActiveCamera = !isActiveCamera;

		if ( isActiveCamera )
		{
			QCursor::setPos( ui.viewport->mapToGlobal( QPoint( ui.viewport->width() / 2, ui.viewport->height() / 2 ) ) );
			tempCursor.setShape( Qt::BlankCursor );
		}
		else
			tempCursor.setShape( Qt::ArrowCursor );

		setCursor( tempCursor );
		break;
	}

	case Qt::Key_A:
		moveFlags |= le::CSM_LEFT;
		break;

	case Qt::Key_D:
		moveFlags |= le::CSM_RIGHT;
		break;

	case Qt::Key_W:
		moveFlags |= le::CSM_FORWARD;
		break;

	case Qt::Key_S:
		moveFlags |= le::CSM_BACKWARD;
		break;
	}
}

//-------------------------------------------------------------------------//

/*
 * Событие отпускания кнопки на виджете
 * ------------------------------
 */

void Window_EditorScene::keyReleaseEvent( QKeyEvent* Event )
{
	switch ( Event->nativeVirtualKey() )
	{
	case Qt::Key_W:
		moveFlags &= ~le::CSM_FORWARD;
		break;

	case Qt::Key_S:
		moveFlags &= ~le::CSM_BACKWARD;
		break;

	case Qt::Key_A:
		moveFlags &= ~le::CSM_LEFT;
		break;

	case Qt::Key_D:
		moveFlags &= ~le::CSM_RIGHT;
		break;
	}
}

//-------------------------------------------------------------------------//

/*
 * Фильтр событий
 * ------------------------------
 */

bool Window_EditorScene::eventFilter( QObject* Object, QEvent* Event )
{
	if ( Event->type() != QEvent::KeyPress && Event->type() != QEvent::KeyRelease ) return false;
	QKeyEvent*			keyEvent = static_cast< QKeyEvent* >( Event );

	switch ( Event->type() )
	{
	case QEvent::KeyPress:
		ui.treeView_exploer3DModels->keyPressEvent( keyEvent );

		if ( !ui.viewport->isEnabled() )	break;
		keyPressEvent( keyEvent );
		break;

	case QEvent::KeyRelease:
		if ( !ui.viewport->isEnabled() )	break;
		keyReleaseEvent( keyEvent );
		break;

	default:
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------//

/*
 * Событие обновления кадра во вьюпорте
 * ------------------------------
 */

void Window_EditorScene::on_viewport_Update( float DeltaTime )
{
	// Если курсор внутри вьюпорта - даем возможность сместить камеру

	if ( ui.viewport->geometry().contains( mapFromGlobal( QCursor::pos() ) ) )
	{
		if ( moveFlags & le::CSM_FORWARD )			camera->Move( le::CSM_FORWARD, 150.f * DeltaTime );
		if ( moveFlags & le::CSM_BACKWARD )			camera->Move( le::CSM_BACKWARD, 150.f * DeltaTime );
		if ( moveFlags & le::CSM_LEFT )				camera->Move( le::CSM_LEFT, 150.f * DeltaTime );
		if ( moveFlags & le::CSM_RIGHT )			camera->Move( le::CSM_RIGHT, 150.f * DeltaTime );
	}

	// Обновляем контролы в зависимости от текущего типа манипулятора

	switch ( toolsControl.GetSelectTypeTool() )
	{
	case TT_MOVE:
	{
		if ( !toolsControl.GetToolSelect().IsSelectedObject() )		break;

		glm::vec3		worldPosition = toolsControl.GetToolSelect().GetSelectedObject()->GetPosition();
		glm::vec3		screenPosition = camera->WorldToScreen( worldPosition, glm::vec2( ui.viewport->width(), ui.viewport->height() ) );
		le::Ray			ray = camera->ScreenToWorld( screenPosition, glm::vec2( ui.viewport->width(), ui.viewport->height() ) );

		toolsControl.GetToolMove().SetPosition_ControlModel( camera->GetPosition() + glm::normalize( worldPosition - ray.origin ) * 5.1f );
	}
	break;

	case TT_ADD_MODEL:
	{
		QPoint		screenPosition = ui.viewport->mapFromGlobal( ui.viewport->cursor().pos() );
		le::Ray		ray = camera->ScreenToWorld( glm::vec2( screenPosition.x(), screenPosition.y() ),
												 glm::vec2( ui.viewport->width(), ui.viewport->height() ) );

		float t = -ray.origin.y / ray.direction.y;
		float worldX = ray.origin.x + ray.direction.x * t;
		float worldZ = ray.origin.z + ray.direction.z * t;

		toolsControl.GetToolAddModel().SetPosition_AddableObject( glm::vec3( worldX, 0, worldZ ) );
	}
	break;
	}

	// Если камера активирована, то поварачиваем ее вслед за мышкой

	if ( isActiveCamera )
	{
		QPoint          centerWindow( ui.viewport->width() / 2, ui.viewport->height() / 2 );
		QPoint			globalCenterWindow = ui.viewport->mapToGlobal( centerWindow );
		QPoint          cursorPosition = ui.viewport->cursor().pos();

		camera->RotateByMouse( cursorPosition.x() - globalCenterWindow.x(), cursorPosition.y() - globalCenterWindow.y(), 0.15f );
		QCursor::setPos( globalCenterWindow );
	}

	// Обновляем сцену

	engineAPI.GetScene()->Update();
}

//-------------------------------------------------------------------------//

/*
 * Событие изменения размеров вьюпорта
 * ------------------------------
 */

void Window_EditorScene::on_viewport_Resize( const QSize& NewSize )
{
	camera->InitProjection_Perspective( 75.f, ( float ) NewSize.width() / NewSize.height(), 0.1f, 1500.f );
}

//-------------------------------------------------------------------------//