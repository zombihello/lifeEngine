//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef KEYSCONTROL_H
#define KEYSCONTROL_H

#include <qevent.h>
#include <qobject.h>

//-------------------------------------------------------------------------//

class KeysControl : public QObject
{
public:
	/* Конструктор */
	KeysControl();

	/* Деструктор */
	~KeysControl();

	/* Фильтр событий */
	bool eventFilter( QObject* Object, QEvent* Event );
};

//-------------------------------------------------------------------------//

#endif // !KEYSCONTROL_H
