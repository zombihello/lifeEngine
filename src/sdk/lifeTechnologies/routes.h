//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef ROUTES_H
#define ROUTES_H

//-------------------------------------------------------------------------//

#define ROUTE_DARK_THEME							":/Resources/styles/dark/style.css"
#define ROUTE_EDITOR_CONFIGURATIONS					"lifeTechnologies.json"
#define ROUTE_DIR_LIFEENGINE                        "./"

#define ROUTE_ACTION_ICON_SELECT                    ":/Resources/icons/select.png"
#define ROUTE_ACTION_ICON_SELECT_FOCUS              ":/Resources/icons/select_focus.png"
#define ROUTE_ACTION_ICON_MOVE                      ":/Resources/icons/move.png"
#define ROUTE_ACTION_ICON_MOVE_FOCUS                ":/Resources/icons/move_focus.png"
#define ROUTE_ACTION_ICON_ROTATE                    ":/Resources/icons/rotate.png"
#define ROUTE_ACTION_ICON_ROTATE_FOCUS              ":/Resources/icons/rotate_focus.png"
#define ROUTE_ACTION_ICON_SCALE                     ":/Resources/icons/scale.png"
#define ROUTE_ACTION_ICON_SCALE_FOCUS               ":/Resources/icons/scale_focus.png"
#define ROUTE_ACTION_ICON_BRUSH                     ":/Resources/icons/brush.png"
#define ROUTE_ACTION_ICON_BRUSH_FOCUS               ":/Resources/icons/brush_focus.png"
#define ROUTE_ACTION_ICON_ENTITY                    ":/Resources/icons/entity.png"
#define ROUTE_ACTION_ICON_ENTITY_FOCUS              ":/Resources/icons/entity_focus.png"

//-------------------------------------------------------------------------//

#endif // !ROUTES_H
