//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qapplication.h>
#include <qfile.h>
#include <qtextstream.h>
#include <qdebug.h>
#include <qglobal.h>
#include <iostream>
#include <fstream>
using namespace std;

#include "engine/lifeEngine.h"
#include "engine/logger.h"
#include "window_welcome.h"
#include "window_editorscene.h"
#include "version.h"
#include "settings.h"
#include "configuration.h"
#include "localisation.h"
#include "routes.h"
#include "keyscontrol.h"

#if defined( PLATFORM_LINUX )
#	include <unistd.h>
#endif

//-------------------------------------------------------------------------//

ofstream            logFile;

//-------------------------------------------------------------------------//

/*
 * Вывод сообщений в файл логов
 * -------------------------------
 */

void MessageOutput( QtMsgType Type, const QMessageLogContext &Context, const QString &Message )
{
    switch ( Type )
    {
    case QtDebugMsg:
        logFile << "[Debug] " << Message.toLocal8Bit().data() << endl;
        break;

    case QtInfoMsg:
        logFile << "[Info] " << Message.toLocal8Bit().data() << endl;
        break;

    case QtWarningMsg:
        logFile << "[Warning] " << Message.toLocal8Bit().data() << endl;
        break;

    case QtCriticalMsg:
        logFile << "[Error] " << Message.toLocal8Bit().data() << endl;
        break;

    case QtFatalMsg:
        logFile << "[Fatal] " << Message.toLocal8Bit().data() << endl;
        abort();
    }
}

//-------------------------------------------------------------------------//

/*
 * Точка входа
 * ---------------------
 */

int main( int argc, char *argv[] )
{
    // Задаем рабочий каталог

#if defined( PLATFORM_LINUX )
    {
        string 			workDir = argv[0];
        if ( workDir.find_last_of( '/' ) != -1 )
            workDir.erase( workDir.find_last_of( '/' ), workDir.size() );

        chdir( workDir.c_str() );
    }
#endif

    // Настраиваем вывод логов в файл

    logFile.open( "./lifeTechnologies.log" );

    cout.rdbuf( logFile.rdbuf() );
    cerr.rdbuf( logFile.rdbuf() );
    qInstallMessageHandler( MessageOutput  );

    cout.setf( ios::dec | ios::showbase );

    LOG_INFO( "*** lifeTechnologies " << Version::major << "." << Version::minor << "." << Version::patch << " (build " << Version::build << ") start ***" );
    LOG_INFO( "Qt version: " << qVersion() );
	
    QApplication			application( argc, argv );
	KeysControl				keysControl;

    // Загружаем стиль редактора

    QFile*		fileStyle = new QFile( ROUTE_DARK_THEME );
    if ( fileStyle->exists() )
    {
        fileStyle->open( QFile::ReadOnly | QFile::Text );
        QTextStream			textStream( fileStyle );

        application.setStyleSheet( textStream.readAll() );
    }
    delete fileStyle;

    // Загружаем настройки, а так же файл локализаций редактора

    Settings			settings;

    if ( !settings.LoadFromFile( ROUTE_EDITOR_CONFIGURATIONS ) )
        settings.SaveToFile( ROUTE_EDITOR_CONFIGURATIONS );

    Localisation::LoadFromFile( "localisation" );
    Localisation::SetLocale( settings.GetLastLanguage() );

    Configuration			configuration;
    int                     result = 0;
	
    if ( Window_Welcome::GetConfiguration( configuration, settings ) != Window_Welcome::DC_EXIT )
    {
        Window_EditorScene		window_EditorScene( configuration, settings );

        window_EditorScene.showMaximized();
        result = application.exec();
    }

    LOG_INFO( "*** lifeTechnologies " << Version::major << "." << Version::minor << "." << Version::patch << " (build " << Version::build << ") end ***" );
    return result;
}

//-------------------------------------------------------------------------//
