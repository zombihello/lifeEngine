//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qfile.h>
#include <qapplication.h>
#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qjsonarray.h>

#include "engine/logger.h"
#include "local.h"

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ----------------------------------------
 */

Local::Local() :
    isLoaded( false )
{}

//-------------------------------------------------------------------------//

/*
 * Загрузить файл локализации
 * ----------------------------------------
 */

bool Local::LoadFromFile( const QString &Route )
{
    LOG_INFO( "Local [" << Route.toStdString() << "] loading" );

    QFile		fileLocal( Route );
    if ( !fileLocal.open( QIODevice::ReadOnly ) )
    {
        LOG_ERROR( "Local not loaded. File or directory not found" );
        return false;
    }

    QJsonDocument		jsonDoc = QJsonDocument::fromJson( fileLocal.readAll() );
    QJsonObject			jsonObject = jsonDoc.object();

    if ( jsonObject.contains( "translates" ) )
    {
        QJsonArray			jsonArray = jsonObject[ "translates" ].toArray();
        QJsonObject			tempJsonObject;

        for ( size_t index = 0, count = jsonArray.size(); index < count; ++index )
        {
            tempJsonObject = jsonArray[ index ].toObject();
            QStringList         stringList = tempJsonObject.keys();

            for ( size_t indexKey = 0, countKey = stringList.size(); indexKey < stringList.size(); ++indexKey )
                translates[ stringList[ indexKey ] ] = tempJsonObject[ stringList[ indexKey ] ].toString();
        }
    }
    else
        LOG_WARNING( "Local not contains json object [translates]" );

    LOG_INFO( "Local loaded" );
    isLoaded = true;
    return true;
}

//-------------------------------------------------------------------------//

/*
 * Получить локализацию строки
 * ----------------------------------------
 */

const QString& Local::operator[]( const QString &Key )
{
    return translates[ Key ];
}

//-------------------------------------------------------------------------//
