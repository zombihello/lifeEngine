//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qdebug.h>

#include "engineapi.h"
#include "engine/dirsinfo.h"
#include "engine/version.h"
#include "engine/logger.h"
#include "routes.h"

//-------------------------------------------------------------------------//

EngineAPI			engineAPI;

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ----------------------------------------
 */

EngineAPI::EngineAPI() :
	LE_LoadConfig( nullptr ),
	LE_LoadGameManifest( nullptr ),
	LE_CreateCoreEngine( nullptr ),
	LE_DeleteCoreEngine( nullptr ),
	LE_CreateRenderSystem( nullptr ),
	LE_DeleteRenderSystem( nullptr ),
	engine( nullptr ),
	renderSystem( nullptr ),
	scene( nullptr ),
	modelRenderer( nullptr ),
	textRenderer( nullptr ),
	textureManager( nullptr ),
	materialManager( nullptr ),
	meshManager( nullptr ),
	fontManager( nullptr )
{}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------------------------------
 */

EngineAPI::~EngineAPI()
{
	UnloadRenderSystem();
	UnloadCoreEngine();
}

//-------------------------------------------------------------------------//

/*
 * Загрузить ядро движка
 * ----------------------------------------
 */

bool EngineAPI::LoadCoreEngine( const QString& Route )
{
	if ( engine )		UnloadCoreEngine();

	coreEngineLib.setFileName( "./" LENGINE_ENGINE_LIB );

	try
	{
		if ( !coreEngineLib.load() )
			throw coreEngineLib.errorString();

		LE_LoadConfig = ( le::Func_LE_LoadConfig_t ) coreEngineLib.resolve( "LE_LoadConfig" );
		if ( !LE_LoadConfig )
			throw coreEngineLib.errorString();

		LE_LoadGameManifest = ( le::Func_LE_LoadGameManifest_t ) coreEngineLib.resolve( "LE_LoadGameManifest" );
		if ( !LE_LoadGameManifest )
			throw coreEngineLib.errorString();

		LE_CreateCoreEngine = ( le::Func_LE_CreateCoreEngine_t ) coreEngineLib.resolve( "LE_CreateCoreEngine" );
		if ( !LE_CreateCoreEngine )
			throw coreEngineLib.errorString();

		LE_DeleteCoreEngine = ( le::Func_LE_DeleteCoreEngine_t ) coreEngineLib.resolve( "LE_DeleteCoreEngine" );
		if ( !LE_DeleteCoreEngine )
			throw coreEngineLib.errorString();

		engine = LE_CreateCoreEngine();
	}
	catch ( const QString& Message )
	{	
		errorMessage = Message;
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------//

/*
 * Выгрузить ядро движка
 * ----------------------------------------
 */

void EngineAPI::UnloadCoreEngine()
{
	if ( !coreEngineLib.isLoaded() )   return;
	if ( engine )		LE_DeleteCoreEngine( engine );
	
	engine = nullptr;
	coreEngineLib.unload();
}

//-------------------------------------------------------------------------//

/*
 * Выгрузить ядро движка
 * ----------------------------------------
 */

bool EngineAPI::LoadRenderSystem( const QString& Route )
{
	if ( renderSystem )		UnloadRenderSystem();

	renderSystemLib.setFileName( "./" LENGINE_RENDER_SYSTEM_LIB );

	try
	{
		if ( !renderSystemLib.load() )
			throw renderSystemLib.errorString();

		LE_CreateRenderSystem = ( le::Func_LE_CreateRenderSystem_t ) renderSystemLib.resolve( "LE_CreateRenderSystem" );
		if ( !LE_CreateRenderSystem )
			throw renderSystemLib.errorString();

		LE_DeleteRenderSystem = ( le::Func_LE_DeleteRenderSystem_t ) renderSystemLib.resolve( "LE_DeleteRenderSystem" );
		if ( !LE_DeleteRenderSystem )
			throw renderSystemLib.errorString();
	}
	catch ( const QString& Message )
	{
		errorMessage = Message;
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------//

/*
 *  Выгрузить модуль визуализации
 * ----------------------------------------
 */

void EngineAPI::UnloadRenderSystem()
{
	if ( !renderSystemLib.isLoaded() )		return;
	if ( renderSystem )		LE_DeleteRenderSystem( renderSystem );

	renderSystem = nullptr;
	scene = nullptr;
	modelRenderer = nullptr;
	textRenderer = nullptr;
	textureManager = nullptr;
	materialManager = nullptr;
	meshManager = nullptr;
	fontManager = nullptr;

	renderSystemLib.unload();
}

//-------------------------------------------------------------------------//

/*
 * Загрузить ядро движка
 * ----------------------------------------
 */

bool EngineAPI::LoadGameManifest( const QString& GameDir )
{
	try
	{
		if ( !coreEngineLib.isLoaded() )
			throw "Core engine not loaded";

		if ( !LE_LoadGameManifest( GameDir.toUtf8(), gameManifest ) )
			throw "Failed load game manifest";
	}
	catch ( const QString& Message )
	{
		errorMessage = Message;
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------//

/*
 * Инициализировать модуль визуализации
 * ----------------------------------------
 */

bool EngineAPI::InitRenderSystem( le::WindowHandle_t WindowHandle, le::Configurations& Configurations )
{
	if ( renderSystem ) 
		return true;

	try
	{
		if ( !renderSystemLib.isLoaded() )
			throw "Render system not loaded";

		renderSystem = LE_CreateRenderSystem();

		if ( !renderSystem->Initialize( ROUTE_DIR_LIFEENGINE, WindowHandle, gameManifest, Configurations ) )
			throw "Failed initialize render system";

		scene = renderSystem->GetScene();
		modelRenderer = renderSystem->GetModelRenderer();
		textRenderer = renderSystem->GetTextRenderer();
		textureManager = renderSystem->GetTextureManager();
		materialManager = renderSystem->GetMaterialManager();
		meshManager = renderSystem->GetMeshManager();
		fontManager = renderSystem->GetFontManager();

		le::Version     version = renderSystem->GetVersion();
		LOG_INFO( "Loaded render system " << version.major << "." << version.minor << "." << version.path << " (build " << version.build << " " << version.date << " " << version.time << ")" );

	}
	catch ( const QString& Message )
	{
		errorMessage = Message;
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------//