//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef LOCALKEYS_H
#define LOCALKEYS_H

//-------------------------------------------------------------------------//

#define LOCALISATION_WINDOW_WELCOME							"window_welcome"
#define LOCALISATION_WINDOW_CONFIGURE_GAME					"window_configuregame"
#define LOCALISATION_WINDOW_INPUT							"window_input"
#define LOCALISATION_WINDOW_SELECT_GAME_MANIFEST			"window_selectGameManifest"
#define LOCALISATION_WINDOW_ERROR							"window_error"
#define LOCALISATION_WINDOW_ABOUT_QT                        "window_aboutQt"
#define LOCALISATION_WINDOW_ABOUT_LIFETECHNOLOGIES          "window_aboutLifeTechnologies"

#define LOCALISATION_DOCKWIDGET_OBJECTS						"dockWidget_objects"
#define LOCALISATION_DOCKWIDGET_OBJECT_CREATION				"dockWidget_objectCreation"
#define LOCALISATION_DOCKWIDGET_PROPERTIES					"dockWidget_properties"

#define LOCALISATION_TAB_3D_MODELS							"tab_3dModels"
#define LOCALISATION_TAB_ENTITIES							"tab_entities"

#define LOCALISATION_MENU_FILE								"menu_file"
#define LOCALISATION_MENU_EDIT								"menu_edit"
#define LOCALISATION_MENU_MAP								"menu_map"
#define LOCALISATION_MENU_TOOLS								"menu_tools"
#define LOCALISATION_MENU_HELP								"menu_help"
#define LOCALISATION_MENU_FILE_NEW_FILE                     "menu_file_newFile"
#define LOCALISATION_MENU_FILE_OPEN_FILE                    "menu_file_openFile"
#define LOCALISATION_MENU_FILE_SAVE_FILE                    "menu_file_saveFile"
#define LOCALISATION_MENU_FILE_SAVE_FILE_AS                 "menu_file_saveFileAs"
#define LOCALISATION_MENU_FILE_CLOSE_FILE                   "menu_file_closeFile"
#define LOCALISATION_MENU_FILE_EXIT                         "menu_file_exit"
#define LOCALISATION_MENU_EDIT_UNDO                         "menu_edit_undo"
#define LOCALISATION_MENU_EDIT_REDO                         "menu_edit_redo"
#define LOCALISATION_MENU_MAP_SETTINGS_MAP                  "menu_map_settingsMap"
#define LOCALISATION_MENU_TOOLS_SETTINGS_EDITOR             "menu_tools_settingsEditor"
#define LOCALISATION_MENU_HELP_ABOUT_QT                     "menu_help_aboutQt"
#define LOCALISATION_MENU_HELP_ABOUT_LIFETECHNOLOGIES       "menu_help_aboutLifeTechnologies"

#define LOCALISATION_TOOLTIP_CURSOR                         "tooltip_cursor"
#define LOCALISATION_TOOLTIP_MOVE                           "tooltip_move"
#define LOCALISATION_TOOLTIP_ROTATE                         "tooltip_rotate"
#define LOCALISATION_TOOLTIP_SCALE                          "tooltip_scale"

#define LOCALISATION_LABEL_GAME								"label_game"
#define LOCALISATION_LABEL_LANGUAGE							"label_language"
#define LOCALISATION_LABEL_GAME_DIRECTORY					"label_gameDirectory"
#define LOCALISATION_LABEL_ENTER_NAME_CONFIGURATION_GAME	"label_enterNameConfigurationGame"
#define LOCALISATION_LABEL_NOT_SELECTED_CONFIGURATION_GAME	"label_notSelectedConfigurationGame"
#define LOCALISATION_LABEL_OBJECTS                          "label_objects"

#define LOCALISATION_BUTTON_START_EDITOR					"button_startEditor"
#define LOCALISATION_BUTTON_CONFIGURE_EDITOR				"button_configureEditor"
#define LOCALISATION_BUTTON_EXIT							"button_exit"
#define LOCALISATION_BUTTON_SELECT							"button_select"
#define LOCALISATION_BUTTON_ADD_CONFIGURE_GAME				"button_addConfigureGame"
#define LOCALISATION_BUTTON_REMOVE_CONFIGURE_GAME			"button_removeConfigureGame"
#define LOCALISATION_BUTTON_CANCEL							"button_cancel"
#define LOCALISATION_BUTTON_OK								"button_ok"
#define LOCALISATION_BUTTON_BROWSE                          "button_browse"

//-------------------------------------------------------------------------//

#endif // !LOCALKEYS_H
