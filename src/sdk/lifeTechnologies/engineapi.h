//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef ENGINEAPI_H
#define ENGINEAPI_H

#include <qstring.h>
#include <qlibrary.h>

#include "engine/lifeEngine.h"
#include "engine/gamemanifest.h"
#include "engine/iengine.h"
#include "engine/irendersystem.h"
#include "engine/irenderer.h"
#include "engine/iscene.h"
#include "engine/imodel.h"
#include "engine/itext.h"

//-------------------------------------------------------------------------//

class EngineAPI
{
public:
	/* Конструктор */
	EngineAPI();

	/* Деструктор */
	~EngineAPI();

	/* Загрузить ядро движка */
	bool LoadCoreEngine( const QString& Route );

	/* Выгрузить ядро движка */
	void UnloadCoreEngine();

	/* Загрузить модуль визуализации */
	bool LoadRenderSystem( const QString& Route );

	/* Выгрузить модуль визуализации */
	void UnloadRenderSystem();

	/* Загрузить манифест игры */
	bool LoadGameManifest( const QString& GameDir );

	/* Инициализировать модуль визуализации */
	bool InitRenderSystem( le::WindowHandle_t WindowHandle, le::Configurations& Configurations );

	/* Получить сообщение об ошибке */
	inline const QString& GetErrorMessage() const
	{
		return errorMessage;
	}

	/* Получить манифест игры */
	inline const le::GameManifest& GetGameManifest() const
	{
		return gameManifest;
	}

	/* Получить ядро движка */
	le::IEngine* GetCoreEngine() const
	{
		return engine;
	}

	/* Получить модуль визуализации */
	le::IRenderSystem* GetRenderSystem() const
	{
		return renderSystem;
	}

	/* Получить сцену */
	le::IScene* GetScene() const
	{
		return scene;
	}

	/* Получить отрисовщик моделей */
	le::IRenderer<le::IModel>* GetModelRenderer() const
	{
		return modelRenderer;
	}

	/* Получить отрисовщик текста */
	le::IRenderer<le::IText>* GetTextRenderer() const
	{
		return textRenderer;
	}

	/* Получить менеджер текстур */
	inline le::IResurceManager<le::ITexture>* GetTextureManager() const
	{
		return textureManager;
	}

	/* Получить менеджер материалов */
	inline le::IResurceManager<le::IMaterial>* GetMaterialManager() const
	{
		return materialManager;
	}

	/* Получить менеджер мешей */
	inline le::IResurceManager<le::IMesh>* GetMeshManager() const
	{
		return meshManager;
	}

	/* Получить менеджер шрифтов */
	inline le::IResurceManager<le::IFont>* GetFontManager() const
	{
		return fontManager;
	}

	/* Загружено ли ядро движка */
	inline bool IsLoadedCoreEngine() const
	{
		return coreEngineLib.isLoaded() && engine;
	}

	/* Загружен ли модуль визуализации */
	inline bool IsLoadedRenderSystem() const
	{
		return renderSystemLib.isLoaded() && renderSystem;
	}

private:
	QLibrary								coreEngineLib;
	QLibrary								renderSystemLib;
	QString									errorMessage;

	le::Func_LE_LoadConfig_t				LE_LoadConfig;
	le::Func_LE_LoadGameManifest_t			LE_LoadGameManifest;
	le::Func_LE_CreateCoreEngine_t			LE_CreateCoreEngine;
	le::Func_LE_DeleteCoreEngine_t			LE_DeleteCoreEngine;
	le::Func_LE_CreateRenderSystem_t		LE_CreateRenderSystem;
	le::Func_LE_DeleteRenderSystem_t		LE_DeleteRenderSystem;
				
	le::GameManifest						gameManifest;
	le::IEngine*							engine;
	le::IRenderSystem*						renderSystem;
	le::IScene*								scene;
	le::IRenderer<le::IModel>*				modelRenderer;
	le::IRenderer<le::IText>*				textRenderer;
	le::IResurceManager<le::ITexture>*		textureManager;		
	le::IResurceManager<le::IMaterial>*		materialManager;
	le::IResurceManager<le::IMesh>*			meshManager;		
	le::IResurceManager<le::IFont>*			fontManager;		
};

//-------------------------------------------------------------------------//

extern EngineAPI			engineAPI;

//-------------------------------------------------------------------------//

#endif // !ENGINEAPI_H
