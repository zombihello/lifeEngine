//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef TOOLMOVE_H
#define TOOLMOVE_H

#include <qstring.h>
#include <qlist.h>
#include "engine/imesh.h"
#include "engine/imodel.h"
#include "engine/imaterial.h"
#include "engineapi.h"

//-------------------------------------------------------------------------//

class ToolMove
{
public:
	/* Конструктор */
	ToolMove();

	/* Деструктор */
	~ToolMove();

	/* Инициализировать контрол для работы */
	void Initialize();

	/* Показать контрол */
	void ShowControl();

	/* Скрыть контрол */
	void HideControl();

	/* Находится ли модель контрола на сцене */
	inline bool IsOnScene() const
	{
		return isOnScene;
	}

	/* Задать позицию контрола */
	inline void SetPosition_ControlModel( const glm::vec3& Position )
	{
		controlModel->SetPosition( Position );
	}

private:
	bool					isInitialize;
	bool					isOnScene;

	le::IMesh*				controlMesh;
	le::IModel*				controlModel;
	QList<le::IMaterial*>	controlMaterials;
};

//-------------------------------------------------------------------------//

#endif // !TOOLMOVE_H
