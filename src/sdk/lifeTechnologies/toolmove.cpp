//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "toolmove.h"
#include "engine/icamera.h"

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------------
 */

ToolMove::ToolMove() :
	controlModel( nullptr ),
	isOnScene( false ),
	isInitialize( false )
{}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ---------------------
 */

ToolMove::~ToolMove()
{
	HideControl();
}

//-------------------------------------------------------------------------//

/*
 * Инициализировать контрол для работы
 * ---------------------
 */

void ToolMove::Initialize()
{
	if ( isInitialize || !engineAPI.GetRenderSystem() ) return;

	controlModel = engineAPI.GetRenderSystem()->CreateModel();
	controlModel->Load( "control_move.lmd", "control_move.lmd" );
	
	isInitialize = true;
}

//-------------------------------------------------------------------------//

/*
 * Показать контрол
 * ---------------------
 */

void ToolMove::ShowControl()
{
	if ( isOnScene || !isInitialize || !engineAPI.GetScene() ) return;

	engineAPI.GetScene()->AddModel( controlModel );
	isOnScene = true;
}

//-------------------------------------------------------------------------//

/*
 * Скрыть контрол
 * ---------------------
 */

void ToolMove::HideControl()
{
	if ( !isOnScene || !isInitialize || !engineAPI.GetScene() ) return;

	engineAPI.GetScene()->RemoveModel( controlModel );
	isOnScene = false;
}

//-------------------------------------------------------------------------//