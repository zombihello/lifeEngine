//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "level.h"
#include "brush.h"
#include "window_editorscene.h"
#include "engineapi.h"

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ------------------
 */

Level::Level()
{}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * -------------
 */

Level::~Level()
{
    for ( auto it = brushes.begin(), itEnd = brushes.end(); it != itEnd; ++it )
        delete *it;

    brushes.clear();
}

//-------------------------------------------------------------------------//

/*
 * Создать браш на уровень
 * ----------------------------
 */

Brush* Level::CreateBrush( Brush::BRUSH_TYPE BrushType )
{
    Brush*          brush = new Brush();
    brush->Create( BrushType );

	brushes.insert( brush->GetId(), brush );
	engineAPI.GetScene()->AddModel( brush->GetModel() );
    return brush;
}

//-------------------------------------------------------------------------//

/*
 * Удалить браш из уровня
 * -----------------------
 */

void Level::DeleteBrush( Brush* Brush )
{
	auto		it = brushes.find( Brush->GetId() );
	if ( it == brushes.end() ) return;

	brushes.erase( it );
	engineAPI.GetScene()->RemoveModel( Brush->GetModel() );
	delete Brush;
}

//-------------------------------------------------------------------------//

/*
* Очистить уровень
* -----------------------
*/

void Level::Clear()
{
	for ( auto it = brushes.begin(), itEnd = brushes.end(); it != itEnd; ++it )
		delete *it;

	brushes.clear();
	engineAPI.GetScene()->Clear();
}

//-------------------------------------------------------------------------//