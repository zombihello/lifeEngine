//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qdebug.h>
#include <qapplication.h>
#include "keyscontrol.h"
#include "window_editorscene.h"

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ------------------------------
 */

KeysControl::KeysControl()
{
	QApplication::instance()->installEventFilter( this );
}

//-------------------------------------------------------------------------//

/*
 * Деструктор
 * ------------------------------
 */

KeysControl::~KeysControl()
{
	QApplication::instance()->removeEventFilter( this );
}

//-------------------------------------------------------------------------//

/*
 * Фильтр событий
 * ------------------------------
 */

bool KeysControl::eventFilter( QObject* Object, QEvent* Event )
{
	return QApplication::activeWindow() ? QApplication::activeWindow()->eventFilter( Object, Event ) : false;
}

//-------------------------------------------------------------------------//