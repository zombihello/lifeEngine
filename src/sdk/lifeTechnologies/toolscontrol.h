//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef TOOLSCONTROL_H
#define TOOLSCONTROL_H

#include <qobject.h>
#include <qhash.h>

#include "toolselect.h"
#include "tooladdmodel.h"
#include "toolmove.h"

//-------------------------------------------------------------------------//

enum TOOLS_TYPE
{
	TT_NONE,
	TT_SELECT,		// Выбор объектов
	TT_MOVE,		// Перемещение объектов
	TT_ROTATE,		// Вращение объектов
	TT_SCALE,		// Масштабирование объектов
	TT_ADD_MODEL,	// Добавление моделей на сцену
	TT_ADD_ENTITY	// Добавление сущностей на сцену
};

//-------------------------------------------------------------------------//

class ToolsControl
{
public:
	typedef void ( QObject::*ObjectCallback_t )( TOOLS_TYPE ToolType );

	/* Конструктор */
	ToolsControl();

	/* Деструктор */
	~ToolsControl();

	/* Инициализировать контролы для работы */
	void Initialize();

	/* Добавить метод обратного вызова */
	void AddCallback( QObject* Object, ObjectCallback_t ObjectCallback );

	/* Удалить метод обратного вызова */
	void RemoveCallback( QObject* Object );

	/* Задать тип выбранного инструмента */
	void SetSelectTypeTool( TOOLS_TYPE ToolType );

	/* Получить тип выбранного инструмента */
	inline TOOLS_TYPE GetSelectTypeTool() const
	{
		return selectTypeTool;
	}

	/* Получить инструмент выбора объектов */
	inline ToolSelect& GetToolSelect()
	{
		return toolSelect;
	}

	/* Получить инструмент добавления моделей */
	inline ToolAddModel& GetToolAddModel()
	{
		return toolAddModel;
	}

	/* Получить инструмент перемещения моделей */
	inline ToolMove& GetToolMove()
	{
		return toolMove;
	}

private:
	bool									isInitialize;
	TOOLS_TYPE								selectTypeTool;
	ToolSelect								toolSelect;
	ToolAddModel							toolAddModel;
	ToolMove								toolMove;
	QHash<QObject*, ObjectCallback_t>		callbacks;
};

//-------------------------------------------------------------------------//

extern	ToolsControl		toolsControl;

//-------------------------------------------------------------------------//

#endif // !TOOLSCONTROL_H
