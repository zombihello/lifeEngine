//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <qfile.h>
#include <qapplication.h>
#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qjsonarray.h>

#include "engine/logger.h"
#include "localisation.h"

//-------------------------------------------------------------------------//

uint32_t						Localisation::idCurrentLocale = 0;
QString							Localisation::dirLocalisations;
Local							Localisation::locale;
vector<LocaleSettings>			Localisation::localSettings;

//-------------------------------------------------------------------------//

/*
 * Загрузить файл локализаций
 * ----------------------------------------
 */

bool Localisation::LoadFromFile( const QString& DirLocalisations, const QString& FileName )
{
    LOG_INFO( "File localisations [" << FileName.toStdString() << "] in directory [" << DirLocalisations.toStdString() << "] loading" );

    dirLocalisations = DirLocalisations;
    QFile		fileLocalisations( DirLocalisations + "/" + FileName );

    if ( !fileLocalisations.open( QIODevice::ReadOnly ) )
    {
        LOG_ERROR( "File localisation not loaded. File or directory not found" );
        return false;
    }

    QJsonDocument		jsonDoc = QJsonDocument::fromJson( fileLocalisations.readAll() );
    QJsonObject			jsonObject = jsonDoc.object();

    if ( jsonObject.contains( "localisations" ) )
    {
        QJsonArray			jsonArray = jsonObject[ "localisations" ].toArray();
        QJsonObject			tempJsonObject;
        LocaleSettings      localeSettings;

        for ( size_t index = 0, count = jsonArray.size(); index < count; ++index )
        {
            tempJsonObject = jsonArray[ index ].toObject();

            if ( tempJsonObject.contains( "route" ) )
                localeSettings.route = tempJsonObject[ "route" ].toString();
            else
                continue;

            if ( tempJsonObject.contains( "key" ) )
                localeSettings.key = tempJsonObject[ "key" ].toString();
            else
                continue;

            if ( tempJsonObject.contains( "name" ) )
                localeSettings.name = tempJsonObject[ "name" ].toString();
            else
                localeSettings.name = localeSettings.key;

            if ( tempJsonObject.contains( "icon" ) )
                localeSettings.icon = tempJsonObject[ "icon" ].toString();

			localSettings.push_back( localeSettings );
        }
    }
    else
    {
        LOG_ERROR( "File localisation not contains json object [localisations]" );
        return false;
    }

    if ( localSettings.empty() )
        LOG_WARNING( "Local settings is empty" );

    LOG_INFO( "File localisations loaded" );
    return !localSettings.empty();
}

//-------------------------------------------------------------------------//

/*
 * Получить перевод текста
 * ----------------------------------------
 */

QString Localisation::Translate( const QString& Key )
{
    return locale[ Key ];
}

//-------------------------------------------------------------------------//

/*
 * Получить перевод текста
 * ----------------------------------------
 */

QString Localisation::Translate( const QString &Key, vector<QString>& Arguments )
{
    QString         translateString = locale[ Key ];

    for ( size_t index = 0, count = Arguments.size(); index < count; ++index )
        translateString.replace( translateString.indexOf( "%" ), 1, Arguments[ index ] );

    return translateString;
}

//-------------------------------------------------------------------------//

/*
 * Задать текущую локализацию
 * ----------------------------------------
 */

bool Localisation::SetLocale( const QString& Key )
{	
    LocaleSettings*     localeSettings = nullptr;

    for ( size_t index = 0, count = localSettings.size(); index < count; ++index )
        if ( localSettings[ index ].key == Key )
        {
            localeSettings = &localSettings[ index ];
			idCurrentLocale = index;
            break;
        }

    if ( !localeSettings )
    {
        LOG_ERROR( "Failed change local [" << Key.toStdString() << "]" );
        return false;
    }

    return locale.LoadFromFile( dirLocalisations + "/" + localeSettings->route );
}

//-------------------------------------------------------------------------//

/*
 * Задать текущую локализацию
 * ----------------------------------------
 */

bool Localisation::SetLocale( unsigned int Index )
{
    if ( Index > localSettings.size() )
    {
        LOG_ERROR( "Failed change local with id [" << Index << "]. Index big count locales" );
        return false;
    }

	idCurrentLocale = Index;
    return locale.LoadFromFile( dirLocalisations + "/" + localSettings[ Index ].route );
}

//-------------------------------------------------------------------------//
