//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef WINDOW_CONFIGURE_GAME_H
#define WINDOW_CONFIGURE_GAME_H

#include <qdialog.h>

#include "configuration.h"
#include "settings.h"
#include "ui_window_configuregame.h"

//-------------------------------------------------------------------------//

class Window_ConfigureGame : public QDialog
{
	Q_OBJECT

public:
	enum DIALOG_CODE
	{
		DC_CANCEL,
		DC_OK
	};

	/* Конструктор */
	Window_ConfigureGame( const vector<Configuration>& Configurations, QWidget *parent = Q_NULLPTR );

	/* Деструктор */
	~Window_ConfigureGame();

	/* Получить конфигурации */
	inline const vector<Configuration>& GetConfigurations() const
	{
		return configurations;
	}

private slots:
	/* Событие выбора конфигурации */
	void on_comboBox_game_currentIndexChanged( int Index );

	/* Событие нажатия кнопки "Добавить конфигурацию игры" */
	void on_pushButton_addConfigure_clicked();

	/* Событие нажатия кнопки "Выбрать" */
	void on_pushButton_selectDir_clicked();

	/* Событие нажатия кнопки "Удалить конфигурацию игры" */
	void on_pushButton_removeConfigure_clicked();

	/* Событие нажатия кнопки "Отмена" */
	void on_pushButton_cancel_clicked();

	/* Событие нажатия кнопки "Ок" */
	void on_pushButton_ok_clicked();

private:
	/* Обновить локализацию окна */
	void UpdateLocalisation();
	
	Ui::Window_ConfigureGame		ui;
	vector<Configuration>			configurations;
};

//-------------------------------------------------------------------------//

#endif // !WINDOW_CONFIGURE_GAME_H