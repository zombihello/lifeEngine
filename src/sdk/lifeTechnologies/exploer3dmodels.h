//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef EXPLOER3DMODELS_H
#define EXPLOER3DMODELS_H

#include <qtreeview.h>
#include <qfilesystemmodel.h>
#include <qproxystyle.h>

//-------------------------------------------------------------------------//

class Exploer3DModels : public QTreeView
{
public:
    /* Конструктор */
	Exploer3DModels( QWidget *parent = nullptr );

    /* Деструктор */
    ~Exploer3DModels();

    /* Задать каталог с моделями */
    void SetRootDir( const QDir& RootDir );

    /* Получить модель файловой системы */
    QFileSystemModel& GetFileSystemModel();

    /* Получить каталог с моделями */
    const QDir& GetRootDir() const;

	/* Событие нажатий кнопки мыши */
	void mousePressEvent( QMouseEvent* Event );

	/* Событие нажатия клавиши */
	void keyPressEvent( QKeyEvent* Event );

private:
	QDir					rootDir;
	QFileSystemModel		fileSystemModel;
};

//-------------------------------------------------------------------------//

#endif // !EXPLOER3DMODELS_H
