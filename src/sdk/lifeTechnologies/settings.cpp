//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////


#include <qfile.h>
#include <qcoreapplication.h>
#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qjsonarray.h>

#include "engine/logger.h"
#include "settings.h"

//-------------------------------------------------------------------------//

/*
 * Конструктор
 * ----------------------------------------
 */

Settings::Settings() :
	lastLanguage( "en" ),
	lastIDConfiguration( -1 )
{}

//-------------------------------------------------------------------------//

/*
 * Загрузить конфигурации
 * ----------------------------------------
 */

bool Settings::LoadFromFile( const QString& Route )
{
    LOG_INFO( "Settings editor [" << Route.toStdString() << "] loading" );

	QFile		file( Route );
	if ( !file.open( QIODevice::ReadOnly ) )
    {
        LOG_ERROR( "Settings editor not loaded. File or directory not found" );
		return false;
    }

	QJsonDocument		jsonDoc = QJsonDocument::fromJson( file.readAll() );
	QJsonObject			jsonRoot = jsonDoc.object();

	bool				isContainsConfigurations = jsonRoot.contains( "configurations" );
	bool				isContainsGeneral = jsonRoot.contains( "general" );

	// Считываем блок конфигураций

	if ( isContainsConfigurations && jsonRoot[ "configurations" ].isArray() )
	{
		QJsonArray			jsonConfigurations = jsonRoot[ "configurations" ].toArray();
		QJsonObject			jsonObject;
		Configuration		configuration;

		for ( uint32_t index = 0, count = jsonConfigurations.size(); index < count; ++index )
			if ( jsonConfigurations[ index ].isObject() )
			{			
				jsonObject = jsonConfigurations[ index ].toObject();
				configuration.name = jsonObject.contains( "name" ) && jsonObject[ "name" ].isString() ? jsonObject[ "name" ].toString() : "";
				if ( configuration.name.isEmpty() ) continue;

				configuration.gameDir = jsonObject.contains( "directory" ) && jsonObject[ "directory" ].isString() ? jsonObject[ "directory" ].toString() : "";			
				configurations.push_back( configuration );
			}
	}

	// Считываем блок общих настроек

	if ( isContainsGeneral )
	{
		QJsonObject			jsonGeneral = jsonRoot[ "general" ].toObject();
		lastLanguage = jsonGeneral.contains( "lastLanguage" ) && jsonGeneral[ "lastLanguage" ].isString() ? jsonGeneral[ "lastLanguage" ].toString() : "en";
		lastIDConfiguration = jsonGeneral.contains( "lastConfiguration" ) && jsonGeneral[ "lastConfiguration" ].isDouble() ? jsonGeneral[ "lastConfiguration" ].toInt() : -1;
	}

    LOG_INFO( "Settings editor loaded" );
	return true;
}

//-------------------------------------------------------------------------//

/*
 * Сохранить конфигурации
 * ----------------------------------------
 */

void Settings::SaveToFile( const QString& Route ) const
{
	// Формируем блок конфигураций

	QJsonArray			jsonConfigurations;
	{
		QJsonObject			jsonObject;
		for ( auto it = configurations.begin(), itEnd = configurations.end(); it != itEnd; ++it )
		{
			QJsonObject			jsonObject;
			jsonObject[ "name" ] = it->name;
			jsonObject[ "directory" ] = it->gameDir;
			jsonConfigurations.append( jsonObject );
		}
	}

	// Формируем блок общих настроек

	QJsonObject			jsonGeneral;
	jsonGeneral[ "lastLanguage" ] = lastLanguage;
	jsonGeneral[ "lastConfiguration" ] = lastIDConfiguration;

	// Формируем корневой блок настроек

	QJsonObject			jsonRoot;
	jsonRoot[ "configurations" ] = jsonConfigurations;
	jsonRoot[ "general" ] = jsonGeneral;

	QJsonDocument		jsonDoc = QJsonDocument( jsonRoot );
	QFile				fileProjects( Route );

	if ( fileProjects.open( QIODevice::WriteOnly | QIODevice::Truncate ) )
    {
		fileProjects.write( jsonDoc.toJson() );
        LOG_INFO( "Settings editor saved to [" << Route.toStdString() << "]" );
    }
    else
        LOG_ERROR( "Failed save settings editor. Error: " << fileProjects.errorString().toStdString() );
}

//-------------------------------------------------------------------------//
