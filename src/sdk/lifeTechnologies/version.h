//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef VERSION_H
#define VERSION_H

#include <qstring.h>

//-------------------------------------------------------------------------//

struct Version
{
	static QString				ToString();

    static unsigned short		major;
    static unsigned short		minor;
    static unsigned short		patch;
    static unsigned int			build;
};

//-------------------------------------------------------------------------//

#endif // !VERSION_H
