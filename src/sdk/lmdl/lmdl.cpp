//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <filesystem>
#include "settings.h"
#include "mesh.h"
namespace fs = std::experimental::filesystem;
using namespace std;

#define GOLD_DATE				43379			// Oct 8 2019 (начало разработки lmdl)
#define LMDL_VERSION_MAJOR		0
#define LMDL_VERSION_MINOR		1
#define LMDL_VERSION_PATCH		0

//---------------------------------------------------------------------//

/*
 * Высчитать номер сборки
 * -----------------
 */

int ComputeBuildNumber( int GoldDate )
{
	const char* date = __DATE__;

	const char* month[ 12 ] =
	{ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	const char		month_days[ 12 ] =
	{ 31,    28,    31,    30,    31,    30,    31,    31,    30,    31,    30,    31 };

	int				buildNumber = 0;
	int				months = 0;
	int				days = 0;
	int				years = 0;

	for ( months = 0; months < 11; ++months )
	{
		if ( strncmp( &date[ 0 ], month[ months ], 3 ) == 0 )
			break;

		days += month_days[ months ];
	}

	days += atoi( &date[ 4 ] ) - 1;
	years = atoi( &date[ 7 ] ) - 1900;

	buildNumber = days + static_cast< int >( ( years - 1 ) * 365.25f );

	if ( ( years % 4 == 0 ) && months > 1 )
		++buildNumber;

	buildNumber -= GoldDate;
	return buildNumber;
}

//---------------------------------------------------------------------//

/*
 * Соответсвует ли строка маске
 * ------------------------------
 */

bool IsMatchMask( const string& Name, const string& Mask )
{
	uint32_t		indexMask = 0;
	uint32_t		indexName = 0;
	uint32_t		countMask = Mask.size();
	uint32_t		countName = Name.size();

	while ( indexName < countName || indexMask < countMask )
	{
		if ( indexName + 1 < countName && Name[ indexName ] == '.' &&
			( Name[ indexName + 1 ] == '/' || Name[ indexName + 1 ] == '\\' ) )
		{
			indexName += 2;
			continue;
		}

		else if ( Mask[ indexMask ] == '*' )
		{
			++indexMask;
			if ( indexMask + 1 >= countMask ) break;

			while ( indexName < countName )
			{
				if ( Name[ indexName ] == Mask[ indexMask ] )
					break;

				++indexName;
			}

			continue;
		}

		else if ( Mask[ indexMask ] != '/' && Mask[ indexMask ] != '\\' &&
				  Name[ indexName ] != '/' && Name[ indexName ] != '\\' &&
				  Name[ indexName ] != Mask[ indexMask ] )
			return false;

		++indexName;
		++indexMask;
	}

	return true;
}

//---------------------------------------------------------------------//

/*
 * Вывод использования утилиты
 * ------------------------------
 */

void PrintUsage()
{
	cout << "-------------------------\n"
		<< "lifeEngine Model " << LMDL_VERSION_MAJOR << "." << LMDL_VERSION_MINOR << "." << LMDL_VERSION_PATCH << " (build " << ComputeBuildNumber( GOLD_DATE ) << ") by Egor Pogulyaka\n"
		<< "Converter models to LMD format\n"
		<< "-------------------------\n\n"
		<< "Usage: lmdl [options] [files]\n"
		<< "Example: lmdl -outdir outDir *.obj monster1/*.fbx\n\n"
		<< "-h | -help\t->\tshow this message\n"
		<< "-od | -outdir\t->\tset out directory for models\n"
		<< "-sd | -srcdir\t->\tset source directory with models\n";
}

//---------------------------------------------------------------------//

/*
 * Пропарсить аргументы запуска
 * ------------------------------
 */

void ParseArgs( int argc, char** argv )
{
	uint32_t		lastIndexArg = 1;

	// Парсим аргументы запуска

	for ( uint32_t index = 1; index < argc; ++index )
	{
		// Вывод информации об использовании утилиты

		if ( strstr( argv[ index ], "-h" ) || strstr( argv[ index ], "-help" ) )
		{
			PrintUsage();
			exit( 0 );
		}

		// Выходной каталог для моделей

		else if ( ( strstr( argv[ index ], "-od" ) || strstr( argv[ index ], "-outdir" ) ) && index + 1 < argc )
		{
			Settings::outDirectory = argv[ index + 1 ];
			++index;
		}

		// Исходный каталог с моделями

		else if ( ( strstr( argv[ index ], "-sd" ) || strstr( argv[ index ], "-srcdir" ) ) && index + 1 < argc )
		{
			Settings::srcDirectory = argv[ index + 1 ];
			++index;
		}

		// Иначе продолжем парсить
		else continue;

		// Если мы нашли аргумент, то запоминаем следующий индекс.
		// Там могут быть пути к моделям
		lastIndexArg = index + 1;
	}

	// Забираем из аргументов запуска пути к моделям

	string		srcFile;
	string		srcDir;

	for ( uint32_t index = lastIndexArg; index < argc; ++index )
	{
		srcFile = argv[ index ];
		srcDir.clear();

		// Заменяем в пути к модели '\' на '/'

		uint32_t	indexSlash = srcFile.find_first_of( '\\' );
		while ( indexSlash != string::npos )
		{
			srcFile[ indexSlash ] = '/';
			indexSlash = srcFile.find_first_of( '\\' );
		}

		indexSlash = srcFile.find_last_of( '/' );
		if ( indexSlash != string::npos )
		{
			srcDir = srcFile;
			srcDir.erase( indexSlash, srcFile.size() );
		}

		for ( const auto& file : fs::directory_iterator( srcDir.empty() ? Settings::srcDirectory : srcDir ) )
		{
			if ( !fs::is_regular_file( file.status() ) )
				continue;

			string fileName = file.path().u8string();

			if ( !IsMatchMask( fileName, srcFile ) )
				continue;

			string		path = file.path().u8string();
			string		name = file.path().filename().u8string();
			string		format = file.path().extension().u8string();

			// Убираем формат в названии файла

			if ( name.find( format ) != string::npos )
				name.erase( name.find( format ), format.size() );

			Settings::srcFiles.push_back( File( path, name, format ) );
		}
	}
}

//---------------------------------------------------------------------//

/*
 * Точка входа
 * ------------------------------
 */

int main( int argc, char** argv )
{
	if ( argc > 1 )
	{
		// Парсим аргументы запуска

		ParseArgs( argc, argv );

		// Нашли ли мы модели, которые нужно конвертировать?

		if ( Settings::srcFiles.empty() )
		{
			cout << "Source files not found or not set\n";
			return 1;
		}

		if ( !Settings::outDirectory.empty() && !fs::exists( Settings::outDirectory ) && !fs::create_directory( Settings::outDirectory ) )
		{
			cout << "Fail created directory [" << Settings::outDirectory << "]\n";
			return 1;
		}

		// Проходимся по всем файлам и конвертируем

		Mesh			mesh;
		string			outFileName;

		for ( auto it = Settings::srcFiles.begin(), itEnd = Settings::srcFiles.end(); it != itEnd; ++it )
		{
			File*		file = &*it;
			outFileName = Settings::outDirectory + "/" + file->name + ".lmd";

			cout << "\nModel: " << it->path << endl;
			cout << "----------------\nModel converting...\n";

			// Загружаем меш

			if ( !mesh.Load( it->path ) )
			{
				cout << "Converted: fail\n";
				continue;
			}
			else
				cout << "Converted: success\nSaving model..\n";

			// Сохраняем меш в формате lifeEngine Model

			if ( !mesh.Save( outFileName ) )
				cout << "Saving: fail\n";
			else
				cout << "Saving: success\n";
		}
	}
	else
		PrintUsage();

	return 0;
}

//---------------------------------------------------------------------//