//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef MATERIAL_H
#define MATERIAL_H

#include <string>
using namespace std;

//---------------------------------------------------------------------//

struct aiMaterial;

//---------------------------------------------------------------------//

class Material
{
public:
	/* Конструктор */
	Material();

	/* Загрузить материал */
	void Load( aiMaterial* aiMaterial, const string& RootDirectory = "" );

	/* Сохранить материал */
	void Save( const string& Directory );

	inline const string& GetName() const
	{
		return name;
	}

private:
	string			name;
	string			routeDiffuseMap;
	string			routeNormalMap;
	string			routeSpecularMap;
	string			routeEmissionMap;

	float			ambientColor[ 4 ];
	float			diffuseColor[ 4 ];
	float			emissionColor[ 4 ];
	float			specular;
	float			shininess;
};

//---------------------------------------------------------------------//

#endif // !MATERIAL_H
