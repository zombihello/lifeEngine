//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>
#include <unordered_map>
#include <assimp/vector2.h>
#include <assimp/vector3.h>
#include "material.h"
using namespace std;

//---------------------------------------------------------------------//

struct aiNode;
struct aiMesh;
struct aiScene;

//---------------------------------------------------------------------//

class Mesh
{
public:

	//---------------------------------------------------------------------//

	struct Vertex
	{
		/* Конструктор */
		Vertex();

		float		position[ 3 ];		// Позиция вершины
		float		normal[ 3 ];		// Нормаль вершины
		float		texCoords[ 2 ];		// Текстурные координаты
		float		tangent[ 3 ];		// Тангент
		float		bitangent[ 3 ];		// Битангент

		bool operator==( const Vertex& Right );
	};

	//---------------------------------------------------------------------//

	struct Surface
	{
		/* Конструктор */
		Surface();

		uint32_t		materialId;				// ID материала
		uint32_t        startVertexIndex;       // Начальный индекс вершины
		uint32_t        countVertexIndex;       // Количество индексов вершины
	};

	//---------------------------------------------------------------------//

	/* Конструктор */
	Mesh();

	/* Деструктора */
	~Mesh();

	/* Загрузить меш */
	bool Load( const string& Route );

	/* Сохранить меш */
	bool Save( const string& Route );

	/* Очистить загруженый меш */
	void Clear();

private:
	/* Оброботать узел сцены Assimp'a */
	void ProcessNode( aiNode* aiNode, const aiScene* &aiScene, unordered_map<uint32_t, vector<aiMesh*>>& aiMeshes );

	bool					isLoaded;
	string					directory;

	vector<Vertex>			verteces;
	vector<uint32_t>		indices;
	vector<Surface>			surfaces;
	vector<Material>		materials;
};

//---------------------------------------------------------------------//

#endif // !MESH_H
