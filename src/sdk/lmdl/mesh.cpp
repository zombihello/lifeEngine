//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <fstream>
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/material.h>
#include "mesh.h"
#include "settings.h"
using namespace std;

const char							strId[ 3 ] = { 'L', 'M', 'D' };
const unsigned short				version = 2;

//---------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------
 */

Mesh::Vertex::Vertex()
{
	position[ 0 ] = 0.f;
	position[ 1 ] = 0.f;
	position[ 2 ] = 0.f;

	normal[ 0 ] = 0.f;
	normal[ 1 ] = 0.f;
	normal[ 2 ] = 0.f;

	texCoords[ 0 ] = 0.f;
	texCoords[ 1 ] = 0.f;

	tangent[ 0 ] = 0.f;
	tangent[ 1 ] = 0.f;
	tangent[ 2 ] = 0.f;

	bitangent[ 0 ] = 0.f;
	bitangent[ 1 ] = 0.f;
	bitangent[ 2 ] = 0.f;
}

//---------------------------------------------------------------------//

bool Mesh::Vertex::operator==( const Vertex& Right )
{
	return
		position[ 0 ] == Right.position[ 0 ] &&
		position[ 1 ] == Right.position[ 1 ] &&
		position[ 2 ] == Right.position[ 2 ] &&

		normal[ 0 ] == Right.normal[ 0 ] &&
		normal[ 1 ] == Right.normal[ 1 ] &&
		normal[ 2 ] == Right.normal[ 2 ] &&

		texCoords[ 0 ] == Right.texCoords[ 0 ] &&
		texCoords[ 1 ] == Right.texCoords[ 1 ] &&

		tangent[ 0 ] == Right.tangent[ 0 ] &&
		tangent[ 1 ] == Right.tangent[ 1 ] &&
		tangent[ 2 ] == Right.tangent[ 2 ] &&

		bitangent[ 0 ] == Right.bitangent[ 0 ] &&
		bitangent[ 1 ] == Right.bitangent[ 1 ] &&
		bitangent[ 2 ] == Right.bitangent[ 2 ];
}

//---------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------
 */

Mesh::Surface::Surface() :
	materialId( 0 ),
	startVertexIndex( 0 ),
	countVertexIndex( 0 )
{}

//---------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------
 */

Mesh::Mesh() :
	isLoaded( false )
{}

//---------------------------------------------------------------------//

/*
 * Деструктор
 * ---------------
 */

Mesh::~Mesh()
{}

//---------------------------------------------------------------------//

/*
 * Загрузить меш
 * ---------------
 */

bool Mesh::Load( const string& Route )
{
	// Загружаем меш с помощью Assimp'a

	Assimp::Importer		import;
	const aiScene*			scene = import.ReadFile( Route, aiProcess_Triangulate | aiProcess_CalcTangentSpace | aiProcess_GenSmoothNormals | aiProcess_LimitBoneWeights | aiProcess_Triangulate );
	
	// Если не удалось загрузить - выходим,
	// а так же если ранее был загружен
	// меш, то очищаем

	if ( !scene )		return false;
	if ( isLoaded )		Clear();

	// Из пути получаем директорию
	// где находится меш, а	
	// так же заменяем в пути к модели '\' на '/'
	
	directory = Route;
	uint32_t	indexSlash = directory.find_first_of( '\\' );
	while ( indexSlash != string::npos )
	{
		directory[ indexSlash ] = '/';
		indexSlash = directory.find_first_of( '\\' );
	}

	indexSlash = directory.find_last_of( '/' );
	if ( indexSlash != string::npos )
		directory.erase( indexSlash, directory.size() );

	// Собираем все меши со сцены Assimp'a и сортируем по материалам
	// чтобы объеденить в один меш

	unordered_map<uint32_t, vector<aiMesh*>>			aiMeshes;

	ProcessNode( scene->mRootNode, scene, aiMeshes );
	if ( aiMeshes.empty() )		return false;

	// Проходимся по ID материалов, берем меш и записываем его вершины, и индексы
	// в общий буфер
	
	Surface				surface;
	Vertex				vertex;
	vector<Vertex>		vertexBuffer;
	cout << "scene->mNumMaterials: " << scene->mNumMaterials << endl;
	for ( auto itRoot = aiMeshes.begin(), itRootEnd = aiMeshes.end(); itRoot != itRootEnd; ++itRoot )
	{
		
		surface.startVertexIndex = indices.size();
		surface.materialId = materials.size();

		for ( auto itMesh = itRoot->second.begin(), itMeshEnd = itRoot->second.end(); itMesh != itMeshEnd; ++itMesh )
		{
			aiMesh*				mesh = *itMesh;
			
			// Подготавливаем буфер вершин.
			// Если в буфер не влезут вершины меша, то
			// расширяем его

			if ( vertexBuffer.size() < mesh->mNumVertices )
				vertexBuffer.resize( vertexBuffer.size() + mesh->mNumVertices );

			// Считываем все вершины
			// *********************

			for ( uint32_t index = 0; index < mesh->mNumVertices; ++index )
			{
				aiVector3D		tempVector = mesh->mVertices[ index ];
				vertex.position[ 0 ] = tempVector.x;
				vertex.position[ 1 ] = tempVector.y;
				vertex.position[ 2 ] = tempVector.z;
			
				tempVector = mesh->mNormals[ index ];
				vertex.normal[ 0 ] = tempVector.x;
				vertex.normal[ 1 ] = tempVector.y;
				vertex.normal[ 2 ] = tempVector.z;

				if ( mesh->mTangents )
				{
					tempVector = mesh->mTangents[ index ];
					vertex.tangent[ 0 ] = tempVector.x;
					vertex.tangent[ 1 ] = tempVector.y;
					vertex.tangent[ 2 ] = tempVector.z;
				}

				if ( mesh->mBitangents )
				{
					tempVector = mesh->mBitangents[ index ];
					vertex.bitangent[ 0 ] = tempVector.x;
					vertex.bitangent[ 1 ] = tempVector.y;
					vertex.bitangent[ 2 ] = tempVector.z;
				}

				if ( mesh->mTextureCoords[ 0 ] )
				{
					tempVector = mesh->mTextureCoords[ 0 ][ index ];
					vertex.texCoords[ 0 ] = tempVector.x;
					vertex.texCoords[ 1 ] = tempVector.y;
				}

				vertexBuffer[ index ] = vertex;
			}

			// Считываем все индексы вершин
			// *********************

			for ( uint32_t indexFace = 0; indexFace < mesh->mNumFaces; ++indexFace )
			{
				aiFace*			face = &mesh->mFaces[ indexFace ];
				
				for ( uint32_t indexVertex = 0; indexVertex < face->mNumIndices; ++indexVertex )
				{
					uint32_t		index = face->mIndices[ indexVertex ];
					auto			it = find( verteces.begin(), verteces.end(), vertexBuffer[ index ] );

					// Ищем индекс вершины в общем буфере вершин,
					// если не нашли - добавляем вершину в буфер, 
					// а пото записываем ее индекс

					if ( it == verteces.end() )
					{
						indices.push_back( verteces.size() );
						verteces.push_back( vertexBuffer[ index ] );
					}
					else
						indices.push_back( it - verteces.begin() );
				}
			}
		}

		// Обрабатываем материалы
		// **********************
		
		if ( itRoot->first < scene->mNumMaterials )
		{
			aiMaterial*			aiMaterial = scene->mMaterials[ itRoot->first ];
			Material			material;

			material.Load( aiMaterial, directory );
			materials.push_back( material );
		}
		else
		{
			cout << "Warning! Material with id " << itRoot->first << " large. Surface not created" << itRoot->first << endl;
			continue;
		}

		surface.countVertexIndex = indices.size() - surface.startVertexIndex;
		surfaces.push_back( surface );
	}

	isLoaded = true;
	return true;
}

//---------------------------------------------------------------------//

/*
 * Сохранить меш
 * ---------------
 */

bool Mesh::Save( const string& Route )
{
	// Сохраняем все материалы
	
	for ( uint32_t index = 0, count = materials.size(); index < count; ++index )
		materials[ index ].Save( Settings::outDirectory.empty() ? directory : Settings::outDirectory );

	// Сохраняем меш

	ofstream				file( Route, ios::binary );
	if ( !file.is_open() )	return false;

	// Записываем заголовок файла
	// **************************

	file.write( strId, 3 );
	file.write( ( char* ) &version, sizeof( unsigned short ) );

	// Записываем все материалы модели
	// **************************

	string						fileNameMaterial;
	uint32_t					sizeArrayMaterials = materials.size();
	uint32_t					sizeString = 0;

	file.write( ( char* ) &sizeArrayMaterials, sizeof( uint32_t ) );

	for ( uint32_t index = 0; index < sizeArrayMaterials; ++index )
	{
		Material*		material = &materials[ index ];
		fileNameMaterial = material->GetName() + ".lmt";

		sizeString = fileNameMaterial.size();
		file.write( ( char* ) &sizeString, sizeof( uint32_t ) );
		file.write( fileNameMaterial.data(), sizeString );
	}

	// Записываем все вершины модели
	// **************************

	uint32_t					sizeArrayVertexes = verteces.size();

	file.write( ( char* ) &sizeArrayVertexes, sizeof( uint32_t ) );
	file.write( ( char* ) &verteces[ 0 ], sizeArrayVertexes * sizeof( Vertex ) );

	// Записываем все индексы вершин
	// **************************

	uint32_t					sizeArrayIndices = indices.size();

	file.write( ( char* ) &sizeArrayIndices, sizeof( uint32_t ) );
	file.write( ( char* ) &indices[ 0 ], sizeArrayIndices * sizeof( uint32_t ) );

	// Записываем все поверхности модели
	// **************************

	Surface*				surface;
	uint32_t				sizeArraySurfaces = surfaces.size();

	file.write( ( char* ) &sizeArraySurfaces, sizeof( uint32_t ) );

	for ( uint32_t index = 0; index < sizeArraySurfaces; ++index )
	{
		surface = &surfaces[ index ];

		file.write( ( char* ) &surface->materialId, sizeof( uint32_t ) );
		file.write( ( char* ) &surface->startVertexIndex, sizeof( uint32_t ) );
		file.write( ( char* ) &surface->countVertexIndex, sizeof( uint32_t ) );
	}

	return true;
}

//---------------------------------------------------------------------//

/*
 * Очистить загруженый меш
 * ---------------
 */

void Mesh::Clear()
{
	directory.clear();
	verteces.clear();
	indices.clear();
	surfaces.clear();
	materials.clear();

	isLoaded = false;
}

//---------------------------------------------------------------------//

/*
 * Оброботать узел сцены
 * ---------------
 */

void Mesh::ProcessNode( aiNode* aiNode, const aiScene* &aiScene, unordered_map<uint32_t, vector<aiMesh*>>& aiMeshes )
{
	for ( uint32_t index = 0; index < aiNode->mNumMeshes; ++index )
		aiMeshes[ aiScene->mMeshes[ aiNode->mMeshes[ index ] ]->mMaterialIndex ].push_back( aiScene->mMeshes[ aiNode->mMeshes[ index ] ] );

	for ( uint32_t index = 0; index < aiNode->mNumChildren; ++index )
		ProcessNode( aiNode->mChildren[ index ], aiScene, aiMeshes );
}

//---------------------------------------------------------------------//