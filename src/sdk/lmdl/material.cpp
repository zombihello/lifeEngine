//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "material.h"
#include <fstream>
#include <assimp/material.h>
using namespace std;

//---------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------
 */

Material::Material()
{}

//---------------------------------------------------------------------//

/*
 * Загрузить материал
 * ---------------
 */

void Material::Load( aiMaterial* aiMaterial, const string& RootDirectory )
{
	aiString			temp_aiString;
	aiColor4D 			color;
	Material			material;
	float				tempValue = 0.f;

	//
	// Данный код временный (точнее, его говночасть). После реализации
	// системы материалов данная часть конвертера будет переписана
	//

	// Получаем название материала
	// *****************************

	aiMaterial->Get( AI_MATKEY_NAME, temp_aiString );
	name = temp_aiString.C_Str();

	// Получаем фоновый цвет
	// *****************************

	if ( aiMaterial->Get( AI_MATKEY_COLOR_AMBIENT, color ) != AI_SUCCESS )
		ambientColor[ 0 ] = ambientColor[ 1 ] = ambientColor[ 2 ] = ambientColor[ 3 ] = 0.f;
	else
	{
		ambientColor[ 0 ] = color.r;
		ambientColor[ 1 ] = color.g;
		ambientColor[ 2 ] = color.b;
		ambientColor[ 3 ] = color.a;
	}

	// Получаем рассеяный цвет
	// ***************************

	if ( aiMaterial->GetTextureCount( aiTextureType_DIFFUSE ) > 0 )
	{
		aiMaterial->GetTexture( aiTextureType_DIFFUSE, 0, &temp_aiString );

		string		routeFile = temp_aiString.C_Str();
		uint32_t	indexSlash = routeFile.find_first_of( '\\' );
		while ( indexSlash != string::npos )
		{
			routeFile[ indexSlash ] = '/';
			indexSlash = routeFile.find_first_of( '\\' );
		}

		if ( routeFile.find_last_of( "/" ) == -1 )
			routeFile = RootDirectory + "/" + temp_aiString.C_Str();

		routeDiffuseMap = routeFile;
		if ( routeDiffuseMap.find_last_of( '.' ) != string::npos )
			routeDiffuseMap.erase( routeDiffuseMap.find_last_of( '.' ), routeDiffuseMap.size() );
		routeDiffuseMap += ".ktx";
	}
	else
	{
		
		if ( aiMaterial->Get( AI_MATKEY_COLOR_DIFFUSE, color ) != AI_SUCCESS )
			diffuseColor[ 0 ] = diffuseColor[ 1 ] = diffuseColor[ 2 ] = diffuseColor[ 3 ] = 1.f;
		else
		{
			diffuseColor[ 0 ] = color.r;
			diffuseColor[ 1 ] = color.g;
			diffuseColor[ 2 ] = color.b;
			diffuseColor[ 3 ] = color.a;
		}
	}

	// Получаем путь к карте нормалей
	// *****************************

	if ( aiMaterial->GetTextureCount( aiTextureType_NORMALS ) > 0 )
	{
		aiMaterial->GetTexture( aiTextureType_NORMALS, 0, &temp_aiString );

		string		routeFile = temp_aiString.C_Str();
		uint32_t	indexSlash = routeFile.find_first_of( '\\' );
		while ( indexSlash != string::npos )
		{
			routeFile[ indexSlash ] = '/';
			indexSlash = routeFile.find_first_of( '\\' );
		}

		if ( routeFile.find_last_of( "/" ) == -1 )
			routeFile = RootDirectory + "/" + temp_aiString.C_Str();

		routeNormalMap = routeFile;
		if ( routeNormalMap.find_last_of( '.' ) != string::npos )
			routeNormalMap.erase( routeNormalMap.find_last_of( '.' ), routeNormalMap.size() );
		routeNormalMap += ".ktx";
	}

	// Получаем фактор отражения
	// *****************************

	if ( aiMaterial->GetTextureCount( aiTextureType_SPECULAR ) > 0 )
	{
		aiMaterial->GetTexture( aiTextureType_SPECULAR, 0, &temp_aiString );

		string		routeFile = temp_aiString.C_Str();
		uint32_t	indexSlash = routeFile.find_first_of( '\\' );
		while ( indexSlash != string::npos )
		{
			routeFile[ indexSlash ] = '/';
			indexSlash = routeFile.find_first_of( '\\' );
		}

		if ( routeFile.find_last_of( "/" ) == -1 )
			routeFile = RootDirectory + "/" + temp_aiString.C_Str();

		routeSpecularMap = routeFile;
		if ( routeSpecularMap.find_last_of( '.' ) != string::npos )
			routeSpecularMap.erase( routeSpecularMap.find_last_of( '.' ), routeSpecularMap.size() );
		routeSpecularMap += ".ktx";
	}
	else
	{
		if ( aiMaterial->Get( AI_MATKEY_COLOR_SPECULAR, color ) == AI_SUCCESS )
			tempValue = ( color.r + color.g + color.b + color.a ) / 4.f;

		specular = tempValue;
	}

	// Получаем цвет свечения
	// *****************************

	if ( aiMaterial->GetTextureCount( aiTextureType_EMISSIVE ) > 0 )
	{
		aiMaterial->GetTexture( aiTextureType_EMISSIVE, 0, &temp_aiString );

		string		routeFile = temp_aiString.C_Str();
		uint32_t	indexSlash = routeFile.find_first_of( '\\' );
		while ( indexSlash != string::npos )
		{
			routeFile[ indexSlash ] = '/';
			indexSlash = routeFile.find_first_of( '\\' );
		}

		if ( routeFile.find_last_of( "/" ) == -1 )
			routeFile = RootDirectory + "/" + temp_aiString.C_Str();

		routeEmissionMap = routeFile;
		if ( routeEmissionMap.find_last_of( '.' ) != string::npos )
			routeEmissionMap.erase( routeEmissionMap.find_last_of( '.' ), routeEmissionMap.size() );
		routeEmissionMap += ".ktx";
	}
	else
	{
		if ( aiMaterial->Get( AI_MATKEY_COLOR_EMISSIVE, color ) != AI_SUCCESS )
			emissionColor[ 0 ] = emissionColor[ 1 ] = emissionColor[ 2 ] = emissionColor[ 3 ] = 0.f;
		else
		{
			emissionColor[ 0 ] = color.r;
			emissionColor[ 1 ] = color.g;
			emissionColor[ 2 ] = color.b;
			emissionColor[ 3 ] = color.a;
		}
	}

	// Получаем фактор блеска
	// *****************************

	if ( aiMaterial->Get( AI_MATKEY_SHININESS, tempValue ) != AI_SUCCESS )
		tempValue = 1.f;
	else
		shininess = tempValue;
}

//---------------------------------------------------------------------//

/*
 * Сохранить материал
 * ---------------
 */

void Material::Save( const string& Directory )
{
	ofstream				file( Directory + "/" + name + ".lmt" );
	if ( !file.is_open() )	return;

	//
	// Данный код временный (точнее, его говночасть). После реализации
	// системы материалов данная часть конвертера будет переписана
	//

	file << "{\n";

	// Записываем текстуры, если они есть

	if ( !routeDiffuseMap.empty() || !routeEmissionMap.empty() ||
		 !routeNormalMap.empty() || !routeSpecularMap.empty() )
	{
		file << "\t\"textures\": [\n";
		file << "\t\t{\n";
		file << "\t\t\t\"layer\": 0,\n";
		file << "\t\t\t\"route\": \"" << routeDiffuseMap << "\"" << endl;
		file << "\t\t}\n\t],\n\n";
	}

	file << "\t\"constants\": {\n";
	file << "\t\t\"diffuse\": [" << diffuseColor[ 0 ] << ", " << diffuseColor[ 1 ] << ", " << diffuseColor[ 2 ] << ", " << diffuseColor[ 3 ] << "],\n";//[1, 1, 1, 1],\n";
	file << "\t\t\"specular\": " << specular << ",\n";
	file << "\t\t\"shininess\": " << shininess << ",\n";
	file << "\t\t\"ambient\": [" << ambientColor[ 0 ] << ", " << ambientColor[ 1 ] << ", " << ambientColor[ 2 ] << ", " << ambientColor[ 3 ] << "],\n";
	file << "\t\t\"emission\": [" << emissionColor[ 0 ]  << ", " << emissionColor[ 1 ] << ", " << emissionColor[ 2 ] << ", " << emissionColor[ 3 ] << "]\n";
	file << "\t},\n\n";

	file << "\t\"shader\": {\n";
	file << "\t\t\"flags\": " << ( !routeDiffuseMap.empty() ? 1 : 0 ) << ",\n";
	file << "\t\t\"name\": \"material\"\n";
	file << "\t}\n";
	file << "}\n";
}

//---------------------------------------------------------------------//