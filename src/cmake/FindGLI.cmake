# 	---------------------------------
#	[in] 	GLI_PATH		- root dir compressonator
#	[out] 	GLI_INCLUDE		- dir with includes GLI
#	[out]	GLI_GLM_INCLUDE	- dir with includes GLM for GLI
#	[out]	GLI_FOUND		- is found compressonator
# 	---------------------------------

SET( GLI_SEARCH_PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	${GLI_PATH}
)

find_path( 		GLI_INCLUDE
				NAMES "gli/gli.hpp"
				PATHS ${GLI_SEARCH_PATHS} )	
find_path( 		GLI_GLM_INCLUDE
				NAMES "external/glm/glm.hpp"
				PATHS ${GLI_SEARCH_PATHS} )
				
if ( NOT GLI_INCLUDE OR NOT GLI_GLM_INCLUDE )
    message( SEND_ERROR "Failed to find GLM" )
    return()
else()
	set( GLI_FOUND true )
endif()