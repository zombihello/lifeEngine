# 	---------------------------------
#	[in] 	COMPRESSONATOR_PATH			- root dir compressonator
#	[out] 	COMPRESSONATOR_INCLUDE		- dir with includes
#	[out]	COMPRESSONATOR_LIB			- lib compressonator
#	[out]	COMPRESSONATOR_FOUND		- is found compressonator
# 	---------------------------------

SET( COMPRESSONATOR_SEARCH_PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	${COMPRESSONATOR_PATH}
)

find_path( 		COMPRESSONATOR_INCLUDE
				NAMES "Compressonator/Compressonator.h"
				PATH_SUFFIXES include Header
				PATHS ${COMPRESSONATOR_SEARCH_PATHS} )		
find_library( 	COMPRESSONATOR_LIB 
                NAMES Compressonator
				PATH_SUFFIXES lib
                PATHS ${COMPRESSONATOR_SEARCH_PATHS} )
		
if ( NOT COMPRESSONATOR_INCLUDE OR NOT COMPRESSONATOR_LIB )
    message( SEND_ERROR "Failed to find Compressonator" )
    return()
else()
	set( COMPRESSONATOR_FOUND true )
endif()