# 	---------------------------------
#	[in] 	KTX_PATH		- root dir ktx
#	[out] 	KTX_INCLUDE		- dir with includes
#	[out]	KTX_LIB			- lib ktx
#	[out]	KTX_FOUND		- is found ktx
# 	---------------------------------

SET( KTX_SEARCH_PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	${KTX_PATH}
)

find_path( 		KTX_INCLUDE
				NAMES "ktx/ktx.h"
				PATH_SUFFIXES include
				PATHS ${KTX_SEARCH_PATHS} )		
find_library( 	KTX_LIB 
                NAMES libktx.gl ktx.gl
				PATH_SUFFIXES lib
                PATHS ${KTX_SEARCH_PATHS} )
		
if ( NOT KTX_INCLUDE OR NOT KTX_LIB )
    message( SEND_ERROR "Failed to find KTX" )
    return()
else()
	set( KTX_FOUND true )
endif()