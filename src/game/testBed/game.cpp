//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include <codecvt>

#include "testBed_pch.h"
#include "engine/iguisystem.h"
#include "engine/iinputsystem.h"
#include "engine/iscene.h"
#include "engine/irendersystem.h"
#include "engine/iresourcesystem.h"
#include "common/version.h"
#include "engine/icamera.h"
#include "engine/iwindow.h"
#include "engine/imesh.h"
#include "engine/imaterial.h"
#include "engine/iconsolesystem.h"
#include "engine/iconvar.h"
#include "game.h"

//---------------------------------------------------------------------//

le::ICore* core = nullptr;					///< Указатель на ядро движка
le::IGuiSystem* guiSystem = nullptr;			///< Указатель на систему графического интерфейса
le::IInputSystem* inputSystem = nullptr;			///< Указатель на систему ввода
le::IScene* scene = nullptr;				///< Указатель на менеджер сцены
le::IRenderSystem* renderSystem = nullptr;			///< Указатель на систему визуализации
le::IWindow* window = nullptr;				///< Указатель на окно приложения
le::IConsoleSystem* consoleSystem = nullptr;
le::IResourceManager<le::ITexture>* textureManager = nullptr;		///< Указатель на менеджер текстур
le::IResourceManager<le::IMaterial>* materialManager = nullptr;		///< Указатель на менеджер материалов
le::IResourceManager<le::IMesh>* meshManager = nullptr;			///< Указатель на менеджер мешей
le::IResourceManager<le::IFont>* fontManager = nullptr;			///< Указатель на менеджер шрифтов
le::IFactory* factory = nullptr;				///< Указатель на фабрику движка
le::ICamera* cameraPlayer = nullptr;			///< Указатель на камеру игрока	

//---------------------------------------------------------------------//

LIFEENGINE_GAME_API( Game )

//---------------------------------------------------------------------//

/*
 * Конструктор
 * ---------------
 */

	void Callback_ConVar_ConText( le::IConVar* Var )
{
	if ( Var->GetType() != le::CVT_STRING ) return;
	LOG_INFO( "change con_text to \"" << Var->GetValueString() << "\"" );
}

class Console
{
public:
	void Init( le::IFont* Font, const glm::ivec2& WindowSize )
	{
		commandLine = ( le::IText* ) factory->Create( TEXT_INTERFECE_VERSION );
		commandLine->SetCharacterSize( 25 );
		commandLine->SetFont( Font );
		commandLine->SetPosition( glm::vec3( 5, 5, 1 ) );
		commandLine->SetText( "_" );

		background = ( le::ISprite* ) factory->Create( SPRITE_INTERFECE_VERSION );
		le::IMaterial* mat_consoleBg = ( le::IMaterial* ) factory->Create( MATERIAL_INTERFECE_VERSION );
		mat_consoleBg->SetDiffuseColor( glm::vec4( 0.2, 0.2, 0.2, 1 ) );

		background->SetPosition( glm::vec3( 0, 0, 0 ) );
		background->SetSize( glm::vec2( WindowSize.x, WindowSize.y / 2 ) );
		background->SetMaterial( mat_consoleBg );


		for ( int i = 0; i < 9; ++i )
		{
			log[ i ] = ( le::IText* ) factory->Create( TEXT_INTERFECE_VERSION );
			log[ i ]->SetCharacterSize( 25 );
			log[ i ]->SetFont( Font );
			log[ i ]->SetPosition( glm::vec3( 5, (1+i) * ( 5 + commandLine->GetCharacterSize() ), 1 ) );
		}
	}

	void Draw()
	{
		renderSystem->Draw( background, le::TL_HUD );
		renderSystem->Draw( commandLine, le::TL_HUD );

		for ( int i = 0; i < 9; ++i )
			renderSystem->Draw( log[ i ], le::TL_HUD );
	}

	void Update()
	{
		if ( inputSystem->IsInputText() )
		{
			if ( !command.empty() ) 
				command.erase( command.end() - 1 );

			command += inputSystem->GetInputText();
			command += L"_";

			commandLine->SetText( command );
		}

		if ( command.size() >= 2 && inputSystem->IsKeyUp( le::KK_BACKSPACE ) )
		{
			command.erase( command.end() - 2, command.end() );
			command += L"_";
			commandLine->SetText( command );
		}
		else if ( inputSystem->IsKeyUp( le::KK_ENTER ) )
		{
			command.erase( command.end() - 1 );

			consoleSystem->Exec( wstring_convert<codecvt_utf8<wchar_t>>().to_bytes( command ) );

			command.clear();
			command += L"_";

			commandLine->SetText( command );
		}
	}

	void Callback_PrintConsole( const string& Message )
	{
		wstring		tempString = log[ 0 ]->GetText();
		log[ 0 ]->SetText( Message );

		for ( int i = 1; i < 9; ++i )
		{
			wstring		tmpString = log[ i ]->GetText();
			log[ i ]->SetText( tempString );
			tempString = tmpString;
		}
	}

private:
	wstring			command;
	le::IText*		log[ 9 ];
	le::IText*		commandLine;
	le::ISprite*	background;
} con;

Game::Game() :
	worldCamera( nullptr ),
	uiCamera( nullptr ),
	text( nullptr ),
	textHelp( nullptr ),
	font( nullptr ),
	isConsoleOpen( false ),
	fire( nullptr )
{}

//---------------------------------------------------------------------//

/*
 * Деструктор
 * ----------------
 */

Game::~Game()
{
	if ( font )				fontManager->Delete( font );
	if ( text )				factory->Delete( text );
	if ( textHelp )			factory->Delete( textHelp );
	if ( worldCamera )		factory->Delete( worldCamera );
	if ( uiCamera )			factory->Delete( uiCamera );
	if ( fire )				factory->Delete( fire );
}

//---------------------------------------------------------------------//

/*
 * Инициализировать игру
 * -----------------------
 */
vector<le::FloatRect> anims;
float currentFrame = 0;

vector<le::FloatRect> animsFire;
float currentFrameFire = 0;

le::IConVar* con_bool;

void Test( const vector<string>& Arguments )
{
	for ( int i = 0; i < Arguments.size(); i++ )
		LOG_INFO( Arguments[ i ] );
}

bool Game::Initialize( le::ICore* Core )
{
	// Получаем необходимые подсистемы

	inputSystem = static_cast< le::IInputSystem* >( Core->GetSubsystem( le::EST_INPUT_SYSTEM ) );
	if ( !inputSystem ) return false;

	guiSystem = static_cast< le::IGuiSystem* >( Core->GetSubsystem( le::EST_GUI_SYSTEM ) );
	if ( !guiSystem ) return false;

	renderSystem = static_cast< le::IRenderSystem* >( Core->GetSubsystem( le::EST_RENDER_SYSTEM ) );
	if ( !renderSystem ) return false;

	scene = static_cast< le::IScene* >( Core->GetSubsystem( le::EST_SCENE_MANAGER ) );
	if ( !scene ) return false;

	le::IResourceSystem* resourceSystem = static_cast< le::IResourceSystem* >( Core->GetSubsystem( le::EST_RESOURCE_MANAGER ) );
	if ( !resourceSystem ) return false;

	consoleSystem = static_cast< le::IConsoleSystem* >( Core->GetSubsystem( le::EST_CONSOLE_SYSTEM ) );
	if ( !consoleSystem ) return false;

	core = Core;
	window = Core->GetWindow();
	factory = Core->GetFactory();

	textureManager = resourceSystem->GetTextureManager();
	materialManager = resourceSystem->GetMaterialManager();
	meshManager = resourceSystem->GetMeshManager();
	fontManager = resourceSystem->GetFontManager();

	font = fontManager->Load( "ui.ttf", "ui" );

	con.Init( font, window->GetSize() );
	consoleSystem->SetPrintConsoleCallback( bind( &Console::Callback_PrintConsole, &con, placeholders::_1 ) );
	le::Log::ConnectConsoleSystem( consoleSystem );

	le::IConVar* con_text = ( le::IConVar* ) factory->Create( CONVAR_INTERFECE_VERSION );
	con_text->Initialize( "con_text", "hello world", le::CVT_STRING, "Test string", true, 3, false, 4, Callback_ConVar_ConText );

	le::IConVar* con_make = ( le::IConVar* ) factory->Create( CONVAR_INTERFECE_VERSION );
	con_make->Initialize( "make", "", le::CVT_STRING, "Create model on scene", []( le::IConVar* Var )
						  { 
							  if ( Var->GetValueString() == "куб" )
							  {
								  le::IModel* model = ( le::IModel* ) factory->Create( MODEL_INTERFECE_VERSION );
								  model->SetMesh( meshManager->Load( "box.lmd" ) );
								  model->SetPosition( cameraPlayer->GetPosition() );

								  scene->AddModel( model );
							  }
							  else
								  LOG_ERROR( "Unknow argument" );
						  } );

	con_bool = ( le::IConVar* ) factory->Create( CONVAR_INTERFECE_VERSION );
	con_bool->Initialize( "con_bool", "1", le::CVT_BOOL, "Show *BOM* or no", true, 0, true, 1, []( le::IConVar* Var ) { LOG_INFO( "change con_bool to \"" << Var->GetValueBool() << "\"" ); } );

	consoleSystem->RegisterVar( con_text );
	consoleSystem->RegisterVar( con_bool );
	consoleSystem->RegisterVar( con_make );

	consoleSystem->RegisterCommand( "test", Test );

	glm::ivec2			windowSize = window->GetSize();
	window->SetShowCursor( false );

	// Загружаем необходимые ресурсы

	// Создаем геометрию
	text = ( le::IText* ) factory->Create( TEXT_INTERFECE_VERSION );
	worldCamera = ( le::ICamera* ) factory->Create( CAMERA_INTERFECE_VERSION );
	uiCamera = ( le::ICamera* ) factory->Create( CAMERA_INTERFECE_VERSION );

	cameraPlayer = worldCamera;

	// Инициализируем геометрию
	text->SetCharacterSize( 45 );
	text->SetFont( font );
	text->SetText( "lifeEngine (build " + to_string( Core->GetVersion().build ) + ")" );
	text->SetPosition( glm::vec3( 25, 25, 0 ) );

	textHelp = ( le::IText* ) factory->Create( TEXT_INTERFECE_VERSION );
	textHelp->SetCharacterSize( 25 );
	textHelp->SetFont( font );
	textHelp->SetText( "W | A | S | D - Передвижение\n" \
					   "MOUSE - Обзор\n" \
					   "Q | E - Наклон камеры\n" \
					   "T - Убрать\\вернуть красный цвет у модели в UI\n" \
					   "UP | DOWN | RIGHT | LEFT - Управлять 2D спрайтом\n" \
					   "ESC - Выход" );
	textHelp->SetPosition( glm::vec3( 25.f, windowSize.y - 25.f, 0.f ) );

	// Указываем камеру для отрисовки мира и интерфейса
	worldCamera->InitProjection_Perspective( 75.f, ( float ) windowSize.x / ( float ) windowSize.y, 0.1f, 1500.f );
	uiCamera->InitProjection_Ortho( 0.0f, ( float ) windowSize.x, 0.0f, ( float ) windowSize.y, -150.f, 150.f );

	renderSystem->SetActiveCamera( worldCamera, le::TL_WORLD );
	renderSystem->SetActiveCamera( uiCamera, le::TL_HUD );

	for ( int x = 0; x < 2; x++ )
		for ( int y = 0; y < 2; y++ )
			for ( int z = 0; z < 2; z++ )
			{
				le::IModel* model = ( le::IModel* ) factory->Create( MODEL_INTERFECE_VERSION );
				model->SetMesh( meshManager->Load( "box.lmd" ) );
				model->SetPosition( glm::vec3( 25 * x, 25 * y, 25 * z ) );

				scene->AddModel( model );
			}


	fire = ( le::ISprite* ) factory->Create( SPRITE_INTERFECE_VERSION );
	le::IMaterial* mat_fire = ( le::IMaterial* ) factory->Create( MATERIAL_INTERFECE_VERSION );
	mat_fire->SetMaterialFlags( le::SF_DIFFUSE_MAP );
	mat_fire->AddTexture( textureManager->Load( "fire_001.png" ), 0 );

	fire->SetPosition( glm::vec3( 50, 0, -50.f ) );
	fire->SetSize( glm::vec2( 50.f, 50.f ) );
	fire->SetType( le::ST_SPRITE_ROTATING_ONLY_VERTICAL );
	fire->SetMaterial( mat_fire );

	animsFire =
	{
		le::FloatRect( 51, 32, 87, 113 ),
		le::FloatRect( 239, 12, 93, 143 ),
		le::FloatRect( 601, 5, 133, 181 ),
		le::FloatRect( 781, 3, 164, 185 ),
		le::FloatRect( 17, 190, 150, 192 ),
		le::FloatRect( 195, 188, 173, 199 ),
		le::FloatRect( 392, 183, 162, 198 ),
		le::FloatRect( 584, 193, 176, 180 ),
		le::FloatRect( 772, 198, 181, 177 ),
		le::FloatRect( 6, 384, 183, 186 ),
		le::FloatRect( 197, 392, 180, 181 ),
		le::FloatRect( 386, 384, 188, 186 ),
		le::FloatRect( 584, 380, 177, 184 ),
		le::FloatRect( 778, 384, 180, 177 ),
		le::FloatRect( 10, 570, 185, 188 ),
		le::FloatRect( 216, 573, 168, 186 ),
		le::FloatRect( 407, 574, 163, 175 ),
		le::FloatRect( 609, 570, 143, 188 ),
		le::FloatRect( 771, 574, 189, 184 )
	};
	fire->SetTextureRect( animsFire[ 0 ] );


	return true;
}

//---------------------------------------------------------------------//

/*
 * Обновить логику игры
 * -----------------------
 */

void Game::Update( uint32_t DeltaTime )
{
	if ( inputSystem->IsKeyDown( le::KK_ESCAPE ) )
		core->StopSimulation();

	if ( inputSystem->IsKeyUp( le::KK_GRAVE ) )
		isConsoleOpen = !isConsoleOpen;

	if ( isConsoleOpen )
	{
		con.Update();
		con.Draw();
	}
	else
	{
		if ( inputSystem->IsKeyDown( le::KK_A ) )
			worldCamera->Move( le::CSM_LEFT, 0.08f * DeltaTime );

		if ( inputSystem->IsKeyDown( le::KK_D ) )
			worldCamera->Move( le::CSM_RIGHT, 0.08f * DeltaTime );

		if ( inputSystem->IsKeyDown( le::KK_S ) )
			worldCamera->Move( le::CSM_BACKWARD, 0.08f * DeltaTime );

		if ( inputSystem->IsKeyDown( le::KK_W ) )
			worldCamera->Move( le::CSM_FORWARD, 0.08f * DeltaTime );

		if ( inputSystem->IsKeyDown( le::KK_Q ) )
			worldCamera->Rotate( 0.f, 0.f, -0.08f * DeltaTime );

		if ( inputSystem->IsKeyDown( le::KK_E ) )
			worldCamera->Rotate( 0.f, 0.f, 0.08f * DeltaTime );
	}

	currentFrame += 1.f / 150.f * DeltaTime;
	if ( currentFrame > anims.size() ) currentFrame = 0;

	currentFrameFire += 1.f / 80.f * DeltaTime;
	if ( currentFrameFire > animsFire.size() ) currentFrameFire = 0;

	worldCamera->RotateByMouse( inputSystem->GetMouseOffset(), inputSystem->GetMouseSensitivity() * 0.01f * DeltaTime );

	fire->SetTextureRect( animsFire[ currentFrameFire ] );

	renderSystem->Draw( text, le::TL_HUD );
	renderSystem->Draw( textHelp, le::TL_HUD );

	if ( con_bool->GetValueBool() )
		renderSystem->Draw( fire, le::TL_WORLD );

	guiSystem->Update( DeltaTime );
	scene->Update();
}

//---------------------------------------------------------------------//
