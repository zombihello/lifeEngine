//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#ifndef GAME_H
#define GAME_H

#include <list>
using namespace std;

#include "engine/igame.h"
#include "engine/icore.h"
#include "engine/imodel.h"
#include "engine/isprite.h"
#include "engine/ifont.h"
#include "engine/itext.h"
#include "engine/icamera.h"
#include "engine/ifactory.h"
#include "engine/iresourcemanager.h"
#include "engine/iwindow.h"

//---------------------------------------------------------------------//

///////////////////////////////////////////////////////////////////////////
/// \brief Игра
///
/// В данном классе содержится игровая логика игры
////////////////////////////////////////////////////////////////////////////
class Game : public le::IGame
{
public:
	////////////////////////////////////////////////////////////////////////////
	/// \brief Конструктор
	////////////////////////////////////////////////////////////////////////////
	Game();

	////////////////////////////////////////////////////////////////////////////
	/// \brief Деструктор
	////////////////////////////////////////////////////////////////////////////
	~Game();

	///////////////////////////////////////////////////////////////////////////
	/// \brief Инициализировать логику игры
	///
	/// \param[in] Core Указатель на ядро движка
	/// \return True логика игры успешно инициализирована, иначе false
	////////////////////////////////////////////////////////////////////////////
	bool Initialize( le::ICore* Core );

	//////////////////////////////////////////////////////////////////////////
	/// \brief Обновить логику игры
	/// 
	/// \param[in] DeltaTime Прошедшее время в милисекундах
	//////////////////////////////////////////////////////////////////////////
	void Update( uint32_t DeltaTime );

private:
	bool					isConsoleOpen;	///< Открыта ли консоль

	le::ICamera*			worldCamera;	///< Камера игрока
	le::ICamera*			uiCamera;		///< Камера интерфейса	
	le::IFont*				font;			///< Шрифт для текста
	le::IText*				text;			///< Текст
	le::IText*				textHelp;		///< Текст-помощь
	le::ISprite*			fire;			///< Спрайт взрыва
};

//---------------------------------------------------------------------//

extern le::ICore*									core;				///< Указатель на ядро движка
extern le::IGuiSystem*								guiSystem;			///< Указатель на систему графического интерфейса
extern le::IInputSystem*							inputSystem;		///< Указатель на систему ввода
extern le::IScene*									scene;				///< Указатель на менеджер сцены
extern le::IRenderSystem*							renderSystem;		///< Указатель на систему визуализации
extern le::IWindow*									window;				///< Указатель на окно приложения
extern le::IResourceManager<le::ITexture>*			textureManager;		///< Указатель на менеджер текстур
extern le::IResourceManager<le::IMaterial>*			materialManager;	///< Указатель на менеджер материалов
extern le::IResourceManager<le::IMesh>*				meshManager;		///< Указатель на менеджер мешей
extern le::IResourceManager<le::IFont>*				fontManager;		///< Указатель на менеджер шрифтов
extern le::IFactory*								factory;			///< Указатель на фабрику движка

//---------------------------------------------------------------------//

#endif // !GAME_H
