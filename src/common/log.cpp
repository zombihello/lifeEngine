//////////////////////////////////////////////////////////////////////////
//
//			*** lifeEngine (Двигатель жизни) ***
//				Copyright (C) 2018-2019
//
// Репозиторий движка:  https://gitlab.com/zombihello/lifeEngine
// Авторы:				Егор Погуляка (zombiHello)
//
//////////////////////////////////////////////////////////////////////////

#include "common/log.h"

le::IConsoleSystem*				le::Log::consoleSystem = nullptr;

// ------------------------------------------------------------------------------------ //
// Конструктор
// ------------------------------------------------------------------------------------ //
le::Log::Log( MESSAGE_TYPE Type ) :
	type( Type )
{}

// ------------------------------------------------------------------------------------ //
// Деструктор
// ------------------------------------------------------------------------------------ //
le::Log::~Log()
{
	if ( !consoleSystem )
	{
		switch ( type )
		{
		case MT_INFO:
			cout << "[Info]  " << stream.str() << endl;;
			break;

		case MT_WARNING:
			cout << "[Warning]  " << stream.str() << endl;
			break;

		case MT_ERROR:
			cout << "[Error]  " << stream.str() << endl;
			break;
		}

		return;
	}

	switch ( type )
	{
	case MT_INFO:
		consoleSystem->PrintInfo( stream.str() );
		break;

	case MT_WARNING:
		consoleSystem->PrintWarning( stream.str() );
		break;

	case MT_ERROR:
		consoleSystem->PrintError( stream.str() );
		break;
	}
}

// ------------------------------------------------------------------------------------ //
// Присоединить подсистему консоли к логам
// ------------------------------------------------------------------------------------ //
void le::Log::ConnectConsoleSystem( IConsoleSystem* ConsoleSystem )
{
	consoleSystem = ConsoleSystem;
}

// ------------------------------------------------------------------------------------ //
// Отсоединить подсистему консоли от логов
// ------------------------------------------------------------------------------------ //
void le::Log::DisconnectConsoleSystem()
{
	consoleSystem = nullptr;
}
